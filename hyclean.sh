#!/bin/sh

cd work/mingw32/fontforge

make clean;
make distclean;

if [ -f fontforge.configure-complete ]; then
	rm -rf fontforge.configure-complete
fi

cd ../../..

