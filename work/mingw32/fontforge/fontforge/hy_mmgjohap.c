/****************************************************
*													*
*				hy_mmgjohap.c						*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_mmg.h"
#include "hy_mmgjohap.h"
#include "hy_debug.h"



void HY_MMGJohapApplyTransBase( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bb1, real t1[6], DBounds *bb2, real t2[6], DBounds *bb3, real t3[6] )
{
	RefChar *rf;
	real dx1, dx2;
	real dy1, dy2;
	real tx, ty, dx, dy;
	real t[6], trans[6];

	for( rf = sf->glyphs[sf->map->map[index]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
	{
		if( rf->sc->mmg_type == mmgtype )
		{
			// Scale & Move Transform
			switch( jungtype )
			{
				case JUNG_TYPE_HORI:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = 1;//(bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = 0;//(bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JONG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;

				case JUNG_TYPE_VERT:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = 1;//(bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JONG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;

				case JUNG_TYPE_COMP:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = 1;//(bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = 0;//(bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JONG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;
			}

			t[0] = tx;
			t[1] = 0;
			t[2] = 0;
			t[3] = ty;
			t[4] = dx;
			t[5] = dy;

			trans[0] = rf->transform[0] * t[0] + rf->transform[1] * t[2];
			trans[1] = rf->transform[0] * t[1] + rf->transform[1] * t[3];
			trans[2] = rf->transform[2] * t[0] + rf->transform[3] * t[2];
			trans[3] = rf->transform[2] * t[1] + rf->transform[3] * t[3];
			trans[4] = rf->transform[4] * t[0] + rf->transform[5] * t[2] + t[4];
			trans[5] = rf->transform[4] * t[1] + rf->transform[5] * t[3] + t[5];
			memcpy( rf->transform, trans, sizeof(trans) );
			SCReinstanciateRefChar(sf->glyphs[sf->map->map[index]], rf, ly_fore);
			SCUpdateAll(sf->glyphs[sf->map->map[index]]);
			RefCharFindBounds(rf);

#if TRANS_DBG
			hydebug( "\t\t [Do Transform]================================================================\n" );
			hydebug( "\t\t [Do Transform] mmgtype: %d / trans: [%f  %f  %f  %f  %f  %f]\n", mmgtype, trans[0], trans[1], trans[2], trans[3], trans[4], trans[5] );
			hydebug( "\t\t [Do Transform]================================================================\n\n" );
#endif
		}
	}
}

void HY_MMGJohapApplyTransNoneJong( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bb1, real t1[6], DBounds *bb2, real t2[6], DBounds *bb3, real t3[6] )
{
	RefChar *rf;
	real dx1, dx2;
	real dy1, dy2;
	real tx, ty, dx, dy;
	real t[6], trans[6];

#if TRANS_DBG
	hydebug( "[%s] \t t1: [%f  %f  %f  %f  %f  %f]\n", __FUNCTION__, t1[0], t1[1], t1[2], t1[3], t1[4], t1[5] );
	hydebug( "[%s] \t t2: [%f  %f  %f  %f  %f  %f]\n", __FUNCTION__, t2[0], t2[1], t2[2], t2[3], t2[4], t2[5] );
	hydebug( "[%s] \t t3: [%f  %f  %f  %f  %f  %f]\n", __FUNCTION__, t3[0], t3[1], t3[2], t3[3], t3[4], t3[5] );
#endif

	for( rf = sf->glyphs[sf->map->map[index]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
	{
		if( rf->sc->mmg_type == mmgtype )
		{
			// Scale & Move Transform
			switch( jungtype )
			{
				case JUNG_TYPE_HORI:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = 1;//(bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = 0;//(bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;

				case JUNG_TYPE_VERT:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = 1;//(bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;

				case JUNG_TYPE_COMP:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = 1;//(bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = 0;//(bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;
			}

			t[0] = tx;
			t[1] = 0;
			t[2] = 0;
			t[3] = ty;
			t[4] = dx;
			t[5] = dy;

			trans[0] = rf->transform[0] * t[0] + rf->transform[1] * t[2];
			trans[1] = rf->transform[0] * t[1] + rf->transform[1] * t[3];
			trans[2] = rf->transform[2] * t[0] + rf->transform[3] * t[2];
			trans[3] = rf->transform[2] * t[1] + rf->transform[3] * t[3];
			trans[4] = rf->transform[4] * t[0] + rf->transform[5] * t[2] + t[4];
			trans[5] = rf->transform[4] * t[1] + rf->transform[5] * t[3] + t[5];

			memcpy( rf->transform, trans, sizeof(trans) );
			SCReinstanciateRefChar(sf->glyphs[sf->map->map[index]], rf, ly_fore);
			SCUpdateAll(sf->glyphs[sf->map->map[index]]);
			RefCharFindBounds(rf);

#if TRANS_DBG
			hydebug( "[%s]================================================================\n", __FUNCTION__ );
			hydebug( "[%s] \t mmgtype: %d / trans: [%f  %f  %f  %f  %f  %f]\n", __FUNCTION__, mmgtype, trans[0], trans[1], trans[2], trans[3], trans[4], trans[5] );
			hydebug( "[%s]================================================================\n", __FUNCTION__ );
#endif
		}
	}
}

void HY_MMGJohapApplyTrans( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bb1, real t1[6], DBounds *bb2, real t2[6], DBounds *bb3, real t3[6] )
{
	RefChar *rf;
	real dx1, dx2;
	real dy1, dy2;
	real tx, ty, dx, dy;
	real t[6], trans[6];

	for( rf = sf->glyphs[sf->map->map[index]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
	{
		if( rf->sc->mmg_type == mmgtype )
		{
			// Scale & Move Transform
			switch( jungtype )
			{
				case JUNG_TYPE_HORI:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = 1;//(bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = 0;//(bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JONG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;

				case JUNG_TYPE_VERT:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = 1;//(bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JONG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;

				case JUNG_TYPE_COMP:
				{
					if( mmgtype == MG_TYPE_CHO )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JUNG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = /*1;*/(bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = /*0;*/(bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}

					if( mmgtype == MG_TYPE_JONG )
					{
						tx = (bb2->maxx - bb2->minx) / (bb1->maxx - bb1->minx);
						ty = (bb2->maxy - bb2->miny) / (bb1->maxy - bb1->miny);
						dx = (bb3->minx * (1 - tx)) + (bb2->minx - bb1->minx);
						dy = (bb3->miny * (1 - ty)) + (bb2->miny - bb1->miny);
					}
				}
				break;
			}

			t[0] = tx;
			t[1] = 0;
			t[2] = 0;
			t[3] = ty;
			t[4] = dx;
			t[5] = dy;

			trans[0] = rf->transform[0] * t[0] + rf->transform[1] * t[2];
			trans[1] = rf->transform[0] * t[1] + rf->transform[1] * t[3];
			trans[2] = rf->transform[2] * t[0] + rf->transform[3] * t[2];
			trans[3] = rf->transform[2] * t[1] + rf->transform[3] * t[3];
			trans[4] = rf->transform[4] * t[0] + rf->transform[5] * t[2] + t[4];
			trans[5] = rf->transform[4] * t[1] + rf->transform[5] * t[3] + t[5];

#if 1
			memcpy( rf->transform, trans, sizeof(trans) );
			SCReinstanciateRefChar(sf->glyphs[sf->map->map[index]], rf, ly_fore);
#else
			memcpy( rf->transform, trans, sizeof(trans) );
			SCReinstanciateRefChar(sf->glyphs[sf->map->map[index]], rf, ly_fore);
#endif
			SCUpdateAll(sf->glyphs[sf->map->map[index]]);
			RefCharFindBounds(rf);

#if TRANS_DBG
			hydebug( "\t [Do Transform]================================================================\n" );
			hydebug( "\t [Do Transform] mmgtype: %d / trans: [%f  %f  %f  %f  %f  %f]\n", mmgtype, trans[0], trans[1], trans[2], trans[3], trans[4], trans[5] );
			hydebug( "\t [Do Transform]================================================================\n\n" );
#endif
		}
	}
}

void HY_AddLinkToMG( SplineFont *sf, int from, int into, int mmgtype, int copytrans )
{
	RefChar * srcrf;
	RefChar * dstrf;

	for( srcrf = sf->glyphs[sf->map->map[from]]->layers[ly_fore].refs ; srcrf != NULL ; srcrf = srcrf->next )
	{
		if( srcrf->sc->mmg_type == mmgtype )
		{
			dstrf = RefCharCreate();
			dstrf->sc = srcrf->sc;
			dstrf->unicode_enc = srcrf->unicode_enc;
			dstrf->orig_pos = srcrf->orig_pos;
			dstrf->adobe_enc = getAdobeEnc(srcrf->sc->name);
			if( copytrans )
			{
				dstrf->transform[0] = srcrf->transform[0];
				dstrf->transform[1] = srcrf->transform[1];
				dstrf->transform[2] = srcrf->transform[2];
				dstrf->transform[3] = srcrf->transform[3];
				dstrf->transform[4] = srcrf->transform[4];
				dstrf->transform[5] = srcrf->transform[5];
			}
			else
			{
				dstrf->transform[0] = dstrf->transform[3] = 1.0;
				dstrf->transform[1] = dstrf->transform[2] = 0.0;
				dstrf->transform[4] = dstrf->transform[5] = 0.0;
			}
			dstrf->next = sf->glyphs[sf->map->map[into]]->layers[ly_fore].refs;
			sf->glyphs[sf->map->map[into]]->layers[ly_fore].refs = dstrf;
			SCReinstanciateRefChar( sf->glyphs[sf->map->map[into]], dstrf, ly_fore );
			SCMakeDependent(sf->glyphs[sf->map->map[into]], dstrf->sc);
			SCUpdateAll(sf->glyphs[sf->map->map[into]]);

#if MAIN_DBG
			hydebug( "\t [ADD Link] MMG Type: (%d) / Add Link: [%s  %s]\n\n", mmgtype, dstrf->sc->name, sf->glyphs[sf->map->map[into]]->name );
#endif
		}
	}
}

void HY_GetBaseCharTransInfo( SplineFont *sf, int index, int mmgtype, DBounds *bounds, real trans[6] )
{
	RefChar * rf;

	for( rf = sf->glyphs[sf->map->map[index]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
	{
		if( rf->sc->mmg_type == mmgtype )
		{
			RefCharFindBounds(rf);
			bounds->minx = rf->bb.minx;
			bounds->miny = rf->bb.miny;
			bounds->maxx = rf->bb.maxx;
			bounds->maxy = rf->bb.maxy;

			trans[0] = rf->transform[0];
			trans[1] = rf->transform[1];
			trans[2] = rf->transform[2];
			trans[3] = rf->transform[3];
			trans[4] = rf->transform[4];
			trans[5] = rf->transform[5];

#if TRANS_DBG
			hydebug( "\t [GET Metrics] mmgtype: %d / index: %d <0X%x>\n", mmgtype, index, index );
			hydebug( "\t [GET Metrics] trans: [%f  %f  %f  %f  %f  %f]\n", trans[0], trans[1], trans[2], trans[3], trans[4], trans[5] );
			hydebug( "\t [GET Metrics] bb: [%f  %f  %f  %f]\n\n", bounds->minx, bounds->miny, bounds->maxx, bounds->maxy );
#endif
		}
	}
}

void HY_MakeNoneBaseChar( SplineFont *sf, int index, int jungtype, int mmgtype, DBounds *bounds, real trans[6] )
{
	int cho = 0, jung = 0, jong = 0;
	int index1, index2, index3;
	real t1[6], t2[6], t3[6];
	DBounds bb1, bb2, bb3;
	RefChar * rf;
	int i, gid;
	int basech = 0xAC00;
	int jungarg;

	// Unicode - ChoSung / JungSung / JongSung
	cho = (index - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
	jung = ((index - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
	jong = (index - 0xAC00) % NUM_OF_JONG;

#if MAIN_DBG
	hydebug( "\t\t [MAKE BaseChar] Make Base Glyph <0X%x> / mmgtype: %d \n", index, mmgtype );
	hydebug( "\t\t [MAKE BaseChar] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", cho, jung, jong );
#endif

	// Check Glyph
	gid = sf->map->map[index];
	if( gid == -1 )
	{
		SFMakeChar( sf, sf->map, index );
		SCUpdateAll(sf->glyphs[sf->map->map[index]]);
	}
#if MAIN_DBG
	hydebug( "\t\t [MAKE BaseChar] (0X%x) sf->glyph[%d]\n", index, gid );
#endif

	for( i = MG_TYPE_CHO ; i < MG_TYPE_JONG + 1 ; i++ )
	{
		memset( t1, 0, sizeof(t1) );
		memset( t2, 0, sizeof(t2) );
		memset( t3, 0, sizeof(t3) );

		switch( jungtype )
		{
			case JUNG_TYPE_HORI:
			{
				jungarg = 5;
				if( i == MG_TYPE_CHO )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] ChoSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] ChoSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] ChoSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t ChoSung : <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG & Apply MMG Johap Transform
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}

				if( i == MG_TYPE_JUNG )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] JungSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] JungSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] JungSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t JungSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}

				if( i == MG_TYPE_JONG )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] JongSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] JongSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] JongSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t JongSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG & Apply MMG Johap Transform
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}
			}
			break;

			case JUNG_TYPE_VERT:
			{
				jungarg = 8;
				if( i == MG_TYPE_CHO )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] ChoSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] ChoSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] ChoSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t ChoSung : <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG & Apply MMG Johap Transform
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}

				if( i == MG_TYPE_JUNG )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] JungSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] JungSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] JungSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t JungSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}

				if( i == MG_TYPE_JONG )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] JongSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] JongSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] JongSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t JongSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG & Apply MMG Johap Transform
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}
			}
			break;

			case JUNG_TYPE_COMP:
			{
				jungarg = 9;
				if( i == MG_TYPE_CHO )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] ChoSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] ChoSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] ChoSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t ChoSung : <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG & Apply MMG Johap Transform
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}

				if( i == MG_TYPE_JUNG )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] JungSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] JungSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] JungSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: \t\t\t JungSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}

				if( i == MG_TYPE_JONG )
				{
					index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jungarg) + 16;
					index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + 16;
					index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * jungarg) + jong;
#if MAIN_DBG
					hydebug( "\t\t [MAKE BaseChar] JongSung index1: (%d / 0X%x)\n", index1, index1 );
					hydebug( "\t\t [MAKE BaseChar] JongSung index2: (%d / 0X%x)\n", index2, index2 );
					hydebug( "\t\t [MAKE BaseChar] JongSung index3: (%d / 0X%x)\n", index3, index3 );
					hydebug( "\n" );
#endif
					//hydebug( "Sub: JongSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, index );

					// Get Transform
					HY_GetBaseCharTransInfo( sf, index1, i, &bb1, t1 );
					HY_GetBaseCharTransInfo( sf, index2, i, &bb2, t2 );
					HY_GetBaseCharTransInfo( sf, index3, i, &bb3, t3 );

					// Add Link MG & Apply MMG Johap Transform
					HY_AddLinkToMG( sf, index3, index, i, true );
					HY_MMGJohapApplyTransBase( sf, index, jungtype, i, &bb1, t1, &bb2, t2, &bb3, t3 );
				}
			}
			break;
		}

		if( i == mmgtype )
		{
			// Get Bounding Box & Transform
			for( rf = sf->glyphs[sf->map->map[index]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
			{
				if( rf->sc->mmg_type == mmgtype )
				{
					bounds->minx = rf->bb.minx;
					bounds->miny = rf->bb.miny;
					bounds->maxx = rf->bb.maxx;
					bounds->maxy = rf->bb.maxy;

					trans[0] = rf->transform[0];
					trans[1] = rf->transform[1];
					trans[2] = rf->transform[2];
					trans[3] = rf->transform[3];
					trans[4] = rf->transform[4];
					trans[5] = rf->transform[5];
				}
			}
		}
	}
}

// EOF
