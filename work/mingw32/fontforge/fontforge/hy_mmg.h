/****************************************************
*													*
*					hy_mmg.h						*
*													*
****************************************************/

#ifndef HY_MMG_H
#define HY_MMG_H

#include "views.h"

extern void HY_CheckMMGFont( SplineFont *sf );
extern void HY_FitAllMMGBounds( SplineChar * sc );
extern void HY_MGCopy( FontViewBase * fv, int cp_flags );
extern void HY_MGPaste( FontViewBase *fv, int pasteinto, real trans[6] );
extern void HY_ClearMasterGlyphs(FontViewBase *fv );
extern void HY_ClearMasterGlyphAll(FontViewBase *fv );

#endif			/* HY_MMG_H */

