/********************************************************
 *														*
 *						hy_debug.h						*
 *														*
 ********************************************************/
#ifndef HY_DEBUG_H
#define HY_DEBUG_H


#define xxxHYDebug



#ifdef HYDebug
#define BUILD_YYYY		"2015"
#define BUILD_MM		"04"
#define BUILD_DD		"24 (Debug)"
#else
#define BUILD_YYYY		"2015"
#define BUILD_MM		"04"
#define BUILD_DD		"24 (Release)"
#endif


#ifdef HYMODIFY
#define hydebug(fmt,args...)		printf( "" fmt, ## args )
#define putlog(fmt,args...)			fprintf( "" fmt, ## args )
#else
#define hydebug(fmt,args...);
#define putlog(fmt,args...);
#endif


#endif	// HY_DEBUG_H

