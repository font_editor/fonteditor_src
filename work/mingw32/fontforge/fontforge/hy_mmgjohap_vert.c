/********************************************************
*														*
*				hy_mmgjohap_vert.c						*
*														*
********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_mmg.h"
#include "hy_mmgjohap.h"
#include "hy_debug.h"



void HY_VTypeJohap( SplineFont *sf, int uni, int type, int cho, int jung, int jong)
{
	int basech = 0xAC00;
	int index1, index2, index3;
	DBounds bb1, bb2, bb3;
	real t1[6], t2[6], t3[6];
	int mmgtype = MG_TYPE_NONE;

	/****************************************
				Cho Sung MMG Johap			
	****************************************/
	{
		mmgtype = MG_TYPE_CHO;

		if( jong > 0 )
		{
			index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * 8) + 16;
			index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung) + jong;
			index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + ( NUM_OF_JONG * 8) + 16;
		}
		else
		{
			index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * 8) + 16;
			index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung);
			index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + ( NUM_OF_JONG * 8) + 16;
		}
#if JOHAP_TBL_DBG
		hydebug( "V: ChoSung : <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, uni );
#endif

		// Check Glyph
		if( sf->map->map[uni] == -1 )
		{
			SFMakeChar( sf, sf->map, uni );
			SCUpdateAll(sf->glyphs[sf->map->map[uni]]);
		}

		// ChoSung Index1
		if( sf->map->map[index1] == -1 )
		{
			SFMakeChar( sf, sf->map, index1 );
			SCUpdateAll( sf->glyphs[sf->map->map[index1]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index1]]) )
		{
			HY_GetBaseCharTransInfo( sf, index1, mmgtype, &bb1, t1 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index1, type, mmgtype, &bb1, t1 );
		}

		// ChoSung Index2
		if( sf->map->map[index2] == -1 )
		{
			SFMakeChar( sf, sf->map, index2 );
			SCUpdateAll( sf->glyphs[sf->map->map[index2]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index2]]) )
		{
			HY_GetBaseCharTransInfo( sf, index2, mmgtype, &bb2, t2 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index2, type, mmgtype, &bb2, t2 );
		}

		// ChoSung Index3
		if( sf->map->map[index3] == -1 )
		{
			SFMakeChar( sf, sf->map, index3 );
			SCUpdateAll( sf->glyphs[sf->map->map[index3]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index3]]) )
		{
			HY_GetBaseCharTransInfo( sf, index3, mmgtype, &bb3, t3 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index3, type, mmgtype, &bb3, t3 );
		}

		// Add ChoSung Link & Apply MMG Johap Transform
		if( jong > 0 )
		{
			HY_AddLinkToMG( sf, index3, uni, mmgtype, true );
			HY_MMGJohapApplyTrans( sf, uni, type, mmgtype, &bb1, t1, &bb2, t2, &bb3, t3 );
		}
		else
		{
			HY_AddLinkToMG( sf, index3, uni, mmgtype, true );
			HY_MMGJohapApplyTransNoneJong( sf, uni, type, mmgtype, &bb1, t1, &bb2, t2, &bb3, t3 );
		}
	}
#if JUNG_VERT_DBG
	hydebug( "[%s] ========================================================================\n", __FUNCTION__ );
	hydebug( "[%s] ChoSung MMG Johap Done\n", __FUNCTION__ );
	hydebug( "[%s] ========================================================================\n\n\n", __FUNCTION__ );
#endif

	/****************************************
				Jung Sung MMG Johap			
	****************************************/
	{
		mmgtype = MG_TYPE_JUNG;

		if( jong > 0 )
		{
			index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * 8) + 16;
			index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * 8) + 16;
			index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + ( NUM_OF_JONG * jung ) + jong;
		}
		else
		{
			index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * 8) + 16;
			index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + (NUM_OF_JONG * 8) + 16;
			index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * jung);
		}
#if JOHAP_TBL_DBG
		hydebug( "V: JungSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, uni );
#endif

		// JungSung Index1
		if( sf->map->map[index1] == -1 )
		{
			SFMakeChar( sf, sf->map, index1 );
			SCUpdateAll( sf->glyphs[sf->map->map[index1]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index1]]) )
		{
			HY_GetBaseCharTransInfo( sf, index1, mmgtype, &bb1, t1 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index1, type, mmgtype, &bb1, t1 );
		}

		// JungSung Index2
		if( sf->map->map[index2] == -1 )
		{
			SFMakeChar( sf, sf->map, index2 );
			SCUpdateAll( sf->glyphs[sf->map->map[index2]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index2]]) )
		{
			HY_GetBaseCharTransInfo( sf, index2, mmgtype, &bb2, t2 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index2, type, mmgtype, &bb2, t2 );
		}

		// JungSung Index3
		if( sf->map->map[index3] == -1 )
		{
			SFMakeChar( sf, sf->map, index3 );
			SCUpdateAll( sf->glyphs[sf->map->map[index3]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index3]]) )
		{
			HY_GetBaseCharTransInfo( sf, index3, mmgtype, &bb3, t3 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index3, type, mmgtype, &bb3, t3 );
		}

		// Add JungSung Link & Apply MMG Johap Transform
		if( jong > 0 )
		{
			HY_AddLinkToMG( sf, index3, uni, mmgtype, true );
			HY_MMGJohapApplyTrans( sf, uni, type, mmgtype, &bb1, t1, &bb2, t2, &bb3, t3 );
		}
		else
		{
			HY_AddLinkToMG( sf, index3, uni, mmgtype, true );
			HY_MMGJohapApplyTransNoneJong( sf, uni, type, mmgtype, &bb1, t1, &bb2, t2, &bb3, t3 );
		}
	}
#if JUNG_VERT_DBG
	hydebug( "[%s] ========================================================================\n", __FUNCTION__ );
	hydebug( "[%s] JungSung MMG Johap Done\n", __FUNCTION__ );
	hydebug( "[%s] ========================================================================\n\n\n", __FUNCTION__ );
#endif

	/****************************************
				Jong Sung MMG Johap			
	****************************************/
	if( jong > 0 )
	{
		mmgtype = MG_TYPE_JONG;
		index1 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * 8) + 16;
		index2 = basech + (NUM_OF_JUNG * NUM_OF_JONG * cho) + ( NUM_OF_JONG * jung ) + 16;
		index3 = basech + (NUM_OF_JUNG * NUM_OF_JONG * 6) + (NUM_OF_JONG * 8) + jong;

#if JOHAP_TBL_DBG
		hydebug( "V: JongSung: <0X%x> => <0X%x> | <0X%x> => <0X%x>\n", index1, index2, index3, uni );
#endif

		// Check Glyph
		if( sf->map->map[uni] == -1 )
		{
			SFMakeChar( sf, sf->map, uni );
			SCUpdateAll(sf->glyphs[sf->map->map[uni]]);
		}

		if( sf->map->map[index1] == -1 )
		{
			SFMakeChar( sf, sf->map, index1 );
			SCUpdateAll( sf->glyphs[sf->map->map[index1]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index1]]) )
		{
			HY_GetBaseCharTransInfo( sf, index1, mmgtype, &bb1, t1 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index1, type, mmgtype, &bb1, t1 );
		}

		if( sf->map->map[index2] == -1 )
		{
			SFMakeChar( sf, sf->map, index2 );
			SCUpdateAll( sf->glyphs[sf->map->map[index2]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index2]]) )
		{
			HY_GetBaseCharTransInfo( sf, index2, mmgtype, &bb2, t2 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index2, type, mmgtype, &bb2, t2 );
		}

		if( sf->map->map[index3] == -1 )
		{
			SFMakeChar( sf, sf->map, index3 );
			SCUpdateAll( sf->glyphs[sf->map->map[index3]] );
		}
		if( SCWorthOutputting(sf->glyphs[sf->map->map[index3]]) )
		{
			HY_GetBaseCharTransInfo( sf, index3, mmgtype, &bb3, t3 );
		}
		else
		{
			HY_MakeNoneBaseChar( sf, index3, type, mmgtype, &bb3, t3 );
		}

		// Add JongSung Link & Apply MMG Johap Transform
		HY_AddLinkToMG( sf, index3, uni, mmgtype, true );
		HY_MMGJohapApplyTrans( sf, uni, type, mmgtype, &bb1, t1, &bb2, t2, &bb3, t3 );
	}
	else
	{
#if JOHAP_TBL_DBG
		hydebug( "V: JongSung: N/A\n" );
#endif
	}
#if JUNG_VERT_DBG
	hydebug( "[%s] ========================================================================\n", __FUNCTION__ );
	hydebug( "[%s] JongSung MMG Johap Done\n", __FUNCTION__ );
	hydebug( "[%s] ========================================================================\n\n\n", __FUNCTION__ );
#endif
}


// EOF
