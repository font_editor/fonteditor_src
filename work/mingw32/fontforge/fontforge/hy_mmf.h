/****************************************************
*													*
*					hy_mmf.h						*
*													*
****************************************************/

#ifndef HY_MMF_H
#define HY_MMF_H

#include "views.h"



extern void HY_MMGBlendChar( MMSet * mm, int gid );
extern void HY_MMGBlendChar2( MMSet * mm, int gid );

#endif			/* HY_MMF_H */

