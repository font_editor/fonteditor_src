/****************************************************
*													*
*					hy_rulejohap.h					*
*													*
****************************************************/

#ifndef HY_RULEJOHAP_H
#define HY_RULEJOHAP_H

#include "views.h"


#define	JOHAP_INDEX_NEW					0
#define	JOHAP_INDEX_COPY					1

#define	MAX_JASO_PER_JASOTYPE			32	// 00000 ~ 11111
#define	MAX_GROUP_PER_JASOGROUPLIST		MAX_JASO_PER_JASOTYPE

#define	NUM_JASO_TYPES					3	// chosung group / jungsung group / jongsung group
#define	NUM_COMBI_RULE					4	// chosung rule / jungsung rule / jongsung rule / width rule

#define	NUM_JASO_FC						19
#define	NUM_JASO_MV						21
#define	NUM_JASO_LC						28
#define	TOTAL_NUM_OF_JASO				(NUM_JASO_FC + NUM_JASO_MV + NUM_JASO_LC) 

#define	RULE_FC							0
#define	RULE_MV							1
#define	RULE_LC								2
#define	RULE_WIDTH						3

#define	JASO_FC							0
#define	JASO_MV							1
#define	JASO_LC								2

#define	MAX_JASO							NUM_JASO_LC
#define	MAX_NUM_JASO_IN_JASO_TYPE		NUM_JASO_LC
#define	MAX_JASO_COMBI					(NUM_JASO_FC * NUM_JASO_MV * NUM_JASO_LC)

#define	UNICODE_HANGUL_CNT				11172

#define	HY_REFCHAR_MAX					30

typedef struct rollback_gitem_t
{
	int numJaso;
	uint8 JasoVCode[MAX_JASO_PER_JASOTYPE];

	int bSelect[MAX_JASO_PER_JASOTYPE];

	struct rollback_gitem_t * prev;
	struct rollback_gitem_t * next;
} RollBack_GItem;

typedef struct rollback_glist_t
{
	int nGroups;
	int JasoGroupMap[MAX_JASO_PER_JASOTYPE];
	RollBack_GItem * rollback_gitem;
} RollBack_GList;

typedef struct rollback_rule_t
{
	RollBack_GList * rollback_glist[NUM_JASO_TYPES];

	int GlyfIndexMap[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];
	int Lsb[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];
	int GlyfWidthMap[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];
} RollBack_RULE;

typedef struct rollback_johap_t
{
	SplineFont *sf;
	RollBack_RULE * rollback_rule[NUM_COMBI_RULE];
} RollBack_Johap;

struct compositionrules_t
{
	uint8 chosung_groups[NUM_COMBI_RULE][NUM_JASO_FC];
	uint8 jungsung_groups[NUM_COMBI_RULE][NUM_JASO_MV];
	uint8 jongsung_groups[NUM_COMBI_RULE][NUM_JASO_LC];

	uint16 num_groups[NUM_COMBI_RULE][NUM_JASO_TYPES];
	uint16 * chosung_remap;
	uint16 * jungsung_remap;
	uint16 * jongsung_remap;
	uint16 * width_map;
	int16 * lsb_map;

	uint8 chosung_varients[NUM_JASO_FC];
	uint8 jungsung_varients[NUM_JASO_MV];
	uint8 jongsung_varients[NUM_JASO_LC];
} CompositionRules;


typedef struct jasogroupitem_t
{
	int numJaso;
	uint8 JasoVCode[MAX_JASO_PER_JASOTYPE];

	int bSelect[MAX_JASO_PER_JASOTYPE];

	struct jasogroupitem_t * prev;
	struct jasogroupitem_t * next;
} JasoGroupItem;

typedef struct jasogrouplist_t
{
	int nGroups;
	int JasoGroupMap[MAX_JASO_PER_JASOTYPE];

	JasoGroupItem * jasoitem;
} JasoGroupList;

typedef struct rule_t
{
	int jasotype;

	JasoGroupList * grouplist[NUM_JASO_TYPES];

	int vcode_NumOfGlyf[MAX_JASO_PER_JASOTYPE];

	int *old_maxval;

	int tmpindex[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];

	int tmpgindex[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];
	int GlyfIndexMap[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];

	int tmplsb[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];
	int Lsb[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];

	int tmpwidth[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];
	int GlyfWidthMap[NUM_JASO_FC][NUM_JASO_MV][NUM_JASO_LC];

	GGadget *hsb;
	GGadget *vsb;
	GGadget *ok;
	GGadget *cancel;

	int IndexArr_RowCnt;
	int IndexArr_ColCnt;
	int rows;
	int cols;

	int offtop;
	int offleft;

	int tf_col;
	int tf_row;

	int tf_cho;
	int tf_ju;
	int tf_jo;

	int tfvalue_new;
	int tfvalue_org;
} RULE;

typedef struct johaprule_t
{
	FontView * fv;
	SplineFont * sf;

	char f_ruleinfo[PATH_MAX];
	FILE * rulefp;

	int ruletype;
	RULE * rule[4];

	GWindow group_view;
	GWindow group_subview;
	GWindow johapindex_view;
	GWindow johapindex_subview;
	GWindow widthmap_view;
	GWindow widthmap_subview;

	GFont ** fontset;
	GGadget * tf;

	int johapIndexAddOpt;
	int threepairmethod;
	int bChangedIndex;
	int bChangedWidth;
	int bChangedGroup;

	int curSelBeginCol;
	int curSelBeginRow;
	int curSelEndCol;
	int curSelEndRow;
	int bPressed;

	int bDrag;
	int dstX;
	int dstY;
	int OldMouseX;
	int OldMouseY;
} JohapRule;


/***** FUNCTION DEFINITIONS *****/
extern void HY_WriteRuleSFD( FILE * sfd, SplineFont * sf, JohapRule * johap, SplineChar ** indexmap, int glyphcnt );
extern void HY_SetupJohapIndexView( JohapRule * johap, int ruletype );
extern int HY_GetMaxJohapIndexValue( JohapRule * johap, int ruletype, int groupindex );
extern void HY_CheckJohapIndexMaxValue( JohapRule * johap );
extern JohapRule * HY_InitJohapRule( SplineFont * sf, JohapRule * johap );


extern JohapRule * HY_Setup_VCodeNumOfGlyf( SplineFont * sf, JohapRule * johap );
extern JohapRule * HY_SetupGroupEditContext( SplineFont * sf, JohapRule * johap );
extern JohapRule * HY_ConvertOldJohapToNew( struct compositionrules_t * rules );
extern void HY_RulesFillupVarientCnts( struct compositionrules_t *rules );
extern JohapRule * HY_SFDReadOldCompositionRules( FILE * sfd, SplineFont * sf );
extern JohapRule * HY_SFDReadNewCompositionRules( FILE * sfd, SplineFont * sf );
//extern void HY_MakeJohapEncoding( FontView * fv, SplineFont * sf );
//extern struct ttflangname *TTFLangNamesCopy( struct ttflangname *old );
//extern SplineFont * HY_ImportJohapRules( FontView * fv, SplineFont * imsf );

//extern void HY_JasoGroupChange( GWindow pixmap, GEvent * event, JohapRule * johap );
//extern void HY_JasoGroupChange_atHead( JohapRule * johap, RULE * rule, JasoGroupList * src_grouplist, JasoGroupItem * src_jasoitem, int srcgroup, int dstgroup, int srcindex );
//extern void HY_JasoGroupChange_atTail( JohapRule * johap, RULE * rule, JasoGroupList * src_grouplist, JasoGroupItem * src_jasoitem, int srcgroup, int dstgroup, int srcindex );
//extern void HY_JasoGroupChange_inMidl( JohapRule * johap, RULE * rule, JasoGroupList * src_grouplist, JasoGroupItem * src_jasoitem, int srcgroup, int dstgroup, int srcindex );
//extern void HY_ArrayRemoveItem( uint8 * remove_pos, int item_index, int num_items, size_t item_size );
//extern void HY_PreChangeForRemoveGroupItem( JohapRule * johap, int ruletype, int jasotype, int groupindex );
//extern void HY_RemoveJasoItem_In_GroupList( JohapRule * johap, int ruletype, int jasotype, int groupindex );
//extern void HY_RemoveJasoItem_by_GroupIndex( JohapRule * johap, JasoGroupList * grouplist, int groupindex );
//extern void HY_RemoveJasoItem_In_Map( JohapRule * johap, int ruletype, int jasotype, int groupindex );
//extern void HY_SetupGlyfIndex( JohapRule * johap );
//extern int	 HY_Get_LastGlyfIndex( JohapRule * johap, RULE * rule, int nog_fc, int nog_mv, int nog_lc );
//extern int	 HY_VCodeCompare( const void * elem1, const void * elem2 );

extern void HY_SFDDumpNewJohapRules( FILE * sfd, SplineFont * sf );
extern void HY_RulesResetVarientCnts( JohapRule * johap );
extern void HY_SFDDumpJasoVCode( FILE * sfd, JasoGroupItem * jasoitem );
extern void HY_SFDDumpOldCompositionRules( FILE *sfd, struct compositionrules_t *rules );

//extern void HY_Reset_GlyfHMetric( SplineFont * sf, JohapRule * johap );
//extern void HY_Reset_GlyfList_Normal( SplineFont * sf, JohapRule * johap );
//extern void HY_Reset_GlyfList_ThreepairJohap( SplineFont *sf, JohapRule * johap );
//extern void HY_Reset_GlyfList_MMG( SplineFont * sf, JohapRule * johap );
//extern SplineChar * HY_SCMakeKoreanExtJasoGlyph( SplineFont * sf, SplineChar * oldsc, int enc );
//extern SplineChar * HY_SCMakeKoreanMasterGlyph( SplineFont * sf, SplineChar * oldsc, int enc );
extern SplineChar * HY_SCMakeKoreanComponent( SplineFont * sf, int vcode, int varient, int enc );//, SplineChar ** indexmap  );
extern SplineChar * HY_SCFindOrMakeKoreanComponent( SplineFont * sf, int vcode, int varient, int enc, SplineChar ** oldglyphs, int oldglyphcnt );//, SplineChar ** indexmap );

//extern JasoGroupList * HY_InsertJasoItem_by_GroupIndex( JohapRule * johap, JasoGroupList * grouplist, JasoGroupItem * jasoitem_new, int pos );
//extern JasoGroupItem * HY_GetJasoItem_By_GroupIndex( JasoGroupList * grouplist, int groupindex );
extern struct compositionrules_t * HY_ConvertNewJohapToOld( JohapRule * johap );

//extern void HYTest( FontView *fv );


#endif			/* HY_RULEJOHAP_H */
