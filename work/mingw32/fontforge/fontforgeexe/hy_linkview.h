/********************************************************
 *														*
 *					hy_linkview.h							*
 *														*
 ********************************************************/
#ifndef HY_LINKVIEW_H
#define HY_LINKVIEW_H


#include "views.h"


#define	VIEW_HANGUL								0
#define	VIEW_UNICODE								1
#define	SCROLL_BAR_WIDTH							13
#define	SCROLL_BAR_WIDTH_PX						17
#define	DEFAULT_COMPOSITE_GLYFS_VIEW_WIDTH	(500+13)
#define	DEFAULT_GLYFS_SIZE						48
#define	DEFAULT_GLYFS_DPI							72
#define	DEFAULT_NAME_BOX_HEIGHT					27
#define	DEFAULT_MAX_COL_NUM						5
#define	DEFAULT_MAX_ROW_NUM						4


extern void HY_LinkViewGetPageCount( CharView * cv );
extern void HY_ResetLinkView( CharView * cv, int flags );
extern void HY_DoLinkView( CharView *cv );


#endif	// HY_LINKVIEW_H
