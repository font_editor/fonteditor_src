/****************************************************
*													*
*					hy_mmgui.h						*
*													*
****************************************************/

#ifndef HY_MMGUI_H
#define HY_MMGUI_H


extern void HY_DTOS( char *buf, real val );
extern void HY_DrawMGPLine( CharView *cv, GWindow pixmap, int x1, int y1, int x2, int y2, Color col );
extern void HY_CVDrawMMGBB( CharView *cv, GWindow pixmap, DBounds *bb, Color col );
extern void HY_DrawMMGBBox( CharView * cv, GWindow pixmap );
extern void HY_SetMasterGlyphDlg( FontView * fv );
extern void HY_DrawMMGGuildBox( CharView * cv, GWindow pixmap );

#endif			/* HY_MMGUI_H */

