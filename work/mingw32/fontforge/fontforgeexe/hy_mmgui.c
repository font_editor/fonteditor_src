/****************************************************
*													*
*					hy_mmgui.c						*
*													*
****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <utype.h>
#include <ustring.h>
#include <gfile.h>
#include "fontforge.h"
#include "fontforgeui.h"
#include "splinefont.h"

#include "hy_mmgui.h"

static Color mgbearingxcol = 0xff0000;
static DBounds mg_chobb	= { 100.0, -100.0, 900.0, 700.0 };
static DBounds mg_jungbb	= { 50.0, -150.0, 950.0, 750.0 };
static DBounds mg_jongbb	= { 100.0, -100.0, 900.0, 300.0 };

void HY_DTOS( char *buf, real val )
{
	char *pt;

	sprintf( buf,"%.1f", (double) val);
	pt = buf+strlen(buf);
	if ( pt[-1] == '0' && pt[-2] == '.' )
	{
		pt[-2] = '\0';
	}
}

void HY_DrawMGPLine( CharView *cv, GWindow pixmap, int x1, int y1, int x2, int y2, Color col )
{
	if ( x1 == x2 || y1 == y2 )
	{
		if ( x1 < 0 )
		{
			x1 = 0;
		}
		else if ( x1 > cv->width )
		{
			x1 = cv->width;
		}

		if ( x2 < 0 )
		{
			x2 = 0;
		}
		else if ( x2 > cv->width )
		{
			x2 = cv->width;
		}

		if ( y1 < 0 )
		{
			y1 = 0;
		}
		else if ( y1 > cv->height )
		{
			y1 = cv->height;
		}

		if ( y2 < 0 )
		{
			y2 = 0;
		}
		else if ( y2 > cv->height )
		{
			y2 = cv->height;
		}
		else if ( y1 < -1000 || y2 < -1000 || x1 < -1000 || x2 < -1000 || y1 > cv->height + 1000 || y2 > cv->height + 1000 || x1 > cv->width + 1000 || x2 > cv->width + 1000 )
		{
			return;
		}
	}
	GDrawDrawLine(pixmap,x1,y1,x2,y2,col);
}

#if 0
void HY_DrawMMGGuildBox( CharView * cv, GWindow pixmap )
{
	SplineChar *sc = cv->b.sc;
	int layer = CVLayer( (CharViewBase *) cv);
	Color col = 0x0000ff;
	GRect r;
	int off = cv->xoff + cv->height - cv->yoff;

	if( layer <= 1 )
	{
		return;
	}

	if( cv->b.sc->layers[layer].splines == NULL )
	{
		return;
	}

	if( cv->b.sc->mmg_type == mg_cho )
	{
		r.x		=  cv->xoff + rint(mg_chobb.minx * cv->scale);
		r.y		= -cv->yoff + cv->height - rint(mg_chobb.maxy * cv->scale);
		r.width	= rint((mg_chobb.maxx - mg_chobb.minx) * cv->scale);
		r.height	= rint((mg_chobb.maxy - mg_chobb.miny) * cv->scale);
	}
	else if( cv->b.sc->mmg_type == mg_jung )
	{
		r.x		=  cv->xoff + rint(mg_jungbb.minx * cv->scale);
		r.y		= -cv->yoff + cv->height - rint(mg_jungbb.maxy * cv->scale);
		r.width	= rint((mg_jungbb.maxx - mg_jungbb.minx) * cv->scale);
		r.height	= rint((mg_jungbb.maxy - mg_jungbb.miny) * cv->scale);
	}
	else if( cv->b.sc->mmg_type == mg_jong )
	{
		r.x		=  cv->xoff + rint(mg_jongbb.minx * cv->scale);
		r.y		= -cv->yoff + cv->height - rint(mg_jongbb.maxy * cv->scale);
		r.width	= rint((mg_jongbb.maxx - mg_jongbb.minx) * cv->scale);
		r.height	= rint((mg_jongbb.maxy - mg_jongbb.miny) * cv->scale);
	}
#if 0
	if( cv->b.sc->mmg_type == mg_cho )
	{
		r.x		=  cv->xoff + rint(mg_chobb.minx * cv->scale);
		r.y		= -cv->yoff + cv->height - rint(mg_chobb.maxy * cv->scale);
		r.width	= rint((mg_chobb.maxx - mg_chobb.minx) * cv->scale);
		r.height	= rint((mg_chobb.maxy - mg_chobb.miny) * cv->scale);
	}
	else if( cv->b.sc->mmg_type == mg_jung )
	{
		r.x		=  cv->xoff + rint(mg_jungbb.minx * cv->scale);
		r.y		= -cv->yoff + cv->height - rint(mg_jungbb.maxy * cv->scale);
		r.width	= rint((mg_jungbb.maxx - mg_jungbb.minx) * cv->scale);
		r.height	= rint((mg_jungbb.maxy - mg_jungbb.miny) * cv->scale);
	}
	else if( cv->b.sc->mmg_type == mg_jong )
	{
		r.x		=  cv->xoff + rint(mg_jongbb.minx * cv->scale);
		r.y		= -cv->yoff + cv->height - rint(mg_jongbb.maxy * cv->scale);
		r.width	= rint((mg_jongbb.maxx - mg_jongbb.minx) * cv->scale);
		r.height	= rint((mg_jongbb.maxy - mg_jongbb.miny) * cv->scale);
	}
#endif
	GDrawDrawRect(pixmap, &r, col );
}
#endif

void HY_CVDrawMMGBB( CharView *cv, GWindow pixmap, DBounds *bb, Color col )
{
	GRect r;
	int off = cv->xoff + cv->height - cv->yoff;

	r.x		=  cv->xoff + rint(bb->minx*cv->scale);
	r.y		= -cv->yoff + cv->height - rint(bb->maxy*cv->scale);
	r.width	= rint((bb->maxx-bb->minx)*cv->scale);
	r.height	= rint((bb->maxy-bb->miny)*cv->scale);
	GDrawSetDashedLine(pixmap, 1, 1, off);
	GDrawDrawRect(pixmap, &r, col );
	GDrawSetDashedLine(pixmap,0,0,0);
}

void HY_DrawMMGBBox( CharView * cv, GWindow pixmap )
{
	SplineChar *sc = cv->b.sc;
	int layer = CVLayer( (CharViewBase *) cv);
	BasePoint *bounds[4];
	DBounds bb;
	Color col = 0xffe08c;
	int x, y, x2, y2, l;
	char buf[20];

	if( layer <= 1 )
	{
		return;
	}

	if( cv->b.sc->layers[layer].splines == NULL )
	{
		return;
	}

	SplineCharLayerFindBounds( cv->b.sc, layer, &bb);
	HY_CVDrawMMGBB( cv, pixmap, &bb, col );
	GDrawSetFont( pixmap, cv->small );


	/*****************************************************************************/
	if ( bb.minx != 0 )
	{
		x = rint( bb.minx * cv->scale ) + cv->xoff;
		y = cv->height - cv->yoff - rint( bb.maxy * cv->scale );
		HY_DrawMGPLine( cv, pixmap, cv->xoff, y, x, y, mgbearingxcol );

		/* arrow heads */
		HY_DrawMGPLine( cv, pixmap, cv->xoff,y, cv->xoff + 4, y + 4, mgbearingxcol );
		HY_DrawMGPLine( cv, pixmap, cv->xoff,y, cv->xoff + 4, y - 4, mgbearingxcol );
		HY_DrawMGPLine( cv, pixmap, x, y, x - 4, y - 4, mgbearingxcol );
		HY_DrawMGPLine( cv, pixmap, x, y, x - 4, y + 4, mgbearingxcol );
		HY_DTOS( buf, bb.minx );

		x = cv->xoff + ( x - cv->xoff - GDrawGetText8Width( pixmap, buf, -1 ) ) / 2;
		GDrawDrawText8( pixmap, x, y - 4, buf, -1, mgbearingxcol );
	}


	/*****************************************************************************/
	if ( sc->width != bb.maxx )
	{
		x = rint( bb.maxx * cv->scale ) + cv->xoff;
		y = cv->height - cv->yoff - rint( bb.miny * cv->scale );
		x2 = rint( sc->width * cv->scale ) + cv->xoff;
		HY_DrawMGPLine( cv, pixmap, x, y, x2, y, mgbearingxcol );

		/* arrow heads */
		HY_DrawMGPLine( cv, pixmap, x, y, x + 4, y + 4, mgbearingxcol );
		HY_DrawMGPLine( cv, pixmap, x, y, x + 4, y - 4, mgbearingxcol );
		HY_DrawMGPLine( cv, pixmap, x2, y, x2 - 4, y - 4, mgbearingxcol );
		HY_DrawMGPLine( cv, pixmap, x2, y, x2 - 4, y + 4, mgbearingxcol );
		HY_DTOS( buf, sc->width - bb.maxx );

		x = x + ( x2 - x - GDrawGetText8Width( pixmap, buf, -1 ) ) / 2;
		GDrawDrawText8( pixmap, x, y - 4, buf, -1, mgbearingxcol );
	}


	/*****************************************************************************/
	x = rint( bb.maxx * cv->scale ) + cv->xoff;
	y = cv->height - cv->yoff - rint( bb.miny * cv->scale );
	y2 = cv->height - cv->yoff - rint( -sc->parent->descent * cv->scale );
	HY_DrawMGPLine( cv, pixmap, x, y2, x, y, mgbearingxcol );

	/* arrow heads */
	HY_DrawMGPLine( cv, pixmap, x, y2, x - 4, y2 - 4, mgbearingxcol );
	HY_DrawMGPLine( cv, pixmap, x, y2, x + 4, y2 - 4, mgbearingxcol );
	HY_DrawMGPLine( cv, pixmap, x, y, x + 4, y + 4, mgbearingxcol);
	HY_DrawMGPLine( cv, pixmap, x, y, x - 4, y + 4, mgbearingxcol);
	HY_DTOS( buf, bb.miny - sc->parent->descent );

	y = cv->height - rint( bb.miny * cv->scale) - cv->yoff;
	GDrawDrawText8( pixmap, x + 8, y + 20 , buf, -1, mgbearingxcol );


	/*****************************************************************************/
	x = rint( bb.minx * cv->scale ) + cv->xoff;
	y = cv->height - cv->yoff - rint( bb.maxy * cv->scale );
	y2 = cv->height - cv->yoff - rint( sc->parent->ascent * cv->scale );
	HY_DrawMGPLine( cv, pixmap, x, y, x, y2, mgbearingxcol );

	/* arrow heads */
	HY_DrawMGPLine( cv, pixmap, x, y, x - 4, y - 4, mgbearingxcol );
	HY_DrawMGPLine( cv, pixmap, x, y, x + 4, y - 4, mgbearingxcol );
	HY_DrawMGPLine( cv, pixmap, x, y2, x + 4, y2 + 4, mgbearingxcol );
	HY_DrawMGPLine( cv, pixmap, x, y2, x - 4, y2 + 4, mgbearingxcol );
	HY_DTOS( buf, sc->vwidth - bb.maxy );

	y = cv->height - rint( bb.maxy * cv->scale ) - cv->yoff;
	GDrawDrawText8( pixmap, x + 8, y - 8, buf, -1, mgbearingxcol );
}

#define CID_LabelBaseGlyph_Start			1001
#define CID_TextFieldBaseGlyph_Start		1002
#define CID_LabelBaseGlyph_End			1003
#define CID_TextFieldBaseGlyph_End		1004
#define CID_Btn_BaseGlyph_OK			1005
#define CID_Btn_BaseGlyph_Cancel		1006
#define CID_LabelChoSung				1101
#define CID_TextFieldChoSung_Start		1102
#define CID_TextFieldChoSung_End		1103
#define CID_LabelJungSung				1104
#define CID_TextFieldJungSung_Start		1105
#define CID_TextFieldJungSung_End		1106
#define CID_LabelJongSung				1107
#define CID_TextFieldJongSung_Start		1108
#define CID_TextFieldJongSung_End		1109
#define CID_Btn_SetSung_OK				1110
#define CID_Btn_SetSung_Cancel			1111

static int s_baseGlyphStart = 0;
static int s_baseGlyphEnd = 0;


static int HY_SetBaseGlyph_OK( GGadget *g, GEvent *e )
{
	if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate )
	{
		GWindow gw = GGadgetGetWindow(g);
		FontView * fv = GDrawGetUserData(gw);
		char * strstart = NULL;
		char * strend = NULL;
		int istart = 0;
		int iend = 0;
		int tempval = 0;
		int gid;
		int i;

		GGadget * g1 = GWidgetGetControl( gw, CID_TextFieldBaseGlyph_Start );
		strstart = GGadgetGetTitle8( g1 );
		istart = atoi( strstart );

		GGadget * g2 = GWidgetGetControl( gw, CID_TextFieldBaseGlyph_End );
		strend = GGadgetGetTitle8( g2 );
		iend = atoi( strend );

		if( strstart == NULL || strend == NULL )
		{
			return( false );
		}

		for( i = 65536 ; i < fv->b.map->enccount ; i++ )
		{
			gid = fv->b.map->map[i];
			if( gid != -1 && SCWorthOutputting(fv->b.sf->glyphs[gid]) )
			{
				fv->b.sf->glyphs[gid]->bmaster = 0;
			}
		}

		for( i = 65536 ; i < fv->b.map->enccount ; i++ )
		{
			gid = fv->b.map->map[i];
			if( gid != -1 && SCWorthOutputting(fv->b.sf->glyphs[gid]) )
			{
				if( i == istart )
				{
					istart = i;
				}
				else if( i == iend )
				{
					iend = i;
				}
			}
		}

		if( istart > iend )
		{
			tempval = istart;
			istart = iend;
			iend = tempval;
		}
		s_baseGlyphStart = istart;
		s_baseGlyphEnd = iend;

		for( i = 65536 ; i < fv->b.map->enccount ; i++ )
		{
			gid = fv->b.map->map[i];
			if( gid != -1 && SCWorthOutputting(fv->b.sf->glyphs[gid]) )
			{
				if( i >= s_baseGlyphStart && i <= s_baseGlyphEnd )
				{
					fv->b.sf->glyphs[gid]->bmaster = 1;
				}
			}
		}
		fv->b.sf->bMMGFont = 1;
		fv->b.sf->mgstartPos = s_baseGlyphStart;
		fv->b.sf->mgendPos = s_baseGlyphEnd;

		GDrawRequestExpose(fv->v, NULL, false);
		GDrawRequestExpose(fv->gw, NULL, false);
		GDrawDestroyWindow(gw);
	}
	return( true );
}

static int HY_SetBaseGlyph_Cancel( GGadget *g, GEvent *e )
{
	if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate )
	{
		GWindow gw = GGadgetGetWindow( g );
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static int setbaseglyph_e_h( GWindow gw, GEvent * event )
{
	switch( event->type )
	{
		case et_close:
			GDrawDestroyWindow(gw);
		break;
	}
	return( true );
}

void HY_SetMasterGlyphDlg( FontView * fv )
{
	GRect pos;
	GWindow gw;
	GWindowAttrs wattrs;
	GTextInfo label[15];
	GGadgetCreateData gcd[15];
	int k = 0;

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask				= wam_events | wam_cursor | wam_utf8_wtitle | wam_undercursor | wam_isdlg;
	wattrs.event_masks		= ~( 1 << et_charup );
	wattrs.restrict_input_to_me	= 1;
	wattrs.undercursor			= 1;
	wattrs.cursor				= ct_pointer;
	wattrs.utf8_window_title		= _("Setting Master Glyph");
	wattrs.is_dlg				= true;
	pos.x = pos.y				= 10;
#ifdef LJS
	pos.width					= 280 + 50;
#else
	pos.width					= 280;
#endif
	pos.height				= 150;

	gw = GDrawCreateTopWindow( NULL, &pos, setbaseglyph_e_h, fv, &wattrs );
	GDrawSetVisible( gw, true );

	memset( &label, 0, sizeof( label ) );
	memset( &gcd, 0, sizeof( gcd ) );

	label[k].text					= (unichar_t *) _("Master Glyph Start:");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 10;
	gcd[k].gd.pos.y				= 10;
#ifdef LJS
	gcd[k].gd.pos.width			= 100 + 20;
#else
	gcd[k].gd.pos.width			= 100;
#endif
	gcd[k].gd.pos.height			= 17;
	gcd[k].gd.flags				= gg_enabled|gg_visible;
	gcd[k].gd.cid					= CID_LabelBaseGlyph_Start;
	gcd[k++].creator				= GLabelCreate;

//	label[k].text					= (unichar_t *) _("");
//	label[k].text_is_1byte			= true;
//	gcd[k].gd.label				= &label[k];

#ifdef LJS
	gcd[k].gd.pos.x				= 120 + 20;
#else
	gcd[k].gd.pos.x				= 120;
#endif
	gcd[k].gd.pos.y				= 10;
	gcd[k].gd.pos.width			= 80;
	gcd[k].gd.pos.height			= 20;
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_TextFieldBaseGlyph_Start;
	gcd[k++].creator				= GTextFieldCreate;

	label[k].text					= (unichar_t *) _("Master Glyph End:");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 10;
	gcd[k].gd.pos.y				= 40;
#ifdef LJS
	gcd[k].gd.pos.width			= 100 + 20;
#else
	gcd[k].gd.pos.width			= 100;
#endif
	gcd[k].gd.pos.height			= 17;
	gcd[k].gd.flags				= gg_enabled|gg_visible;
	gcd[k].gd.cid					= CID_LabelBaseGlyph_End;
	gcd[k++].creator				= GLabelCreate;

//	label[k].text					= (unichar_t *) _("");
//	label[k].text_is_1byte			= true;
//	gcd[k].gd.label				= &label[k];
#ifdef LJS
	gcd[k].gd.pos.x				= 120 + 20;
#else
	gcd[k].gd.pos.x				= 120;
#endif
	gcd[k].gd.pos.y				= 40;
	gcd[k].gd.pos.width			= 80;
	gcd[k].gd.pos.height			= 20;
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_TextFieldBaseGlyph_End;
	gcd[k++].creator				= GTextFieldCreate;

	label[k].text					= (unichar_t *)_("OK");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 20;
	gcd[k].gd.pos.y				= 80;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 20; 
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_Btn_BaseGlyph_OK;
	gcd[k].gd.handle_controlevent	= HY_SetBaseGlyph_OK;
	gcd[k++].creator				= GButtonCreate;

	label[k].text					= (unichar_t *)_("Cancel");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 95;
	gcd[k].gd.pos.y				= 80;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 20; 
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_Btn_BaseGlyph_Cancel;
	gcd[k].gd.handle_controlevent	= HY_SetBaseGlyph_Cancel;
	gcd[k++].creator				= GButtonCreate;

	GGadgetsCreate( gw, gcd );
}

// END OF FILE

