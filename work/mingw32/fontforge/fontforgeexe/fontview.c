/* Copyright (C) 2000-2012 by George Williams */
/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.

 * The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <fontforge-config.h>

#include "inc/gnetwork.h"
#include "collabclientui.h"
#include "collabclientpriv.h"

#include "fontforgeui.h"
#include "groups.h"
#include "psfont.h"
#include <gfile.h>
#include <gio.h>
#include <gresedit.h>
#include <ustring.h>
#include "../fontforge/ffglib.h"
#include <gkeysym.h>
#include <utype.h>
#include <chardata.h>
#include <gresource.h>
#include <math.h>
#include <unistd.h>

#include "gutils/unicodelibinfo.h"
#include "sfundo.h"

#if defined (__MINGW32__)
#include <windows.h>
#endif

#include "xvasprintf.h"

#ifdef HYMODIFY
#include <time.h>
#include "hy_mmgui.h"
#include "hy_rulejohapui.h"
#include "../fontforge/hy_debug.h"
#include "../fontforge/hy_rulejohap.h"
#include "../fontforge/hy_mmg.h"
#include "../fontforge/hy_mmgjohap.h" 
#include "../fontforge/hy_mmglinktbl.h" 
#include "../fontforge/hy_sfd.h"
#include "../fontforge/hy_glyphutils.h"
#include "../fontforge/hy_mmf.h"
#include <curl/curl.h>
#include <curl/easy.h>
#include <iconv.h>


#define TEMP_RULE_SFD			"temp.sfd"

#define FVDRAW_DBG				0
#define USE_LOG					0
#define TEST						0
#define RESET_GLYF_TEST		1
#define AUTO_MAKE_MG_TEST		1
#define USE_HANDWRITE			1
#define USE_MMG					1
#define USE_MG_JOHAP			1
#define REGIST_FONT				1

#define COMMAND_USER_LOGIN	"login"
#define COMMAND_IMPORT_SFD	"importSFD"

#define BASE_API_URL		"http://www.koreafont.com/api/hangultuel.php"
#define REGISTER_URL		"http://www.koreafont.com/reg/reg.font.popup.php"

// stem weight, emsize
static int s_weight = 20;
static int s_emsize = 1000;

// cho-sung, jung-sung, jong-sung bounding box range
static DBounds chobb			= { 100.0, 900.0, -100.0, 700.0 };
static DBounds jungbb_vert	= { 50.0, 950.0, -150.0, 500.0 };
static DBounds jungbb_hori	= { 500.0, 900.0, -150.0, 750.0 };
static DBounds jungbb_comp	= { 50.0, 950.0, -150.0, 750.0 };
static DBounds jongbb			= { 100.0, 900.0, -100.0, 300.0 };
#endif

int OpenCharsInNewWindow = 0;
char *RecentFiles[RECENT_MAX] = { NULL };
int save_to_dir = 0;			/* use sfdir rather than sfd */
unichar_t *script_menu_names[SCRIPT_MENU_MAX];
char *script_filenames[SCRIPT_MENU_MAX];
extern int onlycopydisplayed, copymetadata, copyttfinstr, add_char_to_name_list;
int home_char='A';
int compact_font_on_open=0;
int navigation_mask = 0;		/* Initialized in startui.c */

#ifdef CKS	// HYMODIFY :: 2016.01.20
static char *fv_fontnames = HY_UI_FAMILIES;
#else
static char *fv_fontnames = MONO_UI_FAMILIES;
#endif
extern char* pref_collab_last_server_connected_to;
extern void python_call_onClosingFunctions();

#define	FV_LAB_HEIGHT	15

#ifdef BIGICONS
#define fontview_width 32
#define fontview_height 32
static unsigned char fontview_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0xfe, 0xff, 0xff, 0xff, 0x02, 0x20, 0x80, 0x00,
   0x82, 0x20, 0x86, 0x08, 0x42, 0x21, 0x8a, 0x14, 0xc2, 0x21, 0x86, 0x04,
   0x42, 0x21, 0x8a, 0x14, 0x42, 0x21, 0x86, 0x08, 0x02, 0x20, 0x80, 0x00,
   0xaa, 0xaa, 0xaa, 0xaa, 0x02, 0x20, 0x80, 0x00, 0x82, 0xa0, 0x8f, 0x18,
   0x82, 0x20, 0x91, 0x24, 0x42, 0x21, 0x91, 0x02, 0x42, 0x21, 0x91, 0x02,
   0x22, 0x21, 0x8f, 0x02, 0xe2, 0x23, 0x91, 0x02, 0x12, 0x22, 0x91, 0x02,
   0x3a, 0x27, 0x91, 0x24, 0x02, 0xa0, 0x8f, 0x18, 0x02, 0x20, 0x80, 0x00,
   0xfe, 0xff, 0xff, 0xff, 0x02, 0x20, 0x80, 0x00, 0x42, 0x20, 0x86, 0x18,
   0xa2, 0x20, 0x8a, 0x04, 0xa2, 0x20, 0x86, 0x08, 0xa2, 0x20, 0x8a, 0x10,
   0x42, 0x20, 0x8a, 0x0c, 0x82, 0x20, 0x80, 0x00, 0x02, 0x20, 0x80, 0x00,
   0xaa, 0xaa, 0xaa, 0xaa, 0x02, 0x20, 0x80, 0x00};
#else
#define fontview2_width 16
#define fontview2_height 16
static unsigned char fontview2_bits[] = {
   0x00, 0x07, 0x80, 0x08, 0x40, 0x17, 0x40, 0x15, 0x60, 0x09, 0x10, 0x02,
   0xa0, 0x01, 0xa0, 0x00, 0xa0, 0x00, 0xa0, 0x00, 0x50, 0x00, 0x52, 0x00,
   0x55, 0x00, 0x5d, 0x00, 0x22, 0x00, 0x1c, 0x00};
#endif

extern int _GScrollBar_Width;

static int fv_fontsize = 9/*11*/, fv_fs_init=0;
static Color fvselcol = 0xffff00, fvselfgcol=0x000000;
Color view_bgcol;
static Color fvglyphinfocol = 0xff0000;
static Color fvemtpyslotfgcol = 0xd08080;
static Color fvchangedcol = 0x000060;
static Color fvhintingneededcol = 0x0000ff;

#ifdef HYMODIFY
static Color mg_col	= 0x38adc1;
static Color chomg_col	= 0xbce55c;
static Color jumg_col	= 0x86e57f;
static Color jomg_col	= 0x5cd1e5;

#define MG_TYPE_NONE		0
#define MG_TYPE_CHO		1
#define MG_TYPE_JUNG		2
#define MG_TYPE_JONG		3

#define MID_HY_SetMG						30000
#define MID_HY_UnsetMG						30001
#define MID_HY_SetMGTypeCho				30002
#define MID_HY_SetMGTypeJung				30003
#define MID_HY_SetMGTypeJong				30004
#define MID_HY_UnsetMGType					30005
#define MID_HY_CopyMG						30006
#define MID_HY_MoveMG						30007
#define MID_HY_CutMG						30008
#define MID_HY_PasteMG						30009
#define MID_HY_ClearMG						30010
#define MID_HY_ClearAllMG					30011
#define MID_HY_FitMGBBox					30012
#define MID_HYMMGJohap_Hangul_All			30013
#define MID_HYMMGJohap_Jung_Hori			30014
#define MID_HYMMGJohap_Jung_Vert			30015
#define MID_HYMMGJohap_Jung_Comp			30016
#define MID_HYMMGJohap_Hangul_KSC5601	30017
#define MID_HYAutoMakeMG					30018
#define MID_HYImportHandWrite				30019
#define MID_HYRegisterFont					30020

#define MID_HYEZJohap_Hangul_All			30103
#define MID_HYEZJohap_Hangul_KSC5601		30104
#define MID_HYEZJohap_Hangul_Hori			30105
#define MID_HYEZJohap_Hangul_Vert			30106
#define MID_HYEZJohap_Hangul_Comp			30107
#define MID_HYDetachNoneBaseChar			30108
#define MID_HYGetBaseCharLinkMap			30109
#define MID_HYGetBaseCharToHex				30110
#define MID_HYGetBaseCharToUni				30111
#define MID_HYBaseCharReform_1				30112
#define MID_HYBaseCharReform_2				30113
#define MID_HYBaseCharReform_3				30114
#define MID_HYTest							50001
#endif

enum glyphlable { gl_glyph, gl_name, gl_unicode, gl_encoding };
int default_fv_showhmetrics=false, default_fv_showvmetrics=false,
	default_fv_glyphlabel = gl_glyph;
#define METRICS_BASELINE 0x0000c0
#define METRICS_ORIGIN	 0xc00000
#define METRICS_ADVANCE	 0x008000
FontView *fv_list=NULL;

static void AskAndMaybeCloseLocalCollabServers( void );

#ifdef HYMODIFY
static int _FVMenuClose(FontView *fv);
static void HY_StrokeLayers( SplineFont *sf, SplineChar *sc );
#endif
#ifdef CKS	// HYMODIFY :: 2016.02.05
static void HY_DoRegisterFontDlg( SplineFont *sf );
#endif
#ifdef CKS	// HYMODIFY :: 2016.02.17
static void HY_DoAutoMakeMG( SplineFont *sf );
enum command_type
{
	cm_importsfd = 0,
	cm_registerfont
};
#endif
#ifdef CKS	// HYMODIFY :: 2016.02.24
static int	numglyf[NUM_JASO_TYPES][MAX_JASO];
static int JasoNum[NUM_JASO_TYPES] = { NUM_JASO_FC, NUM_JASO_MV, NUM_JASO_LC };

static void HY_Get_RuleInfo( SplineFont *sf )
{
	JohapRule *johap = sf->johap;
	int ruletype = 0;
	int jasotype = 0;
	int groupindex = 0;
	int jasoindex = 0;
	int jasocount = 0;
	int fc;
	int mv;
	int lc;
	FILE * log;

	log = fopen( "RuleInfo.log", "w" );
	if( log == NULL )
	{
		ff_post_error( _("Error"), _("Failed to open log file") );
		return;
	}

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE - 1 ; ruletype++ )
	{
		RULE *rule = johap->rule[ruletype];

		for( jasotype = 0 ; jasotype < NUM_JASO_TYPES ; jasotype++ )
		{
			JasoGroupList *glist = rule->grouplist[jasotype];
			JasoGroupItem *gitem = glist->jasoitem;

			switch( jasotype )
			{
				case JASO_FC:
				{
					for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) JasoGroupMap[%d]: %d\n", ruletype, jasotype, jasoindex, glist->JasoGroupMap[jasoindex] );
					}
					for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) vcode_NumOfGlyf[%d]: %d\n", ruletype, jasotype, jasoindex, rule->vcode_NumOfGlyf[jasoindex] );
						numglyf[ruletype][jasoindex] = rule->vcode_NumOfGlyf[jasoindex];
						//fprintf( log, "numglyf[%d][%d]: %d\n", ruletype, jasoindex, numglyf[ruletype][jasoindex] );
					}
				}
				break;
				case JASO_MV:
				{
					for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) JasoGroupMap[%d]: %d\n", ruletype, jasotype, jasoindex, glist->JasoGroupMap[jasoindex] );
					}
					for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) vcode_NumOfGlyf[%d]: %d\n", ruletype, jasotype, jasoindex, rule->vcode_NumOfGlyf[jasoindex] );
						numglyf[ruletype][jasoindex] = rule->vcode_NumOfGlyf[jasoindex];
						//fprintf( log, "numglyf[%d][%d]: %d\n", ruletype, jasoindex, numglyf[ruletype][jasoindex] );
					}
				}
				break;
				case JASO_LC:
				{
					for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) JasoGroupMap[%d]: %d\n", ruletype, jasotype, jasoindex, glist->JasoGroupMap[jasoindex] );
					}
					for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) vcode_NumOfGlyf[%d]: %d\n", ruletype, jasotype, jasoindex, rule->vcode_NumOfGlyf[jasoindex] );
						numglyf[ruletype][jasoindex] = rule->vcode_NumOfGlyf[jasoindex];
						//fprintf( log, "numglyf[%d][%d]: %d\n", ruletype, jasoindex, numglyf[ruletype][jasoindex] );
					}
				}
				break;
			}

			fprintf( log, "\n" );
			if( ruletype == jasotype )
			{
				for( groupindex = 0 ; groupindex < glist->nGroups ; groupindex++ )
				{
					for( jasoindex = 0 ; jasoindex < gitem->numJaso ; jasoindex++ )
					{
						fprintf( log, "(%d | %d) JasoVCode[%d]: %d\n", ruletype, jasotype, jasoindex, gitem->JasoVCode[jasoindex] );
					}
					gitem = gitem->next;
				}
			}
		}
	}

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE - 1 ; ruletype++ )
	{
		RULE *rule = johap->rule[ruletype];

		fprintf( log, "\n" );
		switch( ruletype )
		{
			case RULE_FC:
			{
				for( fc = 0 ; fc < NUM_JASO_FC ; fc++ )
				{
					for( mv = 0 ; mv < NUM_JASO_MV ; mv++ )
					{
						for( lc = 0 ; lc < NUM_JASO_LC ; lc++ )
						{
							fprintf( log, "(rule: %d) GlyfIndexMap[%d][%d][%d]: %d\n", ruletype, fc, mv, lc, rule->GlyfIndexMap[fc][mv][lc] );
						}
					}
				}
			}
			break;
			case RULE_MV:
			{
				for( mv = 0 ; mv < NUM_JASO_MV ; mv++ )
				{
					for( fc = 0 ; fc < NUM_JASO_FC ; fc++ )
					{
						for( lc = 0 ; lc < NUM_JASO_LC ; lc++ )
						{
							fprintf( log, "(rule: %d) GlyfIndexMap[%d][%d][%d]: %d\n", ruletype, fc, mv, lc, rule->GlyfIndexMap[fc][mv][lc] );
						}
					}
				}
			}
			break;
			case RULE_LC:
			{
				for( lc = 0 ; lc < NUM_JASO_LC ; lc++ )
				{
					for( fc = 0 ; fc < NUM_JASO_FC ; fc++ )
					{
						for( mv = 0 ; mv < NUM_JASO_MV ; mv++ )
						{
							fprintf( log, "(rule: %d) GlyfIndexMap[%d][%d][%d]: %d\n", ruletype, fc, mv, lc, rule->GlyfIndexMap[fc][mv][lc] );
						}
					}
				}
			}
			break;
		}
	}

	fclose( log );
}
#endif

#ifdef CKS	// HYMODIFY :: 2016.02.03
static int HY_GetSCDependentCount( SplineChar *sc )
{
	int count = 0;
	struct splinecharlist * dep;

	if( sc == NULL )
	{
		return count;
	}

	for( dep = sc->dependents ; dep != NULL ; dep = dep->next )
	{
		count++;
	}
	return count;
}
#endif

static void FV_ToggleCharChanged(SplineChar *sc) {
    int i, j;
    int pos;
    FontView *fv;

    for ( fv = (FontView *) (sc->parent->fv); fv!=NULL; fv=(FontView *) (fv->b.nextsame) ) {
	if ( fv->b.sf!=sc->parent )		/* Can happen in CID fonts if char's parent is not currently active */
    continue;
	if ( fv->v==NULL || fv->colcnt==0 )	/* Can happen in scripts */
    continue;
	for ( pos=0; pos<fv->b.map->enccount; ++pos ) if ( fv->b.map->map[pos]==sc->orig_pos ) {
	    i = pos / fv->colcnt;
	    j = pos - i*fv->colcnt;
	    i -= fv->rowoff;
 /* Normally we should be checking against fv->rowcnt (rather than <=rowcnt) */
 /*  but every now and then the WM forces us to use a window size which doesn't */
 /*  fit our expectations (maximized view) and we must be prepared for half */
 /*  lines */
	    if ( i>=0 && i<=fv->rowcnt ) {
		GRect r;
		r.x = j*fv->cbw+1; r.width = fv->cbw-1;
		r.y = i*fv->cbh+1; r.height = fv->lab_height-1;
		GDrawRequestExpose(fv->v,&r,false);
	    }
	}
    }
}

void FVMarkHintsOutOfDate(SplineChar *sc) {
    int i, j;
    int pos;
    FontView *fv;

    if ( sc->parent->onlybitmaps || sc->parent->multilayer || sc->parent->strokedfont )
return;
    for ( fv = (FontView *) (sc->parent->fv); fv!=NULL; fv=(FontView *) (fv->b.nextsame) ) {
	if ( fv->b.sf!=sc->parent )		/* Can happen in CID fonts if char's parent is not currently active */
    continue;
	if ( sc->layers[fv->b.active_layer].order2 )
    continue;
	if ( fv->v==NULL || fv->colcnt==0 )	/* Can happen in scripts */
    continue;
	for ( pos=0; pos<fv->b.map->enccount; ++pos ) if ( fv->b.map->map[pos]==sc->orig_pos ) {
	    i = pos / fv->colcnt;
	    j = pos - i*fv->colcnt;
	    i -= fv->rowoff;
 /* Normally we should be checking against fv->rowcnt (rather than <=rowcnt) */
 /*  but every now and then the WM forces us to use a window size which doesn't */
 /*  fit our expectations (maximized view) and we must be prepared for half */
 /*  lines */
	    if ( i>=0 && i<=fv->rowcnt ) {
		GRect r;
		r.x = j*fv->cbw+1; r.width = fv->cbw-1;
		r.y = i*fv->cbh+1; r.height = fv->lab_height-1;
		GDrawDrawLine(fv->v,r.x,r.y,r.x,r.y+r.height-1,fvhintingneededcol);
		GDrawDrawLine(fv->v,r.x+1,r.y,r.x+1,r.y+r.height-1,fvhintingneededcol);
		GDrawDrawLine(fv->v,r.x+r.width-1,r.y,r.x+r.width-1,r.y+r.height-1,fvhintingneededcol);
		GDrawDrawLine(fv->v,r.x+r.width-2,r.y,r.x+r.width-2,r.y+r.height-1,fvhintingneededcol);
	    }
	}
    }
}

static int FeatureTrans(FontView *fv, int enc) {
    SplineChar *sc;
    PST *pst;
    char *pt;
    int gid;

    if ( enc<0 || enc>=fv->b.map->enccount || (gid = fv->b.map->map[enc])==-1 )
return( -1 );
    if ( fv->cur_subtable==NULL )
return( gid );

    sc = fv->b.sf->glyphs[gid];
    if ( sc==NULL )
return( -1 );
    for ( pst = sc->possub; pst!=NULL; pst=pst->next ) {
	if (( pst->type == pst_substitution || pst->type == pst_alternate ) &&
		pst->subtable == fv->cur_subtable )
    break;
    }
    if ( pst==NULL )
return( -1 );
    pt = strchr(pst->u.subs.variant,' ');
    if ( pt!=NULL )
	*pt = '\0';
    gid = SFFindExistingSlot(fv->b.sf, -1, pst->u.subs.variant );
    if ( pt!=NULL )
	*pt = ' ';
return( gid );
}

static void FVDrawGlyph(GWindow pixmap, FontView *fv, int index, int forcebg )
{
	GRect box, old2;
	int feat_gid;
	SplineChar *sc;
	struct _GImage base;
	GImage gi;
	GClut clut;
	int i,j;
	int em = fv->b.sf->ascent+fv->b.sf->descent;
	int yorg = fv->magnify*(fv->show->ascent);

	i = index / fv->colcnt;
	j = index - i*fv->colcnt;
	i -= fv->rowoff;

	if ( index<fv->b.map->enccount && (fv->b.selected[index] || forcebg))
	{
		box.x = j*fv->cbw+1; box.width = fv->cbw-1;
		box.y = i*fv->cbh+fv->lab_height+1; box.height = fv->cbw;
		GDrawFillRect(pixmap,&box,fv->b.selected[index] ? fvselcol : view_bgcol );
	}
	feat_gid = FeatureTrans(fv,index);
	sc = feat_gid!=-1 ? fv->b.sf->glyphs[feat_gid]: NULL;
	if ( !SCWorthOutputting(sc) )
	{
		int x = j*fv->cbw+1, xend = x+fv->cbw-2;
		int y = i*fv->cbh+fv->lab_height+1, yend = y+fv->cbw-1;
		GDrawDrawLine(pixmap,x,y,xend,yend,fvemtpyslotfgcol);
		GDrawDrawLine(pixmap,x,yend,xend,y,fvemtpyslotfgcol);
	}
	if ( sc!=NULL )
	{
#ifdef HYMODIFY
		if( sc->bmaster )
		{
			box.x = j*fv->cbw+2;
			box.width = fv->cbw-2;
			box.y = i*fv->cbh+fv->lab_height+2;
			box.height = fv->cbw-1;

			if( sc->bmaster && sc->mmg_type == MG_TYPE_CHO )
			{
				GDrawFillRect(pixmap,&box,fv->b.selected[index] ? fvselcol : chomg_col );
			}
			else if( sc->bmaster && sc->mmg_type == MG_TYPE_JUNG )
			{
				GDrawFillRect(pixmap,&box,fv->b.selected[index] ? fvselcol : jumg_col );
			}
			else if( sc->bmaster && sc->mmg_type == MG_TYPE_JONG )
			{
				GDrawFillRect(pixmap,&box,fv->b.selected[index] ? fvselcol : jomg_col );
			}
			else
			{
				GDrawFillRect(pixmap,&box,fv->b.selected[index] ? fvselcol : mg_col );
			}
		}
#endif

		BDFChar *bdfc;

		if ( fv->show!=NULL && fv->show->piecemeal &&
			feat_gid!=-1 &&
			(feat_gid>=fv->show->glyphcnt || fv->show->glyphs[feat_gid]==NULL) &&
			fv->b.sf->glyphs[feat_gid]!=NULL )
		{
			BDFPieceMeal(fv->show,feat_gid);
		}

		if ( fv->show!=NULL && feat_gid!=-1 &&
			feat_gid < fv->show->glyphcnt &&
			fv->show->glyphs[feat_gid]==NULL &&
			SCWorthOutputting(fv->b.sf->glyphs[feat_gid]) )
		{
			/* If we have an outline but no bitmap for this slot */
			box.x = j*fv->cbw+1; box.width = fv->cbw-2;
			box.y = i*fv->cbh+fv->lab_height+2; box.height = box.width+1;
			GDrawDrawRect(pixmap,&box,0xff0000);
			++box.x; ++box.y; box.width -= 2; box.height -= 2;
			GDrawDrawRect(pixmap,&box,0xff0000);
			/* When reencoding a font we can find times where index>=show->charcnt */
		}
		else if ( fv->show!=NULL && feat_gid<fv->show->glyphcnt && feat_gid!=-1 && fv->show->glyphs[feat_gid]!=NULL )
		{
			/* If fontview is set to display an embedded bitmap font (not a temporary font, */
			/* rasterized specially for this purpose), then we can't use it directly, as bitmap */
			/* glyphs may contain selections and references. So create a temporary copy of */
			/* the glyph merging all such elements into a single bitmap */
			bdfc = fv->show->piecemeal ? fv->show->glyphs[feat_gid] : BDFGetMergedChar( fv->show->glyphs[feat_gid] );

			memset(&gi,'\0',sizeof(gi));
			memset(&base,'\0',sizeof(base));
			if ( bdfc->byte_data )
			{
				gi.u.image = &base;
				base.image_type = it_index;
				if ( !fv->b.selected[index] )
				{
					base.clut = fv->show->clut;
				}
				else
				{
					int bgr=((fvselcol>>16)&0xff), bgg=((fvselcol>>8)&0xff), bgb= (fvselcol&0xff);
					int fgr=((fvselfgcol>>16)&0xff), fgg=((fvselfgcol>>8)&0xff), fgb= (fvselfgcol&0xff);
					int i;
					memset(&clut,'\0',sizeof(clut));
					base.clut = &clut;
					clut.clut_len = fv->show->clut->clut_len;
					for ( i=0; i<clut.clut_len; ++i )
					{
						clut.clut[i] = COLOR_CREATE( bgr + (i*(fgr-bgr))/(clut.clut_len-1), bgg + (i*(fgg-bgg))/(clut.clut_len-1), bgb + (i*(fgb-bgb))/(clut.clut_len-1));
					}
				}
				GDrawSetDither(NULL, false);	/* on 8 bit displays we don't want any dithering */
			}
			else
			{
				memset(&clut,'\0',sizeof(clut));
				gi.u.image = &base;
				base.image_type = it_mono;
				base.clut = &clut;
				clut.clut_len = 2;
				clut.clut[0] = fv->b.selected[index] ? fvselcol : view_bgcol ;
				clut.clut[1] = fv->b.selected[index] ? fvselfgcol : 0 ;
			}
			base.trans = 0;
			base.clut->trans_index = 0;

			base.data = bdfc->bitmap;
			base.bytes_per_line = bdfc->bytes_per_line;
			base.width = bdfc->xmax-bdfc->xmin+1;
			base.height = bdfc->ymax-bdfc->ymin+1;
			box.x = j*fv->cbw; box.width = fv->cbw;
			box.y = i*fv->cbh+fv->lab_height+1; box.height = box.width+1;
			GDrawPushClip(pixmap,&box,&old2);
	    if ( !fv->b.sf->onlybitmaps && fv->show!=fv->filled &&
		    sc->layers[fv->b.active_layer].splines==NULL && sc->layers[fv->b.active_layer].refs==NULL &&
		    !sc->widthset &&
		    !(bdfc->xmax<=0 && bdfc->xmin==0 && bdfc->ymax<=0 && bdfc->ymax==0) )
	    {
		/* If we have a bitmap but no outline character... */
		GRect b;
		b.x = box.x+1; b.y = box.y+1; b.width = box.width-2; b.height = box.height-2;
		GDrawDrawRect(pixmap,&b,0x008000);
		++b.x; ++b.y; b.width -= 2; b.height -= 2;
		GDrawDrawRect(pixmap,&b,0x008000);
	    }
	    /* I assume that the bitmap image matches the bounding*/
	    /*  box. In some bitmap fonts the bitmap has white space on the*/
	    /*  right. This can throw off the centering algorithem */
	    if ( fv->magnify>1 )
	    {
		GDrawDrawImageMagnified(pixmap,&gi,NULL,
			j*fv->cbw+(fv->cbw-1-fv->magnify*base.width)/2,
			i*fv->cbh+fv->lab_height+1+fv->magnify*(fv->show->ascent-bdfc->ymax),
			fv->magnify*base.width,fv->magnify*base.height);
	    }
	    else if ( (GDrawHasCairo(pixmap)&gc_alpha) && base.image_type==it_index )
	    {
		GDrawDrawGlyph(pixmap,&gi,NULL,
			j*fv->cbw+(fv->cbw-1-base.width)/2,
			i*fv->cbh+fv->lab_height+1+fv->show->ascent-bdfc->ymax);
	    }
	    else
	    {
		GDrawDrawImage(pixmap,&gi,NULL,
			j*fv->cbw+(fv->cbw-1-base.width)/2,
			i*fv->cbh+fv->lab_height+1+fv->show->ascent-bdfc->ymax);
	    }
	    if ( fv->showhmetrics )
	    {
		int x1, x0 = j*fv->cbw+(fv->cbw-1-fv->magnify*base.width)/2- bdfc->xmin*fv->magnify;
		/* Draw advance width & horizontal origin */
		if ( fv->showhmetrics&fvm_origin )
		    GDrawDrawLine(pixmap,x0,i*fv->cbh+fv->lab_height+yorg-3,x0,
			    i*fv->cbh+fv->lab_height+yorg+2,METRICS_ORIGIN);
		x1 = x0 + fv->magnify*bdfc->width;
		if ( fv->showhmetrics&fvm_advanceat )
		    GDrawDrawLine(pixmap,x1,i*fv->cbh+fv->lab_height+1,x1,
			    (i+1)*fv->cbh-1,METRICS_ADVANCE);
		if ( fv->showhmetrics&fvm_advanceto )
		    GDrawDrawLine(pixmap,x0,(i+1)*fv->cbh-2,x1,
			    (i+1)*fv->cbh-2,METRICS_ADVANCE);
	    }
	    if ( fv->showvmetrics )
	    {
		int x0 = j*fv->cbw+(fv->cbw-1-fv->magnify*base.width)/2- bdfc->xmin*fv->magnify
			+ fv->magnify*fv->show->pixelsize/2;
		int y0 = i*fv->cbh+fv->lab_height+yorg;
		int yvw = y0 + fv->magnify*sc->vwidth*fv->show->pixelsize/em;
		if ( fv->showvmetrics&fvm_baseline )
		    GDrawDrawLine(pixmap,x0,i*fv->cbh+fv->lab_height+1,x0,
			    (i+1)*fv->cbh-1,METRICS_BASELINE);
		if ( fv->showvmetrics&fvm_advanceat )
		    GDrawDrawLine(pixmap,j*fv->cbw,yvw,(j+1)*fv->cbw,
			    yvw,METRICS_ADVANCE);
		if ( fv->showvmetrics&fvm_advanceto )
		    GDrawDrawLine(pixmap,j*fv->cbw+2,y0,j*fv->cbw+2,
			    yvw,METRICS_ADVANCE);
		if ( fv->showvmetrics&fvm_origin )
		    GDrawDrawLine(pixmap,x0-3,i*fv->cbh+fv->lab_height+yorg,x0+2,i*fv->cbh+fv->lab_height+yorg,METRICS_ORIGIN);
	    }
	    GDrawPopClip(pixmap,&old2);
	    if ( !fv->show->piecemeal ) BDFCharFree( bdfc );
	}
    }
}

static void FVToggleCharSelected(FontView *fv,int enc) {
    int i, j;

    if ( fv->v==NULL || fv->colcnt==0 )	/* Can happen in scripts */
return;

    i = enc / fv->colcnt;
    j = enc - i*fv->colcnt;
    i -= fv->rowoff;
 /* Normally we should be checking against fv->rowcnt (rather than <=rowcnt) */
 /*  but every now and then the WM forces us to use a window size which doesn't */
 /*  fit our expectations (maximized view) and we must be prepared for half */
 /*  lines */
    if ( i>=0 && i<=fv->rowcnt )
	FVDrawGlyph(fv->v,fv,enc,true);
}

static void FontViewRefreshAll(SplineFont *sf) {
    FontView *fv;
    for ( fv = (FontView *) (sf->fv); fv!=NULL; fv = (FontView *) (fv->b.nextsame) )
	if ( fv->v!=NULL )
	    GDrawRequestExpose(fv->v,NULL,false);
}

void FVDeselectAll(FontView *fv) {
    int i;

    for ( i=0; i<fv->b.map->enccount; ++i ) {
	if ( fv->b.selected[i] ) {
	    fv->b.selected[i] = false;
	    FVToggleCharSelected(fv,i);
	}
    }
    fv->sel_index = 0;
}

static void FVInvertSelection(FontView *fv) {
    int i;

    for ( i=0; i<fv->b.map->enccount; ++i ) {
	fv->b.selected[i] = !fv->b.selected[i];
	FVToggleCharSelected(fv,i);
    }
    fv->sel_index = 1;
}

static void FVSelectAll(FontView *fv) {
    int i;

    for ( i=0; i<fv->b.map->enccount; ++i ) {
	if ( !fv->b.selected[i] ) {
	    fv->b.selected[i] = true;
	    FVToggleCharSelected(fv,i);
	}
    }
    fv->sel_index = 1;
}

static void FVReselect(FontView *fv, int newpos) {
    int i;

    if ( newpos<0 ) newpos = 0;
    else if ( newpos>=fv->b.map->enccount ) newpos = fv->b.map->enccount-1;

    if ( fv->pressed_pos<fv->end_pos ) {
	if ( newpos>fv->end_pos ) {
	    for ( i=fv->end_pos+1; i<=newpos; ++i ) if ( !fv->b.selected[i] ) {
		fv->b.selected[i] = fv->sel_index;
		FVToggleCharSelected(fv,i);
	    }
	} else if ( newpos<fv->pressed_pos ) {
	    for ( i=fv->end_pos; i>fv->pressed_pos; --i ) if ( fv->b.selected[i] ) {
		fv->b.selected[i] = false;
		FVToggleCharSelected(fv,i);
	    }
	    for ( i=fv->pressed_pos-1; i>=newpos; --i ) if ( !fv->b.selected[i] ) {
		fv->b.selected[i] = fv->sel_index;
		FVToggleCharSelected(fv,i);
	    }
	} else {
	    for ( i=fv->end_pos; i>newpos; --i ) if ( fv->b.selected[i] ) {
		fv->b.selected[i] = false;
		FVToggleCharSelected(fv,i);
	    }
	}
    } else {
	if ( newpos<fv->end_pos ) {
	    for ( i=fv->end_pos-1; i>=newpos; --i ) if ( !fv->b.selected[i] ) {
		fv->b.selected[i] = fv->sel_index;
		FVToggleCharSelected(fv,i);
	    }
	} else if ( newpos>fv->pressed_pos ) {
	    for ( i=fv->end_pos; i<fv->pressed_pos; ++i ) if ( fv->b.selected[i] ) {
		fv->b.selected[i] = false;
		FVToggleCharSelected(fv,i);
	    }
	    for ( i=fv->pressed_pos+1; i<=newpos; ++i ) if ( !fv->b.selected[i] ) {
		fv->b.selected[i] = fv->sel_index;
		FVToggleCharSelected(fv,i);
	    }
	} else {
	    for ( i=fv->end_pos; i<newpos; ++i ) if ( fv->b.selected[i] ) {
		fv->b.selected[i] = false;
		FVToggleCharSelected(fv,i);
	    }
	}
    }
    fv->end_pos = newpos;
    if ( newpos>=0 && newpos<fv->b.map->enccount && (i = fv->b.map->map[newpos])!=-1 &&
	    fv->b.sf->glyphs[i]!=NULL &&
	    fv->b.sf->glyphs[i]->unicodeenc>=0 && fv->b.sf->glyphs[i]->unicodeenc<0x10000 )
	GInsCharSetChar(fv->b.sf->glyphs[i]->unicodeenc);
}

static void FVFlattenAllBitmapSelections(FontView *fv)
{
	BDFFont *bdf;
	int i;

	for ( bdf = fv->b.sf->bitmaps; bdf!=NULL; bdf=bdf->next )
	{
		for ( i=0; i<bdf->glyphcnt; ++i )
		{
			if ( bdf->glyphs[i]!=NULL && bdf->glyphs[i]->selection!=NULL )
			{
				BCFlattenFloat(bdf->glyphs[i]);
			}
		}
	}
}

static int AskChanged(SplineFont *sf)
{
	int ret;
	char *buts[4];
	char *filename, *fontname;

	if ( sf->cidmaster!=NULL )
	{
		sf = sf->cidmaster;
	}

	filename = sf->filename;
	fontname = sf->fontname;

	if ( filename==NULL && sf->origname!=NULL && sf->onlybitmaps && sf->bitmaps!=NULL && sf->bitmaps->next==NULL )
	{
		filename = sf->origname;
	}
	if ( filename==NULL )
	{
		filename = "untitled.sfd";
	}
	filename = GFileNameTail(filename);
	buts[0] = _("_Save");
	buts[1] = _("_Don't Save");
	buts[2] = _("_Cancel");
	buts[3] = NULL;
	ret = gwwv_ask( _("Font changed"),(const char **) buts,0,2,_("Font %1$.40s in file %2$.40s has been changed.\nDo you want to save it?"),fontname,filename);
	return( ret );
}

int _FVMenuGenerate( FontView *fv, int family )
{
	FVFlattenAllBitmapSelections(fv);
	return( SFGenerateFont(fv->b.sf, fv->b.active_layer, family, fv->b.normal == NULL ? fv->b.map : fv->b.normal) );
}

static void FVMenuGenerate(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);

#ifdef CKS	// HYMODIFY :: 2016.02.05
	int ret = _FVMenuGenerate(fv, gf_none);

	if( ret && fv->b.sf->registerfont )
	{
		usleep(1000);
		HY_DoRegisterFontDlg( fv->b.sf );
	}
#else
	_FVMenuGenerate(fv, gf_none);
#endif
}

static void FVMenuGenerateFamily(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    _FVMenuGenerate(fv,gf_macfamily);
}

static void FVMenuGenerateTTC(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    _FVMenuGenerate(fv,gf_ttc);
}

extern int save_to_dir;

static int SaveAs_FormatChange( GGadget *g, GEvent *e )
{
	if ( e->type==et_controlevent && e->u.control.subtype == et_radiochanged )
	{
		GGadget *fc = GWidgetGetControl(GGadgetGetWindow(g),1000);
		char *oldname = GGadgetGetTitle8(fc);
		int *_s2d = GGadgetGetUserData(g);
		int s2d = GGadgetIsChecked(g);
		char *pt, *newname = malloc(strlen(oldname)+8);
		strcpy(newname,oldname);
		pt = strrchr(newname,'.');
		if ( pt==NULL )
		{
			pt = newname+strlen(newname);
		}
		strcpy(pt,s2d ? ".sfdir" : ".sfd" );
		GGadgetSetTitle8(fc,newname);
		save_to_dir = *_s2d = s2d;
		SavePrefs(true);
	}
	return( true );
}


static enum fchooserret _FVSaveAsFilterFunc( GGadget *g, struct gdirentry *ent, const unichar_t *dir )
{
#ifdef CKS	// HYMODIFY :: 2016.01.19
	if( uc_strcmp(ent->mimetype, "application/vnd.font-fontforge-sfd") == 0 )
	{
		return( fc_show );
	}

	if( ent->isdir )
	{
		return fc_show;
	}
	return fc_hide;
#else
	char *n = u_to_c(ent->name);
	int ew = endswithi( n, "sfd" ) || endswithi( n, "sfdir" );

	if( ew )
	{
		return fc_show;
	}

	if( ent->isdir )
	{
		return fc_show;
	}
	return fc_hide;
#endif
}

int _FVMenuSaveAs(FontView *fv)
{
	char *temp;
	char *ret;
	char *filename;
	int ok;
	int s2d = fv->b.cidmaster != NULL ? fv->b.cidmaster->save_to_dir : fv->b.sf->mm != NULL ? fv->b.sf->mm->normal->save_to_dir : fv->b.sf->save_to_dir;
	GGadgetCreateData gcd;
	GTextInfo label;

	if ( fv->b.cidmaster!=NULL && fv->b.cidmaster->filename!=NULL )
	{
		temp=def2utf8_copy(fv->b.cidmaster->filename);
	}
	else if ( fv->b.sf->mm!=NULL && fv->b.sf->mm->normal->filename!=NULL )
	{
		temp=def2utf8_copy(fv->b.sf->mm->normal->filename);
	}
	else if ( fv->b.sf->filename!=NULL )
	{
		temp=def2utf8_copy(fv->b.sf->filename);
	}
	else
	{
		SplineFont *sf = fv->b.cidmaster?fv->b.cidmaster: fv->b.sf->mm!=NULL?fv->b.sf->mm->normal:fv->b.sf;
		char *fn = sf->defbasefilename ? sf->defbasefilename : sf->fontname;
		temp = malloc((strlen(fn)+10));
		strcpy(temp,fn);
		if ( sf->defbasefilename!=NULL )
		{
			/* Don't add a default suffix, they've already told us what name to use *///;
		}
		else if ( fv->b.cidmaster!=NULL )
		{
			strcat(temp,"CID");
		}
		else if ( sf->mm==NULL )
		{
			//;
		}
		else if ( sf->mm->apple )
		{
			strcat(temp,"Var");
		}
		else
		{
			strcat(temp,"MM");
		}
		strcat(temp,save_to_dir ? ".sfdir" : ".sfd");
		s2d = save_to_dir;
	}

	memset(&gcd,0,sizeof(gcd));
	memset(&label,0,sizeof(label));
	gcd.gd.flags = s2d ? (gg_visible | gg_enabled | gg_cb_on) : (gg_visible | gg_enabled);
	label.text = (unichar_t *) _("Save as _Directory");
	label.text_is_1byte = true;
	label.text_in_resource = true;
	gcd.gd.label = &label;
	gcd.gd.handle_controlevent = SaveAs_FormatChange;
	gcd.data = &s2d;
	gcd.creator = GCheckBoxCreate;

	GFileChooserInputFilenameFuncType FilenameFunc = GFileChooserDefInputFilenameFunc;

#if defined(__MINGW32__)
	//
	// If they are "saving as" but there is no path, lets help
	// the poor user by starting someplace sane rather than in `pwd`
	//
	if( !GFileIsAbsolute(temp) )
	{
		char* defaultSaveDir = GFileGetHomeDocumentsDir();
		printf("save-as:%s\n", temp );
		char* temp2 = GFileAppendFile( defaultSaveDir, temp, 0 );
		free(temp);
		temp = temp2;
	}
#endif

#ifdef CKS	// HYMODIFY :: 2016.01.12
	GFileRemoveExtension(temp);
#endif

	ret = GWidgetSaveAsFileWithGadget8(_("Save as..."), temp, 0, NULL, _FVSaveAsFilterFunc, FilenameFunc, &gcd );
	free(temp);
	if ( ret==NULL )
	{
		return( 0 );
	}
	filename = utf82def_copy(ret);
	free(ret);

	if( !(endswithi( filename, ".sfdir") || endswithi( filename, ".sfd")) )
	{
		// they forgot the extension, so we force the default of .sfd
		// and alert them to the fact that we have done this and we
		// are not saving to a OTF, TTF, UFO formatted file
		char* extension = ".sfd";
		char* newpath = copyn( filename, strlen(filename) + strlen(".sfd") + 1 );
		strcat( newpath, ".sfd" );
		char* oldfn = GFileNameTail( filename );
		char* newfn = GFileNameTail( newpath );
		LogError( _("You tried to save with the filename %s but it was saved as %s. "), oldfn, newfn );
		LogError( _("Please choose File/Generate Fonts to save to other formats."));
		free(filename);
		filename = newpath;
	}

#ifdef CKS	// HYMODIFY :: 2016.01.12
	if( !s2d )
	{
		char *p = NULL;
		p = strstr( filename, ".sfd.sfd" );
		if( p != NULL )
		{
			*(p + 4) = '\0';
		}
	}
#endif

	FVFlattenAllBitmapSelections(fv);
	fv->b.sf->compression = 0;
	ok = SFDWrite(filename,fv->b.sf,fv->b.map,fv->b.normal,s2d);
	if ( ok )
	{
		SplineFont *sf = fv->b.cidmaster?fv->b.cidmaster:fv->b.sf->mm!=NULL?fv->b.sf->mm->normal:fv->b.sf;
		free(sf->filename);
		sf->filename = filename;
		sf->save_to_dir = s2d;
		free(sf->origname);
		sf->origname = copy(filename);
		sf->new = false;
		if ( sf->mm!=NULL )
		{
			int i;
			for ( i=0; i<sf->mm->instance_count; ++i )
			{
				free(sf->mm->instances[i]->filename);
				sf->mm->instances[i]->filename = filename;
				free(sf->mm->instances[i]->origname);
				sf->mm->instances[i]->origname = copy(filename);
				sf->mm->instances[i]->new = false;
			}
		}
		SplineFontSetUnChanged(sf);
		FVSetTitles(fv->b.sf);
	}
	else
	{
		free(filename);
	}
	return( ok );
}

static void FVMenuSaveAs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);

	_FVMenuSaveAs(fv);
}

static int IsBackupName(char *filename) {

    if ( filename==NULL )
return( false );
return( filename[strlen(filename)-1]=='~' );
}

#ifdef HYMODIFY
static void HY_MakeJohapEncoding( FontView * fv, SplineFont * sf )
{
	Encoding *	enc		= NULL;
	int			oldcnt	= fv->b.map->enccount;

	enc = FindOrMakeEncoding( "UnicodeBmp" );
	if( enc == NULL )
	{
		IError("Known encoding could not be found");
		return;
	}

	SFForceEncoding( fv->b.sf, fv->b.map, enc );
	if( oldcnt < fv->b.map->enccount )
	{
		fv->b.selected = realloc( fv->b.selected, fv->b.map->enccount );
		memset( fv->b.selected + oldcnt, 0, fv->b.map->enccount - oldcnt );
	}
	if( fv->b.normal != NULL )
	{
		EncMapFree( fv->b.normal );
		fv->b.normal = NULL;
	}
	SFReplaceEncodingBDFProps( fv->b.sf, fv->b.map );
	FontViewReformatOne( &fv->b );
}

static void HY_KoreanBuild_Import( SplineFont * into )
{
	JohapRule *johap = into->johap;
	SplineChar *nsc = NULL;
	SplineChar *jasosc = NULL;
	int uni = 0;
	int glyfindex = 0, jasoindex = 0;
	int cgroup = 0, jugroup = 0, jogroup = 0;
	int cho, jung, jong;

	ff_progress_start_indicator( 10 , _("Import JohapRule"),_("Building Korean...") , 0 , 11172 , 1 );
	for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
	{
		for( jung = 0 ; jung < NUM_JASO_MV ; jung++ )
		{
			for( jong = 0 ; jong < NUM_JASO_LC ; jong++ )
			{
				uni = 0xac00 + ( cho * NUM_JASO_MV + jung ) * NUM_JASO_LC + jong;
				nsc = SFMakeChar( into, into->map, uni );


				//-----------------------------------------------------------------------------------------
				//-----------------------------------------------------------------------------------------
				glyfindex = 0;
				jasoindex = 0;
				for( jasoindex = 0 ; jasoindex < cho ; jasoindex++ )
				{
					glyfindex += johap->rule[0]->vcode_NumOfGlyf[jasoindex];
				}

				cgroup = johap->rule[0]->grouplist[0]->JasoGroupMap[cho];
				jugroup = johap->rule[0]->grouplist[1]->JasoGroupMap[jung];
				jogroup = johap->rule[0]->grouplist[2]->JasoGroupMap[jong];
				glyfindex += johap->rule[0]->GlyfIndexMap[cgroup][jugroup][jogroup];

				HY_SCAddKoreanRef( nsc, into->glyphs[glyfindex] );


				//-----------------------------------------------------------------------------------------
				//-----------------------------------------------------------------------------------------
				glyfindex = 0;
				jasoindex = 0;
				for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
				{
					glyfindex += johap->rule[0]->vcode_NumOfGlyf[jasoindex];
				}
				for( jasoindex = 0 ; jasoindex < jung ; jasoindex++ )
				{
					glyfindex += johap->rule[1]->vcode_NumOfGlyf[jasoindex];
				}

				cgroup = johap->rule[1]->grouplist[0]->JasoGroupMap[cho];
				jugroup = johap->rule[1]->grouplist[1]->JasoGroupMap[jung];
				jogroup = johap->rule[1]->grouplist[2]->JasoGroupMap[jong];
				glyfindex += johap->rule[1]->GlyfIndexMap[cgroup][jugroup][jogroup];

				HY_SCAddKoreanRef( nsc, into->glyphs[glyfindex] );


				//-----------------------------------------------------------------------------------------
				//-----------------------------------------------------------------------------------------
				if( jong > 0)
				{
					glyfindex = 0;
					jasoindex = 0;
					for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
					{
						glyfindex += johap->rule[0]->vcode_NumOfGlyf[jasoindex];
					}
					for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
					{
						glyfindex += johap->rule[1]->vcode_NumOfGlyf[jasoindex];
					}
					for( jasoindex = 0 ; jasoindex < jong ; jasoindex++ )
					{
						glyfindex += johap->rule[2]->vcode_NumOfGlyf[jasoindex];
					}

					cgroup = johap->rule[2]->grouplist[0]->JasoGroupMap[cho];
					jugroup = johap->rule[2]->grouplist[1]->JasoGroupMap[jung];
					jogroup = johap->rule[2]->grouplist[2]->JasoGroupMap[jong];
					glyfindex += johap->rule[2]->GlyfIndexMap[cgroup][jugroup][jogroup];

					HY_SCAddKoreanRef( nsc, into->glyphs[glyfindex] );
				}

				if ( !ff_progress_next())
				{
					break;
				}
			}
		}
	}
	ff_progress_end_indicator();
}

static struct ttflangname *TTFLangNamesCopy( struct ttflangname *old )
{
	struct ttflangname *base = NULL, *last, *cur;
	int i;

	while( old != NULL )
	{
		cur = calloc( 1, sizeof( struct ttflangname ) );
		cur->lang = old->lang;

		for( i = 0 ; i < ttf_namemax ; ++i )
		{
			cur->names[i] = copy( old->names[i] );
		}

		if( base )
		{
			last->next = cur;
		}
		else
		{
			base = cur;
		}
		last = cur;
		old = old->next;
	}
	return( base );
}

static void _HY_ImportJohapRules( FontView * fv, SplineFont * from )
{
	SplineFont * into = fv->b.sf;
	JohapRule * johap = from->johap;
	int i;

	// Copy Johap Rule
	into->johap	= from->johap;

	// Copy SplineFont arguments
	into->weight			= copy( from->weight );
	into->copyright		= copy( from->copyright );
	into->xuid			= copy( from->xuid );
	into->version			= copy( from->version );
	into->top_enc			= em_unicode;
	into->display_size		= from->display_size;
	into->display_antialias	= from->display_antialias;
	into->names			= TTFLangNamesCopy( from->names );
	into->private			= PSDictCopy( from->private );
	into->layers			= from->layers;

	// Replace origname
	char *pt = 0;
	pt = GFileNameTail( into->origname );

	char *buf;

	into->origname		= copy( buf );

	for( i = 0 ; i < from->glyphcnt ; i++ )
	{
		into->map->map[65536 + i] = i;
		into->map->backmap[into->map->map[65536 + i]] = 65536 + i;

		into->glyphs[i]					= SFSplineCharCreate( into );
		into->glyphs[i]					= SplineCharCopy( from->glyphs[i], into, NULL );
		into->glyphs[i]->parent			= into;
		into->glyphs[i]->name				= copy( from->glyphs[i]->name );
		into->glyphs[i]->views				= from->glyphs[i]->views;
		into->glyphs[i]->unicodeenc			= -1;
		into->glyphs[i]->orig_pos			= into->map->map[65536 + i];
		into->glyphs[i]->bCompositionUnit	= 1;
		into->glyphs[i]->VCode			= from->glyphs[i]->VCode;
		into->glyphs[i]->Varient			= from->glyphs[i]->Varient;
		into->glyphs[i]->widthset			= 1;
		into->glyphs[i]->width				= into->ascent + into->descent;
		into->glyphs[i]->vwidth			= into->ascent + into->descent;
		//into->glyphs[i]->width			= from->glyphs[i]->width;
		//into->glyphs[i]->vwidth			= from->glyphs[i]->vwidth;
		into->glyphs[i]->color				= from->glyphs[i]->color;

		SFHashGlyph( into, into->glyphs[i] );

		into->glyphcnt++;
	}
	into->glyphmax = into->glyphcnt + 10;
	FontViewReformatAll( into );

	HY_KoreanBuild_Import( into );
}

static SplineFont * HY_ImportJohapRules( FontView * fv, SplineFont * from )
{
	SplineFont *into = (SplineFont *)fv->b.sf;

	FVAddUnencoded( (FontViewBase *)fv, from->glyphcnt );
	_HY_ImportJohapRules( fv, from );

	return into;
}

static void FVMenuHYImportRuleJohap(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = (SplineFont *) fv->b.sf;
	SplineFont *imsf;
	FontView *test;
	char *temp;
	char *eod;
	char *fpt;
	char *file;
	char *full;
	int fvcnt;
	int fvtest;

	if( sf->hasRule )
	{
		ff_post_error( _("Error"), _("SplineFont already has ruletable...") );
		return;
	}

	for(fvcnt = 0, test = fv_list; test != NULL; ++fvcnt, test=(FontView *) (test->b.next));
		
	temp = GetPostScriptFontName(NULL, true);
	if(temp == NULL)
	{
		return;
	}

	imsf = calloc(1, sizeof(SplineFont));

	eod = strrchr(temp, '/');
	*eod = '\0';
	file = eod + 1;
	do
	{
		fpt = strstr(file, "; ");
		if(fpt != NULL)
		{
			*fpt = '\0';
		}
		full = malloc(strlen(temp) + 1 + strlen(file) + 1);
		strcpy(full, temp);
		strcat(full, "/");
		strcat(full, file);

		imsf = LoadSplineFont(full, 0);
		if(imsf->hasRule)
		{
			sf->hasRule = true;
			fv->b.sf = HY_ImportJohapRules(fv, imsf);
		}
		else
		{
			ff_post_error( _("Error"), _("File has not Rule Table") );
			sf->hasRule = false;
			free(full);
			free(temp);
			return;
		}
		file = fpt + 2;
		free(full);
	}while (fpt != NULL);
	free(temp);

	default_fv_font_size = fv->filled->pixelsize = 48;

	for(fvtest = 0, test = fv_list; test != NULL; ++fvtest, test = (FontView *) (test->b.next));
}

#define CID_MainTabSet					1001
#define CID_SubTabSet					1002
#define CID_BtnJoHapIndexEdit			1003
#define CID_BtnRtfEditOK					1004
#define CID_JasoWidthEditOpt				1005
#define CID_BtnRtfEditCancel				1006
#define CID_GroupView					1007
#define CID_BtnExportToRTF				1008
#define CID_BtnExportToHRT				1009
#define CID_LabelJohapIndexOption		1010
#define CID_RadioJohapIndexNew			1011
#define CID_RadioJohapIndexCopy			1012
#define CID_CheckNoneFixedHMetric		1013
#define CID_CheckThreePairMethod		1014
#define CID_GroupViewSB					1015
#define CID_OldKorJasoView				1016
#define CID_LabelImportUserID			1217
#define CID_TextFieldImportUserID		1218
#define CID_LabelImportPasswd			1219
#define CID_TextFieldImportPasswd		1220
#define CID_BtnImportAutoOK				1221
#define CID_BtnImportAuthCancel			1222
#define CID_LabelRegUserID				1223
#define CID_TextFieldRegUserID			1224
#define CID_LabelRegPasswd				1225
#define CID_TextFieldRegPasswd			1226
#define CID_BtnRegAutoOK				1227
#define CID_BtnRegAuthCancel			1228

#define MAX_FILE_PATH_LEN				1024

#define GVIEW_LEFT_BORDER				10
#define GVIEW_TOP_BORDER				10
#define GVIEW_JASO_SPACE_GAP			2
#define PPEM_JASO_IMG					24
#define XSPACE_JASO_IMG				( PPEM_JASO_IMG + GVIEW_JASO_SPACE_GAP )
#define YSPACE_JASO_IMG				( PPEM_JASO_IMG + GVIEW_JASO_SPACE_GAP )

#define JOHAP_INDEX_MAX_COL			26
#define JOHAP_INDEX_MAX_ROW			26
#define JOHAP_INDEX_LEFT_BORDER		0//10
#define JOHAP_INDEX_TOP_BORDER		0//5//10

#define JOHAP_INDEX_GAP				5//10

#define CELLWIDTH						25//30
#define CELLHEIGHT						25//30

#define COLOR_UNKNOWN					((Color) 0xffffffff)
#define COLOR_TRANSPARENT				((Color) 0xffffffff)
#define COLOR_DEFAULT					((Color) 0xfffffffe)

static Color bgColor			= 0xffffff;
static Color fgColor			= 0x000000;
static Color johapindexfgColor	= 0x000000;

static int	hScrollBarHeight = 13;
static int	vScrollBarWidth = 13;
static int s_widthmapdlg = 0;
	
static int ChoSungIdx[19] = 
{
	0, 1, 3, 6, 7, 8,16,17,18,20,
	21,22,23,24,25,26,27,28,29
};

static int JungSungIdx[21] = 
{
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
	10, 11,12,13,14,15,16,17,18,19,
	20
};

static int JongSungIdx[27] =
{
	0, 1, 2, 3, 4, 5,    6, 8, 9,
	10,11,12,13,14,15,16,17,   19,
	20,21,22,23,25,26,27,28,29
};

// Jaso Images
static char * res_jaso[3][28] =
{
	{
		"F1.BMP", "F2.BMP", "F3.BMP", "F4.BMP", "F5.BMP", "F6.BMP", "F7.BMP", "F8.BMP", "F9.BMP", "F10.BMP",
		"F11.BMP", "F12.BMP", "F13.BMP", "F14.BMP", "F15.BMP", "F16.BMP", "F17.BMP", "F18.BMP", "F19.BMP", NULL,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
	},

	{
		"M1.BMP", "M2.BMP", "M3.BMP", "M4.BMP", "M5.BMP", "M6.BMP", "M7.BMP", "M8.BMP", "M9.BMP", "M10.BMP",
		"M11.BMP", "M12.BMP", "M13.BMP", "M14.BMP", "M15.BMP", "M16.BMP", "M17.BMP", "M18.BMP", "M19.BMP", "M20.BMP",
		"M21.BMP", NULL, NULL, NULL, NULL, NULL, NULL, NULL
	},

	{
		"L0.BMP", "L1.BMP", "L2.BMP", "L3.BMP", "L4.BMP", "L5.BMP", "L6.BMP", "L7.BMP", "L8.BMP", "L9.BMP",
		"L10.BMP", "L11.BMP", "L12.BMP", "L13.BMP", "L14.BMP", "L15.BMP", "L16.BMP", "L17.BMP", "L18.BMP", "L19.BMP",
		"L20.BMP", "L21.BMP", "L22.BMP", "L23.BMP", "L24.BMP", "L25.BMP", "L26.BMP", "L27.BMP"
	}
};


SplineChar * HY_SCMakeKoreanComponent( SplineFont * sf, int vcode, int varient, int enc )
{
	char			buffer[20];
	int			uni, i;

	SplineChar *	sc			= chunkalloc( sizeof( SplineChar ) );
	sc->color				= COLOR_DEFAULT;
	sc->unicodeenc			= -1;
	sc->layer_cnt				= sf->layer_cnt;
	sc->layers				= calloc( sf->layer_cnt, sizeof( Layer ) );

	for( i = 0 ; i < sc->layer_cnt ; i++ )
	{
		LayerDefault( &sc->layers[i] );
	}
	sc->tex_height = sc->tex_depth = sc->italic_correction = sc->top_accent_horiz = TEX_UNDEF;
	sc->bCompositionUnit = true;
	sc->VCode	= vcode;
	sc->Varient	= varient;
	sc->width	= sf->ascent + sf->descent;
	sc->vwidth	= sf->ascent + sf->descent;
	sc->widthset	= true;
	for( i = 0 ; i < sf->layer_cnt ; ++i )
	{
	    sc->layers[i].background	= sf->layers[i].background;
	    sc->layers[i].order2		= sf->layers[i].order2;
	}
	sc->parent = sf;

	if( sc->VCode < NUM_JASO_FC )
	{
		uni = 0x1100 + vcode;
	}
	else if( sc->VCode < NUM_JASO_FC + NUM_JASO_MV )
	{
		uni = 0x1161 + ( vcode - NUM_JASO_FC );
	}
	else
	{
		uni = 0x11a8 + ( vcode - ( NUM_JASO_FC + NUM_JASO_MV + 1 ) );
	}
	sprintf( buffer, "jamo%4x-%d", uni, varient );
	sc->name		= copy( buffer );
	sc->parent		= sf;
	return( sc );
}

SplineChar * HY_SCFindOrMakeKoreanComponent( SplineFont * sf, int vcode, int varient, int enc, SplineChar ** oldglyphs, int oldglyphcnt )
{
	SplineChar * sc;
	int i;
	int index;

	for( i = 0 ; i < oldglyphcnt ; i++ )
	{
		index = 11172 + i;
		if( oldglyphs[i] != NULL )
		{
			if( oldglyphs[i]->VCode == vcode && oldglyphs[i]->Varient == varient )
			{
				sc							= SplineCharCopy( oldglyphs[i], NULL, NULL );
				sc->unicodeenc				= -1;
				sc->VCode					= oldglyphs[i]->VCode;
				sc->Varient					= oldglyphs[i]->Varient;
				sc->bCompositionUnit			= 1;
				sc->widthset					= 1;
				sc->width					= oldglyphs[i]->width;
				sc->vwidth					= oldglyphs[i]->vwidth;
				sc->color					= oldglyphs[i]->color;
				return( sc );
			}
		}
	}
	return( HY_SCMakeKoreanComponent( sf, vcode, varient, enc ) );
}

int HY_GetMaxJohapIndexValue( JohapRule * johap, int ruletype, int groupindex )
{
	RULE * rule;
	int nog_fc, nog_mv, nog_lc;
	int fc, mv, lc;
	int last_gi = 0;

	switch( ruletype )
	{
		case RULE_FC:
		{
			rule		= johap->rule[ruletype];
			nog_mv	= rule->grouplist[JASO_MV]->nGroups;
			nog_lc	= rule->grouplist[JASO_LC]->nGroups;
			for( lc = 0 ; lc < nog_lc ; lc++ )
			{
				for( mv = 0 ; mv < nog_mv ; mv++ )
				{
					last_gi = ( last_gi >= rule->GlyfIndexMap[groupindex][mv][lc] ) ? last_gi : rule->GlyfIndexMap[groupindex][mv][lc];
				}
			}
		}
		break;
		case RULE_MV:
		{
			rule		= johap->rule[ruletype];
			nog_fc	= rule->grouplist[JASO_FC]->nGroups;
			nog_lc	= rule->grouplist[JASO_LC]->nGroups;
			for( lc = 0 ; lc < nog_lc ; lc++ )
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					last_gi = ( last_gi >= rule->GlyfIndexMap[fc][groupindex][lc] ) ? last_gi : rule->GlyfIndexMap[fc][groupindex][lc];
				}
			}
		}
		break;
		case RULE_LC:
		{
			rule		= johap->rule[ruletype];
			nog_fc	= rule->grouplist[JASO_FC]->nGroups;
			nog_mv	= rule->grouplist[JASO_MV]->nGroups;
			for( fc = 0 ; fc < nog_fc ; fc++ )
			{
				for( mv = 0 ; mv < nog_mv ; mv++ )
				{
					last_gi = ( last_gi >= rule->GlyfIndexMap[fc][mv][groupindex] ) ? last_gi : rule->GlyfIndexMap[fc][mv][groupindex];
				}
			}
		}
		break;
	}
	return( last_gi );
}

void HY_RulesResetVarientCnts( JohapRule * johap )
{
	int nog_fc, fc;
	int nog_mv, mv;
	int nog_lc, lc;
	int last_gi, maxval, ruletype, j, k;
	RULE * rule;
	JasoGroupList * grouplist;

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE ; ruletype++ )
	{
		rule		= johap->rule[ruletype];
		nog_fc	= rule->grouplist[JASO_FC]->nGroups;
		nog_mv	= rule->grouplist[JASO_MV]->nGroups;
		nog_lc	= rule->grouplist[JASO_LC]->nGroups;
		maxval	= 0;
		switch( ruletype )
		{
			case RULE_FC:
			{
				grouplist = rule->grouplist[JASO_FC];
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					last_gi = 0;
					maxval = HY_GetMaxJohapIndexValue( johap, ruletype, fc );
					for( lc = 0 ; lc < nog_lc ; lc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							if( maxval < rule->old_maxval[fc] )
							{
								last_gi = rule->old_maxval[fc];
							}
							else
							{
								last_gi = ( last_gi >= rule->GlyfIndexMap[fc][mv][lc] ) ? last_gi : rule->GlyfIndexMap[fc][mv][lc];
							}
						}
					}

					for( k = 0 ; k < NUM_JASO_FC ; k++ )
					{
						if( grouplist->JasoGroupMap[k] == fc )
						{
							rule->vcode_NumOfGlyf[k] = last_gi + 1; 
						}
					}
				}
			}
			break;
			case RULE_MV:
			{
				grouplist = rule->grouplist[JASO_MV];
				for( mv = 0 ; mv < nog_mv ; ++mv )
				{
					last_gi = 0;
					maxval = HY_GetMaxJohapIndexValue( johap, ruletype, mv );
					for( lc = 0 ; lc < nog_lc ; ++lc )
					{
						for( fc = 0 ; fc < nog_fc ; ++fc )
						{
							if( maxval < rule->old_maxval[mv] )
							{
								last_gi = rule->old_maxval[mv];
							}
							else
							{
								last_gi = ( last_gi >= rule->GlyfIndexMap[fc][mv][lc] ) ? last_gi : rule->GlyfIndexMap[fc][mv][lc];
							}
						}
					}

					for( k = 0 ; k < NUM_JASO_MV ; k++ )
					{
						if( grouplist->JasoGroupMap[k] == mv )
						{
							rule->vcode_NumOfGlyf[k] = last_gi + 1;
						}
					}
				}
			}
			break;
			case RULE_LC:
			{
				grouplist = rule->grouplist[JASO_LC];
				for( lc = 0 ; lc < nog_lc ; ++lc )
				{
					last_gi = 0;
					maxval = HY_GetMaxJohapIndexValue( johap, ruletype, lc );
					for( fc = 0 ; fc < nog_fc ; ++fc )
					{
						for( mv = 0 ; mv < nog_mv ; ++mv )
						{
							if( maxval < rule->old_maxval[lc] )
							{
								last_gi = rule->old_maxval[lc];
							}
							else
							{
								last_gi = ( last_gi >= rule->GlyfIndexMap[fc][mv][lc] ) ? last_gi : rule->GlyfIndexMap[fc][mv][lc];
							}
						}
					}

					for( k = 0 ; k < NUM_JASO_LC ; k++ )
					{
						if( grouplist->JasoGroupMap[k] == lc )
						{
							rule->vcode_NumOfGlyf[k] = last_gi + 1;
						}
					}
				}
				rule->vcode_NumOfGlyf[0] = 0;
			}
			break;
		}
	}
}

static void HY_KoreanBuild_ThreeMothod2( SplineFont * sf, int uni, int cho, int jung, int jong )
{
	SplineChar *nsc;
	SplineChar *chosc;
	SplineChar *jungsc;
	SplineChar *jongsc;
	int index;
	int width;

	nsc = SFMakeChar( sf, sf->map, uni );

	index = cho;
	chosc = sf->glyphs[index];
	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], chosc );

	index = NUM_JASO_FC + jung;
	jungsc = sf->glyphs[index];
	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], jungsc );

	if( jong > 0 )
	{
		index = NUM_JASO_FC + NUM_JASO_MV + jong - 1;
		jongsc = sf->glyphs[index];
	 	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], jongsc );
	}
	width = sf->ascent + sf->descent;
}

static void HY_KoreanBuild_ThreeMothod( SplineFont * sf, SplineChar **glyphs, int uniglyphcnt, int cho, int jung, int jong )
{
	SplineChar *sc;
	SplineChar *chosc;
	SplineChar *jungsc;
	SplineChar *jongsc;
	int index = 0, width;
	int uni = 0xac00 + ( cho * NUM_JASO_MV + jung ) * NUM_JASO_LC + jong;

	index = uniglyphcnt + cho;
	chosc = sf->glyphs[index];
	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], chosc );

	index = uniglyphcnt + NUM_JASO_FC + jung;
	jungsc = sf->glyphs[index];
	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], jungsc );

	if( jong > 0 )
	{
		index = uniglyphcnt + NUM_JASO_FC + NUM_JASO_MV + jong - 1;
		jongsc = sf->glyphs[index];
	 	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], jongsc );
	}
	width = sf->ascent + sf->descent;

}

static SplineChar *HY_FindNewJasoRefChar( SplineChar **jasoglyphs, int jasocount, SplineChar *sc, char *name )
{
	int i;

	if( sc == NULL )
	{
		return NULL;
	}

	for( i = 0 ; i < jasocount ; i++ )
	{
		if( strcmp( jasoglyphs[i]->name, name ) == 0 )
		{
			return( jasoglyphs[i] );
		}
	}
	return NULL;
}

static int HY_FindNewUniRefChar_New( SplineFont *sf, int enc )
{
	int gid;

	for( gid = 0 ; gid < sf->glyphcnt ; gid++ )
	{
		if( gid != -1 )
		{
			if( sf->glyphs[gid]->unicodeenc == enc )
			{
				return( gid );
			}
		}
	}
	return -1;
}

static SplineChar *HY_FindNewUniRefChar( SplineChar **uniglyphs, int unicount, SplineChar *sc, char *name )
{
	int i;

	if( sc == NULL )
	{
		return NULL;
	}

	for( i = 0 ; i < unicount ; i++ )
	{
		if( strcmp( uniglyphs[i]->name, name ) == 0 )
		{
			return( uniglyphs[i] );
		}
	}
	return NULL;
}

static char *HY_GetHangulteulDocDir(void)
{
	return NULL;
/*
	char *filepath;
	filepath = copy(GFileGetHomeDocumentsDir());
	strcat( filepath, "한글틀/" );
	strcat( filepath, "test.txt" );
*/
}

static void HY_RollBackJohapIndex( JohapRule *johap )
{
	int nog_fc, nog_mv, nog_lc;
	int fc, mv, lc;
	int ruletype;

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE - 1 ; ruletype++ )
	{
		RULE * rule = johap->rule[ruletype];
		nog_fc = rule->grouplist[JASO_FC]->nGroups;
		nog_mv = rule->grouplist[JASO_MV]->nGroups;
		nog_lc = rule->grouplist[JASO_LC]->nGroups;

		switch( ruletype )
		{
			case RULE_FC:
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					for( mv = 0 ; mv < nog_mv ; mv++ )
					{
						for( lc = 0 ; lc < nog_lc ; lc++ )
						{
							rule->tmpgindex[fc][mv][lc] = rule->GlyfIndexMap[fc][mv][lc];
						}
					}
				}
			}
			break;
			case RULE_MV:
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					for( mv = 0 ; mv < nog_mv ; mv++ )
					{
						for( lc = 0 ; lc < nog_lc ; lc++ )
						{
							rule->tmpgindex[fc][mv][lc] = rule->GlyfIndexMap[fc][mv][lc];
						}
					}
				}
			}
			break;
			case RULE_LC:
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					for( mv = 0 ; mv < nog_mv ; mv++ )
					{
						for( lc = 0 ; lc < nog_lc ; lc++ )
						{
							rule->tmpgindex[fc][mv][lc] = rule->GlyfIndexMap[fc][mv][lc];
						}
					}
				}
			}
			break;
		}
	}
}

static void HY_RollbackRulesFillupVarientcnts( struct compositionrules_t *rules )
{
	int i, j, k, grp, size, vcnt, index;

	for( i = 0 ; i < NUM_JASO_FC ; ++i )
	{
		grp = rules->chosung_groups[0][i];
		size = rules->num_groups[0][1] * rules->num_groups[0][2];
		k = grp * size;
		for( j = vcnt = 0 ; j < size ; ++j )
		{
			if ( rules->chosung_remap[k + j] > vcnt )
			{
				vcnt = rules->chosung_remap[k + j];
			}
		}
		rules->chosung_varients[i] = vcnt + 1;
	}

	for ( i = 0 ; i < NUM_JASO_MV ; ++i )
	{
		grp = rules->jungsung_groups[1][i];
		for ( j=vcnt=0; j<rules->num_groups[1][2]; ++j )
		{
			for ( k=0; k<rules->num_groups[1][0]; ++k )
			{
				index = (k*rules->num_groups[1][1]+grp)*rules->num_groups[1][2]+j;
				if ( rules->jungsung_remap[index]>vcnt )
				{
					vcnt = rules->jungsung_remap[index];
				}
			}
		}
		rules->jungsung_varients[i] = vcnt + 1;
	}

	rules->jongsung_varients[0] = 0;
	for ( i = 1 ; i < NUM_JASO_LC ; ++i )
	{
		/* jongsung @0 is blank, so no need for varients */
		grp = rules->jongsung_groups[2][i];
		for ( j=vcnt=0; j<rules->num_groups[2][1]; ++j )
		{
			for ( k=0; k<rules->num_groups[2][0]; ++k )
			{
				index = (k*rules->num_groups[2][1]+j)*rules->num_groups[2][2]+grp;
				if ( rules->jongsung_remap[index]>vcnt )
				{
					vcnt = rules->jongsung_remap[index];
				}
			}
		}
		rules->jongsung_varients[i] = vcnt + 1;
	}

/*
	for ( i = 0 ; i < NUM_COMBI_RULE ; ++i )
	{
		rules->groups[i][0] = rules->chosung_groups[i];
		rules->groups[i][1] = rules->jungsung_groups[i];
		rules->groups[i][2] = rules->jongsung_groups[i];
	}
*/
}

struct compositionrules_t * HY_ConvertToRollbackFile( JohapRule * johap )
{
	struct compositionrules_t * rules = calloc( 1, sizeof( struct compositionrules_t ) );
	int nog_fc, nog_mv, nog_lc;
	int fc, mv, lc;
	int i, j, k;
	int index, size;

#if USE_LOG
	FILE *log;
	log = fopen( "HY_ConvertToRollbackFile.log", "w" );
	if( log == NULL )
	{
		hydebug( "Failed to open HY_ConvertToRollbackFile.log\n" );
	}
#endif

	for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
	{
		RULE * rule = johap->rule[i];
		for( j = 0 ; j < NUM_JASO_TYPES ; j++ )
		{
			JasoGroupList * grouplist = rule->grouplist[j];

			rules->num_groups[i][j] = grouplist->nGroups;
#if USE_LOG
			fprintf( log, "rule:(%d), jaso:(%d) - num_groups[%d][%d]: %d\n", i, j, i, j, rules->num_groups[i][j] );
#endif

			nog_fc	= rule->grouplist[JASO_FC]->nGroups;
			nog_mv	= rule->grouplist[JASO_MV]->nGroups;
			nog_lc	= rule->grouplist[JASO_LC]->nGroups;
			size = nog_fc * nog_mv * nog_lc;
			index = 0;

			switch( i )
			{
				case RULE_FC:
				{
					// Setup chosung_remap[]
					rules->chosung_remap = malloc( size * sizeof( uint16 ) );
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								rules->chosung_remap[index] = rule->GlyfIndexMap[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
				case RULE_MV:
				{
					// Setup jungsung_remap[]
					rules->jungsung_remap = malloc( size * sizeof( uint16 ) );
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								rules->jungsung_remap[index] = rule->GlyfIndexMap[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
				case RULE_LC:
				{
					// Setup jongsung_remap[]
					rules->jongsung_remap = malloc( size * sizeof( uint16 ) );
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								rules->jongsung_remap[index] = rule->GlyfIndexMap[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
				case RULE_WIDTH:
				{
					// Setup width_map[]
					rules->width_map = malloc( size * sizeof( uint16 ) );
					rules->lsb_map = malloc( size * sizeof( uint16 ) );
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								rules->width_map[index] = rule->GlyfWidthMap[fc][mv][lc];
								rules->lsb_map[index] = rule->Lsb[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
			}

			switch( j )
			{
				case JASO_FC:
				{
					// Setup chosung_groups[][], chosung_varients[]
					for( k = 0 ; k < NUM_JASO_FC ; k++ )
					{
						rules->chosung_groups[i][k]	 = grouplist->JasoGroupMap[k];
					}
				}
				break;
				case JASO_MV:
				{
					// Setup jungsung_groups[][], jungsung_varients[]
					for( k = 0 ; k < NUM_JASO_MV ; k++ )
					{
						rules->jungsung_groups[i][k] = grouplist->JasoGroupMap[k];
					}
				}
				break;
				case JASO_LC:
				{
					// Setup jongsung_groups[][], jongsung_varients[]
					for( k = 0 ; k < NUM_JASO_LC ; k++ )
					{
						rules->jongsung_groups[i][k] = grouplist->JasoGroupMap[k];
					}
				}
				break;
			}
		}
	}

#if USE_LOG
	if( log != NULL )
	{
		fclose( log );
	}
#endif
	return( rules );
}

JohapRule * HY_ConvertToSplineFont( struct compositionrules_t * rules )
{
	int i, j, k, g;

#if USE_LOG
	FILE *log;
	log = fopen( "HY_ConvertToSplineFont.log", "w" );
	if( log == NULL )
	{
		hydebug( "Failed to open HY_ConvertToSplineFont.log file!\n" );
	}

	fprintf( log, "===============================================\n" );
#endif

	JohapRule * johap	= calloc( 1, sizeof( JohapRule ) );
	johap->ruletype	= 0;

	for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
	{
		RULE * rule = calloc( 1, sizeof( RULE ) );
		johap->rule[i] = rule;
		rule->jasotype = 0;

		for( j = 0 ; j < NUM_JASO_TYPES ; j++ )
		{
			JasoGroupList * grouplist = calloc( 1, sizeof( JasoGroupList ) );

			rule->grouplist[j] = grouplist;
#if USE_LOG
			fprintf( log, "Before :: rule:(%d), jaso:(%d) - nGroups: %d\n", i, j, grouplist->nGroups );
#endif
			grouplist->nGroups = rules->num_groups[i][j];

#if USE_LOG
			fprintf( log, "After :: rule:(%d), jaso:(%d) - nGroups: %d\n", i, j, grouplist->nGroups );
#endif

			// Init JasoGroupMap
			for( k = 0 ; k < grouplist->nGroups ; k++ )
			{
				for( g = 0 ; g < MAX_JASO_PER_JASOTYPE ; g++ )
				{
					grouplist->JasoGroupMap[g] = -1;
				}
			}

			// Setup JasoGroupMap
			switch( j )
			{
				case JASO_FC:
				{
					for( k = 0 ; k < NUM_JASO_FC ; k++ )
					{
						grouplist->JasoGroupMap[k] = rules->chosung_groups[i][k];
#if USE_LOG
						fprintf( log, "rule:(%d), jaso:(%d) - JasoGroupMap[%d]: %d\n", i, j, k, grouplist->JasoGroupMap[k] );
#endif
					}
				}
				break;
				case JASO_MV:
				{
					for( k = 0 ; k < NUM_JASO_MV ; k++ )
					{
						grouplist->JasoGroupMap[k] = rules->jungsung_groups[i][k];
#if USE_LOG
						fprintf( log, "rule:(%d), jaso:(%d) - JasoGroupMap[%d]: %d\n", i, j, k, grouplist->JasoGroupMap[k] );
#endif
					}
				}
				break;
				case JASO_LC:
				{
					for( k = 0 ; k < NUM_JASO_LC ; k++ )
					{
						grouplist->JasoGroupMap[k] = rules->jongsung_groups[i][k];
#if USE_LOG
						fprintf( log, "rule:(%d), jaso:(%d) - JasoGroupMap[%d]: %d\n", i, j, k, grouplist->JasoGroupMap[k] );
#endif
					}
				}
				break;
			}

			// Setup JasoGroupItem
			JasoGroupItem * jasoitem_prev = NULL;
			JasoGroupItem * jasoitem = ( JasoGroupItem * ) calloc( 1, sizeof( JasoGroupItem ) );
			grouplist->jasoitem = jasoitem;

			for( k = 0 ; k < grouplist->nGroups ; k++ )
			{
				jasoitem->numJaso = 0;
				for( g = 0 ; g < MAX_JASO_PER_JASOTYPE ; g++ )
				{
					jasoitem->bSelect[g] = false;
					if(  k == grouplist->JasoGroupMap[g] )
					{
						jasoitem->JasoVCode[jasoitem->numJaso] = ( uint8 )g;
#if USE_LOG
						fprintf( log, "rule:(%d), jaso:(%d) - jasoitem->JasoVCode[%d]: %d\n", i, j, jasoitem->numJaso, jasoitem->JasoVCode[jasoitem->numJaso] );
#endif
						jasoitem->numJaso++;
					}
					if( k > 0 )
					{
						jasoitem->prev = jasoitem_prev;
					}
				}
				jasoitem_prev = jasoitem;
				jasoitem->next = ( JasoGroupItem * ) calloc( 1, sizeof( JasoGroupItem ) );
				jasoitem = jasoitem->next;
			}
		}
	}

	int nog_fc, nog_mv, nog_lc;
	int fc, mv, lc, index = 0;
	SplineChar *sc;
	int uni, gid, cho, ju, jo;
	for( i = 0 ; i < NUM_COMBI_RULE - 1 ; i++ )
	{
		RULE * rule2 = johap->rule[i];
		nog_fc = rule2->grouplist[JASO_FC]->nGroups;
		nog_mv = rule2->grouplist[JASO_MV]->nGroups;
		nog_lc = rule2->grouplist[JASO_LC]->nGroups;

		// Init vcode_NumOfGlyf
		for( j = 0 ; j < MAX_JASO_PER_JASOTYPE ; j++ )
		{
			rule2->vcode_NumOfGlyf[j] = -1;
		}

		index = 0;
		switch( i )
		{
			case RULE_FC:
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					for( mv = 0 ; mv < nog_mv ; mv++ )
					{
						for( lc = 0 ; lc < nog_lc ; lc++ )
						{
							rule2->tmpgindex[fc][mv][lc] = rule2->GlyfIndexMap[fc][mv][lc] = rules->chosung_remap[index];
							index++;
						}
					}
				}

				// Setup vcode_NumOfGlyf
				for( j = 0 ; j < NUM_JASO_FC ; j++ )
				{
					rule2->vcode_NumOfGlyf[j] = rules->chosung_varients[j];
#if USE_LOG
					fprintf( log, "rule:(%d), jaso:(%d) - vcode_NumOfGlyf[%d]: %d\n", i, j, j, rule2->vcode_NumOfGlyf[j] );
#endif
				}
			}
			break;

			case RULE_MV:
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					for( mv = 0 ; mv < nog_mv ; mv++ )
					{
						for( lc = 0 ; lc < nog_lc ; lc++ )
						{
							rule2->tmpgindex[fc][mv][lc] = rule2->GlyfIndexMap[fc][mv][lc] = rules->jungsung_remap[index];
							index++;
						}
					}
				}

				// Setup vcode_NumOfGlyf
				for( j = 0 ; j < NUM_JASO_MV ; j++ )
				{
					rule2->vcode_NumOfGlyf[j] = rules->jungsung_varients[j];
#if USE_LOG
					fprintf( log, "rule:(%d), jaso:(%d) - vcode_NumOfGlyf[%d]: %d\n", i, j, j, rule2->vcode_NumOfGlyf[j] );
#endif
				}
			}
			break;

			case RULE_LC:
			{
				for( fc = 0 ; fc < nog_fc ; fc++ )
				{
					for( mv = 0 ; mv < nog_mv ; mv++ )
					{
						for( lc = 0 ; lc < nog_lc ; lc++ )
						{
							rule2->tmpgindex[fc][mv][lc] = rule2->GlyfIndexMap[fc][mv][lc] = rules->jongsung_remap[index];
							index++;
						}
					}
				}

				// Setup vcode_NumOfGlyf
				for( j = 0 ; j < NUM_JASO_LC ; j++ )
				{
					rule2->vcode_NumOfGlyf[j] = rules->jongsung_varients[j];
#if USE_LOG
					fprintf( log, "rule:(%d), jaso:(%d) - vcode_NumOfGlyf[%d]: %d\n", i, j, j, rule2->vcode_NumOfGlyf[j] );
#endif
				}
			}
			break;
		}
	}

#if USE_LOG
	fprintf( log, "===============================================\n" );
	if( log != NULL )
	{
		fclose( log );
	}
#endif
	return( johap );
}

static JohapRule *HY_DoRollbackRuleJohap( FILE *tmpfp, SplineFont *sf )
{
	JohapRule * johap;
	struct compositionrules_t * rules;
	int ruletype = 0;
	int jasoindex = 0;
	int val = 0;

#if USE_LOG
	FILE *log;
	log = fopen( "HY_DoRollbackRuleJohap.log", "w" );
	if( log == NULL )
	{
		hydebug( "Failed to open HY_DoRollbackRuleJohap.log file!\n" );
	}
#endif

	/********************************************************
			GroupIndexMap - ChoSung , JungSung, JongSung, Width
	********************************************************/
	johap = calloc( 1, sizeof( JohapRule ) );
	rules = calloc( 1, sizeof( struct compositionrules_t ) );

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE ; ++ruletype )
	{
		for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; ++jasoindex )
		{
			fscanf( tmpfp, "%d ", &val );
			if( val == 255 )
			{
				val = 0;
			}
			rules->chosung_groups[ruletype][jasoindex] = val;
		}

		for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; ++jasoindex )
		{
			fscanf( tmpfp, "%d ", &val );
			if( val == 255 )
			{
				val = 0;
			}
			rules->jungsung_groups[ruletype][jasoindex] = val;
		}

		for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; ++jasoindex )
		{
			fscanf( tmpfp, "%d ", &val );
			if( val == 255 )
			{
				val = 0;
			}
			rules->jongsung_groups[ruletype][jasoindex] = val;
		}
		fscanf( tmpfp, "%hd %hd %hd\n", &rules->num_groups[ruletype][0], &rules->num_groups[ruletype][1], &rules->num_groups[ruletype][2] );
#if USE_LOG
		fprintf( log, "num_groups[%hd][0]: %d\n", ruletype, rules->num_groups[ruletype][0] );
		fprintf( log, "num_groups[%hd][1]: %d\n", ruletype, rules->num_groups[ruletype][1] );
		fprintf( log, "num_groups[%hd][2]: %d\n", ruletype, rules->num_groups[ruletype][2] );
#endif
	}


	/********************************************************
			Remap - ChoSung, JungSung, JongSung
	********************************************************/
	int remapindex;
	int remapsize;
	remapsize = rules->num_groups[RULE_FC][JASO_FC] * rules->num_groups[RULE_FC][JASO_MV] * rules->num_groups[RULE_FC][JASO_LC];
	rules->chosung_remap = calloc( remapsize, sizeof(uint16) );
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fscanf( tmpfp, "%hd ", &rules->chosung_remap[remapindex]);
	}

	remapsize = rules->num_groups[RULE_MV][JASO_FC]*rules->num_groups[RULE_MV][JASO_MV]*rules->num_groups[RULE_MV][JASO_LC];
	rules->jungsung_remap = calloc( remapsize, sizeof(uint16) );
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fscanf( tmpfp, "%hd ", &rules->jungsung_remap[remapindex]);
	}

	remapsize = rules->num_groups[RULE_LC][JASO_FC]*rules->num_groups[RULE_LC][JASO_MV]*rules->num_groups[RULE_LC][JASO_LC];
	rules->jongsung_remap = calloc( remapsize, sizeof(uint16) );
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fscanf( tmpfp, "%hd ", &rules->jongsung_remap[remapindex] );
	}

	// width map
	if( rules->num_groups[RULE_WIDTH][JASO_FC] == 0 )
	{
		rules->num_groups[RULE_WIDTH][JASO_FC] = 1;
	}
	if( rules->num_groups[RULE_WIDTH][JASO_MV] == 0 )
	{
		rules->num_groups[RULE_WIDTH][JASO_MV] = 1;
	}
	if( rules->num_groups[RULE_WIDTH][JASO_LC] == 0 )
	{
		rules->num_groups[RULE_WIDTH][JASO_LC] = 1;
	}

	int lsbmapindex;
	int lsbmapsize;
	fscanf(tmpfp, "%d", &val );
	if( val )
	{
		remapsize = rules->num_groups[RULE_WIDTH][0] * rules->num_groups[RULE_WIDTH][1] * rules->num_groups[RULE_WIDTH][2];
		rules->width_map = calloc( remapsize, sizeof(uint16) );
		for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
		{
			fscanf( tmpfp, "%hd ", &rules->width_map[remapindex]);
		}

		lsbmapsize = rules->num_groups[RULE_WIDTH][0] * rules->num_groups[RULE_WIDTH][1] * rules->num_groups[RULE_WIDTH][2];
		rules->lsb_map = calloc( lsbmapsize, sizeof(uint16) );
		for( lsbmapindex = 0 ; lsbmapindex < lsbmapsize ; ++lsbmapindex )
		{
			fscanf( tmpfp, "%hd ", &rules->lsb_map[lsbmapindex]);
		}
	}
	else
	{
		remapsize = 1;
		rules->width_map = calloc( remapsize, sizeof(uint16) );
		for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
		{
			fscanf( tmpfp, "%hd ", &rules->width_map[remapindex]);
		}

		lsbmapsize = 1;
		rules->lsb_map = calloc( lsbmapsize, sizeof(uint16) );
		for( lsbmapindex = 0 ; lsbmapindex < lsbmapsize ; ++lsbmapindex )
		{
			rules->lsb_map[lsbmapindex] = 0;
		}
	}
	HY_RollbackRulesFillupVarientcnts(rules);

#if USE_LOG
	if( log != NULL )
	{
		fclose( log );
	}
#endif

	sf->johap = HY_ConvertToSplineFont( rules );
	sf->johap->sf = sf;

	return sf->johap;
}

static void HY_RollBackRuleJoahp( JohapRule *johap )
{
	hydebug( "[%s] RollBack Rule Info\n", __FUNCTION__ );

	FILE *tmpfp;
	if( johap->sf->rollbak_fname[0] != '\0' )
	{
		tmpfp = fopen( johap->sf->rollbak_fname, "r" );
		if( tmpfp != NULL )
		{
			johap = HY_DoRollbackRuleJohap( tmpfp, johap->sf );
			fclose( tmpfp );
		}
		else
		{
			hydebug( "[%s] 2. Failed to open rule info file\n", __FUNCTION__ );
		}

		if( access(johap->sf->rollbak_fname, F_OK) == 0 )
		{
			DeleteFile(johap->sf->rollbak_fname);
			memset( johap->sf->rollbak_fname, '\0', PATH_MAX );
		}
		else
		{
			hydebug( "[%s] 2. file not exist\n", __FUNCTION__ );
		}
	}
}

static JohapRule *HY_LoadRollbackRuleJohap( FILE *tmpfp )
{
	JohapRule * johap;
	struct compositionrules_t * rules;
	int ruletype = 0;
	int jasoindex = 0;
	int val = 0;

	/********************************************************
			GroupIndexMap - ChoSung , JungSung, JongSung, Width
	********************************************************/
	johap = calloc( 1, sizeof( JohapRule ) );
	rules = calloc( 1, sizeof( struct compositionrules_t ) );

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE ; ++ruletype )
	{
		for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; ++jasoindex )
		{
			fscanf( tmpfp, "%d ", &val );
			if( val == 255 )
			{
				val = 0;
			}
			rules->chosung_groups[ruletype][jasoindex] = val;
		}

		for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; ++jasoindex )
		{
			fscanf( tmpfp, "%d ", &val );
			if( val == 255 )
			{
				val = 0;
			}
			rules->jungsung_groups[ruletype][jasoindex] = val;
		}

		for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; ++jasoindex )
		{
			fscanf( tmpfp, "%d ", &val );
			if( val == 255 )
			{
				val = 0;
			}
			rules->jongsung_groups[ruletype][jasoindex] = val;
		}
		fscanf( tmpfp, "%hd %hd %hd\n", &rules->num_groups[ruletype][0], &rules->num_groups[ruletype][1], &rules->num_groups[ruletype][2] );
	}


	/********************************************************
			Remap - ChoSung, JungSung, JongSung
	********************************************************/
	int remapindex;
	int remapsize;
	remapsize = rules->num_groups[RULE_FC][JASO_FC] * rules->num_groups[RULE_FC][JASO_MV] * rules->num_groups[RULE_FC][JASO_LC];
	rules->chosung_remap = calloc( remapsize, sizeof(uint16) );
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fscanf( tmpfp, "%hd ", &rules->chosung_remap[remapindex]);
	}

	remapsize = rules->num_groups[RULE_MV][JASO_FC]*rules->num_groups[RULE_MV][JASO_MV]*rules->num_groups[RULE_MV][JASO_LC];
	rules->jungsung_remap = calloc( remapsize, sizeof(uint16) );
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fscanf( tmpfp, "%hd ", &rules->jungsung_remap[remapindex]);
	}

	remapsize = rules->num_groups[RULE_LC][JASO_FC]*rules->num_groups[RULE_LC][JASO_MV]*rules->num_groups[RULE_LC][JASO_LC];
	rules->jongsung_remap = calloc( remapsize, sizeof(uint16) );
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fscanf( tmpfp, "%hd ", &rules->jongsung_remap[remapindex] );
	}

	// width map
	if( rules->num_groups[RULE_WIDTH][JASO_FC] == 0 )
	{
		rules->num_groups[RULE_WIDTH][JASO_FC] = 1;
	}
	if( rules->num_groups[RULE_WIDTH][JASO_MV] == 0 )
	{
		rules->num_groups[RULE_WIDTH][JASO_MV] = 1;
	}
	if( rules->num_groups[RULE_WIDTH][JASO_LC] == 0 )
	{
		rules->num_groups[RULE_WIDTH][JASO_LC] = 1;
	}

	int lsbmapindex;
	int lsbmapsize;
	fscanf(tmpfp, "%d", &val );
	if( val )
	{
		remapsize = rules->num_groups[RULE_WIDTH][0] * rules->num_groups[RULE_WIDTH][1] * rules->num_groups[RULE_WIDTH][2];
		rules->width_map = calloc( remapsize, sizeof(uint16) );
		for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
		{
			fscanf( tmpfp, "%hd ", &rules->width_map[remapindex]);
		}

		lsbmapsize = rules->num_groups[RULE_WIDTH][0] * rules->num_groups[RULE_WIDTH][1] * rules->num_groups[RULE_WIDTH][2];
		rules->lsb_map = calloc( lsbmapsize, sizeof(uint16) );
		for( lsbmapindex = 0 ; lsbmapindex < lsbmapsize ; ++lsbmapindex )
		{
			fscanf( tmpfp, "%hd ", &rules->lsb_map[lsbmapindex]);
		}
	}
	else
	{
		remapsize = 1;
		rules->width_map = calloc( remapsize, sizeof(uint16) );
		for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
		{
			fscanf( tmpfp, "%hd ", &rules->width_map[remapindex]);
		}

		lsbmapsize = 1;
		rules->lsb_map = calloc( lsbmapsize, sizeof(uint16) );
		for( lsbmapindex = 0 ; lsbmapindex < lsbmapsize ; ++lsbmapindex )
		{
			rules->lsb_map[lsbmapindex] = 0;
		}
	}

	HY_RollbackRulesFillupVarientcnts(rules);
	/*sf->*/johap = HY_ConvertToSplineFont( rules );

	return /*sf->*/johap;
}

static void _HY_ImportJohapRules_ThreePairMethod( FontView *fv, SplineFont *imsf, int *unimap, SplineChar **uniglyphs, int uniglyphcnt, SplineChar **jasoglyphs, int jasoglyphcnt )
{
	SplineFont *sf = fv->b.sf;
	JohapRule *johap;
	RefChar *rf;
	int i, j, gid, jindex = 65536;
	int cho, jung, jong;
	int refcnt = 0;
	int refindex = 0;
	char **refname;
	SplineChar *rsc;

	for( i = 0 ; i < 65536 ; i++ )
	{
		if( unimap[i] != -1 )
		{
			SFAddGlyphAndEncode( sf, uniglyphs[unimap[i]], sf->map, i );
			if( uniglyphs[unimap[i]]->layers[ly_fore].refs != NULL )
			{
				if( i < 0xac00 || i > 0xd7a3 )
				{
					refcnt = HY_GetRefCharCount( uniglyphs[unimap[i]] );
					refname = malloc( refcnt * sizeof(char *));
					for( rf = uniglyphs[unimap[i]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
					{
						refname[refindex] = strdup( rf->sc->name );
						refindex++;
					}
					SCClearAll( sf->glyphs[unimap[i]], ly_fore );
					for( j = 0 ; j < refcnt ; j++ )
					{
						rsc = HY_FindNewUniRefChar( uniglyphs, uniglyphcnt, uniglyphs[unimap[i]], refname[j] );
						if( rsc != NULL )
						{
							HY_SCAddKoreanRef( sf->glyphs[unimap[i]], rsc );
						}
						else
						{
							rsc = HY_FindNewJasoRefChar( jasoglyphs, jasoglyphcnt, uniglyphs[unimap[i]], refname[j] );
							if( rsc != NULL )
							{
								HY_SCAddKoreanRef( sf->glyphs[unimap[i]], rsc );
							}
							else
							{
								continue;
							}
						}
					}
					free( refname );
					refindex = 0;
				}
			}

		}
	}
	for( i = 0 ; i < jasoglyphcnt ; i++ )
	{
		SFAddGlyphAndEncode( sf, jasoglyphs[i], sf->map, jindex );
		jindex++;
	}

	int layer = 0;
	for( i = 0xac00 ; i < 0xd7a3 ; i++ )
	{
		for( layer = 0 ; layer < sf->layer_cnt ; layer++ )
		{
			SCClearAll( sf->glyphs[sf->map->map[i]], layer );
		}
	}

	ff_progress_start_indicator( 10 , _("Korean Build"),_("Building Three Pair Korean...") , 0 , 11172 , 1 );
	for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
	{
		for( jung = 0 ; jung < NUM_JASO_MV ; jung++ )
		{
			for( jong = 0 ; jong < NUM_JASO_LC ; jong++ )
			{
				HY_KoreanBuild_ThreeMothod( sf, jasoglyphs, uniglyphcnt, cho, jung, jong );
				if ( !ff_progress_next())
				{
					break;
				}
			}
		}
	}
	ff_progress_end_indicator();
}

void HY_Reset_GlyfList_ThreepairJohap_New( SplineFont *sf, JohapRule * johap )
{
	SplineChar **uniglyphs;
	SplineChar **jasoglyphs;
	SplineChar *sc;
	int i, gid;
	int uniglyphcnt;
	int jasoglyphcnt;
	int totalglyphcnt;
	int jasoindex;
	int jasocount;
	int vcode;

	// Get glyphs count
	uniglyphcnt = jasoglyphcnt = totalglyphcnt = 0;
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 && SCWorthOutputting( sf->glyphs[gid] ) )
		{
			if( i <= 0xffff )
			{
				uniglyphcnt++;
			}
			else
			{
				jasoglyphcnt++;
			}      
			totalglyphcnt++;
		}
	}

	if( jasoglyphcnt <= 0 || jasoglyphcnt > sf->glyphcnt )
	{
		ff_post_error( _("Error"),_("Invalid Jaso glyph count %g") );
		return;
	}


	// Copy unicode glyphs
	uniglyphs = calloc( 65536, sizeof(SplineChar *) );
	for( i = 0 ; i < 65536 ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 )
		{
			uniglyphs[i]					= SplineCharCopy( sf->glyphs[gid], NULL, NULL );
			if( i == 0 )
			{
				uniglyphs[i]->unicodeenc	= -1;	// .Notdef
			}
			else
			{
				uniglyphs[i]->unicodeenc	= i;
			}
			uniglyphs[i]->name			= copy( sf->glyphs[gid]->name );
			uniglyphs[i]->views			= sf->glyphs[gid]->views;
			uniglyphs[i]->widthset			= 1;
			uniglyphs[i]->width			= sf->glyphs[gid]->width;
			uniglyphs[i]->vwidth			= sf->glyphs[gid]->vwidth;
			uniglyphs[i]->color				= sf->glyphs[gid]->color;
		}
		else
		{
			uniglyphs[i]					= NULL;
		}
	}


	// Copy jaso glyphs
	gid = -1;
	jasoindex = jasocount = 0;
	jasoglyphs = calloc( NUM_JASO_FC + NUM_JASO_MV + NUM_JASO_LC - 1, sizeof(SplineChar *) );
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 )
		{
			if( i > 0xffff )
			{
				if( sc->Varient == 0 )
				{
					jasoglyphs[jasoindex]							= SplineCharCopy( sc, NULL, NULL );
					jasoglyphs[jasoindex]->unicodeenc				= -1;
					jasoglyphs[jasoindex]->VCode					= sc->VCode;
					jasoglyphs[jasoindex]->Varient					= sc->Varient;
					jasoglyphs[jasoindex]->bCompositionUnit			= 1;
					jasoglyphs[jasoindex]->widthset					= 1;
					jasoglyphs[jasoindex]->width					= sc->width;
					jasoglyphs[jasoindex]->vwidth					= sc->vwidth;
					jasoglyphs[jasoindex]->color					= sc->color;
					jasoindex++;
					jasocount++;
					if( jasoindex == NUM_JASO_FC + NUM_JASO_MV + NUM_JASO_LC )
					{
						break;
					}
				}
			}
		}
	}

	// Clear all glyphs
	FVDetachAndRemoveGlyphs( (FontViewBase *)sf->fv );
	FVRemoveUnused( sf->fv );

	for( i = 0 ; i < sf->map->enccount; i++ )
	{
		sf->map->map[i] = -1;
	}

	for( i = 0 ; i < sf->glyphcnt; i++ )
	{
		sf->map->backmap[i] = -1;
	}
	sf->glyphcnt = sf->glyphmax = 0;

	GlyphHashFree( sf );
	FontViewReformatAll( sf );

	// Set jaso glyphs to splinefont
	for( i = 0 ; i < jasocount ; i++ )
	{
		sf->map->map[65536 + i] = i;
		sf->map->backmap[sf->map->map[65536 + i]] = 65536 + i;

		sf->glyphs[i]					= SFSplineCharCreate( sf );
		sf->glyphs[i]					= SplineCharCopy( jasoglyphs[i], sf, NULL );
		sf->glyphs[i]->parent			= sf;
		sf->glyphs[i]->name			= copy( jasoglyphs[i]->name );
		sf->glyphs[i]->views			= jasoglyphs[i]->views;
		sf->glyphs[i]->unicodeenc		= -1;
		sf->glyphs[i]->orig_pos		= sf->map->map[65536 + i];
		sf->glyphs[i]->bCompositionUnit	= 1;
		sf->glyphs[i]->VCode			= jasoglyphs[i]->VCode;
		sf->glyphs[i]->Varient			= jasoglyphs[i]->Varient;
		sf->glyphs[i]->widthset		= 1;
		sf->glyphs[i]->width			= sf->ascent + sf->descent;
		sf->glyphs[i]->vwidth			= sf->ascent + sf->descent;
		sf->glyphs[i]->color			= jasoglyphs[i]->color;

		SFHashGlyph( sf, sf->glyphs[i] );

		sf->glyphcnt++;
	}
	sf->glyphmax = sf->glyphcnt + 10;
	FVRemoveUnused( sf->fv );
	FontViewReformatAll( sf );

	// set unicode glyphs to splinefont
	int cho, jung, jong;
	ff_progress_start_indicator( 10 , _("Korean Build"),_("Building Korean...") , 0 , uniglyphcnt, 1 );
	for( i = 0 ; i < 65536 ; i++ )
	{
		if( uniglyphs[i] != NULL )
		{
			if( i < 0xac00 || i > 0xd7a3 )
			{
				SFMakeChar( sf, sf->map, i );

				sf->glyphs[sf->map->map[i]]				= SplineCharCopy( uniglyphs[i], sf, NULL );
				sf->glyphs[sf->map->map[i]]->unicodeenc	= uniglyphs[i]->unicodeenc;
				sf->glyphs[sf->map->map[i]]->orig_pos		= sf->map->map[i];
				sf->glyphs[sf->map->map[i]]->name		= copy( uniglyphs[i]->name );
				sf->glyphs[sf->map->map[i]]->views		= uniglyphs[i]->views;
				sf->glyphs[sf->map->map[i]]->widthset		= 1;
				sf->glyphs[sf->map->map[i]]->width		= uniglyphs[i]->width;
				sf->glyphs[sf->map->map[i]]->vwidth		= uniglyphs[i]->vwidth;
				sf->glyphs[sf->map->map[i]]->color		= uniglyphs[i]->color;
			}
			else
			{
				cho = ( i - 0xac00 ) / ( NUM_JASO_MV * NUM_JASO_LC );
				jung = ( ( i - 0xac00 ) - ( cho * NUM_JASO_MV * NUM_JASO_LC ) ) / NUM_JASO_LC;
				jong = ( i - 0xac00 ) % NUM_JASO_LC;
				HY_KoreanBuild_ThreeMothod2( sf, i, cho, jung, jong );
			}

			if ( !ff_progress_next())
			{
				break;
			}
		}
	}
	ff_progress_end_indicator();

	// Set Glyphs Dependecy
	RefChar *ref;
	int refcount = 0;
	int refindex = 0;
	int refenc[HY_REFCHAR_MAX];
	real trans[HY_REFCHAR_MAX][6];
	int refglyfindex = 0;
	int j;

	for( i = 0 ; i < 65536 ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 )
		{
			if( i < 0xac00 || i > 0xd7a3 )
			{
				refcount = 0;
				refindex = 0;

				for( ref = sf->glyphs[gid]->layers[ly_fore].refs ; ref != NULL ; ref = ref->next )
				{
					refenc[refindex] = ref->unicode_enc;
					trans[refindex][0] = ref->transform[0];
					trans[refindex][1] = ref->transform[1];
					trans[refindex][2] = ref->transform[2];
					trans[refindex][3] = ref->transform[3];
					trans[refindex][4] = ref->transform[4];
					trans[refindex][5] = ref->transform[5];
					refindex++;
					refcount++;
				}

				SCRemoveDependents( sf->glyphs[gid] );

				for( j = 0 ; j < refcount ; j++ )
				{
					refglyfindex = HY_FindNewUniRefChar_New( sf, refenc[j] );
					if( refglyfindex != -1 )
					{
						HY_SCAddKoreanRef_New( sf->glyphs[gid], sf->glyphs[refglyfindex], trans[j] );
					}
				}
			}
		}
	}

	// Free memory
	free( uniglyphs );
	free( jasoglyphs );
}

void HY_Reset_GlyfList_ThreepairJohap( SplineFont *sf, JohapRule * johap )
{
	SplineChar **uniglyphs;
	SplineChar **jasoglyphs;
	SplineChar *sc;
	int *unimap;
	int i, gid;
	int uniglyphcnt = 0, jasoglyphcnt = 0, totalglyphcnt = 0;
	int unicount = 0, jasocount = 0;
	int uniindex, jasoindex;
	int vcode;
	int cho, jung, jong;

	/* Get Glyph Count */
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 && SCWorthOutputting( sf->glyphs[gid] ) )
		{
			if( i <= 0xffff )
			{
				uniglyphcnt++;
			}
			else
			{
				jasoglyphcnt++;
			}      
			totalglyphcnt++;
		}
	}

	if( jasoglyphcnt <= 0 || jasoglyphcnt > sf->glyphcnt )
	{
		ff_post_error( _("Error"),_("Invalid Jaso glyph count %g") );
		return;
	}

	gid = -1;
	unicount = 0;
	jasocount = 0;
	uniindex = 0;
	jasoindex = 0;
	sc = NULL;
	unimap = calloc( (sf->map->enccount - jasoglyphcnt), sizeof(int) );
	uniglyphs = calloc( uniglyphcnt, sizeof(SplineChar *) );
	jasoglyphs = calloc( NUM_JASO_FC + NUM_JASO_MV + NUM_JASO_LC - 1, sizeof(SplineChar *) );
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sc) )
		{
			if( i <= 0xffff )
			{
				unimap[i] = uniindex;
				uniglyphs[uniindex]						= SFSplineCharCreate( sf );
				uniglyphs[uniindex]						= SplineCharCopy( sf->glyphs[gid], sf, NULL );
				uniglyphs[uniindex]->parent					= sf;
				uniglyphs[uniindex]->name					= copy( sf->glyphs[gid]->name );
				uniglyphs[uniindex]->orig_pos				= uniindex;
				if( i == 0 )
				{
					uniglyphs[uniindex]->unicodeenc				= -1;
				}
				else
				{
					uniglyphs[uniindex]->unicodeenc				= i;
				}
				uniglyphs[uniindex]->changed				= 1;
				uniglyphs[uniindex]->views					= sf->glyphs[gid]->views;
				uniglyphs[uniindex]->widthset				= 1;
				uniglyphs[uniindex]->width					= sf->glyphs[gid]->width;
				uniglyphs[uniindex]->vwidth					= sf->glyphs[gid]->vwidth;
				uniglyphs[uniindex]->color					= sf->glyphs[gid]->color;
				uniglyphs[uniindex]->layers[ly_fore].undoes	= NULL;
				uniglyphs[uniindex]->layers[ly_fore].redoes	= NULL;
				uniindex++;
				unicount++;
			}
			else
			{
				if( sc->Varient == 0 )
				{
					jasoglyphs[jasoindex]							= SFSplineCharCreate( sf );
					jasoglyphs[jasoindex]							= SplineCharCopy( sc, sf, NULL );
					jasoglyphs[jasoindex]->parent					= sf;
					jasoglyphs[jasoindex]->name					= copy( sc->name );
					jasoglyphs[jasoindex]->orig_pos					= jasoindex;
					jasoglyphs[jasoindex]->unicodeenc				= -1;
					jasoglyphs[jasoindex]->VCode					= sc->VCode;
					jasoglyphs[jasoindex]->Varient					= sc->Varient;
					jasoglyphs[jasoindex]->bCompositionUnit			= 1;
					jasoglyphs[jasoindex]->changed					= 1;
					jasoglyphs[jasoindex]->views					= sc->views;
					jasoglyphs[jasoindex]->instructions_out_of_date	= sc->instructions_out_of_date;
					jasoglyphs[jasoindex]->widthset					= true;
					jasoglyphs[jasoindex]->width					= sc->width;
					jasoglyphs[jasoindex]->vwidth					= sc->vwidth;
					jasoglyphs[jasoindex]->color					= sc->color;
					jasoglyphs[jasoindex]->bmaster					= 0;
					jasoglyphs[jasoindex]->layers[ly_fore].undoes		= NULL;
					jasoglyphs[jasoindex]->layers[ly_fore].redoes		= NULL;
					jasoindex++;
					if( jasoindex == NUM_JASO_FC + NUM_JASO_MV + NUM_JASO_LC )
					{
						break;
					}
				}
			}
		}
		else
		{
			if( i <= 0xffff )
			{
				unimap[i] = -1;
			}
		}
	}

	/* Write New Rule Component Sfd */
	FILE *newsfd;
	char * filepath;
	char cmd[200];

	if( RecentFiles[0] != NULL )
	{
		filepath = GFileDirNameEx( RecentFiles[0], true );
	}
	else
	{
		filepath = copy(GFileGetHomeDocumentsDir());
	}
	strcat( filepath, TEMP_RULE_SFD );

	newsfd = fopen( filepath, "w" );
	if( newsfd != NULL )
	{
		HY_WriteRuleSFD( newsfd, sf, johap, jasoglyphs, NUM_JASO_FC + NUM_JASO_MV + NUM_JASO_LC - 1 );
		fclose( newsfd );
	}

	SplineFont *imsf;
	imsf = calloc(1, sizeof(SplineFont));
	imsf = LoadSplineFont( filepath, 0 );

	FontView * newfv;
	newfv = FontNew();
	newfv->b.sf->hasRule = true;
	newfv->b.sf->johap = johap;

	_HY_ImportJohapRules_ThreePairMethod( newfv, imsf, unimap, uniglyphs, unicount, jasoglyphs, imsf->glyphcnt );

	free( jasoglyphs );

	_FVMenuClose( (FontView *)sf->fv );
	if( access(filepath, F_OK) == 0 )
	{
		DeleteFile(filepath);
	}
}

static void HY_KoreanBuild_Modify( SplineFont *sf, SplineChar **indexmap, int uniglyphcnt, int cho, int jung, int jong )
{
	JohapRule *	johap	= sf->johap;
	int			uni		= 0xac00 + ( cho * NUM_JASO_MV + jung ) * NUM_JASO_LC + jong;
	SplineChar *	sc		= NULL;
	SplineChar *	chosc	= NULL;
	SplineChar *	jungsc	= NULL;
	SplineChar *	jongsc	= NULL;
	int			cindex, juindex, joindex;
	int			cidx, juidx, joidx;

	int	numGroup[NUM_COMBI_RULE - 1][NUM_JASO_TYPES];
	int	c_grpmap[NUM_COMBI_RULE - 1][NUM_JASO_FC];
	int	ju_grpmap[NUM_COMBI_RULE - 1][NUM_JASO_MV];
	int	jo_grpmap[NUM_COMBI_RULE - 1][NUM_JASO_LC];
	int	c_remap[MAX_JASO_COMBI];
	int	ju_remap[MAX_JASO_COMBI];
	int	jo_remap[MAX_JASO_COMBI];
	int	c_numglyf[NUM_JASO_TYPES][NUM_JASO_FC];
	int	ju_numglyf[NUM_JASO_TYPES][NUM_JASO_MV];
	int	jo_numglyf[NUM_JASO_TYPES][NUM_JASO_LC];
	int	m, g;
	int	index;
	int	nog_fc, nog_mv, nog_lc, fc, mv, lc;
	int	c_totCnt = 0;
	int	ju_totCnt = 0;
	int	count;

	for( m = 0 ; m < NUM_COMBI_RULE - 1 ; m++ )
	{
		RULE * rule	= johap->rule[m];
		JasoGroupList * c_grouplist = rule->grouplist[JASO_FC];
		JasoGroupList * ju_grouplist = rule->grouplist[JASO_MV];
		JasoGroupList * jo_grouplist = rule->grouplist[JASO_LC];
		nog_fc	= c_grouplist->nGroups;
		nog_mv	= ju_grouplist->nGroups;
		nog_lc	= jo_grouplist->nGroups;

		switch( m )
		{
			case RULE_FC:
			{
				for( g = 0 ; g < NUM_JASO_FC ; g++ )
				{
					c_numglyf[m][g] = rule->vcode_NumOfGlyf[g];
					c_totCnt += rule->vcode_NumOfGlyf[g];
				}
			}
			break;
			case RULE_MV:
			{
				for( g = 0 ; g < NUM_JASO_MV ; g++ )
				{
					ju_numglyf[m][g] = rule->vcode_NumOfGlyf[g];
					ju_totCnt += rule->vcode_NumOfGlyf[g];
				}
			}
			break;
			case RULE_LC:
			{
				for( g = 0 ; g < NUM_JASO_LC ; g++ )
				{
					jo_numglyf[m][g] = rule->vcode_NumOfGlyf[g];
				}
			}
			break;
		}

		for( g = 0 ; g < NUM_JASO_FC ; g++ )
		{
			c_grpmap[m][g] = c_grouplist->JasoGroupMap[g];
		}
		for( g = 0 ; g < NUM_JASO_MV ; g++ )
		{
			ju_grpmap[m][g] = ju_grouplist->JasoGroupMap[g];
		}
		for( g = 0 ; g < NUM_JASO_LC ; g++ )
		{
			jo_grpmap[m][g] = jo_grouplist->JasoGroupMap[g];
		}

		for( g = 0 ; g < NUM_JASO_TYPES ; g++ )
		{
			switch( g )
			{
				case JASO_FC:
				{
					numGroup[m][g]	= rule->grouplist[JASO_FC]->nGroups;
				}
				break;
				case JASO_MV:
				{
					numGroup[m][g]	= rule->grouplist[JASO_MV]->nGroups;
				}
				break;
				case JASO_LC:
				{
					numGroup[m][g]	= rule->grouplist[JASO_LC]->nGroups;
				}
				break;
			}

			index = 0;
			switch( m )
			{
				case JASO_FC:
				{
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								c_remap[index] = rule->GlyfIndexMap[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
				case JASO_MV:
				{
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								ju_remap[index] = rule->GlyfIndexMap[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
				case JASO_LC:
				{
					for( fc = 0 ; fc < nog_fc ; fc++ )
					{
						for( mv = 0 ; mv < nog_mv ; mv++ )
						{
							for( lc = 0 ; lc < nog_lc ; lc++ )
							{
								jo_remap[index] = rule->GlyfIndexMap[fc][mv][lc];
								index++;
							}
						}
					}
				}
				break;
			}
		}
	}

	sc = SFMakeChar( sf, sf->map, uni );

	// chosung build
	count = 0;
	for( fc = 0 ; fc < cho ; fc++ )
	{
		count += c_numglyf[0][fc];
	}

	cindex	= ( c_grpmap[0][cho] * numGroup[0][1] + ju_grpmap[0][jung] ) * numGroup[0][2] + jo_grpmap[0][jong];

	cidx		= uniglyphcnt + count + c_remap[cindex];

	chosc	= sf->glyphs[cidx];

	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], chosc );

#if 0
	// jungsung build
	count = 0;
	for( mv = 0 ; mv < jung ; mv++ )
	{
		count += ju_numglyf[1][mv];
	}

	juindex	= ( c_grpmap[1][cho] * numGroup[1][1] + ju_grpmap[1][jung] ) * numGroup[1][2] + jo_grpmap[1][jong];
	juidx	= uniglyphcnt + c_totCnt + count + ju_remap[juindex];
	jungsc	= sf->glyphs[juidx];
	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], jungsc );

	// jongsung build
	count = 0;
	lc = 0;
	for( lc = 0 ; lc < jong ; lc++ )
	{
		count += jo_numglyf[2][lc];
	}
	if( jong != 0 )
	{
		count += 1;
	}

	if( jong > 0 )
	{
		joindex	= ( c_grpmap[2][cho] * numGroup[2][1] + ju_grpmap[2][jung] ) * numGroup[2][2] + jo_grpmap[2][jong];
		joidx	= uniglyphcnt + c_totCnt + ju_totCnt + count + jo_remap[joindex];
		joidx -= 1;
		jongsc	= sf->glyphs[joidx];
	 	HY_SCAddKoreanRef( sf->glyphs[sf->map->map[uni]], jongsc );
	}
#endif
	SFHashGlyph( sf, sf->glyphs[sf->map->map[uni]] );
}

static void _HY_ImportJohapRules_Modify( FontView *fv, SplineFont *imsf, int *unimap, SplineChar **uniglyphs, int uniglyphcnt, SplineChar ** jasoglyphs, int jasoglyphcnt, int * glyfWidth )
{
	SplineFont *sf = fv->b.sf;
	JohapRule *johap;
	RefChar *rf;
	int i, j, gid, jindex = 65536;
	int cho, jung, jong;
	int refcnt = 0;
	int refindex = 0;
	char **refname;
	SplineChar *rsc;

	for( i = 0 ; i < 65536 ; i++ )
	{
		if( unimap[i] != -1 )
		{
			SFAddGlyphAndEncode( sf, uniglyphs[unimap[i]], sf->map, i );
			if( uniglyphs[unimap[i]]->layers[ly_fore].refs != NULL )
			{
				if( i < 0xac00 || i > 0xd7a3 )
				{
					refcnt = HY_GetRefCharCount( uniglyphs[unimap[i]] );
					refname = malloc( refcnt * sizeof(char *));
					for( rf = uniglyphs[unimap[i]]->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
					{
						refname[refindex] = strdup( rf->sc->name );
						refindex++;
					}

					SCClearAll( sf->glyphs[unimap[i]], ly_fore );
					for( j = 0 ; j < refcnt ; j++ )
					{

						rsc = HY_FindNewUniRefChar( uniglyphs, uniglyphcnt, uniglyphs[unimap[i]], refname[j] );
						if( rsc != NULL )
						{
							HY_SCAddKoreanRef( sf->glyphs[unimap[i]], rsc );
						}
						else
						{
							rsc = HY_FindNewJasoRefChar( jasoglyphs, jasoglyphcnt, uniglyphs[unimap[i]], refname[j] );
							if( rsc != NULL )
							{
								HY_SCAddKoreanRef( sf->glyphs[unimap[i]], rsc );
							}
							else
							{
								continue;
							}
						}
					}
					free( refname );
					refindex = 0;
				}
				sf->glyphs[unimap[i]]->widthset = 1;
				sf->glyphs[unimap[i]]->changed = 1;
			}
			sf->glyphs[unimap[i]]->width = glyfWidth[i];
		}
	}

	for( i = 0 ; i < jasoglyphcnt ; i++ )
	{
		SFAddGlyphAndEncode( sf, jasoglyphs[i], sf->map, jindex );
		jindex++;
	}

	int layer = 0;
	for( i = 0xac00 ; i < 0xd7a3 + 1 ; i++ )
	{
		for( layer = 0 ; layer < sf->layer_cnt ; layer++ )
		{
			SCClearAll( sf->glyphs[sf->map->map[i]], layer );
		}
	}

	ff_progress_start_indicator( 10 , _("Korean Build"),_("Building Korean...") , 0 , 11172 , 1 );
	for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
	{
		for( jung = 0 ; jung < NUM_JASO_MV ; jung++ )
		{
			for( jong = 0 ; jong < NUM_JASO_LC ; jong++ )
			{
				HY_KoreanBuild_Modify( sf, jasoglyphs, uniglyphcnt, cho, jung, jong );
				if ( !ff_progress_next())
				{
					break;
				}
			}
		}
	}
	ff_progress_end_indicator();
}

static void HY_KoreanBuild_New( SplineFont * sf, int uni, int cho, int jung, int jong, SplineChar *uniglyphs )
{
	JohapRule *johap = sf->johap;
	SplineChar *nsc = NULL;
	SplineChar *jasosc = NULL;
	int glyfindex = 0;
	int jasoindex = 0;
	int cgroup = 0;
	int jugroup = 0;
	int jogroup = 0;

	nsc = SFMakeChar( sf, sf->map, uni );

	//-----------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------
	glyfindex = 0;
	jasoindex = 0;
	for( jasoindex = 0 ; jasoindex < cho ; jasoindex++ )
	{
		glyfindex += johap->rule[0]->vcode_NumOfGlyf[jasoindex];
	}

	cgroup = johap->rule[0]->grouplist[0]->JasoGroupMap[cho];
	jugroup = johap->rule[0]->grouplist[1]->JasoGroupMap[jung];
	jogroup = johap->rule[0]->grouplist[2]->JasoGroupMap[jong];
	glyfindex += johap->rule[0]->GlyfIndexMap[cgroup][jugroup][jogroup];

	HY_SCAddKoreanRef( nsc, sf->glyphs[glyfindex] );


	//-----------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------
	glyfindex = 0;
	jasoindex = 0;
	for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
	{
		glyfindex += johap->rule[0]->vcode_NumOfGlyf[jasoindex];
	}
	for( jasoindex = 0 ; jasoindex < jung ; jasoindex++ )
	{
		glyfindex += johap->rule[1]->vcode_NumOfGlyf[jasoindex];
	}

	cgroup = johap->rule[1]->grouplist[0]->JasoGroupMap[cho];
	jugroup = johap->rule[1]->grouplist[1]->JasoGroupMap[jung];
	jogroup = johap->rule[1]->grouplist[2]->JasoGroupMap[jong];
	glyfindex += johap->rule[1]->GlyfIndexMap[cgroup][jugroup][jogroup];

	HY_SCAddKoreanRef( nsc, sf->glyphs[glyfindex] );


	//-----------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------
	if( (( uni % 44032 ) % NUM_JASO_LC) != 0 )
	{
		glyfindex = 0;
		jasoindex = 0;
		for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
		{
			glyfindex += johap->rule[0]->vcode_NumOfGlyf[jasoindex];
		}
		for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
		{
			glyfindex += johap->rule[1]->vcode_NumOfGlyf[jasoindex];
		}
		for( jasoindex = 0 ; jasoindex < jong ; jasoindex++ )
		{
			glyfindex += johap->rule[2]->vcode_NumOfGlyf[jasoindex];
		}

		cgroup = johap->rule[2]->grouplist[0]->JasoGroupMap[cho];
		jugroup = johap->rule[2]->grouplist[1]->JasoGroupMap[jung];
		jogroup = johap->rule[2]->grouplist[2]->JasoGroupMap[jong];
		glyfindex += johap->rule[2]->GlyfIndexMap[cgroup][jugroup][jogroup];

		HY_SCAddKoreanRef( nsc, sf->glyphs[glyfindex] );
	}

	nsc->widthset		= 1;
	nsc->width		= uniglyphs->width;
	nsc->vwidth		= uniglyphs->vwidth;
	nsc->color		= uniglyphs->color;
}

void HY_Reset_GlyfList_Normal_New( SplineFont *sf, JohapRule *johap )
{
	int totalglyphcnt = 0;
	int uniglyphcnt = 0;
	int jasoglyphcnt = 0;
	int newjasoglyphcnt = 0;
	int newglyphcnt = 0;
	int i, j, k, gid = -1;

	SplineChar **jasoglyphs;
	SplineChar **uniglyphs;
	SplineChar **indexmap;

	SplineChar * sc = NULL;
	int glyphindex = 0;


	/* Get Glyphs Count */
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 && SCWorthOutputting( sf->glyphs[gid] ) )
		{
			if( i <= 0xffff )
			{
				uniglyphcnt++;
			}
			else
			{
				jasoglyphcnt++;
			}      
			totalglyphcnt++;
		}
	}

	if( jasoglyphcnt <= 0 || jasoglyphcnt > sf->glyphcnt )
	{
		ff_post_error( _("Error"), _("Invalid Jaso glyph count %g"), jasoglyphcnt );
		return;
	}

	/* Copy Original Glyphs */
	jasoglyphs = calloc( jasoglyphcnt, sizeof(SplineChar *) );
	glyphindex = 0;
	for( i = 0 ; i < sf->map->enccount; i++ )
	{
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sf->glyphs[gid]) )
		{
			if( i > 0xffff )
			{
				jasoglyphs[glyphindex]						= SplineCharCopy( sf->glyphs[gid], NULL, NULL );
				jasoglyphs[glyphindex]->unicodeenc			= -1;
				jasoglyphs[glyphindex]->widthset			= 1;
				jasoglyphs[glyphindex]->width				= sf->glyphs[gid]->width;
				jasoglyphs[glyphindex]->vwidth				= sf->glyphs[gid]->vwidth;
				jasoglyphs[glyphindex]->color				= sf->glyphs[gid]->color;
				glyphindex++;
			}
		}
	}

	uniglyphs = calloc( 65536, sizeof(SplineChar *) );
	for( i = 0 ; i < 65536 ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 )
		{
			uniglyphs[i]					= SplineCharCopy( sf->glyphs[gid], NULL, NULL );
			if( i == 0 )
			{
				uniglyphs[i]->unicodeenc	= -1;	// .Notdef
			}
			else
			{
				uniglyphs[i]->unicodeenc	= i;
			}
			uniglyphs[i]->name			= copy( sf->glyphs[gid]->name );
			uniglyphs[i]->views			= sf->glyphs[gid]->views;
			uniglyphs[i]->widthset			= 1;
			uniglyphs[i]->width			= sf->glyphs[gid]->width;
			uniglyphs[i]->vwidth			= sf->glyphs[gid]->vwidth;
			uniglyphs[i]->color				= sf->glyphs[gid]->color;
		}
		else
		{
			uniglyphs[i] = NULL;
		}
	}

	/* Get New Jaso Count and MaxLine Count */
	for( i = 0 ; i < NUM_COMBI_RULE - 1 ; i++ )
	{
		switch( i )
		{
			case RULE_FC:
			{
				for( j = 0 ; j < NUM_JASO_FC ; j++ )
				{
					newglyphcnt += johap->rule[i]->vcode_NumOfGlyf[j];
				}
			}
			break;
			case RULE_MV:
			{
				for( j = 0 ; j < NUM_JASO_MV ; j++ )
				{
					newglyphcnt += johap->rule[i]->vcode_NumOfGlyf[j];
				}
			}
			break;
			case RULE_LC:
			{
				for( j = 0 ; j < NUM_JASO_LC ; j++ )
				{
					if( j == 0 )
					{
						johap->rule[i]->vcode_NumOfGlyf[j] = 0;
					}
					newglyphcnt += johap->rule[i]->vcode_NumOfGlyf[j];
				}
			}
			break;
		}
	}

	/* Make Changed Glyphs */
	RULE *rule;
	int count = 0;
	int vcode = 0;
	int pos = 0;
	newjasoglyphcnt = 0;
	glyphindex = 0;
	indexmap = calloc( (newglyphcnt + 1), sizeof( SplineChar * ) );
	for( i = 0 ; i < NUM_COMBI_RULE - 1 ; i++ )
	{
		rule	= johap->rule[i];

		switch( i )
		{
			case RULE_FC:
			{
				for( j = 0 ; j < NUM_JASO_FC ; j++ )
				{
					for( k = 0 ; k < rule->vcode_NumOfGlyf[j] ; k++ )
					{
						glyphindex = count + k;
						indexmap[glyphindex] = HY_SCFindOrMakeKoreanComponent( sf, j, k, pos, jasoglyphs, jasoglyphcnt );
						pos++;
						newjasoglyphcnt++;
					}
					count += rule->vcode_NumOfGlyf[j];
					vcode++;
				}
			}
			break;

			case RULE_MV:
			{
				for( j = 0 ; j < NUM_JASO_MV ; j++ )
				{
					for( k = 0 ; k < rule->vcode_NumOfGlyf[j] ; k++ )
					{
						glyphindex = count + k;
						indexmap[glyphindex] = HY_SCFindOrMakeKoreanComponent( sf, NUM_JASO_FC + j, k, pos, jasoglyphs, jasoglyphcnt );
						pos++;
						newjasoglyphcnt++;
					}
					count += rule->vcode_NumOfGlyf[j];
					vcode++;
				}
			}
			break;

			case RULE_LC:
			{
				glyphindex++;
				for( j = 0 ; j < NUM_JASO_LC ; j++ )
				{
					if( vcode != NUM_JASO_FC + NUM_JASO_MV )
					{
						for( k = 0 ; k < rule->vcode_NumOfGlyf[j] ; k++ )
						{
							glyphindex = count + k;
							indexmap[glyphindex] = HY_SCFindOrMakeKoreanComponent( sf, NUM_JASO_FC + NUM_JASO_MV + j, k, pos, jasoglyphs, jasoglyphcnt );
							pos++;
							newjasoglyphcnt++;
						}
					}
					count += rule->vcode_NumOfGlyf[j];
					vcode++;
				}
			}
			break;
		}
	}

	if( newglyphcnt != newjasoglyphcnt )
	{
		free( jasoglyphs );
		free( uniglyphs );
		free( indexmap );
	}

	FVDetachAndRemoveGlyphs( (FontViewBase *)sf->fv );
	FVRemoveUnused( sf->fv );
	FVAddUnencoded( sf->fv, newjasoglyphcnt );

	for( i = 0 ; i < sf->map->enccount; i++ )
	{
		sf->map->map[i] = -1;
	}

	for( i = 0 ; i < sf->glyphcnt; i++ )
	{
		sf->map->backmap[i] = -1;
	}
	sf->glyphcnt = sf->glyphmax = 0;

	GlyphHashFree( sf );
	FontViewReformatAll( sf );


	// Set Jaso Glyph
	glyphindex = 0;
	int mapindex = 65536 + glyphindex;
	for( i = 0 ; i < newjasoglyphcnt ; i++ )
	{
		sf->map->map[65536 + i] = i;
		sf->map->backmap[sf->map->map[65536 + i]] = 65536 + i;

		sf->glyphs[i]					= SFSplineCharCreate( sf );
		sf->glyphs[i]					= SplineCharCopy( indexmap[i], sf, NULL );
		sf->glyphs[i]->parent			= sf;
		sf->glyphs[i]->name			= copy( indexmap[i]->name );
		sf->glyphs[i]->views			= indexmap[i]->views;
		sf->glyphs[i]->unicodeenc		= -1;
		sf->glyphs[i]->orig_pos		= sf->map->map[65536 + i];
		sf->glyphs[i]->bCompositionUnit	= 1;
		sf->glyphs[i]->VCode			= indexmap[i]->VCode;
		sf->glyphs[i]->Varient			= indexmap[i]->Varient;
		sf->glyphs[i]->widthset		= 1;
		sf->glyphs[i]->width			= sf->ascent + sf->descent;
		sf->glyphs[i]->vwidth			= sf->ascent + sf->descent;
		sf->glyphs[i]->color			= indexmap[i]->color;

		SFHashGlyph( sf, sf->glyphs[i] );

		sf->glyphcnt++;
	}
	sf->glyphmax = sf->glyphcnt + 10;
	FVRemoveUnused( sf->fv );
	FontViewReformatAll( sf );

	// Set Unicode Glyph
	int cho = 0, jung = 0, jong = 0;
	ff_progress_start_indicator( 10 , _("Korean Build"),_("Building Korean...") , 0 , uniglyphcnt, 1 );
	for( i = 0 ; i < 65536 ; i++ )
	{
		if( uniglyphs[i] != NULL )
		{
			if( i < 0xac00 || i > 0xd7a3 )
			{
				SFMakeChar( sf, sf->map, i );

				sf->glyphs[sf->map->map[i]]				= SplineCharCopy( uniglyphs[i], sf, NULL );
				sf->glyphs[sf->map->map[i]]->unicodeenc	= uniglyphs[i]->unicodeenc;
				sf->glyphs[sf->map->map[i]]->orig_pos		= sf->map->map[i];
				sf->glyphs[sf->map->map[i]]->name		= copy( uniglyphs[i]->name );
				sf->glyphs[sf->map->map[i]]->views		= uniglyphs[i]->views;
				sf->glyphs[sf->map->map[i]]->widthset		= 1;
				sf->glyphs[sf->map->map[i]]->width		= uniglyphs[i]->width;
				sf->glyphs[sf->map->map[i]]->vwidth		= uniglyphs[i]->vwidth;
				sf->glyphs[sf->map->map[i]]->color		= uniglyphs[i]->color;
			}
			else
			{
				cho = ( i - 0xac00 ) / ( NUM_JASO_MV * NUM_JASO_LC );
				jung = ( ( i - 0xac00 ) - ( cho * NUM_JASO_MV * NUM_JASO_LC ) ) / NUM_JASO_LC;
				jong = ( i - 0xac00 ) % NUM_JASO_LC;
				HY_KoreanBuild_New( sf, i, cho, jung, jong, uniglyphs[i] );
			}
			if ( !ff_progress_next())
			{
				break;
			}
		}
	}
	ff_progress_end_indicator();


	// Set Glyphs Dependecy
	RefChar *ref;
	int refcount = 0;
	int refindex = 0;
	int refenc[HY_REFCHAR_MAX];
	real trans[HY_REFCHAR_MAX][6];
	int refglyfindex = 0;

	for( i = 0 ; i < 65536 ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 )
		{
			if( i < 0xac00 || i > 0xd7a3 )
			{
				refcount = 0;
				refindex = 0;

				for( ref = sf->glyphs[gid]->layers[ly_fore].refs ; ref != NULL ; ref = ref->next )
				{
					refenc[refindex] = ref->unicode_enc;
					trans[refindex][0] = ref->transform[0];
					trans[refindex][1] = ref->transform[1];
					trans[refindex][2] = ref->transform[2];
					trans[refindex][3] = ref->transform[3];
					trans[refindex][4] = ref->transform[4];
					trans[refindex][5] = ref->transform[5];
					refindex++;
					refcount++;
				}

				SCRemoveDependents( sf->glyphs[gid] );

				for( j = 0 ; j < refcount ; j++ )
				{
					refglyfindex = HY_FindNewUniRefChar_New( sf, refenc[j] );
					if( refglyfindex != -1 )
					{
						HY_SCAddKoreanRef_New( sf->glyphs[gid], sf->glyphs[refglyfindex], trans[j] );
					}
				}
			}
		}
	}

	free( jasoglyphs );
	free( uniglyphs );
	free( indexmap );
}

void HY_Reset_GlyfList_Normal( SplineFont *sf, JohapRule *johap )
{
	int totalglyphcnt = 0;
	int uniglyphcnt = 0;
	int jasoglyphcnt = 0;
	int newjasoglyphcnt = 0;
	int newglyphcnt = 0;
	int i, j, k, gid = -1;
	int unicount = 0, uniindex = 0;
	int jasocount = 0, jasoindex = 0;
	SplineChar **jasoglyphs;
	SplineChar **uniglyphs;
	SplineChar **indexmap;
	int *unimap;
	int * glyfWidth;
	int scindex = 0;
	SplineChar * sc = NULL;

	/* Get Glyph Count */
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 && SCWorthOutputting( sf->glyphs[gid] ) )
		{
			if( i <= 0xffff )
			{
				uniglyphcnt++;
			}
			else
			{
				jasoglyphcnt++;
			}      
			totalglyphcnt++;
		}
	}

	if( jasoglyphcnt <= 0 || jasoglyphcnt > sf->glyphcnt )
	{
		ff_post_error( _("Error"), _("Invalid Jaso glyph count %g"), jasoglyphcnt );
		return;
	}

	/* Copy Glyphs */
	gid = -1;
	unicount = 0;
	jasocount = 0;
	uniindex = 0;
	jasoindex = 0;
	sc = NULL;

	glyfWidth = calloc( (sf->map->enccount - jasoglyphcnt), sizeof(int) );
	unimap = calloc( (sf->map->enccount - jasoglyphcnt), sizeof(int) );
	uniglyphs = calloc( uniglyphcnt, sizeof(SplineChar *) );
	jasoglyphs = calloc( jasoglyphcnt, sizeof(SplineChar *) );
	for( i = 0 ; i < sf->map->enccount; i++ )
	{
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sf->glyphs[gid]) )
		{
			if( i <= 0xffff )
			{
				unimap[i] = uniindex;
				glyfWidth[i]									= sf->glyphs[gid]->width;

				uniglyphs[uniindex]							= SFSplineCharCreate( sf );
				uniglyphs[uniindex]							= SplineCharCopy( sf->glyphs[gid], sf, NULL );
				uniglyphs[uniindex]->parent						= sf;
				uniglyphs[uniindex]->name						= copy( sf->glyphs[gid]->name );
				uniglyphs[uniindex]->orig_pos					= uniindex;
				if( i == 0 )
				{
					uniglyphs[uniindex]->unicodeenc				= -1;
				}
				else
				{
					uniglyphs[uniindex]->unicodeenc				= i;
				}
				uniglyphs[uniindex]->changed					= 1;
				uniglyphs[uniindex]->views						= sf->glyphs[gid]->views;
				uniglyphs[uniindex]->widthset					= 1;
				uniglyphs[uniindex]->width						= sf->glyphs[gid]->width;
				uniglyphs[uniindex]->vwidth						= sf->glyphs[gid]->vwidth;
				uniglyphs[uniindex]->color						= sf->glyphs[gid]->color;
				uniglyphs[uniindex]->layers[ly_fore].undoes		= NULL;
				uniglyphs[uniindex]->layers[ly_fore].redoes		= NULL;
				uniindex++;
				unicount++;
			}
			else
			{
				jasoglyphs[jasoindex]							= SFSplineCharCreate( sf );
				jasoglyphs[jasoindex]							= SplineCharCopy( sf->glyphs[gid], sf, NULL );
				jasoglyphs[jasoindex]->parent					= sf;
				jasoglyphs[jasoindex]->name					= copy( sf->glyphs[gid]->name );
				jasoglyphs[jasoindex]->orig_pos					= sf->glyphs[gid]->orig_pos;
				jasoglyphs[jasoindex]->unicodeenc				= -1;
				jasoglyphs[jasoindex]->VCode					= sf->glyphs[gid]->VCode;
				jasoglyphs[jasoindex]->Varient					= sf->glyphs[gid]->Varient;
				jasoglyphs[jasoindex]->bCompositionUnit			= 1;
				jasoglyphs[jasoindex]->changed					= 1;
				jasoglyphs[jasoindex]->views					= sf->glyphs[gid]->views;
				jasoglyphs[jasoindex]->widthset					= 1;
				jasoglyphs[jasoindex]->width					= sf->glyphs[gid]->width;
				jasoglyphs[jasoindex]->vwidth					= sf->glyphs[gid]->vwidth;
				jasoglyphs[jasoindex]->color					= sf->glyphs[gid]->color;
				jasoglyphs[jasoindex]->bmaster					= 0;
				jasoglyphs[jasoindex]->layers[ly_fore].undoes		= NULL;
				jasoglyphs[jasoindex]->layers[ly_fore].redoes		= NULL;
				jasoindex++;
				jasocount++;
			}
		}
		else
		{
			if( i <= 0xffff )
			{
				unimap[i] = -1;
				glyfWidth[i] = -1;
			}
		}
	}

	/* Check Glyphs Count */
	if( unicount != uniglyphcnt )
	{
		ff_post_error( _("Error"), _("Invalid Unicode glyph count (unicount: %d, uniglyphcnt: %d)"), unicount, uniglyphcnt );
		return;
	}

	if( jasocount != jasoglyphcnt )
	{
		ff_post_error( _("Error"), _("Invalid Jaso glyph count (jasocount: %d, jasoglyphcnt: %d)"), jasocount, jasoglyphcnt );
		return;
	}

	/* Get New Jaso Count and MaxLine Count */
	for( i = 0 ; i < NUM_COMBI_RULE - 1 ; i++ )
	{
		switch( i )
		{
			case RULE_FC:
			{
				for( j = 0 ; j < NUM_JASO_FC ; j++ )
				{
					newglyphcnt += johap->rule[i]->vcode_NumOfGlyf[j];
				}
			}
			break;
			case RULE_MV:
			{
				for( j = 0 ; j < NUM_JASO_MV ; j++ )
				{
					newglyphcnt += johap->rule[i]->vcode_NumOfGlyf[j];
				}
			}
			break;
			case RULE_LC:
			{
				for( j = 0 ; j < NUM_JASO_LC ; j++ )
				{
					if( j == 0 )
					{
						if( johap->rule[i]->vcode_NumOfGlyf[j] != 0 )
						{
							johap->rule[i]->vcode_NumOfGlyf[j] = 0;
						}
					}
					newglyphcnt += johap->rule[i]->vcode_NumOfGlyf[j];
				}
			}
			break;
		}
	}

	/* Make Changed Glyphs */
	RULE *rule;
	int count = 0;
	int vcode = 0;
	char *tempName;
	int pos = uniglyphcnt;
	newjasoglyphcnt = 0;

	indexmap = calloc( (newglyphcnt + 1), sizeof( SplineChar * ) );
	for( i = 0 ; i < NUM_COMBI_RULE - 1 ; i++ )
	{
		rule	= johap->rule[i];

		switch( i )
		{
			case RULE_FC:
			{
				for( j = 0 ; j < NUM_JASO_FC ; j++ )
				{
					for( k = 0 ; k < rule->vcode_NumOfGlyf[j] ; k++ )
					{
						scindex = count + k;
						indexmap[scindex] = HY_SCFindOrMakeKoreanComponent( sf, j, k, pos, jasoglyphs, jasoglyphcnt );
						pos++;
						newjasoglyphcnt++;
					}
					count += rule->vcode_NumOfGlyf[j];
					vcode++;
				}
			}
			break;

			case RULE_MV:
			{
				for( j = 0 ; j < NUM_JASO_MV ; j++ )
				{
					for( k = 0 ; k < rule->vcode_NumOfGlyf[j] ; k++ )
					{
						scindex = count + k;
						indexmap[scindex] = HY_SCFindOrMakeKoreanComponent( sf, NUM_JASO_FC + j, k, pos, jasoglyphs, jasoglyphcnt );
						pos++;
						newjasoglyphcnt++;
					}
					count += rule->vcode_NumOfGlyf[j];
					vcode++;
				}
			}
			break;

			case RULE_LC:
			{
				scindex++;
				for( j = 0 ; j < NUM_JASO_LC ; j++ )
				{
					if( vcode != NUM_JASO_FC + NUM_JASO_MV )
					{
						for( k = 0 ; k < rule->vcode_NumOfGlyf[j] ; k++ )
						{
							scindex = count + k;
							indexmap[scindex] = HY_SCFindOrMakeKoreanComponent( sf, NUM_JASO_FC + NUM_JASO_MV + j, k, pos, jasoglyphs, jasoglyphcnt );
							pos++;
							newjasoglyphcnt++;
						}
					}
					count += rule->vcode_NumOfGlyf[j];
					vcode++;
				}
			}
			break;
		}
	}

	/* Write jaso glyph Components sfd file */
	FILE *newsfd;
	char * filepath;
	char cmd[200];

#if 0
	filepath = copy(GFileGetHomeDocumentsDir());
	if( access(filepath, F_OK) == 0 )
	{
		strcat( filepath, "한글틀/" );
		strcat( filepath, TEMP_RULE_SFD );
	}
	else
	{
		fprintf(stderr, "Failed get Home Documents Dir...\n" );
		ff_post_error(_("Error"),_("Failed get Home Documents Dir...") );
		free( jasoglyphs );
		free( indexmap );
		free( glyfWidth );
		return;
	}
#else
	filepath = copy(GFileGetHomeDocumentsDir());
	if( access(filepath, F_OK) == 0 )
	{
		strcat( filepath, TEMP_RULE_SFD );
	}
	else
	{
		fprintf(stderr, "Failed get Home Documents Dir...\n" );
		ff_post_error( _("Error"), _("Failed get Home Documents Dir...") );
		free( jasoglyphs );
		free( indexmap );
		free( glyfWidth );
		return;

	}
#endif

	newsfd = fopen( filepath, "w" );
	if( newsfd != NULL )
	{
		HY_WriteRuleSFD( newsfd, sf, johap, indexmap, newjasoglyphcnt );
		fclose( newsfd );
	}

	SplineFont *imsf;
	imsf = calloc(1, sizeof(SplineFont));
	imsf = LoadSplineFont( filepath, 0 );

	/* Create New FontView & Copy font info... */
	FontView * newfv;
	newfv = FontNew();
	newfv->b.sf->hasRule			= true;
	newfv->b.sf->johap			= johap;
	newfv->b.sf->ascent			= imsf->ascent;
	newfv->b.sf->descent			= imsf->descent;
	newfv->b.sf->weight			= copy( imsf->weight );
	newfv->b.sf->fontname		= copy( imsf->fontname );
	newfv->b.sf->familyname		= copy( imsf->familyname );
	newfv->b.sf->fullname			= copy( imsf->fullname );
	newfv->b.sf->copyright		= copy( imsf->copyright );
	newfv->b.sf->xuid				= copy( imsf->xuid );
	newfv->b.sf->version			= copy( imsf->version );
	newfv->b.sf->vert_base		= imsf->vert_base;
	newfv->b.sf->hasvmetrics		= imsf->hasvmetrics;
	newfv->b.sf->italicangle		= imsf->italicangle;
	newfv->b.sf->upos			= imsf->upos;
	newfv->b.sf->uwidth			= imsf->uwidth;
	newfv->b.sf->top_enc			= em_unicode;
	newfv->b.sf->pfminfo			= imsf->pfminfo;
	newfv->b.sf->display_size		= imsf->display_size;
	newfv->b.sf->display_antialias	= imsf->display_antialias;
	newfv->b.sf->names			= TTFLangNamesCopy( imsf->names );
	newfv->b.sf->private			= PSDictCopy( imsf->private );
	newfv->b.sf->layers			= imsf->layers;
	newfv->b.sf->onlybitmaps		= false;

	_HY_ImportJohapRules_Modify( newfv, imsf, unimap, uniglyphs, uniglyphcnt, indexmap, newjasoglyphcnt, glyfWidth );
	newfv->b.sf->changed = true;
	imsf->changed = false;

	free( imsf );
	free( jasoglyphs );
	free( indexmap );
	free( glyfWidth );

	_FVMenuClose( (FontView *)sf->fv );
	if( access(filepath, F_OK) == 0 )
	{
		DeleteFile(filepath);
	}
}

static int HY_JohapIndexExpose( GWindow pixmap, GEvent * event, JohapRule * johap, GRect * rect )
{
	RULE *		rule			= johap->rule[johap->ruletype];
	int			colcnt		= rule->IndexArr_ColCnt;
	int			rowcnt		= rule->IndexArr_RowCnt;
	int			voff			= rule->offtop;

	int			cellwidth	= CELLWIDTH;
	int			cellheight	= CELLHEIGHT;
	int			leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int			topborder	= JOHAP_INDEX_TOP_BORDER;
	int			innergap	= 5;
	int			maxRowCnt;
	int			maxLineCnt;

	char			imgpath[MAX_FILE_PATH_LEN];
	GImage *	img;

	char			buffer[32];
	int			c, ju, jo;
	int			dx = 0, dy = 0;
	int			dwidth = 0, dheight = 0;
	int			index;
	int 			i, j;
	int			as, ds, ld;

	GRect size;
	GDrawGetSize( johap->johapindex_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 2 ) ) / cellheight;
	maxLineCnt = ( rowcnt > maxRowCnt ) ? maxRowCnt : rowcnt ;

	GDrawFillRect( pixmap, &size, 0xffffff );
	GDrawSetFont( pixmap, johap->fontset[0] );
	GDrawWindowFontMetrics( pixmap, johap->fontset[0], &as, &ds, &ld);

	dx = leftborder + ( cellwidth * 2 ) + 5;
	dy = topborder + cellheight - ds - 5;
	GDrawDrawText8( pixmap, dx, dy, _("Ju"), -1, 0xff0000 );

	dx = leftborder + innergap;
	dy = topborder + ( cellheight * 2 ) - ds - 5;
	GDrawDrawText8( pixmap, dx, dy, _("Jo"), -1, 0xff0000 );

	dx = leftborder + cellwidth + innergap;
	dy = topborder + ( cellheight * 2 ) - ds - 5;
	GDrawDrawText8( pixmap, dx, dy, _("Cho"), -1, 0xff0000 );

	for( i = 0 ; i < 2 ; ++i )
	{
		dx = leftborder + ( cellwidth * i );
		dy = topborder + cellheight;
		dwidth = 0;
		if( rowcnt > maxRowCnt )
		{
			dheight = cellheight * ( maxRowCnt + 1 );
		}
		else
		{
			dheight = cellheight * ( rowcnt + 1 );
		}
		GDrawDrawLine( pixmap, dx, dy, dx, dy + dheight, johapindexfgColor );
	}

	dx = leftborder + ( cellwidth * i );
	dy = topborder + cellheight;
	dwidth = 0;
	if( rowcnt > maxRowCnt )
	{
		dheight = cellheight * ( maxRowCnt + 1 );
	}
	else
	{
		dheight = cellheight * ( rowcnt + 1 );
	}
	GDrawDrawLine( pixmap,  dx, dy, dx, dy + dheight, johapindexfgColor );

	for( i = 0 ; i <= colcnt ; ++i )
	{
		dx = leftborder + ( cellwidth * 2 ) + ( cellwidth * i );
		if( i == 0 )
		{
			dx += 1;
		}
		dy = topborder + cellheight;
		dwidth = 0;
		if( rowcnt > maxRowCnt )
		{
			dheight = cellheight * ( maxRowCnt + 1 );
		}
		else
		{
			dheight = cellheight * ( rowcnt + 1 );
		}
		GDrawDrawLine( pixmap, dx, dy, dx, dy + dheight, johapindexfgColor );
	}

	dx = leftborder;
	dy = topborder + ( cellheight * 2 );
	dwidth = ( cellwidth * 2 ) + ( cellwidth * colcnt );
	dheight = 0;
	GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

	// hori line
	for( i = 0 ; i <= maxLineCnt + 1 ; ++i )
	{
		c = ( i - 1 + voff ) % rule->grouplist[JASO_FC]->nGroups;
		if( i == 0 || i == maxLineCnt + 1 || c == 0 )
		{
			dx = leftborder;
			dy = topborder + ( cellheight * i ) + cellheight;
			dwidth = ( cellwidth * 2 ) + ( cellwidth * colcnt );
			dheight = 0;

			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
		else
		{
			dx = leftborder + cellwidth;
			dy = topborder + ( cellheight * i ) + cellheight;
			dwidth = cellwidth * ( colcnt + 1 );
			dheight = 0;

			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor  );
		}
	}


	JasoGroupList * cholist = rule->grouplist[JASO_FC];
	JasoGroupList * julist = rule->grouplist[JASO_MV];
	JasoGroupList * jolist = rule->grouplist[JASO_LC];
	JasoGroupItem * joitem = jolist->jasoitem;
	int testval;
	int indexcnt = 0;
	unichar_t buf[2];

	dx = leftborder + ( cellwidth * 2 ) + innergap;
	dy = topborder + ( cellheight * 2 ) - ds - 5;

	JasoGroupItem * juitem = julist->jasoitem;
	for( i = 0 ; i < julist->nGroups ; i++ )
	{
		buf[0] = 0x314f + JungSungIdx[juitem->JasoVCode[0]];
		buf[1] = '\0';
		GDrawDrawText( pixmap, dx, dy, buf, 1, 0x000000 );
		dx += cellwidth;
		juitem = juitem->next;
	}

	dx = leftborder + innergap;
	dy = topborder + ( cellheight * 2 ) + innergap;
	for( j = 0 ; j < jolist->nGroups ; j++ )
	{
		JasoGroupItem * choitem = cholist->jasoitem;

		for( i = 0 ; i < cholist->nGroups ; i++ )
		{
			indexcnt = 0;

			testval = indexcnt = ( j * cholist->nGroups ) + i;
			if( testval >= voff )
			{
				if( indexcnt >= maxLineCnt + voff )
				{
					break;
				}
				index = choitem->JasoVCode[0];
				buf[0] = 0x3131 + ChoSungIdx[index];
				buf[1] = '\0';
				GDrawDrawText( pixmap, dx + cellwidth, dy + 15, buf, 1, 0x000000 );
				dy += cellheight;
			}
			choitem = choitem->next;

			if( i == 0 )
			{
				index = joitem->JasoVCode[0];
				if( index == 0 )
				{
					buf[0] = 0x318d;
					buf[1] = '\0';
				}
				else
				{
					buf[0] = 0x11a8 + index-1;
					buf[1] = '\0';
				}
				if( dy + 20 - cellheight > topborder + ( cellheight * 2 ) )
				{
					GDrawDrawText( pixmap, dx, dy + 15 - cellheight, buf, -1, 0x000000 );
				}
			}
		}

		joitem = joitem->next;
	}

	dx = leftborder + ( cellwidth * 2 ) + innergap;
	dy = topborder + ( cellheight * 3 ) - ds - 5;
	for( i = 0 ; i < maxLineCnt ; ++i )
	{
		jo = ( i + voff ) / rule->grouplist[JASO_FC]->nGroups;
		c = (  i + voff ) % rule->grouplist[JASO_FC]->nGroups;

		for( j = 0 ; j < colcnt ; ++j )
		{
			ju = j;
			sprintf( buffer, "%d", rule->tmpgindex[c][ju][jo] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += cellwidth;
		}
		dx = leftborder + ( cellwidth * 2 ) + innergap;
		dy += cellheight;
	}
	return( true );
}

static int HY_JohapIndexFinishUp( JohapRule * johap )
{
	const unichar_t * ret;
	unichar_t * end;
	char * sval;
	int val;
	int ival;

	int ruletype = johap->ruletype;
	RULE * rule = johap->rule[ruletype];
	int jasotype = rule->jasotype;

	int tf_cho = rule->tf_cho;
	int tf_ju = rule->tf_ju;
	int tf_jo = rule->tf_jo;
	int tf_col = rule->tf_col;
	int tf_row = rule->tf_row;

	int nog_fc, nog_mv, nog_lc, fc, mv, lc, last_gi;
	int i, j, k;

	nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	nog_lc	= rule->grouplist[JASO_LC]->nGroups;

	if( rule->tf_col == -1 )
	{
		return( true );
	}

	sval = GGadgetGetTitle8( johap->tf );
	ival = atoi( sval );
	ret = _GGadgetGetTitle( johap->tf );
	val = u_strtol( ret, &end, 10 );

	if( *end != '\0' || *ret == '\0' || val < 0 )
	{
		ff_post_error( _("Error"), _("Invalid Johap Index Value") );
		return( false );
	}

	if( ruletype != RULE_WIDTH )
	{
		JasoGroupList * grouplist;
		switch( ruletype )
		{
			case RULE_FC:
			{
				grouplist = rule->grouplist[JASO_FC];
				for( fc = 0 ; fc < nog_fc ; ++fc )
				{
					last_gi = 0;
					for( lc = 0 ; lc < nog_lc ; ++lc )
					{
						for( mv = 0 ; mv < nog_mv ; ++mv )
						{
							if( tf_cho == fc && tf_ju == mv && tf_jo == lc )
							{
								rule->tmpgindex[fc][mv][lc] = ival;
							}
							last_gi = ( last_gi >= rule->tmpgindex[fc][mv][lc] ) ? last_gi : rule->tmpgindex[fc][mv][lc];
						}

						for( k = 0 ; k < NUM_JASO_FC ; k++ )
						{
							if( grouplist->JasoGroupMap[k] == fc )
							{
								rule->vcode_NumOfGlyf[k] = last_gi + 1;
							}
						}
					}
				}
			}
			break;
			case RULE_MV:
			{
				grouplist = rule->grouplist[JASO_MV];
				for( mv = 0 ; mv < nog_mv ; ++mv )
				{
					last_gi = 0;
					for( lc = 0 ; lc < nog_lc ; ++lc )
					{
						for( fc = 0 ; fc < nog_fc ; ++fc )
						{
							if( tf_cho == fc && tf_ju == mv && tf_jo == lc )
							{
								rule->tmpgindex[fc][mv][lc] = ival;
							}
							last_gi = ( last_gi >= rule->tmpgindex[fc][mv][lc] ) ? last_gi : rule->tmpgindex[fc][mv][lc];
						}
					}

					for( k = 0 ; k < NUM_JASO_MV ; k++ )
					{
						if( grouplist->JasoGroupMap[k] == mv )
						{
							rule->vcode_NumOfGlyf[k] = last_gi + 1;
						}
					}
				}
			}
			break;
			case RULE_LC:
			{
				grouplist = rule->grouplist[JASO_LC];
				for( lc = 0 ; lc < nog_lc ; ++lc )
				{
					last_gi = 0;
					for( fc = 0 ; fc < nog_fc ; ++fc )
					{
						for( mv = 0 ; mv < nog_mv ; ++mv )
						{
							if( tf_cho == fc && tf_ju == mv && tf_jo == lc )
							{
								rule->tmpgindex[fc][mv][lc] = ival;
							}
							last_gi = ( last_gi >= rule->tmpgindex[fc][mv][lc] ) ? last_gi : rule->tmpgindex[fc][mv][lc];
						}
					}

					for( k = 0 ; k < NUM_JASO_LC ; k++ )
					{
						if( grouplist->JasoGroupMap[k] == lc )
						{
							rule->vcode_NumOfGlyf[k] = last_gi + 1;
						}
					}
				}
				rule->vcode_NumOfGlyf[0] = 0;
			}
			break;
		}
	}
	HY_SetupJohapIndexView( johap, ruletype );
	GGadgetSetVisible( johap->tf, false );
	rule->tf_col = -1;
	GDrawRequestExpose( johap->johapindex_view, NULL, false );
	GDrawRequestExpose( johap->johapindex_subview, NULL, false );
	return( true );
}

static void HY_JohapIndexTFBoxPlace( JohapRule * johap )
{
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;
	int		voff			= johap->rule[johap->ruletype]->offtop;
	int		dx, dy;
	GRect	size;

	if( johap->rule[johap->ruletype]->tf_col == -1 )
	{
		return;
	}
	GGadgetGetSize( johap->tf, &size );
	dx = leftborder + ( cellwidth * 2 ) + ( johap->rule[johap->ruletype]->tf_col * cellwidth ) + 2;
	dy = topborder + ( cellheight * 2 ) + ( johap->rule[johap->ruletype]->tf_row - voff ) * cellheight + cellheight - size.height;
	GGadgetMove( johap->tf, dx, dy );
	GGadgetSetVisible( johap->tf, true );
}

static void HY_JohapIndexActivateTF( JohapRule * johap )
{
	int		nog_fc, nog_mv, nog_lc;
	char		buf[32];

	nog_fc	= johap->rule[johap->ruletype]->grouplist[JASO_FC]->nGroups;
	nog_mv	= johap->rule[johap->ruletype]->grouplist[JASO_MV]->nGroups;
	nog_lc	= johap->rule[johap->ruletype]->grouplist[JASO_LC]->nGroups;

	sprintf( buf, "%d", johap->rule[johap->ruletype]->tfvalue_org );
	GGadgetSetTitle8( johap->tf, buf );

	HY_JohapIndexTFBoxPlace( johap );
	GGadgetSetVisible( johap->tf, true );
	GTextFieldSelect( johap->tf, 0, -1 );
}

static void HY_JohapIndexMouseDown( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	RULE *	rule			= johap->rule[johap->ruletype];
	int		colcnt		= rule->IndexArr_ColCnt;
	int		rowcnt		= rule->IndexArr_RowCnt;
	
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;

	int		nog_fc, nog_mv, nog_lc, fc, mv, lc;
	int		dx, dy, validwidth, validheight;
	int		col = 0, row = 0;
	int		maxRowCnt;
	int		maxColCnt;

	GRect size;
	GDrawGetSize( johap->johapindex_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 2 ) ) / cellheight;
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;

	nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	nog_lc	= rule->grouplist[JASO_LC]->nGroups;

	dx			= leftborder + ( cellwidth * 2 );
	dy			= topborder + ( cellheight * 2 );
	validwidth	= cellwidth * colcnt;
	validheight	= cellheight * rowcnt;

	if( event->u.mouse.x <= dx )
	{
		return;
	}

	if( event->u.mouse.y <= dy )
	{
		return;
	}

	if( event->u.mouse.x >= ( dx + validwidth ) )
	{
		return;
	}

	if( event->u.mouse.y >= ( dy + validheight ) )
	{
		return;
	}

	col	= ( event->u.mouse.x - leftborder - ( cellwidth * 2 ) ) / cellwidth;
	row	= ( event->u.mouse.y - topborder - ( cellheight * 2 ) ) / cellheight;

	if( row >= maxRowCnt )
	{
		return;
	}

	if( col >= maxColCnt )
	{
		return;
	}

	row += rule->offtop;

	rule->tf_col	= col;
	rule->tf_row	= row;
	rule->tf_ju	= col;
	rule->tf_cho	= row % nog_fc;
	rule->tf_jo	= row / nog_fc;

	for( lc = 0 ; lc < nog_lc ; lc++ )
	{
		for( fc = 0 ; fc < nog_fc ; fc++ )
		{
			for( mv = 0 ; mv < nog_mv ; mv++ )
			{
				if( rule->tf_cho == fc && rule->tf_ju == mv && rule->tf_jo == lc )
				{
					rule->tfvalue_org = rule->tmpgindex[fc][mv][lc];
					break;
				}
			}
		}
	}
	HY_JohapIndexActivateTF( johap );
	GDrawRequestExpose( pixmap, NULL, false );
}

static int johapindex_sub_e_h( GWindow pixmap, GEvent *event )
{
	JohapRule *	johap = GDrawGetUserData( pixmap );

	switch( event->type )
	{
		case et_expose:
		{
			HY_JohapIndexExpose( pixmap, event, johap, &event->u.expose.rect );
		}
		break;
		case et_mousedown:
		{
			JohapRule *	johap = GDrawGetUserData( pixmap );
			if( !HY_JohapIndexFinishUp( johap ) )
			{
				return( true );
			}
			HY_JohapIndexMouseDown( pixmap, event, johap );
		}
		break;
	}
	return( true );
}

static int HY_JohapIndex_HScroll( JohapRule * johap, struct sbevent * sb )
{
	int		newpos		= johap->rule[johap->ruletype]->offleft;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;
	int		maxColCnt	= JOHAP_INDEX_MAX_COL;
	int		totalColCnt	= johap->rule[johap->ruletype]->IndexArr_ColCnt;
	int		row			= johap->rule[johap->ruletype]->IndexArr_RowCnt;
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	GRect	r;

	switch( sb->type )
	{
		case et_sb_top:
		{
			newpos = 0;
		}
		break;

		case et_sb_uppage:
		{
			newpos -= maxColCnt;
		}
		break;

		case et_sb_left:
		{
			--newpos;
		}
		break;

		case et_sb_right:
		{
			++newpos;
		}
		break;

		case et_sb_downpage:
		{
			newpos += maxColCnt;
		}
		break;

		case et_sb_bottom:
		{
			newpos = totalColCnt - maxColCnt;
		}
		break;

		case et_sb_thumb:
		case et_sb_thumbrelease:
		{
			newpos = sb->pos;
		}
		break;
	}

	if( newpos > totalColCnt - maxColCnt )
	{
		newpos = totalColCnt - maxColCnt;
	}

	if( newpos < 0 )
	{
		newpos =0;
	}

	hydebug( "[%s] newpos: (%d)\n", __FUNCTION__, newpos );
	hydebug( "[%s] offleft: (%d)\n", __FUNCTION__, johap->rule[johap->ruletype]->offleft );

	if( newpos != johap->rule[johap->ruletype]->offleft )
	{
		int diff = newpos - johap->rule[johap->ruletype]->offleft;
		johap->rule[johap->ruletype]->offleft = newpos;
		GScrollBarSetPos( johap->rule[johap->ruletype]->hsb, johap->rule[johap->ruletype]->offleft );

		r.x		= leftborder;
		r.y		= topborder + ( cellheight * 2 );
		r.width	= ( cellwidth * 2 ) + ( cellwidth * row );
		r.height	= cellheight * maxColCnt;

		GDrawScroll( johap->johapindex_view, &r, diff * cellwidth, 0 );
	}
	return( true );
}

static int HY_JohapIndex_VScroll( JohapRule * johap, struct sbevent * sb )
{
	int		newpos		= johap->rule[johap->ruletype]->offtop;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;
	int		maxRowCnt;
	int		totalRowCnt	= johap->rule[johap->ruletype]->IndexArr_RowCnt;
	int		col			= johap->rule[johap->ruletype]->IndexArr_ColCnt;
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	GRect	r;

	GRect size;
	GDrawGetSize( johap->johapindex_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 2 ) ) / cellheight;

	switch( sb->type )
	{
		case et_sb_top:
		{
			newpos = 0;
		}
		break;

		case et_sb_uppage:
		{
			newpos -= maxRowCnt;
		}
		break;

		case et_sb_up:
		{
			--newpos;
		}
		break;

		case et_sb_down:
		{
			++newpos;
		}
		break;

		case et_sb_downpage:
		{
			newpos += maxRowCnt;
		}
		break;

		case et_sb_bottom:
		{
			newpos = totalRowCnt - maxRowCnt;
		}
		break;

		case et_sb_thumb:
		case et_sb_thumbrelease:
		{
			newpos = sb->pos;
		}
		break;
	}

	if( newpos > totalRowCnt - maxRowCnt )
	{
		newpos = totalRowCnt - maxRowCnt;
	}

	if( newpos < 0 )
	{
		newpos =0;
	}

	hydebug( "[%s] newpos: (%d)\n", __FUNCTION__, newpos );
	hydebug( "[%s] offtop: (%d)\n", __FUNCTION__, johap->rule[johap->ruletype]->offtop );

	if( newpos != johap->rule[johap->ruletype]->offtop )
	{
		int diff = newpos - johap->rule[johap->ruletype]->offtop;
		johap->rule[johap->ruletype]->offtop = newpos;
		GScrollBarSetPos( johap->rule[johap->ruletype]->vsb, johap->rule[johap->ruletype]->offtop );

		r.x		= leftborder;
		r.y		= topborder + ( cellheight * 2 );
		r.width	= ( cellwidth * 2 ) + ( cellwidth * col );
		r.height	= cellheight * maxRowCnt;

		GDrawScroll( johap->johapindex_view, &r, 0,  diff * cellheight );
	}
	return( true );
}

static void HY_ApplyJohapIndexMap( JohapRule * johap )
{
	RULE * rule	= johap->rule[johap->ruletype];
	int nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc	= rule->grouplist[JASO_LC]->nGroups;
	int cho, ju, jo;

	for( cho = 0 ; cho < nog_fc ; cho++ )
	{
		for( ju = 0 ; ju < nog_mv ; ju++ )
		{
			for( jo = 0 ; jo < nog_lc ; jo++ )
			{
				rule->GlyfIndexMap[cho][ju][jo] = rule->tmpgindex[cho][ju][jo];
			}
		}
	}
	return;
}

static int johapindex_e_h( GWindow pixmap, GEvent *event )
{
	switch( event->type )
	{
		case et_expose:
		{
			GDrawRequestExpose( pixmap, NULL, false );
		}
		break;
		case et_close:
		{
			hydebug( "[%s] et_close\n", __FUNCTION__ );

			JohapRule *	johap = GDrawGetUserData( pixmap );
			HY_RollBackJohapIndex( johap );
			GDrawDestroyWindow( pixmap );
		}
		break;
		case et_controlevent:
		{
			switch( event->u.control.subtype )
			{
				case et_scrollbarchange:
				{
					JohapRule *	johap = GDrawGetUserData( pixmap );
					if( event->u.control.g == johap->rule[johap->ruletype]->vsb )
					{
						HY_JohapIndex_VScroll( johap, &event->u.control.u.sb );
					}
					else
					{
						HY_JohapIndex_HScroll( johap, &event->u.control.u.sb );
					}
					GDrawRequestExpose( johap->johapindex_subview, NULL, false );
				}
				break;
			}
		}
		break;
	}
	return( true );
}

static int HY_JohapIndexOK( GGadget *g, GEvent * e )
{
	if( e->type == et_controlevent && e->u.control.subtype == et_buttonactivate )
	{
		GWindow		gw		= GGadgetGetWindow( g );
		JohapRule *	johap	= GDrawGetUserData( gw );

		if( !HY_JohapIndexFinishUp( johap ) )
		{
			return( false );
		}
		HY_ApplyJohapIndexMap( johap );
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static int HY_JohapIndexCancel( GGadget *g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw	= GGadgetGetWindow( g );
		JohapRule *johap	= GDrawGetUserData( gw );

		HY_RollBackJohapIndex( johap );
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static void HY_DoJohapIndexDlg( JohapRule * johap )
{
	GWindow				gw;
	GWindow				subw;
	GWindowAttrs			wattrs;
	GGadgetCreateData	mgcd[5];
	GGadgetCreateData	mgcd2[2];
	GTextInfo			mlabel[3];
	static GBox			tfbox;
	GRect				pos;
	GRect				size;

	int					leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int					topborder	= JOHAP_INDEX_TOP_BORDER;
	int					maxRowCnt;
	int					maxColCnt;
	int					cellwidth	= CELLWIDTH;
	int					cellheight	= CELLHEIGHT;
	int					btnWidth	= 60;
	int					btnHeight	= 20;
	int					colcnt		= 1;
	int					rowcnt		= 1;
	int					i;

	HY_SetupJohapIndexView( johap, johap->ruletype );

	johap->rule[johap->ruletype]->offtop = 0;
	johap->rule[johap->ruletype]->offleft = 0;
	colcnt	= johap->rule[johap->ruletype]->IndexArr_ColCnt;
	rowcnt	= johap->rule[johap->ruletype]->IndexArr_RowCnt;
	for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
	{
		johap->rule[i]->tf_col = -1;
		johap->rule[i]->tf_row = -1;
	}

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask				= wam_events | wam_cursor | wam_utf8_wtitle | wam_backcol;
	wattrs.background_color	= COLOR_DEFAULT;
	wattrs.event_masks		= -1;
	wattrs.is_dlg				= 0;
	wattrs.restrict_input_to_me	= 1;
	wattrs.undercursor			= 1;
	wattrs.cursor				= ct_pointer;
	wattrs.icon				= NULL;

	int resx =  GetSystemMetrics(SM_CXSCREEN);
	int resy =  GetSystemMetrics(SM_CYSCREEN);

	pos.x		= 0;
	pos.y		= 0;
	pos.width		= 900;
	pos.height	= resy * 0.7;

	gw = johap->johapindex_view = GDrawCreateTopWindow( NULL, &pos, johapindex_e_h, johap, &wattrs );
	if( johap->ruletype == RULE_FC )
	{
		GDrawSetWindowTitles8( gw, _("ChoSung Rule Johap Index"), NULL );
	}
	else if( johap->ruletype == RULE_MV )
	{
		GDrawSetWindowTitles8( gw, _("JungSung Rule Johap Index"), NULL );
	}
	else if( johap->ruletype == RULE_LC )
	{
		GDrawSetWindowTitles8( gw, _("JongSung Rule Johap Index"), NULL );
	}

	pos.x		= 2;
	pos.y		= 5;
	pos.width		= 730;
	pos.height	= pos.height - 20;
	subw = johap->johapindex_subview = GWidgetCreateSubWindow( johap->johapindex_view, &pos, johapindex_sub_e_h, johap, &wattrs );

	GDrawGetSize( johap->johapindex_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 2 ) ) / cellheight;
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;

	// Make Gadgets
	memset( &mgcd, 0, sizeof( mgcd ) );
	memset( &mgcd2, 0, sizeof( mgcd2 ) );
	memset( &mlabel, 0, sizeof( mlabel ) );

	mgcd[0].gd.pos.x				= GDrawPixelsToPoints( NULL, pos.x + pos.width );
	mgcd[0].gd.pos.y				= GDrawPixelsToPoints( NULL, 5 );
	mgcd[0].gd.pos.width			= GDrawPixelsToPoints( NULL, vScrollBarWidth );
	mgcd[0].gd.pos.height			= GDrawPixelsToPoints( NULL, size.height );
	mgcd[0].gd.flags				= gg_visible | gg_enabled | gg_sb_vert;
	mgcd[0].creator				= GScrollBarCreate;

	mgcd[1].gd.pos.x				= GDrawPixelsToPoints( NULL, 5 );
	mgcd[1].gd.pos.y				= GDrawPixelsToPoints( NULL, pos.y + pos.height );
	mgcd[1].gd.pos.width			= GDrawPixelsToPoints( NULL, size.width );
	mgcd[1].gd.pos.height			= GDrawPixelsToPoints( NULL, hScrollBarHeight );
	mgcd[1].gd.flags				= gg_visible | gg_enabled;
	mgcd[1].creator				= GScrollBarCreate;

	mlabel[0].text					= (unichar_t *) _("OK");
	mlabel[0].text_is_1byte			= true;
	mgcd[2].gd.pos.x				= GDrawPixelsToPoints( NULL, 760 );
	mgcd[2].gd.pos.y				= GDrawPixelsToPoints( NULL, 50 );
	mgcd[2].gd.pos.width			= btnWidth;
	mgcd[2].gd.pos.height			= btnHeight;
	mgcd[2].gd.label				= &mlabel[0];
	mgcd[2].gd.flags				= gg_visible | gg_enabled;
	mgcd[2].gd.handle_controlevent	= HY_JohapIndexOK;
	mgcd[2].creator				= GButtonCreate;

	mlabel[1].text					= (unichar_t *) _("Cancel");
	mlabel[1].text_is_1byte			= true;
	mgcd[3].gd.pos.x				= GDrawPixelsToPoints( NULL, 760 );
	mgcd[3].gd.pos.y				= mgcd[2].gd.pos.y + mgcd[2].gd.pos.height + 10;
	mgcd[3].gd.pos.width			= btnWidth;
	mgcd[3].gd.pos.height			= btnHeight;
	mgcd[3].gd.label				= &mlabel[1];
	mgcd[3].gd.flags				= gg_visible | gg_enabled;
	mgcd[3].gd.handle_controlevent	= HY_JohapIndexCancel;
	mgcd[3].creator				= GButtonCreate;
	GGadgetsCreate( gw, mgcd );

	tfbox.main_background			= 0xf0f0f0;
	tfbox.main_foreground			= 0xff0000;
	mgcd2[0].gd.pos.x				= GDrawPixelsToPoints( NULL, leftborder + ( cellwidth * 2 ) + 2 );
	mgcd2[0].gd.pos.y				= GDrawPixelsToPoints( NULL, topborder + ( cellheight * 2 ) + 2 );
	mgcd2[0].gd.pos.width			= cellwidth - 4;
	mgcd2[0].gd.pos.height			= cellheight - 4;
	mgcd2[0].gd.flags				= gg_enabled | gg_dontcopybox | gg_pos_in_pixels;
	mgcd2[0].gd.box				= &tfbox;
	mgcd2[0].creator				= GTextFieldCreate;
	GGadgetsCreate( subw, mgcd2 );

	johap->rule[johap->ruletype]->vsb		= mgcd[0].ret;
	johap->rule[johap->ruletype]->hsb		= mgcd[1].ret;
	johap->rule[johap->ruletype]->ok		= mgcd[3].ret;
	johap->rule[johap->ruletype]->cancel	= mgcd[4].ret;
	johap->tf							= mgcd2[0].ret;
	johap->johapindex_view				= gw;
	johap->johapindex_subview			= subw;


	// vertical scrollbar
	GScrollBarSetBounds( johap->rule[johap->ruletype]->vsb, 0, rowcnt, maxRowCnt );
	GScrollBarSetPos( johap->rule[johap->ruletype]->vsb, johap->rule[johap->ruletype]->offtop );

	// horizontal scrollbar
	GScrollBarSetBounds( johap->rule[johap->ruletype]->hsb, 0, colcnt, maxColCnt );
	GScrollBarSetPos( johap->rule[johap->ruletype]->hsb, johap->rule[johap->ruletype]->offleft );

	GDrawSetVisible( johap->johapindex_subview, true );
	GDrawSetVisible( johap->johapindex_view, true );

	FontRequest	rq;
	int			as, ds, ld;
	memset( &rq, 0, sizeof( rq ) );
	johap->fontset		= calloc( (6<<2), sizeof(GFont *) );
	rq.utf8_family_name	= HY_UI_FAMILIES;
	rq.point_size			= 8;
	rq.weight				= 400;
	johap->fontset[0]		= GDrawInstanciateFont( gw, &rq );
	GDrawSetFont( gw, johap->fontset[0] );
	GDrawWindowFontMetrics( gw, johap->fontset[0], &as, &ds, &ld );
}

static int HY_WidthMapFinishUp( JohapRule * johap )
{
	const unichar_t * ret;
	unichar_t * end;
	char * sval;
	int val;
	int ival;
	int tf_cho = johap->rule[johap->ruletype]->tf_cho;
	int tf_ju = johap->rule[johap->ruletype]->tf_ju;
	int tf_jo = johap->rule[johap->ruletype]->tf_jo;
	int tf_col = johap->rule[johap->ruletype]->tf_col;
	int tf_row = johap->rule[johap->ruletype]->tf_row;

	int ruletype = johap->ruletype;
	RULE * rule = johap->rule[johap->ruletype];
	int jasotype = rule->jasotype;
	int nog_fc, nog_mv, nog_lc;
	int fc, mv, lc;
	int i, j, k, last_gi;

	nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	nog_lc	= rule->grouplist[JASO_LC]->nGroups;

	if( tf_col == -1 || tf_row == -1 )
	{
		return( true );
	}

	for( fc = 0 ; fc < nog_fc ; ++fc )
	{
		last_gi = 0;
		for( lc = 0 ; lc < nog_lc ; ++lc )
		{
			for( mv = 0 ; mv < nog_mv ; ++mv )
			{
				if( tf_cho == fc && tf_ju == mv && tf_jo == lc )
				{
					if( tf_col == 0 )
					{
						sval = GGadgetGetTitle8( johap->tf );
						ival = atoi( sval );
						ret = _GGadgetGetTitle( johap->tf );
						val = u_strtol( ret, &end, 10 );
						if( *end != '\0' || *ret == '\0' )
						{
							ff_post_error( _("Error"), _("Invalid LSB Value") );
							return( false );
						}

						if( rule->tmplsb[fc][mv][lc] != ival )
						{
							rule->tmplsb[fc][mv][lc] = ival;
							last_gi = ( last_gi >= rule->tmplsb[fc][mv][lc] ) ? last_gi : rule->tmplsb[fc][mv][lc];
						}
					}
					else
					{
						if( tf_col % 2 )
						{
							sval = GGadgetGetTitle8( johap->tf );
							ival = atoi( sval );
							ret = _GGadgetGetTitle( johap->tf );
							val = u_strtol( ret, &end, 10 );
							if( *end != '\0' || *ret == '\0' )
							{
								ff_post_error( _("Error"), _("Invalid Advance Width Value") );
								return( false );
							}

							if( rule->tmpwidth[fc][mv][lc] != ival )
							{
								rule->tmpwidth[fc][mv][lc] = ival;
								last_gi = ( last_gi >= rule->tmpwidth[fc][mv][lc] ) ? last_gi : rule->tmpwidth[fc][mv][lc];
							}
						}
						else
						{
							sval = GGadgetGetTitle8( johap->tf );
							//sval = GGadgetGetTitle8( rule->tf );
							ival = atoi( sval );
							ret = _GGadgetGetTitle( johap->tf );
							//ret = _GGadgetGetTitle( rule->tf );
							val = u_strtol( ret, &end, 10 );
							if( *end != '\0' || *ret == '\0' )
							{
								ff_post_error( _("Error"), _("Invalid LSB Value") );
								return( false );
							}

							if( rule->tmplsb[fc][mv][lc] != ival )
							{
								rule->tmplsb[fc][mv][lc] = ival;
								last_gi = ( last_gi >= rule->tmplsb[fc][mv][lc] ) ? last_gi : rule->tmplsb[fc][mv][lc];
							}
						}
					}
				}
			}
		}
	}
	HY_SetupJohapIndexView( johap, johap->ruletype );
	GGadgetSetVisible( johap->tf, false );
	rule->tf_col = -1;
	rule->tf_row = -1;
	GDrawRequestExpose( johap->widthmap_view, NULL, false );
	GDrawRequestExpose( johap->widthmap_subview, NULL, false );
	return( true );
}

static int HY_WidthMapExpose_111( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	RULE *rule		= johap->rule[johap->ruletype];
	int colcnt		= rule->IndexArr_ColCnt;
	int rowcnt		= rule->IndexArr_RowCnt;
	int leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int topborder		= 0;//JOHAP_INDEX_TOP_BORDER;
	int cellwidth		= CELLWIDTH;
	int cellheight		= CELLHEIGHT;
	int innergap		= 5;
	int maxRowCnt;//	= JOHAP_INDEX_MAX_ROW;
	int maxColCnt;//	= JOHAP_INDEX_MAX_COL;
	int maxLineCnt;
	int maxVLineCnt;
	int voff			= rule->offtop;
	int hoff			= rule->offleft;
	int currentrow	= rowcnt - voff;
	int currentcol	= colcnt - hoff;
	int dx, dy, dwidth, dheight, i, j, index = 0;
	int as, ds, ld;

	hydebug( "[%s] hoff: (%d) / voff: (%d)\n", __FUNCTION__, hoff, voff );

	GRect size;
	GDrawGetSize( johap->widthmap_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 3 ) ) / cellheight;
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;
	maxLineCnt = ( rowcnt > maxRowCnt ) ? maxRowCnt : rowcnt ;
	maxVLineCnt	= ( colcnt > maxColCnt ) ? maxColCnt : colcnt ;

	GDrawFillRect( pixmap, &size, 0xffffff );
	GDrawSetFont( pixmap, johap->fontset[0] );
	GDrawWindowFontMetrics( pixmap, johap->fontset[0], &as, &ds, &ld);

	// draw title : "cho", "ju", "jo" */
	dx = leftborder + ( cellwidth * 2 ) + 5;
	dy = topborder + cellheight - ds - 5;
	GDrawDrawText8( pixmap, dx, dy, _("Ju"), -1, 0xff0000 );

	dx = leftborder + innergap;
	dy = topborder + ( cellheight * 2 )+ cellheight - ds - 5;
	GDrawDrawText8( pixmap, dx, dy, _("Jo"), -1, 0xff0000 );

	dx = leftborder + cellwidth + innergap;
	dy = topborder + ( cellheight * 2 ) + cellheight - ds - 5;
	GDrawDrawText8( pixmap, dx, dy, _("Cho"), -1, 0xff0000 );


	JasoGroupList * cholist = rule->grouplist[JASO_FC];
	JasoGroupList * julist = rule->grouplist[JASO_MV];
	JasoGroupList * jolist = rule->grouplist[JASO_LC];

	/* draw H-Line */
	for( i = 0 ; i < 4 ; i++ )
	{
		if( i == 1 )
		{
			dx = leftborder;
			dy = topborder + (cellheight * i ) + cellheight;
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (julist->nGroups - hoff));
		}
		else if( i == 3 )
		{
			dx = leftborder;
			dy = topborder + (cellheight * (i - 1)) + cellheight -1;
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (julist->nGroups - hoff));
		}
		else
		{
			dx = leftborder;
			dy = topborder + (cellheight * i ) + cellheight;
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (julist->nGroups - hoff));
		}
		GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
	}

	/* draw V-Line */
	for( i = 0 ; i < 4 ; i++ )
	{
		if( i == 1 )
		{
			dx = leftborder + (cellwidth * i);
			dy = topborder + cellheight + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 1);
		}
		else if( i == 3 )
		{
			dx = leftborder + (cellwidth * (i - 1)) - 1;
			dy = topborder + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 2);
		}
		else
		{
			dx = leftborder + (cellwidth * i );
			dy = topborder + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 2);
		}
		GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );
	}


	unichar_t buf[2];
	int cho, jung, jong;

	/* draw jungsung jaso */
	JasoGroupItem * juitem = julist->jasoitem;
	for( jung = 0 ; jung < julist->nGroups ; jung++ )
	{
		index = jung - hoff;
		hydebug( "[%s] draw jungsung jaso : index: (%d)\n", __FUNCTION__, index );
		if( index >= 0 )
		{
			dx = leftborder + (cellwidth * 2) + (cellwidth * index * 2) + 20;
			dy = topborder + cellheight + cellheight - innergap;

			buf[0] = 0x314f + JungSungIdx[juitem->JasoVCode[0]];
			buf[1] = '\0';
			GDrawDrawText( pixmap, dx, dy, buf, 1, 0x000000 );
			juitem = juitem->next;
		}
	}

	/* draw H Line & lsb, aw title */
	index = julist->nGroups - hoff;
	hydebug( "[%s] draw lsb, aw title : index: (%d)\n", __FUNCTION__, index );
	for( i = 0 ; i < index + 1 ; ++i )
	{
		dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2);
		dy = topborder + cellheight;
		dheight = cellheight * (currentrow + 2);
		GDrawDrawLine( pixmap, dx, dy, dx, dy + dheight, johapindexfgColor );

		if( i != index )
		{
			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + innergap;
			dy = topborder + (cellheight * 2) + cellheight - innergap;
			GDrawDrawText8( pixmap, dx, dy, _("lsb"), -1, 0x808080 );

			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + cellwidth;
			dy = topborder + cellheight + cellheight;
			dheight = cellheight * (currentrow + 1);
			GDrawDrawLine( pixmap, dx, dy, dx, dy + dheight, johapindexfgColor );

			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + cellwidth + innergap;
			dy = topborder + (cellheight * 2) + cellheight - innergap;
			GDrawDrawText8( pixmap, dx, dy, _("aw"), -1, 0x808080 );
		}
	}

	int indexcnt = 0;

	/* draw chosung jaso & jongsung jaso & H-Line */
#if 1
	JasoGroupItem * joitem = jolist->jasoitem;

	for( jong = 0 ; jong < jolist->nGroups ; jong++ )
	{
		JasoGroupItem * choitem = cholist->jasoitem;

		for( cho = 0 ; cho < cholist->nGroups ; cho++ )
		{
			indexcnt = 0;
			indexcnt = ( jong * cholist->nGroups ) + cho;
			hydebug( "[%s] draw chosung jaso & jongsung jaso : indexcnt: (%d)\n", __FUNCTION__, indexcnt );

			if( indexcnt >= voff )
			{
				if( indexcnt >= maxLineCnt + voff )
				{
					break;
				}
				hydebug( "[%s] draw chosung jaso & jongsung jaso : index: (%d)\n", __FUNCTION__, index );
				hydebug( "[%s] chosung JasoVCode[0]: (%d)\n", __FUNCTION__, choitem->JasoVCode[0] );
				hydebug( "[%s] ChoSungIdx[%d]: (%d)\n", __FUNCTION__, index, ChoSungIdx[index] );

				buf[0] = 0x3131 + ChoSungIdx[choitem->JasoVCode[0]];
				buf[1] = '\0';

				dx = leftborder + cellwidth + innergap;
				dy = topborder + ( cellheight * 2 ) + cellheight + (cellheight * indexcnt);
				GDrawDrawText( pixmap, dx, dy + 20, buf, 1, 0x000000 );

				dx = leftborder + cellwidth;
				dy += cellheight;
				dwidth = cellwidth + ((cellwidth * 2) * (julist->nGroups - hoff));
				GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
			}
			choitem = choitem->next;
		}

//		if( cho == 0 )
		{
			index = joitem->JasoVCode[0];
			if( index == 0 )
			{
				buf[0] = 0x318d;
				buf[1] = '\0';
			}
			else
			{
				buf[0] = 0x11a8 + index-1;
				buf[1] = '\0';
			}
			if( dy + 20 - cellheight > topborder + ( cellheight * 2 ) )
			{
				dx = leftborder + innergap;
				GDrawDrawText( pixmap, dx, dy + 20 - cellheight, buf, -1, 0x000000 );
			}

			dx = leftborder;
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (julist->nGroups - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
		joitem = joitem->next;
	}
#else
	for( jong = 0 ; jong < nog_lc ; jong++ )
	{
		JasoGroupItem *choitem = cholist->jasoitem;
		for( cho = 0 ; cho < nog_fc ; cho++ )
		{
			indexcnt = (jong * nog_fc) + cho;
			index = indexcnt - voff;
			hydebug( "[%s] indexcnt: (%d)\n", __FUNCTION__, indexcnt );
			hydebug( "[%s] index: (%d)\n", __FUNCTION__, index );
			if( index >= 0 )
			{
				dx = leftborder + cellwidth;
				dy = topborder + (cellheight * 2) + ( cellheight * index ) + ( innergap * 3 );
				buf[0] = 0x3131 + ChoSungIdx[choitem->JasoVCode[0]];
				buf[1] = '\0';
				GDrawDrawText( pixmap, dx, dy, buf, 1, 0x000000 );

				dx = leftborder + cellwidth;
				dy = topborder + (cellheight * 2) + (cellheight * (index + 1));
				dwidth = cellwidth + ((cellwidth * 2) * (nog_mv - hoff));
				GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );

				if( cho == nog_fc - 1 )
				{
					dx = leftborder;
					dy = topborder + (cellheight * 2) + (cellheight * (index + 1)) + 1;
					dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
					dheight = 0;
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, 0xff0000 );///johapindexfgColor );
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, 0xff0000 );//johapindexfgColor );
				}

				if( cho == 0 )
				{
					dx = leftborder;
					dy = topborder + (cellheight * 2) + (cellheight * index);
					dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, 0x0000ff );//johapindexfgColor );

					//if( joitem->JasoVCode[0] == 0 )
					if( index == 0 )
					{
						buf[0] = 0x318d;
						buf[1] = '\0';
					}
					else
					{
						buf[0] = 0x11a8 + joitem->JasoVCode[0] - 1;
						buf[1] = '\0';
					}
					if( dy + 20 - cellheight > topborder + ( cellheight * 2 ) )
					{
						GDrawDrawText( pixmap, dx, dy, buf, -1, 0x000000 );
					}
				}
			}
			choitem = choitem->next;
		}
		joitem = joitem->next;
	}
#endif

#if 0
	char buffer[32];
	dx = leftborder + ( cellwidth * 2 ) + innergap;
	dy = topborder + ( cellheight * 2 ) + (innergap * 3);
	for( i = 0 ; i < currentrow ; i++ )
	{
		jong = ( i + voff ) / cholist->nGroups;
		cho = (  i + voff ) % cholist->nGroups;

		for( j = hoff ; j < julist->nGroups ; j++ )
		{
			jung = j;

			sprintf( buffer, "%d", rule->tmplsb[cho][jung][jong] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += (cellwidth);

			sprintf( buffer, "%d", rule->tmpwidth[cho][jung][jong] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += (cellwidth);
		}
		dx = leftborder + ( cellwidth * 2 ) + innergap;
		dy += cellheight;
	}
#endif
	return( true );
}

static int HY_WidthMapExpose_Org( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	RULE *rule		= johap->rule[johap->ruletype];
	int colcnt		= rule->IndexArr_ColCnt;
	int rowcnt		= rule->IndexArr_RowCnt;
	int leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int topborder		= JOHAP_INDEX_TOP_BORDER;
	int cellwidth		= CELLWIDTH;
	int cellheight		= CELLHEIGHT;
	int innergap		= 5;
	int maxRowCnt;
	int maxColCnt	= JOHAP_INDEX_MAX_COL;
	int maxLineCnt	= ( rowcnt > maxRowCnt ) ? maxRowCnt : rowcnt ;
	int maxVLineCnt	= ( colcnt > maxColCnt ) ? maxColCnt : colcnt ;
	int voff			= rule->offtop;
	int hoff			= rule->offleft;
	int currentrow	= rowcnt - voff;
	int currentcol	= colcnt - hoff;
	int dx, dy, dwidth, dheight, i, j, index = 0;
	int as, ds, ld;

	GDrawSetFont( pixmap, johap->fontset[0] );
	GDrawWindowFontMetrics( pixmap, johap->fontset[0], &as, &ds, &ld);

	JasoGroupList *cholist		= rule->grouplist[JASO_FC];
	JasoGroupList *julist		= rule->grouplist[JASO_MV];
	JasoGroupList *jolist		= rule->grouplist[JASO_LC];
	int nog_fc				= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv				= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc				= rule->grouplist[JASO_LC]->nGroups;

	GRect size;
	GDrawGetSize( johap->widthmap_subview, &size );
	GDrawFillRect( pixmap, &size, 0xffffff );

	maxRowCnt = ( size.height - ( cellheight * 2 ) ) / cellheight;
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;

	hydebug( "[%s] hoff: (%d), voff: (%d)\n", __FUNCTION__, hoff, voff );

	/* horizontal line */
	for( i = 0 ; i < 4 ; i++ )
	{
		if( i == 1 )
		{
			dx = leftborder;
			dy = topborder + (cellheight * i );
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
		else if( i == 3 )
		{
			dx = leftborder;
			dy = topborder + (cellheight * (i - 1)) -1;
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
		else
		{
			dx = leftborder;
			dy = topborder + (cellheight * i );
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
	}

	/* vertical line */
	for( i = 0 ; i < 4 ; i++ )
	{
		if( i == 1 )
		{
			dx = leftborder + (cellwidth * i);
			dy = topborder + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 1);
		}
		else if( i == 3 )
		{
			dx = leftborder + (cellwidth * (i - 1)) - 1;
			dy = topborder;
			dwidth = 0;
			dheight = cellheight * (currentrow + 2);
		}
		else
		{
			dx = leftborder + (cellwidth * i );
			dy = topborder;
			dwidth = 0;
			dheight = cellheight * (currentrow + 2);
		}
		GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );
	}

	// draw jaso type text */
	dx = leftborder + cellwidth;
	dy = topborder + cellheight - innergap;
	GDrawDrawText8( pixmap, dx, dy, _("Ju"), -1, 0xff0000 );

	dx = leftborder + innergap;
	dy = topborder + (cellheight * 2) - innergap;
	GDrawDrawText8( pixmap, dx, dy, _("Jo"), -1, 0xff0000 );

	dx = leftborder + cellwidth + innergap;
	dy = topborder + (cellheight * 2) - innergap;
	GDrawDrawText8( pixmap, dx, dy, _("Cho"), -1, 0xff0000 );

	JasoGroupItem *choitem;
	JasoGroupItem *juitem	= rule->grouplist[JASO_MV]->jasoitem;
	JasoGroupItem *joitem	= jolist->jasoitem;
	int cho, jung, jong;

	char imgpath[MAX_FILE_PATH_LEN];
	char *respath = getPixmapDir();
	GImage *img;
	int imgwidth = 16;
	int indexcnt = 0;
	unichar_t buf[2];
	char buffer[32];


	/* draw jungsung jaso images & vertical lines */
	for( jung = 0 ; jung < nog_mv ; jung++ )
	{
		index = jung - hoff;
		if( index >= 0 )
		{
			dx = leftborder + (cellwidth * 2) + (cellwidth * index * 2) + 20;
			dy = topborder + innergap;

			memset( imgpath, 0, MAX_FILE_PATH_LEN );
			sprintf( imgpath, "%s/johapindex/%s", respath, res_jaso[JASO_MV][juitem->JasoVCode[0]] );
			img = GImageRead( imgpath );
			if( img != NULL )
			{
				GDrawDrawImage(pixmap, img, NULL, dx, dy );
			}
		}
		juitem = juitem->next;
	}

	index = nog_mv - hoff;
	for( i = 0 ; i < index + 1 ; ++i )
	{
		dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2);
		dy = topborder;
		dwidth = 0;
		dheight = cellheight * (currentrow + 2);
		GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

		if( i != index )
		{
			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + innergap;
			dy = topborder + (cellheight * 2) - innergap;
			GDrawDrawText8( pixmap, dx, dy, _("lsb"), -1, 0x808080 );

			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + cellwidth;
			dy = topborder + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 1);
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + cellwidth + innergap;
			dy = topborder + (cellheight * 2) - innergap;
			GDrawDrawText8( pixmap, dx, dy, _("aw"), -1, 0x808080 );
		}
	}

	/* draw chosung jaso elements & jungsung jaso elements & horizontal lines */
	for( jong = 0 ; jong < nog_lc ; jong++ )
	{
		choitem	= cholist->jasoitem;
		for( cho = 0 ; cho < nog_fc ; cho++ )
		{
			indexcnt = (jong * nog_fc) + cho;
			index = indexcnt - voff;
			if( index >= 0 )
			{
				dx = leftborder + cellwidth;
				dy = topborder + (cellheight * 2) + (cellheight * index);
				dwidth = cellwidth + ((cellwidth * 2) * (nog_mv - hoff));
				dheight = 0;
				GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

				memset( imgpath, 0, MAX_FILE_PATH_LEN );
				sprintf( imgpath, "%s/johapindex/%s", respath, res_jaso[JASO_FC][choitem->JasoVCode[0]] );
				img = GImageRead( imgpath );
				if( img != NULL )
				{
					dx = leftborder + cellwidth + innergap;
					dy = topborder + (cellheight * 2) + (cellheight * index) + innergap;
					GDrawDrawImage(pixmap, img, NULL, dx, dy );
				}

				if( cho == nog_fc - 1 )
				{
					dx = leftborder;
					dy = topborder + (cellheight * 2) + (cellheight * (index + 1)) + 1;
					dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
					dheight = 0;
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );
				}

				if( cho == 0 )
				{
					dx = leftborder;
					dy = topborder + (cellheight * 2) + (cellheight * index);
					dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
					dheight = 0;
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

					memset( imgpath, 0, MAX_FILE_PATH_LEN );
					sprintf( imgpath, "%s/johapindex/%s", respath, res_jaso[JASO_LC][joitem->JasoVCode[0]] );
					img = GImageRead( imgpath );
					if( img != NULL )
					{
						dx = leftborder + innergap;
						dy = topborder + (cellheight * 2) + (cellheight * index) + innergap;
						GDrawDrawImage(pixmap, img, NULL, dx, dy );
					}
				}
			}
			choitem = choitem->next;
		}
		joitem = joitem->next;
	}

	dx = leftborder + ( cellwidth * 2 ) + innergap;
	dy = topborder + ( cellheight * 2 ) + (innergap * 3);
	for( i = 0 ; i < currentrow ; i++ )
	{
		jong = ( i + voff ) / nog_fc;
		cho = (  i + voff ) % nog_fc;

		for( j = hoff ; j < nog_mv ; j++ )
		{
			jung = j;

			sprintf( buffer, "%d", rule->tmplsb[cho][jung][jong] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += (cellwidth);

			sprintf( buffer, "%d", rule->tmpwidth[cho][jung][jong] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += (cellwidth);
		}
		dx = leftborder + ( cellwidth * 2 ) + innergap;
		dy += cellheight;
	}
	return( true );
}

static int HY_WidthMapExpose( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	RULE *rule		= johap->rule[johap->ruletype];
	int colcnt		= rule->IndexArr_ColCnt;
	int rowcnt		= rule->IndexArr_RowCnt;
	int leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int topborder		= JOHAP_INDEX_TOP_BORDER;
	int cellwidth		= CELLWIDTH;
	int cellheight		= CELLHEIGHT;
	int innergap		= 5;
	int maxRowCnt;
	int maxColCnt	= JOHAP_INDEX_MAX_COL;
	int maxLineCnt	= ( rowcnt > maxRowCnt ) ? maxRowCnt : rowcnt ;
	int maxVLineCnt	= ( colcnt > maxColCnt ) ? maxColCnt : colcnt ;
	int voff			= rule->offtop;
	int hoff			= rule->offleft;
	int currentrow	= rowcnt - voff;
	int currentcol	= colcnt - hoff;
	int dx, dy, dwidth, dheight, i, j, index = 0;
	int as, ds, ld;

	GDrawSetFont( pixmap, johap->fontset[0] );
	GDrawWindowFontMetrics( pixmap, johap->fontset[0], &as, &ds, &ld);

	JasoGroupList *cholist		= rule->grouplist[JASO_FC];
	JasoGroupList *julist		= rule->grouplist[JASO_MV];
	JasoGroupList *jolist		= rule->grouplist[JASO_LC];
	int nog_fc				= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv				= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc				= rule->grouplist[JASO_LC]->nGroups;

	GRect size;
	GDrawGetSize( johap->widthmap_subview, &size );
	GDrawFillRect( pixmap, &size, 0xffffff );

	maxRowCnt = ( size.height - ( cellheight * 2 ) ) / cellheight;
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;

	/* horizontal line */
	for( i = 0 ; i < 4 ; i++ )
	{
		if( i == 1 )
		{
			dx = leftborder;
			dy = topborder + (cellheight * i );
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
		else if( i == 3 )
		{
			dx = leftborder;
			dy = topborder + (cellheight * (i - 1)) -1;
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
		else
		{
			dx = leftborder;
			dy = topborder + (cellheight * i );
			dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
		}
	}

	/* vertical line */
	for( i = 0 ; i < 4 ; i++ )
	{
		if( i == 1 )
		{
			dx = leftborder + (cellwidth * i);
			dy = topborder + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 1);
		}
		else if( i == 3 )
		{
			dx = leftborder + (cellwidth * (i - 1)) - 1;
			dy = topborder;
			dwidth = 0;
			dheight = cellheight * (currentrow + 2);
		}
		else
		{
			dx = leftborder + (cellwidth * i );
			dy = topborder;
			dwidth = 0;
			dheight = cellheight * (currentrow + 2);
		}
		GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );
	}

	// draw jaso type text */
	dx = leftborder + cellwidth;
	dy = topborder + cellheight - innergap;
	GDrawDrawText8( pixmap, dx, dy, _("Ju"), -1, 0xff0000 );

	dx = leftborder + innergap;
	dy = topborder + (cellheight * 2) - innergap;
	GDrawDrawText8( pixmap, dx, dy, _("Jo"), -1, 0xff0000 );

	dx = leftborder + cellwidth + innergap;
	dy = topborder + (cellheight * 2) - innergap;
	GDrawDrawText8( pixmap, dx, dy, _("Cho"), -1, 0xff0000 );

	JasoGroupItem *choitem;
	JasoGroupItem *juitem	= rule->grouplist[JASO_MV]->jasoitem;
	JasoGroupItem *joitem	= jolist->jasoitem;
	int cho, jung, jong;

	char imgpath[MAX_FILE_PATH_LEN];
	char *respath = getPixmapDir();
	GImage *img;
	int imgwidth = 16;
	int indexcnt = 0;
	unichar_t buf[2];
	char buffer[32];


	/* draw jungsung jaso images & vertical lines */
	for( jung = 0 ; jung < nog_mv ; jung++ )
	{
		index = jung - hoff;
		if( index >= 0 )
		{
			dx = leftborder + (cellwidth * 2) + ((cellwidth * 2) * index) + 20;
			dy = topborder + cellheight - innergap;

			#if 1
			buf[0] = 0x314f + JungSungIdx[juitem->JasoVCode[0]];
			buf[1] = '\0';
			GDrawDrawText( pixmap, dx, dy, buf, 1, 0x000000 );
			#else
			memset( imgpath, 0, MAX_FILE_PATH_LEN );
			sprintf( imgpath, "%s/johapindex/%s", respath, res_jaso[JASO_MV][juitem->JasoVCode[0]] );
			img = GImageRead( imgpath );
			if( img != NULL )
			{
				GDrawDrawImage(pixmap, img, NULL, dx, dy );
			}
			#endif
		}
		juitem = juitem->next;
	}

	index = nog_mv - hoff;
	for( i = 0 ; i < index + 1 ; ++i )
	{
		dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2);
		dy = topborder;
		dwidth = 0;
		dheight = cellheight * (currentrow + 2);
		GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

		if( i != index )
		{
			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + innergap;
			dy = topborder + (cellheight * 2) - innergap;
			GDrawDrawText8( pixmap, dx, dy, _("lsb"), -1, 0x808080 );

			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + cellwidth;
			dy = topborder + cellheight;
			dwidth = 0;
			dheight = cellheight * (currentrow + 1);
			GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy + dheight, johapindexfgColor );

			dx = leftborder + (cellwidth * 2) + (cellwidth * i * 2) + cellwidth + innergap;
			dy = topborder + (cellheight * 2) - innergap;
			GDrawDrawText8( pixmap, dx, dy, _("aw"), -1, 0x808080 );
		}
	}

	/* draw chosung jaso elements & jungsung jaso elements & horizontal lines */
	for( jong = 0 ; jong < nog_lc ; jong++ )
	{
		choitem	= cholist->jasoitem;
		for( cho = 0 ; cho < nog_fc ; cho++ )
		{
			hydebug( "[%s] cho: (%d), jong: (%d)\n", __FUNCTION__, cho, jong );

			indexcnt = (jong * nog_fc) + cho;
			index = indexcnt - voff;
			hydebug( "[%s] voff: (%d), index: (%d), indexcnt: (%d)\n", __FUNCTION__, voff, index, indexcnt );
			if( index >= 0 )
			{
				dx = leftborder + cellwidth;
				dy = topborder + (cellheight * 2) + (cellheight * index);
				dwidth = cellwidth + ((cellwidth * 2) * (nog_mv - hoff));
				GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );

				buf[0] = 0x3131 + ChoSungIdx[choitem->JasoVCode[0]];
				buf[1] = '\0';
				GDrawDrawText( pixmap, dx + cellwidth, dy + 20, buf, 1, 0x000000 );
				hydebug( "[%s] \t draw chosung jaso. ( dy: %d)\n", __FUNCTION__, dy );

				if( cho == nog_fc - 1 )
				{
					dx = leftborder;
					dy = topborder + (cellheight * 2) + (cellheight * (index + 1)) + 1;
					dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );
				}

				if( cho == 0 )
				{
					hydebug( "===0===\n" );
					dx = leftborder;
					dy = topborder + (cellheight * 2) + (cellheight * index);
					hydebug( "dy: (%d)\n", dy );
					dwidth = (cellwidth * 2) + ((cellwidth * 2) * (nog_mv - hoff));
					GDrawDrawLine( pixmap, dx, dy, dx + dwidth, dy, johapindexfgColor );

					#if 1
					if( index == 0 )
					{
						hydebug( "===1===\n" );
						buf[0] = 0x318d;
						buf[1] = '\0';
					}
					else
					{
						hydebug( "===2===\n" );
						buf[0] = 0x11a8 + joitem->JasoVCode[0] - 1;
						buf[1] = '\0';
					}
					hydebug( "dy1: (%d), dy2: (%d)\n", dy + 20, topborder + ( cellheight * 2 ) );
					//if( dy + 20 >= topborder + ( cellheight * 2 ) )
					{
						GDrawDrawText( pixmap, dx, dy + 20 - cellheight, buf, -1, 0x000000 );
						hydebug( "[%s] \t draw jongsung jaso.\n", __FUNCTION__ );
					}
					#else
					memset( imgpath, 0, MAX_FILE_PATH_LEN );
					sprintf( imgpath, "%s/johapindex/%s", respath, res_jaso[JASO_LC][joitem->JasoVCode[0]] );
					img = GImageRead( imgpath );
					if( img != NULL )
					{
						dx = leftborder + innergap;
						dy = topborder + (cellheight * 2) + (cellheight * index) + innergap;
						GDrawDrawImage(pixmap, img, NULL, dx, dy );
					}
					#endif
				}
			}
			choitem = choitem->next;
		}
		joitem = joitem->next;
	}

#if 0
	dx = leftborder + ( cellwidth * 2 ) + innergap;
	dy = topborder + ( cellheight * 2 ) + (innergap * 3);
	for( i = 0 ; i < currentrow ; i++ )
	{
		jong = ( i + voff ) / nog_fc;
		cho = (  i + voff ) % nog_fc;

		for( j = hoff ; j < nog_mv ; j++ )
		{
			jung = j;

			sprintf( buffer, "%d", rule->tmplsb[cho][jung][jong] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += (cellwidth);

			sprintf( buffer, "%d", rule->tmpwidth[cho][jung][jong] );
			GDrawDrawText8( pixmap, dx, dy, buffer, -1, 0x000000 );
			dx += (cellwidth);
		}
		dx = leftborder + ( cellwidth * 2 ) + innergap;
		dy += cellheight;
	}
#endif
	return( true );
}

static void HY_WidthMapTFBoxPlace( JohapRule * johap )
{
	int			cellwidth	= CELLWIDTH;
	int			cellheight	= CELLHEIGHT;
	int			leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int			topborder	= JOHAP_INDEX_TOP_BORDER;
	int			voff			= johap->rule[johap->ruletype]->offtop;
	int			hoff			= johap->rule[johap->ruletype]->offleft;
	int			dx, dy;
	GRect		size;

	if( johap->rule[johap->ruletype]->tf_col == -1 )
	{
		return;
	}
	GGadgetGetSize( johap->tf, &size );
	dx = leftborder + ( cellwidth * 2 ) + ( johap->rule[johap->ruletype]->tf_col -hoff ) * cellwidth + 2;
	dy = topborder + ( cellheight * 2 ) + ( johap->rule[johap->ruletype]->tf_row - voff ) * cellheight + cellheight - size.height;
	GGadgetMove( johap->tf, dx, dy );
	GGadgetSetVisible( johap->tf, true );
}

static void HY_WidthMapActivateTF( JohapRule * johap )
{
	int nog_fc, nog_mv, nog_lc;
	char buf[32];

	sprintf( buf, "%d", johap->rule[johap->ruletype]->tfvalue_org );
	GGadgetSetTitle8( johap->tf, buf );
	HY_WidthMapTFBoxPlace( johap );
	GGadgetSetVisible( johap->tf, true );
	GTextFieldSelect( johap->tf, 0, -1 );
}

static void HY_WidthMapMouseDown( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	RULE *	rule			= johap->rule[johap->ruletype];
	int		colcnt		= rule->IndexArr_ColCnt;
	int		rowcnt		= rule->IndexArr_RowCnt;
	int		voff			= rule->offtop;
	int		hoff			= rule->offleft;
	
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;

	int		nog_fc, nog_mv, nog_lc, fc, mv, lc;
	int		dx, dy, validwidth, validheight;
	int		col = 0, row = 0;

	nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	nog_lc	= rule->grouplist[JASO_LC]->nGroups;

	dx			= leftborder + ( cellwidth * 2 );
	dy			= topborder + ( cellheight * 2 );
	validwidth	= cellwidth * colcnt;
	validheight	= cellheight * rowcnt;

	if( event->u.mouse.x <= dx )
	{
		return;
	}

	if( event->u.mouse.y <= dy )
	{
		return;
	}

	if( event->u.mouse.x >= ( dx + validwidth ) )
	{
		return;
	}

	if( event->u.mouse.y >= ( dy + validheight ) )
	{
		return;
	}

	col	= ( event->u.mouse.x - leftborder - ( cellwidth * 2 ) ) / cellwidth;
	col += hoff;
	row	= ( event->u.mouse.y - topborder - ( cellheight * 2 ) ) / cellheight;
	row += voff;

	rule->tf_col	= col;
	rule->tf_row	= row;
	rule->tf_cho	= row % nog_fc;
	if( col == 0 )
	{
		rule->tf_ju	= col;
	}
	else
	{
		rule->tf_ju	= ( col / 2 );
	}
	rule->tf_jo		= row / nog_fc;

	for( lc = 0 ; lc < nog_lc ; lc++ )
	{
		for( fc = 0 ; fc < nog_fc ; fc++ )
		{
			for( mv = 0 ; mv < nog_mv ; mv++ )
			{
				if( rule->tf_cho == fc && rule->tf_ju == mv && rule->tf_jo == lc )
				{
					if( col == 0 )
					{
						rule->tfvalue_org = rule->tmplsb[fc][mv][lc];
					}
					else
					{
						if( col % 2 )
						{
							rule->tfvalue_org = rule->tmpwidth[fc][mv][lc];
						}
						else
						{
							rule->tfvalue_org = rule->tmplsb[fc][mv][lc];
						}
					}
					break;
				}
			}
		}
	}
	HY_WidthMapActivateTF( johap );
	GDrawRequestExpose( pixmap, NULL, false );
}

static int widthmapsub_e_h( GWindow pixmap, GEvent *event )
{
	JohapRule *	johap = GDrawGetUserData( pixmap );

	switch( event->type )
	{
		case et_expose:
		{
			HY_WidthMapExpose( pixmap, event, johap );
		}
		break;
		case et_close:
		{
			GDrawDestroyWindow( pixmap );
		}
		break;
		case et_mousedown:
		{
			if( !HY_WidthMapFinishUp( johap ) )
			{
				return( true );
			}
			HY_WidthMapMouseDown( pixmap, event, johap );
		}
		break;
	}
	return( true );
}

static int HY_WidthMap_HScroll( JohapRule * johap, struct sbevent * sb )
{
	int		newpos		= johap->rule[johap->ruletype]->offleft;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;
	int		maxColCnt;
	int		totalColCnt	= johap->rule[johap->ruletype]->IndexArr_ColCnt;
	int		row			= johap->rule[johap->ruletype]->IndexArr_RowCnt;
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	GRect	r;

	GRect size;
	GDrawGetSize( johap->widthmap_subview, &size );
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;

	switch( sb->type )
	{
		case et_sb_top:
		{
			newpos = 0;
		}
		break;

		case et_sb_uppage:
		{
			newpos -= maxColCnt;
		}
		break;

		case et_sb_left:
		{
			--newpos;
		}
		break;

		case et_sb_right:
		{
			++newpos;
		}
		break;

		case et_sb_downpage:
		{
			newpos += maxColCnt;
		}
		break;

		case et_sb_bottom:
		{
			newpos = totalColCnt - maxColCnt;
		}
		break;

		case et_sb_thumb:
		case et_sb_thumbrelease:
		{
			newpos = sb->pos;
		}
		break;
	}

	if( newpos > totalColCnt - maxColCnt )
	{
		newpos = totalColCnt - maxColCnt;
	}

	if( newpos < 0 )
	{
		newpos =0;
	}

	if( newpos != johap->rule[johap->ruletype]->offleft )
	{
		int diff = newpos - johap->rule[johap->ruletype]->offleft;
		johap->rule[johap->ruletype]->offleft = newpos;
		GScrollBarSetPos( johap->rule[johap->ruletype]->hsb, johap->rule[johap->ruletype]->offleft );

		r.x		= leftborder;
		r.y		= topborder + ( cellheight * 2 );
		r.width	= ( cellwidth * 2 ) + ( cellwidth * row );
		r.height	= cellheight * maxColCnt;

		GDrawScroll( johap->widthmap_view, &r, diff * cellwidth, 0 );
	}
	return( true );
}

static int HY_WidthMap_VScroll( JohapRule * johap, struct sbevent * sb )
{
	int		newpos		= johap->rule[johap->ruletype]->offtop;
	int		leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int		topborder	= JOHAP_INDEX_TOP_BORDER;
	int		maxRowCnt;
	int		totalRowCnt	= johap->rule[johap->ruletype]->IndexArr_RowCnt;
	int		col			= johap->rule[johap->ruletype]->IndexArr_ColCnt;
	int		cellwidth	= CELLWIDTH;
	int		cellheight	= CELLHEIGHT;
	GRect	r;

	GRect size;
	GDrawGetSize( johap->widthmap_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 3 ) ) / cellheight;

	switch( sb->type )
	{
		case et_sb_top:
		{
			newpos = 0;
		}
		break;

		case et_sb_uppage:
		{
			newpos -= maxRowCnt;
		}
		break;

		case et_sb_up:
		{
			--newpos;
		}
		break;

		case et_sb_down:
		{
			++newpos;
		}
		break;

		case et_sb_downpage:
		{
			newpos += maxRowCnt;
		}
		break;

		case et_sb_bottom:
		{
			newpos = totalRowCnt - maxRowCnt;
		}
		break;

		case et_sb_thumb:
		case et_sb_thumbrelease:
		{
			newpos = sb->pos;
		}
		break;
	}

	if( newpos > totalRowCnt - maxRowCnt )
	{
		newpos = totalRowCnt - maxRowCnt;
	}

	if( newpos < 0 )
	{
		newpos =0;
	}

	if( newpos != johap->rule[johap->ruletype]->offtop )
	{
		int diff = newpos - johap->rule[johap->ruletype]->offtop;
		johap->rule[johap->ruletype]->offtop = newpos;
		GScrollBarSetPos( johap->rule[johap->ruletype]->vsb, johap->rule[johap->ruletype]->offtop );

		r.x		= leftborder;
		r.y		= topborder + ( cellheight * 2 );
		r.width	= ( cellwidth * 2 ) + ( cellwidth * col );
		r.height	= cellheight * maxRowCnt;

		GDrawScroll( johap->widthmap_view, &r, 0,  diff * cellheight );
	}
	return( true );
}

static int widthmap_e_h( GWindow pixmap, GEvent *event )
{
	switch( event->type )
	{
		case et_expose:
		{
			GDrawRequestExpose( pixmap, NULL, false );
		}
		break;
		case et_close:
		{
			GDrawDestroyWindow( pixmap );
		}
		break;
		case et_controlevent:
		{
			switch( event->u.control.subtype )
			{
				case et_scrollbarchange:
				{
					JohapRule *	johap = GDrawGetUserData( pixmap );
					if( event->u.control.g == johap->rule[johap->ruletype]->vsb )
					{
						HY_WidthMap_VScroll( johap, &event->u.control.u.sb );
						GDrawRequestExpose( johap->widthmap_subview, NULL, false );
					}
					else
					{
						HY_WidthMap_HScroll( johap, &event->u.control.u.sb );
						GDrawRequestExpose( johap->widthmap_subview, NULL, false );
					}
				}
				break;
			}
		}
		break;
	}
	return( true );
}

static void HY_ApplyWidthMap( JohapRule * johap )
{
	RULE * rule	= johap->rule[RULE_WIDTH];
	int nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc	= rule->grouplist[JASO_LC]->nGroups;
	int cho, ju, jo;

	for( cho = 0 ; cho < nog_fc ; cho++ )
	{
		for( ju = 0 ; ju < nog_mv ; ju++ )
		{
			for( jo = 0 ; jo < nog_lc ; jo++ )
			{
				rule->Lsb[cho][ju][jo] = rule->tmplsb[cho][ju][jo];
				rule->GlyfWidthMap[cho][ju][jo] = rule->tmpwidth[cho][ju][jo];
			}
		}
	}

	for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
	{
		for( ju = 0 ; ju < NUM_JASO_MV ; ju++ )
		{
			for( jo = 0 ; jo < NUM_JASO_LC ; jo++ )
			{
				rule->tmplsb[cho][ju][jo] = 0;
				rule->tmpwidth[cho][ju][jo] = 0;
			}
		}
	}
}

static void HY_TransRef( RefChar *ref, real transform[6] )
{
	int i;
	real t[6];

	for ( i = 0; i < ref->layer_cnt ; i++ )
	{
		SplinePointListTransform(ref->layers[i].splines, transform, tpt_AllPoints);
	}

	t[0] = ref->transform[0]*transform[0] + ref->transform[1]*transform[2];
	t[1] = ref->transform[0]*transform[1] + ref->transform[1]*transform[3];
	t[2] = ref->transform[2]*transform[0] + ref->transform[3]*transform[2];
	t[3] = ref->transform[2]*transform[1] + ref->transform[3]*transform[3];
	t[4] = ref->transform[4]*transform[0] + ref->transform[5]*transform[2] + transform[4];
	t[5] = ref->transform[4]*transform[1] + ref->transform[5]*transform[3] + transform[5];

	memcpy(ref->transform,t,sizeof(t));
	RefCharFindBounds(ref);
}

static void HY_Reset_GlyfHMetric( SplineFont * sf, JohapRule * johap )
{
	RULE * rule	= johap->rule[RULE_WIDTH];
	int nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc	= rule->grouplist[JASO_LC]->nGroups;
	int cho, ju, jo, uni;
	int choindex, jungindex, jongindex;
	int i, gid;
	int lsboff = 0, widthoff = 0;
	SplineChar *sc;
	real transform[6];
	RefChar *refs;
	DBounds bb;

	ff_progress_start_indicator( 10 , _("Apply..."),_("Change Glyph Width...") , 0 , 11172 , 1 );
	for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
	{
		choindex = rule->grouplist[JASO_FC]->JasoGroupMap[cho];
		for( ju = 0 ; ju < NUM_JASO_MV ; ju++ )
		{
			jungindex = rule->grouplist[JASO_MV]->JasoGroupMap[ju];
			for( jo = 0 ; jo < NUM_JASO_LC ; jo++ )
			{
				jongindex = rule->grouplist[JASO_LC]->JasoGroupMap[jo];

				uni = 0xac00 + ( cho * NUM_JASO_MV + ju ) * NUM_JASO_LC + jo;
				gid = sf->map->map[uni];
				sc = sf->glyphs[gid];
				if( gid != -1 && SCWorthOutputting(sc) )
				{

					lsboff = rule->Lsb[choindex][jungindex][jongindex];
					widthoff = rule->GlyfWidthMap[choindex][jungindex][jongindex];
					if( lsboff == 0 && widthoff == 0 )
					{
						continue;
					}

					/*** Lsb ***/
					sc->prevlsb = sc->curlsb;
					sc->lsb_off = lsboff;
					sc->prevwidth = sc->width;
					sc->width_off = widthoff;

					transform[0] = 1.0;
					transform[1] = 0.0;
					transform[2] = 0.0;
					transform[3] = 1.0;
					transform[4] = lsboff;
					transform[5] = 0;
					if ( sc->inspiro && hasspiro() )
					{
						SplinePointListSpiroTransform(sc->layers[ly_fore].splines, transform, 1);
					}
					else
					{
						SplinePointListTransformExtended( sc->layers[ly_fore].splines, transform, 1, 0 );
					}

					for( refs = sc->layers[ly_fore].refs ; refs != NULL ; refs = refs->next )
					{
						HY_TransRef( refs, transform );
					}
					SplineCharLayerFindBounds( sc, ly_fore, &bb );

					sc->curlsb = bb.minx;
					sc->curwidth = sc->width + widthoff;
					sc->width = sc->curwidth;

					SCUpdateAll(sc);

					if ( !ff_progress_next())
					{
						break;
					}
				}
			}
		}
	}
	ff_progress_end_indicator();
	FontViewReformatAll(sf);
}

static int HY_CheckWidthMapChanged( JohapRule * johap )
{
	RULE * rule = johap->rule[johap->ruletype];
	int nog_fc = rule->grouplist[JASO_FC]->nGroups;
	int nog_mv = rule->grouplist[JASO_MV]->nGroups;
	int nog_lc = rule->grouplist[JASO_LC]->nGroups;
	int cho, ju, jo;

	johap->bChangedWidth = 0;
	for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
	{
		for( ju = 0 ; ju < NUM_JASO_MV ; ju++ )
		{
			for( jo = 0 ; jo < NUM_JASO_LC ; jo++ )
			{
				if( (rule->GlyfWidthMap[cho][ju][jo] != rule->tmpwidth[cho][ju][jo]) || (rule->Lsb[cho][ju][jo] != rule->tmplsb[cho][ju][jo]) )
				{
					johap->bChangedWidth = 1;
				}
			}
		}
	}
	return johap->bChangedWidth;
}

static int HY_WidthMapOK( GGadget *g, GEvent * e )
{
	if( e->type == et_controlevent && e->u.control.subtype == et_buttonactivate )
	{
		GWindow		gw		= GGadgetGetWindow( g );
		JohapRule *	johap	= GDrawGetUserData( gw );
		SplineFont *	sf		= johap->sf;
		int ret = 0;

		ret = HY_WidthMapFinishUp( johap );
		if( !ret )
		{
			return( true );
		}

		if( HY_CheckWidthMapChanged( johap ) )
		{
			if( johap->bChangedWidth )
			{
				HY_ApplyWidthMap( johap );
				HY_Reset_GlyfHMetric( sf, johap );
			}
		}
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static int HY_WidthMapCancel( GGadget *g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw		= GGadgetGetWindow( g );
		JohapRule * johap	= GDrawGetUserData( gw );
		RULE * rule	= johap->rule[johap->ruletype];
		int nog_fc	= rule->grouplist[JASO_FC]->nGroups;
		int nog_mv	= rule->grouplist[JASO_MV]->nGroups;
		int nog_lc	= rule->grouplist[JASO_LC]->nGroups;
		int cho, ju, jo;

		for( cho = 0 ; cho < NUM_JASO_FC ; cho++ )
		{
			for( ju = 0 ; ju < NUM_JASO_MV ; ju++ )
			{
				for( jo = 0 ; jo < NUM_JASO_LC ; jo++ )
				{
					rule->tmplsb[cho][ju][jo] = 0;
					rule->tmpwidth[cho][ju][jo] = 0;
				}
			}
		}
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static void HY_DoWidthMapDlg( JohapRule * johap )
{
	GWindow				gw;
	GWindow				subw;
	GWindowAttrs			wattrs;
	GGadgetCreateData	mgcd[5];
	GGadgetCreateData	mgcd2[2];
	GTextInfo			mlabel[3];
	static GBox			tfbox;
	GRect				pos;
	GRect				size;

	int					leftborder	= JOHAP_INDEX_LEFT_BORDER;
	int					topborder	= JOHAP_INDEX_TOP_BORDER;
	int					gapsize		= JOHAP_INDEX_GAP;
	int					maxRowCnt;
	int					maxColCnt;
	int					cellwidth	= CELLWIDTH;
	int					cellheight	= CELLHEIGHT;
	int					btnWidth	= 60;
	int					btnHeight	= 20;
	int					colcnt		= 1;
	int					rowcnt		= 1;
	int					i;

	HY_SetupJohapIndexView( johap, johap->ruletype );

	johap->rule[johap->ruletype]->offtop = 0;
	johap->rule[johap->ruletype]->offleft = 0;
	colcnt	= johap->rule[johap->ruletype]->IndexArr_ColCnt;
	rowcnt	= johap->rule[johap->ruletype]->IndexArr_RowCnt;
	for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
	{
		johap->rule[i]->tf_col = -1;
		johap->rule[i]->tf_row = -1;
	}

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask				= wam_events | wam_cursor | wam_utf8_wtitle | wam_backcol;
	wattrs.background_color	= COLOR_DEFAULT;
	wattrs.event_masks		= -1;
	wattrs.is_dlg				= 0;
	wattrs.restrict_input_to_me	= 1;
	wattrs.undercursor			= 1;
	wattrs.cursor				= ct_pointer;
	wattrs.utf8_window_title	= _("Width Map Edit");

	int resx =  GetSystemMetrics(SM_CXSCREEN);
	int resy =  GetSystemMetrics(SM_CYSCREEN);

	 /* create top window */
	pos.x		= 0;
	pos.y		= 0;
	pos.width		= 1100;
	pos.height	= resy * 0.7;
	hydebug( "[%s] main window size: [ %d  %d ]\n", __FUNCTION__, pos.width, pos.height );
	gw = johap->widthmap_view = GDrawCreateTopWindow( NULL, &pos, widthmap_e_h, johap, &wattrs );

	/* create sub window */
	pos.x		= 2;
	pos.y		= 2;
	pos.width		= 950;
	pos.height	= pos.height - 20;
	subw = johap->widthmap_subview = GWidgetCreateSubWindow( johap->widthmap_view, &pos, widthmapsub_e_h, johap, &wattrs );
	hydebug( "[%s] sub window size: [ %d  %d ]\n", __FUNCTION__, pos.width, pos.height );

	GDrawGetSize( johap->widthmap_subview, &size );
	maxRowCnt = ( size.height - ( cellheight * 3 ) ) / cellheight;
	maxColCnt = ( size.width - ( cellwidth * 2 ) ) / cellwidth;

	// Make Gadgets
	memset( &mgcd, 0, sizeof( mgcd ) );
	memset( &mgcd2, 0, sizeof( mgcd2 ) );
	memset( &mlabel, 0, sizeof( mlabel ) );


	mgcd[0].gd.pos.x				= GDrawPixelsToPoints( NULL, pos.x + pos.width );
	mgcd[0].gd.pos.y				= GDrawPixelsToPoints( NULL, 5 );
	mgcd[0].gd.pos.width			= GDrawPixelsToPoints( NULL, vScrollBarWidth );
	mgcd[0].gd.pos.height			= GDrawPixelsToPoints( NULL, size.height );
	mgcd[0].gd.flags				= gg_visible | gg_enabled | gg_sb_vert;
	mgcd[0].creator				= GScrollBarCreate;

	mgcd[1].gd.pos.x				= GDrawPixelsToPoints( NULL, 5 );
	mgcd[1].gd.pos.y				= GDrawPixelsToPoints( NULL, pos.y + pos.height );
	mgcd[1].gd.pos.width			= GDrawPixelsToPoints( NULL, size.width );
	mgcd[1].gd.pos.height			= GDrawPixelsToPoints( NULL, hScrollBarHeight );
	mgcd[1].gd.flags				= gg_visible | gg_enabled;
	mgcd[1].creator				= GScrollBarCreate;

	mlabel[0].text					= (unichar_t *) _("OK");
	mlabel[0].text_is_1byte			= true;
	mgcd[2].gd.pos.x				= GDrawPixelsToPoints( NULL, 980 );
	mgcd[2].gd.pos.y				= GDrawPixelsToPoints( NULL, 50 );
	mgcd[2].gd.pos.width			= btnWidth;
	mgcd[2].gd.pos.height			= btnHeight;
	mgcd[2].gd.label				= &mlabel[0];
	mgcd[2].gd.flags				= gg_visible | gg_enabled;
	mgcd[2].gd.handle_controlevent	= HY_WidthMapOK;
	mgcd[2].creator				= GButtonCreate;

	mlabel[1].text					= (unichar_t *) _("Cancel");
	mlabel[1].text_is_1byte			= true;
	mgcd[3].gd.pos.x				= GDrawPixelsToPoints( NULL, 980 );
	mgcd[3].gd.pos.y				= GDrawPixelsToPoints( NULL, 50 + gapsize + btnHeight + gapsize );
	mgcd[3].gd.pos.width			= btnWidth;
	mgcd[3].gd.pos.height			= btnHeight;
	mgcd[3].gd.label				= &mlabel[1];
	mgcd[3].gd.flags				= gg_visible | gg_enabled;
	mgcd[3].gd.handle_controlevent	= HY_WidthMapCancel;
	mgcd[3].creator				= GButtonCreate;
	GGadgetsCreate( gw, mgcd );

	tfbox.main_background			= 0xf0f0f0;
	tfbox.main_foreground			= 0xff0000;
	mgcd2[0].gd.pos.x				= GDrawPixelsToPoints( NULL, leftborder + ( cellwidth * 2 ) + 2 );
	mgcd2[0].gd.pos.y				= GDrawPixelsToPoints( NULL, topborder + ( cellheight * 2 ) + 2 );
	mgcd2[0].gd.pos.width			= cellwidth - 4;
	mgcd2[0].gd.pos.height			= cellheight - 4;
	mgcd2[0].gd.flags				= gg_enabled | gg_dontcopybox | gg_pos_in_pixels;
	mgcd2[0].gd.box				= &tfbox;
	mgcd2[0].creator				= GTextFieldCreate;
	GGadgetsCreate( subw, mgcd2 );

	johap->rule[johap->ruletype]->vsb		= mgcd[0].ret;
	johap->rule[johap->ruletype]->hsb		= mgcd[1].ret;
	johap->rule[johap->ruletype]->ok		= mgcd[3].ret;
	johap->rule[johap->ruletype]->cancel	= mgcd[4].ret;
	johap->tf							= mgcd2[0].ret;
	//johap->rule[johap->ruletype]->tf		= mgcd2[0].ret;
	johap->widthmap_view				= gw;
	johap->widthmap_subview				= subw;

	GScrollBarSetBounds( johap->rule[johap->ruletype]->vsb, 0, rowcnt, maxRowCnt );
	GScrollBarSetPos( johap->rule[johap->ruletype]->vsb, johap->rule[johap->ruletype]->offtop );

	GScrollBarSetBounds( johap->rule[johap->ruletype]->hsb, 0, colcnt, maxColCnt );
	GScrollBarSetPos( johap->rule[johap->ruletype]->hsb, johap->rule[johap->ruletype]->offleft );

	GDrawSetVisible( johap->widthmap_view, true );
	GDrawSetVisible( johap->widthmap_subview, true );

	FontRequest	rq;
	int			as, ds, ld;
	memset( &rq, 0, sizeof( rq ) );
	johap->fontset		= calloc( (6<<2), sizeof(GFont *) );
	rq.utf8_family_name	= HY_UI_FAMILIES;
	rq.point_size			= 8;
	rq.weight				= 400;
	johap->fontset[0]		= GDrawInstanciateFont( gw, &rq );
	GDrawSetFont( gw, johap->fontset[0] );
	GDrawWindowFontMetrics( gw, johap->fontset[0], &as, &ds, &ld );

	s_widthmapdlg = 1;
}

static int HY_JohapIndexEdit( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		JohapRule * johap = GDrawGetUserData(GGadgetGetWindow(g));

		if( johap->ruletype != RULE_WIDTH )
		{
			HY_DoJohapIndexDlg( johap );
		}
		else
		{
			HY_DoWidthMapDlg( johap );
		}
	}
	return( true );
}

static void HY_SetThreePairRuleTable( JohapRule * johap )
{
	int ruletype;
	int jasotype;
	int nGroup;
	int jasoindex;
	int cho, ju, jo;

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE - 1 ; ruletype++ )
	{
		RULE *rule = johap->rule[ruletype];
		switch( ruletype )
		{
			case RULE_FC:
			{
				for( jasotype = 0 ; jasotype < NUM_JASO_TYPES ; jasotype++ )
				{
					rule->grouplist[jasotype]->nGroups = 1;
					for( nGroup = 0 ; nGroup < rule->grouplist[jasotype]->nGroups ; nGroup++ )
					{
						switch( jasotype )
						{
							case JASO_FC:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_FC;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
							case JASO_MV:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_MV;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
							case JASO_LC:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_LC;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
						}
					}
				}
			}
			break;
			case RULE_MV:
			{
				for( jasotype = 0 ; jasotype < NUM_JASO_TYPES ; jasotype++ )
				{
					rule->grouplist[jasotype]->nGroups = 1;
					for( nGroup = 0 ; nGroup < rule->grouplist[jasotype]->nGroups ; nGroup++ )
					{
						switch( jasotype )
						{
							case JASO_FC:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_FC;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
							case JASO_MV:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_MV;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
							case JASO_LC:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_LC;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
						}
					}
				}
			}
			break;
			case RULE_LC:
			{
				for( jasotype = 0 ; jasotype < NUM_JASO_TYPES ; jasotype++ )
				{
					rule->grouplist[jasotype]->nGroups = 1;
					for( nGroup = 0 ; nGroup < rule->grouplist[jasotype]->nGroups ; nGroup++ )
					{
						switch( jasotype )
						{
							case JASO_FC:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_FC;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
							case JASO_MV:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_MV;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
							case JASO_LC:
							{
								rule->grouplist[jasotype]->jasoitem->numJaso = NUM_JASO_LC;
								for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; jasoindex++ )
								{
									rule->vcode_NumOfGlyf[jasoindex] = 1;
									rule->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
									rule->grouplist[jasotype]->jasoitem->JasoVCode[jasoindex] = jasoindex;
								}
							}
							break;
						}
					}
				}
			}
			break;
		}

		for( cho = 0 ; cho < johap->rule[ruletype]->grouplist[JASO_FC]->nGroups ; cho++ )
		{
			for( ju = 0 ; ju < johap->rule[ruletype]->grouplist[JASO_MV]->nGroups ; ju++ )
			{
				for( jo = 0 ; jo < johap->rule[ruletype]->grouplist[JASO_LC]->nGroups ; jo++ )
				{
					johap->rule[ruletype]->tmpgindex[cho][ju][jo] = johap->rule[ruletype]->GlyfIndexMap[cho][ju][jo] = 0;
				}
			}
		}
	}
}

static int HY_CheckThreePairMethod( GGadget *g, GEvent *e )
{
	if ( e==NULL || (e->type==et_controlevent && e->u.control.subtype == et_radiochanged ))
	{
		GWindow gw		= GGadgetGetWindow(g);
		JohapRule * johap	= GDrawGetUserData( gw );
		
		int checked = GGadgetIsChecked(g);
		if( checked )
		{
			johap->threepairmethod = 1;
			HY_SetThreePairRuleTable( johap );
		}
		else
		{
			johap->threepairmethod = 0;
			HY_RollBackRuleJoahp( johap );
		}
	}
	return( true );
}

static int HY_JasoGroupAddOptChange( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_radiochanged )
	{
		GWindow		gw		= GGadgetGetWindow( g );
		JohapRule *	johap	= GDrawGetUserData( gw );

		johap->johapIndexAddOpt = !( johap->johapIndexAddOpt );
	}
	return( true );
}

static int HY_JasoTabChanged( GGadget * g, GEvent * event )
{
	if ( event->type==et_controlevent && event->u.control.subtype == et_radiochanged )
	{
		GWindow		gw		= GGadgetGetWindow( g );
		JohapRule *	johap	= GDrawGetUserData( gw );
		int i;

		for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
		{
			if( i == johap->ruletype )
			{
				johap->rule[i]->jasotype = GTabSetGetSel( g );
			}
		}
	}
	return( true );
}

static int HY_RuleTabChanged( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_radiochanged )
	{
		GWindow gw = GGadgetGetWindow( g );
		JohapRule *johap = GDrawGetUserData( gw );
		int i;

		johap->ruletype = GTabSetGetSel( g );
		HY_SetupJohapIndexView( johap, johap->ruletype );
		if( johap->johapindex_view )
		{
			GDrawRequestExpose( johap->johapindex_subview, NULL, true );
		}
	}
	return( true );
}

static void HY_GroupViewExpose( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	int			ruletype		= johap->ruletype;
	int			jasotype	= johap->rule[ruletype]->jasotype;
	int			imgX		= GVIEW_LEFT_BORDER;
	int			imgY		= GVIEW_TOP_BORDER;
	GRect		size, pos;
	GImage *	img;
	char *		respath;
	char			imgpath[MAX_FILE_PATH_LEN];
	int			i, j, len = 0;

	GDrawGetSize( pixmap, &size );
	GDrawFillRect( pixmap, &size, bgColor );
	size.width	-= 10;
	size.height	-= 10;
	GDrawDrawRect( pixmap, &size, fgColor );

	respath = getPixmapDir();
	if( ruletype != RULE_WIDTH )
	{
		JasoGroupList *	grouplist = johap->rule[ruletype]->grouplist[jasotype];
		JasoGroupItem *	jasoitem = grouplist->jasoitem;

		for( i = 0 ; i < grouplist->nGroups ; i++ )
		{
			for( j = 0 ; j < jasoitem->numJaso ; j++ )
			{
				if( res_jaso[jasotype][jasoitem->JasoVCode[j]] != NULL )
				{
					memset( imgpath, 0, MAX_FILE_PATH_LEN );
					sprintf( imgpath, "%s/group/%s", getPixmapDir(), res_jaso[jasotype][jasoitem->JasoVCode[j]] );
					img = GImageRead( imgpath );
					if( img != NULL )
					{
						GDrawDrawImage( pixmap, img, NULL, imgX, imgY );
						pos.x = imgX - 1;
						pos.y = imgY - 1;
						pos.width = img->u.image->width + 1;
						pos.height = img->u.image->height + 1;
						if( jasoitem->bSelect[j] )
						{
							GDrawDrawRect( pixmap, &pos, 0xff0000 );
						}
						imgX = pos.x + pos.width + GVIEW_JASO_SPACE_GAP;
						img = NULL;
					}
				}
			}
			imgX = GVIEW_LEFT_BORDER;
			imgY = imgY + pos.height + GVIEW_JASO_SPACE_GAP;
			jasoitem = jasoitem->next;
		}
	}
	else
	{
		JasoGroupList *	grouplist = johap->rule[ruletype]->grouplist[jasotype];
		JasoGroupItem *	jasoitem = grouplist->jasoitem;
		for( i = 0 ; i < grouplist->nGroups ; i++ )
		{
			for( j = 0 ; j < jasoitem->numJaso ; j++ )
			{
				if( res_jaso[jasotype][jasoitem->JasoVCode[j]] != NULL )
				{
					memset( imgpath, 0, MAX_FILE_PATH_LEN );
					sprintf( imgpath, "%s/group/%s", getPixmapDir(), res_jaso[jasotype][jasoitem->JasoVCode[j]] );
					img = GImageRead( imgpath );
					if( img != NULL )
					{
						GDrawDrawImage( pixmap, img, NULL, imgX, imgY );
						pos.x = imgX - 1;
						pos.y = imgY - 1;
						pos.width = img->u.image->width + 1;
						pos.height = img->u.image->height + 1;
						if( jasoitem->bSelect[j] )
						{
							GDrawDrawRect( pixmap, &pos, 0xff0000 );
						}
						imgX = pos.x + pos.width + GVIEW_JASO_SPACE_GAP;
						img = NULL;
					}
				}
			}
			imgX = GVIEW_LEFT_BORDER;
			imgY = imgY + pos.height + GVIEW_JASO_SPACE_GAP;
			jasoitem = jasoitem->next;
		}
	}
}

static JasoGroupItem * HY_GetJasoItem_By_GroupIndex( JasoGroupList * grouplist, int groupindex )
{
	int i;

	if( groupindex >= grouplist->nGroups )
	{
		return NULL;
	}

	JasoGroupItem * jasoitem = grouplist->jasoitem;
	for( i = 0 ; i < grouplist->nGroups ; i++ )
	{
		if( i == groupindex )
		{
			break;
		}
		jasoitem = jasoitem->next;
	}
	return jasoitem;
}

static void HY_PreChangeForRemoveGroupItem( JohapRule * johap, int ruletype, int jasotype, int groupindex )
{
	JasoGroupList * grouplist = johap->rule[ruletype]->grouplist[jasotype];
	JasoGroupItem * jasoitem = HY_GetJasoItem_By_GroupIndex( grouplist, groupindex );
	int i;

	for( i = 0 ; i < jasoitem->numJaso ; i++ )
	{
		grouplist->JasoGroupMap[ jasoitem->JasoVCode[i]] = -1;
	}
	//HY_SetupGlyfIndex( johap );
}

static void HY_ArrayRemoveItem( uint8 * remove_pos, int item_index, int num_items, size_t item_size )
{
	uint8 * dp = remove_pos;
	uint8 * sp = remove_pos + item_size;
	int i;

	for( i = item_index ; i <= num_items ; i++ )
	{
		memmove( dp, sp, item_size );
		dp += item_size;
		sp += item_size;
	}
}

static void HY_RemoveJasoItem_by_GroupIndex( JohapRule * johap, JasoGroupList * grouplist, int groupindex )
{
	JasoGroupItem * jasoitem = HY_GetJasoItem_By_GroupIndex( grouplist, groupindex );
	JasoGroupItem * jasoitem_prev = jasoitem->prev;
	JasoGroupItem * jasoitem_next = jasoitem->next;

	if( jasoitem_prev == NULL )
	{
		grouplist->jasoitem = jasoitem_next;
		grouplist->jasoitem->prev = NULL;
	}
	else if( jasoitem_next == NULL )
	{
		jasoitem_prev->next = NULL;
	}
	else
	{
		jasoitem_prev->next = jasoitem->next;
		jasoitem_next->prev = jasoitem->prev;
	}
	free( jasoitem );
	grouplist->nGroups--;
}

static void HY_RemoveJasoItem_In_GroupList( JohapRule * johap, int ruletype, int jasotype, int groupindex )
{
	JasoGroupList * grouplist = johap->rule[ruletype]->grouplist[jasotype];
	HY_RemoveJasoItem_by_GroupIndex( johap, grouplist, groupindex );
}

static void HY_RemoveJasoItem_In_Map( JohapRule * johap, int ruletype, int jasotype, int groupindex )
{
}

static JasoGroupList * HY_InsertJasoItem_by_GroupIndex( JohapRule * johap, JasoGroupList * grouplist, JasoGroupItem * jasoitem_new, int pos )
{
	JasoGroupList * tmpgrouplist = grouplist;

	if( pos < 0 || pos > grouplist->nGroups )
	{
		return NULL;
	}

	if( pos == 0 )
	{
		JasoGroupItem * newhead = jasoitem_new;
		JasoGroupItem * oldhead = grouplist->jasoitem;

		newhead->prev = NULL;
		newhead->next = oldhead;
		oldhead->prev = newhead;
		grouplist->jasoitem = newhead;
	}
	else if( pos == grouplist->nGroups )
	{
		JasoGroupItem * jasoitem = HY_GetJasoItem_By_GroupIndex( grouplist,  pos - 1 );
		jasoitem->next = jasoitem_new;
		jasoitem_new->next = NULL;
		jasoitem_new->prev = jasoitem;
	}
	else
	{
		JasoGroupItem * jasoitem = HY_GetJasoItem_By_GroupIndex( grouplist,  pos );
		jasoitem->prev->next = jasoitem_new;
		jasoitem_new->prev = jasoitem->prev;
		jasoitem_new->next = jasoitem;
		jasoitem->prev = jasoitem_new;
	}
	grouplist->nGroups++;
	return tmpgrouplist;
}

static int HY_Get_LastGlyfIndex( JohapRule * johap, RULE * rule, int nog_fc, int nog_mv, int nog_lc )
{
	int last_gi = 0;
	int i, j, k;

	for( i = 0 ; i < nog_fc ; i++ )
	{
		for( j = 0 ; j < nog_mv ; j++ )
		{
			for( k = 0 ; k < nog_lc ; k++ )
			{
				last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
			}
		}
	}
	return last_gi;
}

static int HY_VCodeCompare( const void * elem1, const void * elem2 )
{
	return ( int )( *( uint8 * )elem1 - *( uint8 * )elem2 );
}

static void HY_JasoGroupChange_inMidl( JohapRule * johap, RULE * rule, JasoGroupList * src_grouplist, JasoGroupItem * src_jasoitem, int srcgroup, int dstgroup, int srcindex )
{
	uint8	mv_JasoVCode	= src_jasoitem->JasoVCode[srcindex];
	int		ruletype			= johap->ruletype;
	int		jasotype		= johap->rule[ruletype]->jasotype;
	int		i, j, k, m, g;

	/* Src JasoGroupItem에서 move_JasoVCode를 삭제 */
	HY_ArrayRemoveItem( (uint8 * )&src_jasoitem->JasoVCode[srcindex], srcindex, src_jasoitem->numJaso, sizeof( uint8 ) );
	src_jasoitem->numJaso--;


	/* JasoItem을 DstGroup에 추가 */
	JasoGroupItem * dst_jasoitem = HY_GetJasoItem_By_GroupIndex( src_grouplist, dstgroup );
	dst_jasoitem->JasoVCode[dst_jasoitem->numJaso] = mv_JasoVCode;
	dst_jasoitem->numJaso++;


	/* JasoVCode를 크기순으로 재배열 */
	qsort( dst_jasoitem->JasoVCode, ( size_t ) dst_jasoitem->numJaso, sizeof( uint8 ), HY_VCodeCompare );


	/* JasoGroupMap 수정
	     JasoGroupMap[move_JasoVCode]를 dest group index로 assign. */
	src_grouplist->JasoGroupMap[mv_JasoVCode] = dstgroup;


	/* JasoVCode를 하나 뺌으로써 empty group item이 되었으면 group list에서 제거.
	    전체적으로 JasoGroup_Change_AddJGI_atHead/Tail()에서의 변경사항에 대한 환원 및 list 중간에서의 remove되는 경우로 볼 수 있음. */

	if( src_jasoitem->numJaso == 0 )
	{
		/* linked-list에서의 remove op. */
		HY_RemoveJasoItem_In_GroupList( johap, ruletype, jasotype, srcgroup );

		/* SrcGroup이 제거됨으로 인한 group index의 변동 처리 -> jgi entry의 변동이 생긴 경우.
		     source group index가 없어진 상태이므로 jgl level에서 group index를 전체적으로 재조정. */
		for( i = 0 ; i < JasoNum[jasotype] ; i++ )
		{
			if( src_grouplist->JasoGroupMap[i] > srcgroup )
			{
				src_grouplist->JasoGroupMap[i]--;
			}
		}

		int	nog_fc	= rule->grouplist[JASO_FC]->nGroups;
		int	nog_mv	= rule->grouplist[JASO_MV]->nGroups;
		int	nog_lc	= rule->grouplist[JASO_LC]->nGroups;

		JasoGroupList *	this_grouplist	= rule->grouplist[jasotype];
		JasoGroupItem *	jasoitem		= this_grouplist->jasoitem;

		if( ruletype != RULE_WIDTH )
		{
			for( m = 0 ; m < JasoNum[jasotype] ; m++ )
			{
				int	gi		= this_grouplist->JasoGroupMap[m];
				int	last_gi	= 0;

				switch( ruletype )
				{
					case JASO_FC:
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi = ( last_gi >= rule->GlyfIndexMap[gi][j][k] ) ? last_gi : rule->GlyfIndexMap[gi][j][k];
							}
						}
					}
					break;

					case JASO_MV:
					{
						for ( i = 0 ; i < nog_fc ; i++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								last_gi = ( last_gi >= rule->GlyfIndexMap[i][gi][k] ) ? last_gi : rule->GlyfIndexMap[i][gi][k];
							}
						}
					}
					break;

					case JASO_LC:
					{
						for ( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][gi] ) ? last_gi : rule->GlyfIndexMap[i][j][gi];
							}	
						}
					}	
					break;
				}
				rule->vcode_NumOfGlyf[m] = last_gi + 1;
			}

			if( ruletype == RULE_LC )
			{
				rule->vcode_NumOfGlyf[0] = 0;
			}
		}
		else
		{
			for( m = 0 ; m < JasoNum[jasotype] ; m++ )
			{
				int	gi			= this_grouplist->JasoGroupMap[m];
				int	last_gi_lsb	= 0;
				int	last_gi		= 0;

				switch( ruletype )
				{
					case JASO_FC:
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi_lsb = ( last_gi_lsb >= rule->Lsb[gi][j][k] ) ? last_gi_lsb : rule->Lsb[gi][j][k];
								last_gi = ( last_gi >= rule->GlyfWidthMap[gi][j][k] ) ? last_gi : rule->GlyfWidthMap[gi][j][k];
							}
						}
					}
					break;

					case JASO_MV:
					{
						for ( i = 0 ; i < nog_fc ; i++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								last_gi_lsb = ( last_gi_lsb >= rule->Lsb[i][gi][k] ) ? last_gi_lsb : rule->Lsb[i][gi][k];
								last_gi = ( last_gi >= rule->GlyfWidthMap[i][gi][k] ) ? last_gi : rule->GlyfWidthMap[i][gi][k];
							}
						}
					}
					break;

					case JASO_LC:
					{
						for ( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi_lsb = ( last_gi_lsb >= rule->Lsb[i][j][gi] ) ? last_gi_lsb : rule->Lsb[i][j][gi];
								last_gi = ( last_gi >= rule->GlyfWidthMap[i][j][gi] ) ? last_gi : rule->GlyfWidthMap[i][j][gi];
							}	
						}
					}	
					break;
				}
			}
		}
	}
}

static void HY_JasoGroupChange_atTail( JohapRule * johap, RULE * rule, JasoGroupList * src_grouplist, JasoGroupItem * src_jasoitem, int srcgroup, int dstgroup, int srcindex )
{
	uint8 mv_JasoVCode = src_jasoitem->JasoVCode[srcindex];
	int ruletype = johap->ruletype;
	int jasotype = johap->rule[ruletype]->jasotype;
	int oldjasonum = src_jasoitem->numJaso;
	int last_gi = 0;
	int i, j, k, m, g;

	// 먼저 src_jgi에 대한 조치로서 move_JasoVCode를 remove.
	HY_ArrayRemoveItem( (uint8 * )&src_jasoitem->JasoVCode[srcindex], srcindex, src_jasoitem->numJaso, sizeof( uint8 ) );
	src_jasoitem->numJaso--;

	// JasoVCode를 하나 뺌으로써 empty group item이 되었으면 group list에서 제거.
	if( src_jasoitem->numJaso == 0 )
	{
		HY_PreChangeForRemoveGroupItem( johap, ruletype, jasotype, srcgroup );
		HY_RemoveJasoItem_In_GroupList( johap, ruletype, jasotype, srcgroup );
		HY_RemoveJasoItem_In_Map( johap, ruletype, jasotype, srcgroup );
	}

	// group item add 전에 가장 큰 glyfindex값을 찾는다.
	last_gi = HY_Get_LastGlyfIndex( johap, rule, rule->grouplist[JASO_FC]->nGroups, rule->grouplist[JASO_MV]->nGroups, rule->grouplist[JASO_LC]->nGroups );

	// 새로운 last group item.
	JasoGroupItem * jasoitem_new = ( JasoGroupItem * ) calloc( 1, sizeof( JasoGroupItem ) );
	jasoitem_new->numJaso = 1;
	jasoitem_new->JasoVCode[0] = mv_JasoVCode;

	src_grouplist = HY_InsertJasoItem_by_GroupIndex( johap, src_grouplist, jasoitem_new, src_grouplist->nGroups );

	// --- JGL Level 변동 --- //
	//::~ Bug Fix : JasoGroupMap - cksun
	if( (oldjasonum == 1) && (src_grouplist->JasoGroupMap[mv_JasoVCode] < src_grouplist->nGroups - 1) )
	{
		for( i = 0 ; i < MAX_JASO_PER_JASOTYPE ; i++ )
		{
			if( src_grouplist->JasoGroupMap[mv_JasoVCode] < src_grouplist->JasoGroupMap[i] )
			{
				src_grouplist->JasoGroupMap[i]--;
			}
		}
	}
	//~::

	src_grouplist->JasoGroupMap[mv_JasoVCode] = dstgroup;

	/// --- RULE Level 변동 --- ///
	int nog_fc		= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv		= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc		= rule->grouplist[JASO_LC]->nGroups;
	int new_gi		= 0;
	int tmpindex		= 0;
	int tmplsb[nog_fc * nog_mv * nog_lc];
	int tmp[nog_fc * nog_mv * nog_lc];

	memset( tmp, -1, nog_fc * nog_mv * nog_lc );

	if( ruletype != RULE_WIDTH )
	{
		if( johap->johapIndexAddOpt == JOHAP_INDEX_COPY )
		{
			switch( jasotype )
			{
				case JASO_FC:
				{
					if( oldjasonum == 1 )
					{
						for( i = srcgroup ; i < nog_fc ; i++)
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( i == srcgroup )
									{
										tmp[tmpindex] = rule->GlyfIndexMap[srcgroup][j][k];
										tmpindex++;
									}
									rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i+1][j][k];
								}
							}
						}

						tmpindex = 0;
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->tmpgindex[dstgroup][j][k] = rule->GlyfIndexMap[dstgroup][j][k] = tmp[tmpindex];
								tmpindex++;
							}
						}
					}
					else
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->tmpgindex[dstgroup][j][k] = rule->GlyfIndexMap[dstgroup][j][k] = rule->GlyfIndexMap[srcgroup][j][k];
							}
						}
					}
				}
				break;
				case JASO_MV:
				{
					if( oldjasonum == 1 )
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = srcgroup ; j < nog_mv ; j++)
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( j == srcgroup )
									{
										tmp[tmpindex] = rule->GlyfIndexMap[i][srcgroup][k];
										tmpindex++;
									}
									rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i][j+1][k];
								}
							}
						}

						tmpindex = 0;
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->tmpgindex[i][dstgroup][k] = rule->GlyfIndexMap[i][dstgroup][k] = tmp[tmpindex];
								tmpindex++;
							}
						}
					}
					else
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->tmpgindex[i][dstgroup][k] = rule->GlyfIndexMap[i][dstgroup][k] = rule->GlyfIndexMap[i][srcgroup][k];
							}
						}
					}
				}
				break;
				case JASO_LC:
				{
					if( oldjasonum == 1 )
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++)
							{
								for( k = srcgroup ; k < nog_lc ; k++ )
								{
									if( k == srcgroup )
									{
										tmp[tmpindex] = rule->GlyfIndexMap[i][j][srcgroup];
										tmpindex++;
									}
									rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i][j][k+1];
								}
							}
						}

						tmpindex = 0;
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								rule->tmpgindex[i][j][dstgroup] = rule->GlyfIndexMap[i][j][dstgroup] = tmp[tmpindex];
								tmpindex++;
							}
						}
					}
					else
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								rule->tmpgindex[i][j][dstgroup] = rule->GlyfIndexMap[i][j][dstgroup] = rule->GlyfIndexMap[i][j][srcgroup];
							}
						}
					}
				}
				break;
			}
		}
		else
		{
			switch( ruletype )
			{
				case RULE_FC:
				{
					switch( jasotype )
					{
						case JASO_FC:
						{
							for( i = nog_fc - 1 ; i < nog_fc ; i++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									for( j = 0 ; j < nog_mv ; j++ )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = new_gi++;
									}
								}
							}
						}
						break;

						case JASO_MV:
						{
							for( i = 0 ; i < nog_fc ; i++ )
							{
								last_gi = 0;
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( k == 0 )
									{
										for( j = 0 ; j < nog_mv - 1 ; j++ )
										{
											last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
										}
										rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = ++last_gi;
									}
									else
									{
										for( j = 0 ; j < nog_mv ; j++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;

						case JASO_LC:
						{
							for( i = 0 ; i < nog_fc ; i++ )
							{
								last_gi = 0;
								for( j = 0 ; j < nog_mv ; j++ )
								{
									last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][nog_lc-2] ) ? last_gi : rule->GlyfIndexMap[i][j][nog_lc-2];
								}

								for( j = 0 ; j < nog_mv ; j++ )
								{
									rule->tmpgindex[i][j][nog_lc-1] = rule->GlyfIndexMap[i][j][nog_lc-1] = ( last_gi + 1 ) + ( ( j == 0 ) ? 0 : ( rule->GlyfIndexMap[i][j][nog_lc-2] - rule->GlyfIndexMap[i][0][nog_lc-2] ) );
								}
							}
						}
						break;
					}
				}
				break;

				case RULE_MV:
				{
					switch( jasotype )
					{
						case JASO_FC:
						{
							for( j = 0 ; j < nog_mv ; j++)
							{
								last_gi = 0;

								if( nog_lc == 1 )
								{
									for( i = 0 ; i < nog_fc ; i++ )
									{
										last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][0] ) ? last_gi : rule->GlyfIndexMap[i][j][0];
									}
									rule->tmpgindex[nog_fc-1][j][0] = rule->GlyfIndexMap[nog_fc-1][j][0] = last_gi + 1;
								}
								else
								{
									for( i = 0 ; i < nog_fc ; i++ )
									{
										last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][nog_lc-2] ) ? last_gi : rule->GlyfIndexMap[i][j][nog_lc-2];
									}
									rule->tmpgindex[nog_fc-1][j][0] = rule->GlyfIndexMap[nog_fc-1][j][0] = last_gi + 1;
								}
							}
						}
						break;

						case JASO_MV:
						{
							for( i = 0 ; i < nog_fc ; i++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = rule->GlyfIndexMap [i][srcgroup][k];
								}
							}
						}
						break;

						case JASO_LC:
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi = 0;

								for( i = 0 ; i < nog_fc ; i++ )
								{
									last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][nog_lc-2] ) ? last_gi : rule->GlyfIndexMap[i][j][nog_lc-2];
								}

								for( i = 0 ; i < nog_fc ; i++ )
								{
									rule->tmpgindex[i][j][nog_lc-1] = rule->GlyfIndexMap[i][j][nog_lc-1] = ( last_gi + 1 ) + ( ( i == 0 ) ? 0 : ( rule->GlyfIndexMap[i][j][nog_lc-2] - rule->GlyfIndexMap[0][j][nog_lc-2] ) );
								}
							}
						}
						break;
					}
				}
				break;

				case RULE_LC:
				{
					switch( jasotype )
					{
						case JASO_FC:
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								last_gi = 0;
								for( i = 0 ; i < nog_fc ; i++ )
								{
									if( i == 0 )
									{
										if( nog_mv == 1 )
										{
											rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = last_gi;
										}
										else
										{
											for( j = 0 ; j < nog_mv - 1 ; j++ )
											{
												last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
											}
											rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = ++last_gi;
										}
									}
									else
									{
										for(j=0; j<nog_mv; j++)
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;

						case JASO_MV:
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								last_gi = 0;
								for( i = 0 ; i < nog_fc ; i++ )
								{
									if( i == 0 )
									{
										for( j = 0 ; j < nog_mv - 1 ; j++ )
										{
											last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
										}
										rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = ++last_gi;
									}
									else
									{
										for( j = 0 ; j < nog_mv ; j++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;

						case JASO_LC:
						{
							for( k = nog_lc - 1 ; k < nog_lc ; k++ )
							{
								for( i = 0 ; i < nog_fc ; i++ )
								{
									for( j = 0 ; j < nog_mv ; j++ )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = new_gi++;
									}
								}
							}
						}
						break;
					}
				}
				break;
			}
		}

		JasoGroupList *	this_grouplist	= rule->grouplist[jasotype];
		JasoGroupItem *	jasoitem		= this_grouplist->jasoitem;

		for( m = 0 ; m < JasoNum[jasotype] ; m++ )
		{
			int gi = this_grouplist->JasoGroupMap[m];
			last_gi = 0;

			switch( ruletype )
			{
				case RULE_FC:
				{
					for( k = 0 ; k < nog_lc ; k++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							last_gi = ( last_gi >= rule->GlyfIndexMap[gi][j][k] ) ? last_gi : rule->GlyfIndexMap[gi][j][k];
						}
					}
				}
				break;

				case RULE_MV:
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							last_gi = ( last_gi >= rule->GlyfIndexMap[i][gi][k] ) ? last_gi : rule->GlyfIndexMap[i][gi][k];
						}
					}
				}
				break;

				case RULE_LC:
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][gi] ) ? last_gi : rule->GlyfIndexMap[i][j][gi];
						}
					}
				}
				break;
			}
			rule->vcode_NumOfGlyf[m] = last_gi + 1;
		}

		if( ruletype == RULE_LC )
		{
			rule->vcode_NumOfGlyf[0] = 0;
		}
	}
	else
	{
		switch( jasotype )
		{
			case JASO_FC:
			{
				if( oldjasonum == 1 )
				{
					for( i = srcgroup ; i < nog_fc ; i++)
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								if( i == srcgroup )
								{
									tmplsb[tmpindex] = rule->Lsb[srcgroup][j][k];
									tmp[tmpindex] = rule->GlyfWidthMap[srcgroup][j][k];
									tmpindex++;
								}
								rule->Lsb[i][j][k] = rule->Lsb[i+1][j][k];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i+1][j][k];
							}
						}
					}

					tmpindex = 0;
					for( j = 0 ; j < nog_mv ; j++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							rule->Lsb[dstgroup][j][k] = tmplsb[tmpindex];
							rule->GlyfWidthMap[dstgroup][j][k] = tmp[tmpindex];
							tmpindex++;
						}
					}
				}
				else
				{
					for( j = 0 ; j < nog_mv ; j++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							rule->Lsb[dstgroup][j][k] = rule->Lsb[srcgroup][j][k];
							rule->GlyfWidthMap[dstgroup][j][k] = rule->GlyfWidthMap[srcgroup][j][k];
						}
					}
				}
			}
			break;
			case JASO_MV:
			{
				if( oldjasonum == 1 )
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = srcgroup ; j < nog_mv ; j++)
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								if( j == srcgroup )
								{
									tmplsb[tmpindex] = rule->Lsb[i][srcgroup][k];
									tmp[tmpindex] = rule->GlyfWidthMap[i][srcgroup][k];
									tmpindex++;
								}
								rule->Lsb[i][j][k] = rule->Lsb[i][j+1][k];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j+1][k];
							}
						}
					}

					tmpindex = 0;
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							rule->Lsb[i][dstgroup][k] = tmplsb[tmpindex];
							rule->GlyfWidthMap[i][dstgroup][k] = tmp[tmpindex];
							tmpindex++;
						}
					}
				}
				else
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							rule->Lsb[i][dstgroup][k] = rule->Lsb[i][srcgroup][k];
							rule->GlyfWidthMap[i][dstgroup][k] = rule->GlyfWidthMap[i][srcgroup][k];
						}
					}
				}
			}
			break;
			case JASO_LC:
			{
				if( oldjasonum == 1 )
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++)
						{
							for( k = srcgroup ; k < nog_lc ; k++ )
							{
								if( k == srcgroup )
								{
									tmplsb[tmpindex] = rule->Lsb[i][j][srcgroup];
									tmp[tmpindex] = rule->GlyfWidthMap[i][j][srcgroup];
									tmpindex++;
								}
								rule->Lsb[i][j][k] = rule->Lsb[i][j][k+1];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j][k+1];
							}
						}
					}

					tmpindex = 0;
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							rule->Lsb[i][j][dstgroup] = tmplsb[tmpindex];
							rule->GlyfWidthMap[i][j][dstgroup] = tmp[tmpindex];
							tmpindex++;
						}
					}
				}
				else
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							rule->Lsb[i][j][dstgroup] = rule->Lsb[i][j][srcgroup];
							rule->GlyfWidthMap[i][j][dstgroup] = rule->GlyfWidthMap[i][j][srcgroup];
						}
					}
				}
			}
			break;
		}
	}
}

/*---------------------------------------------------------------------------
 *	JasoGroup_Change_AddJGI_atHead() -
 *	1st group을 다시 만들고 여기에 mv_JasoVCode를 add.
 ---------------------------------------------------------------------------*/
static void HY_JasoGroupChange_atHead( JohapRule * johap, RULE * rule, JasoGroupList * src_grouplist, JasoGroupItem * src_jasoitem, int srcgroup, int dstgroup, int srcindex )
{
	uint8 mv_JasoVCode	= src_jasoitem->JasoVCode[srcindex];
	int ruletype = johap->ruletype;
	int jasotype = johap->rule[ruletype]->jasotype;
	int oldjasonum = src_jasoitem->numJaso;
	int last_gi = 0;
	int i, j, k, m, g;

	// 먼저 src_jgi에 대한 조치로서 move_JasoVCode를 remove.
	HY_ArrayRemoveItem( (uint8 * )&src_jasoitem->JasoVCode[srcindex], srcindex, src_jasoitem->numJaso, sizeof( uint8 ) );
	src_jasoitem->numJaso--;

	// JasoVCode를 하나 뺌으로써 empty group item이 되었으면 group list에서 제거.
	if( src_jasoitem->numJaso == 0 )
	{
		HY_PreChangeForRemoveGroupItem( johap, ruletype, jasotype, srcgroup );
		HY_RemoveJasoItem_In_GroupList( johap, ruletype, jasotype, srcgroup );
		HY_RemoveJasoItem_In_Map( johap, ruletype, jasotype, srcgroup );
	}

	// group item add 전에 가장 큰 glyfindex값을 찾는다.
	for( i = 0 ; i < rule->grouplist[JASO_FC]->nGroups ; i++)
	{
		for( j = 0 ; j < rule->grouplist[JASO_MV]->nGroups ; j++ )
		{
			for( k = 0 ; k < rule->grouplist[JASO_LC]->nGroups ; k++ )
			{
				last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
			}
		}
	}
	last_gi++;

	// 새로운 1st group item을 선두에 insert.
	JasoGroupItem * jasoitem_new = ( JasoGroupItem * )calloc( 1, sizeof( JasoGroupItem ) );
	jasoitem_new->numJaso = 1;
	jasoitem_new->JasoVCode[0] = mv_JasoVCode;
	src_grouplist = HY_InsertJasoItem_by_GroupIndex( johap, src_grouplist, jasoitem_new, 0 );

	/// --- JGL Level 변동 --- ///
	if( (oldjasonum == 1) && (src_grouplist->JasoGroupMap[mv_JasoVCode] < src_grouplist->nGroups - 1) )
	{
		for( i = 0 ; i < MAX_JASO_PER_JASOTYPE ; i++ )
		{
			if( src_grouplist->JasoGroupMap[mv_JasoVCode] < src_grouplist->JasoGroupMap[i] )
			{
				src_grouplist->JasoGroupMap[i]--;
			}
		}
	}

	if( dstgroup < 0 )
	{
		dstgroup = 0;
	}
	src_grouplist->JasoGroupMap[mv_JasoVCode] = dstgroup;

	for( i = 0 ; i < MAX_JASO_PER_JASOTYPE ; i++ )
	{
		if (src_grouplist->JasoGroupMap[i] >= 0 && i != (int)mv_JasoVCode)
		{
			src_grouplist->JasoGroupMap[i]++;
		}
	}

	int nog_fc	= rule->grouplist[JASO_FC]->nGroups;
	int nog_mv	= rule->grouplist[JASO_MV]->nGroups;
	int nog_lc	= rule->grouplist[JASO_LC]->nGroups;
	int new_gi	= 0;
	int tmplsb[nog_fc * nog_mv * nog_lc];
	int tmp[nog_fc * nog_mv * nog_lc];
	int tmpindex	= 0;

	memset( tmp, -1, nog_fc * nog_mv * nog_lc );
	if( ruletype != RULE_WIDTH )
	{
		if( johap->johapIndexAddOpt == JOHAP_INDEX_COPY )
		{
			switch( jasotype )
			{
				case JASO_FC:
				{
					if( oldjasonum == 1 )
					{
						for( i = srcgroup ; i != 0 ; i--)
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( i == srcgroup )
									{
										if( ruletype != RULE_WIDTH )
										{
											tmp[tmpindex] = rule->GlyfIndexMap[srcgroup][j][k];
										}
										else
										{
											tmplsb[tmpindex] = rule->Lsb[srcgroup][j][k];
											tmp[tmpindex] = rule->GlyfWidthMap[srcgroup][j][k];
										}
										tmpindex++;
									}
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i-1][j][k];
									}
									else
									{
										rule->Lsb[i][j][k] = rule->Lsb[i-1][j][k];
										rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i-1][j][k];
									}
								}
							}
						}

						tmpindex = 0;
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								if( ruletype != RULE_WIDTH )
								{
									rule->tmpgindex[dstgroup][j][k] = rule->GlyfIndexMap[dstgroup][j][k] = tmp[tmpindex];
								}
								else
								{
									rule->Lsb[dstgroup][j][k] = tmplsb[tmpindex];
									rule->GlyfWidthMap[dstgroup][j][k] = tmp[tmpindex];
								}
								tmpindex++;
							}
						}
					}
					else
					{
						for( i = nog_fc ; i != 0 ; i-- )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i-1][j][k];
									}
									else
									{
										rule->Lsb[i][j][k] = rule->Lsb[i-1][j][k];
										rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i-1][j][k];
									}
								}
							}
						}

						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[dstgroup][j][k] = rule->GlyfIndexMap[dstgroup][j][k] = rule->GlyfIndexMap[srcgroup+1][j][k];
									}
									else
									{
										rule->Lsb[dstgroup][j][k] = rule->Lsb[srcgroup+1][j][k];
										rule->GlyfWidthMap[dstgroup][j][k] = rule->GlyfWidthMap[srcgroup+1][j][k];
									}
								}
							}
						}
					}
				}
				break;
				case JASO_MV:
				{
					if( oldjasonum == 1 )
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = srcgroup ; j != 0 ; j--)
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( j == srcgroup )
									{
										if( ruletype != RULE_WIDTH )
										{
											tmp[tmpindex] = rule->GlyfIndexMap[i][srcgroup][k];
										}
										else
										{
											tmplsb[tmpindex] = rule->Lsb[i][srcgroup][k];
											tmp[tmpindex] = rule->GlyfWidthMap[i][srcgroup][k];
										}
										tmpindex++;
									}
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i][j-1][k];
									}
									else
									{
										rule->Lsb[i][j][k] = rule->Lsb[i][j-1][k];
										rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j-1][k];
									}
								}
							}
						}

						tmpindex = 0;
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								if( ruletype != RULE_WIDTH )
								{
									rule->tmpgindex[i][dstgroup][k] = rule->GlyfIndexMap[i][dstgroup][k] = tmp[tmpindex];
								}
								else
								{
									rule->Lsb[i][dstgroup][k] = tmplsb[tmpindex];
									rule->GlyfWidthMap[i][dstgroup][k] = tmp[tmpindex];
								}
								tmpindex++;
							}
						}
					}
					else
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = nog_mv ; j != 0 ; j-- )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i][j-1][k];
									}
									else
									{
										rule->Lsb[i][j][k] = rule->Lsb[i][j-1][k];
										rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j-1][k];
									}
								}
							}
						}

						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][dstgroup][k] = rule->GlyfIndexMap[i][dstgroup][k] = rule->GlyfIndexMap[i][srcgroup+1][k];
									}
									else
									{
										rule->Lsb[i][dstgroup][k] = rule->Lsb[i][srcgroup+1][k];
										rule->GlyfWidthMap[i][dstgroup][k] = rule->GlyfWidthMap[i][srcgroup+1][k];
									}
								}
							}
						}
					}
				}
				break;
				case JASO_LC:
				{
					if( oldjasonum == 1 )
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = srcgroup ; k != 0 ; k-- )
								{
									if( k == srcgroup )
									{
										if( ruletype != RULE_WIDTH )
										{
											tmp[tmpindex] = rule->GlyfIndexMap[i][j][srcgroup];
										}
										else
										{
											tmplsb[tmpindex] = rule->Lsb[i][j][srcgroup];
											tmp[tmpindex] = rule->GlyfWidthMap[i][j][srcgroup];
										}
										tmpindex++;
									}
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i][j][k-1];
									}
									else
									{
										rule->Lsb[i][j][k] = rule->Lsb[i][j][k-1];
										rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j][k-1];
									}
								}
							}
						}

						tmpindex = 0;
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								if( ruletype != RULE_WIDTH )
								{
									rule->tmpgindex[i][j][dstgroup] = rule->GlyfIndexMap[i][j][dstgroup] = tmp[tmpindex];
								}
								else
								{
									rule->Lsb[i][j][dstgroup] = tmplsb[tmpindex];
									rule->GlyfWidthMap[i][j][dstgroup] = tmp[tmpindex];
								}
								tmpindex++;
							}
						}
					}
					else
					{
						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = nog_lc ; k != 0 ; k-- )
								{
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = rule->GlyfIndexMap[i][j][k-1];
									}
									else
									{
										rule->Lsb[i][j][k] = rule->Lsb[i][j][k-1];
										rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j][k-1];
									}
								}
							}
						}

						for( i = 0 ; i < nog_fc ; i++ )
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( ruletype != RULE_WIDTH )
									{
										rule->tmpgindex[i][j][dstgroup] = rule->GlyfIndexMap[i][j][dstgroup] = rule->GlyfIndexMap[i][j][srcgroup+1];
									}
									else
									{
										rule->Lsb[i][j][dstgroup] = rule->Lsb[i][j][srcgroup+1];
										rule->GlyfWidthMap[i][j][dstgroup] = rule->GlyfWidthMap[i][j][srcgroup+1];
									}
								}
							}
						}
					}
				}
				break;
			}
		}
		else
		{
			switch( ruletype )
			{
				case RULE_FC:
				{
					switch( jasotype )
					{
						case JASO_FC:
						{
							for( i = nog_fc - 1 ; i < nog_fc ; i++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									for( j = 0 ; j < nog_mv ; j++ )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = new_gi++;
									}
								}
							}
						}
						break;
						case JASO_MV:
						{
							for( i = 0 ; i < nog_fc ; i++ )
							{
								last_gi = 0;
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( k == 0 )
									{
										for( j = 0 ; j < nog_mv - 1 ; j++ )
										{
											last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
										}
										rule->tmpgindex[i][nog_mv - 1][k] = rule->GlyfIndexMap[i][nog_mv - 1][k] = ++last_gi;
									}
									else
									{
										for ( j = 0 ; j < nog_mv ; j++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;
						case JASO_LC:
						{
							for( i = 0 ; i < nog_fc ; i++ )
							{
								last_gi = 0;
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if( k == 0 )
									{
										if( nog_mv == 1 )
										{
											rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = last_gi;
										}
										else
										{
											for( j = 0 ; j < nog_mv - 1 ; j++ )
											{
												last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
											}
											rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = ++last_gi;
										}
									}
									else
									{
										for( j = 0 ; j < nog_mv ; j++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;
					}
				}
				break;
				case RULE_MV:
				{
					switch( jasotype )
					{
						case JASO_FC:
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi = 0;
								for( i = 0 ; i < nog_fc ; i++ )
								{
									if( i == 0 )
									{
										if( nog_lc == 1 )
										{
											rule->tmpgindex[i][j][nog_lc-1] = rule->GlyfIndexMap[i][j][nog_lc-1] = last_gi;
										}
										else
										{
											for( k = 0 ; k < nog_lc - 1 ; k++ )
											{
												last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
											}
											rule->tmpgindex[i][j][nog_lc-1] = rule->GlyfIndexMap[i][j][nog_lc-1] = ++last_gi;
										}
									}
									else
									{
										for( k = 0 ; k < nog_lc ; k++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;
						case JASO_MV:
						{
							for( j = nog_mv - 1 ; j < nog_mv ; j++ )
							{
								for( k = 0 ; k < nog_lc ; k++ )
								{
									for( i = 0 ; i < nog_fc ; i++ )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = new_gi++;
									}
								}
							}
						}
						break;
						case JASO_LC:
						{
							for( j = 0 ; j < nog_mv ; j++ )
							{
								last_gi = 0;
								for( k = 0 ; k < nog_lc ; k++ )
								{
									if (k==0)
									{
										for( i = 0 ; i < nog_fc - 1 ; i++ )
										{
											last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
										}
										rule->tmpgindex[nog_fc-1][j][k] = rule->GlyfIndexMap[nog_fc-1][j][k] = ++last_gi;
									}
									else
									{
										for( i = 0 ; i < nog_fc ; i++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;
					}
				}
				break;
				case RULE_LC:
				{
					switch( jasotype )
					{
						case JASO_FC:
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								last_gi = 0;
								for( i = 0 ; i < nog_fc ; i++ )
								{
									if( i == 0 )
									{
										if( nog_mv == 1 )
										{
											rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = last_gi;
										}
										else
										{
											for( j = 0 ; j < nog_mv - 1 ; j++ )
											{
												last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
											}
											rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = ++last_gi;
										}
									}
									else
									{
										for( j = 0 ; j < nog_mv ; j++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;

						case JASO_MV:
						{
							for( k = 0 ; k < nog_lc ; k++)
							{
								last_gi = 0;
								for( i = 0 ; i < nog_fc ; i++ )
								{
									if( i == 0 )
									{
										for( j = 0 ; j < nog_mv - 1 ; j++ )
										{
											last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][k] ) ? last_gi : rule->GlyfIndexMap[i][j][k];
										}
										rule->tmpgindex[i][nog_mv-1][k] = rule->GlyfIndexMap[i][nog_mv-1][k] = ++last_gi;
									}
									else
									{
										for( j = 0 ; j < nog_mv ; j++ )
										{
											rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = ++last_gi;
										}
									}
								}
							}
						}
						break;

						case JASO_LC:
						{
							for( k = nog_lc - 1 ; k < nog_lc ; k++ )
							{
								for( i = 0 ; i < nog_fc ; i++ )
								{
									for( j = 0 ; j < nog_mv; j++ )
									{
										rule->tmpgindex[i][j][k] = rule->GlyfIndexMap[i][j][k] = new_gi++;
									}
								}
							}
						}
						break;
					}
				}
				break;
			}
		}

		JasoGroupList *	this_grouplist	= rule->grouplist[jasotype];
		JasoGroupItem *	jasoitem		= this_grouplist->jasoitem;

		for( m = 0 ; m < JasoNum[jasotype] ; m++ )
		{
			int gi = this_grouplist->JasoGroupMap[m];
			last_gi = 0;

			switch( ruletype )
			{
				case JASO_FC:
				{
					for (k=0; k<nog_lc; k++)
					{
						for (j=0; j<nog_mv; j++)
						{
							last_gi = ( last_gi >= rule->GlyfIndexMap[gi][j][k] ) ? last_gi : rule->GlyfIndexMap[gi][j][k];
						}
					}
				}
				break;
				case JASO_MV:
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							last_gi = ( last_gi >= rule->GlyfIndexMap[i][gi][k] ) ? last_gi : rule->GlyfIndexMap[i][gi][k];
						}
					}
				}
				break;
				case JASO_LC:
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							last_gi = ( last_gi >= rule->GlyfIndexMap[i][j][gi] ) ? last_gi : rule->GlyfIndexMap[i][j][gi];
						}
					}
				}
				break;
			}
			rule->vcode_NumOfGlyf[m] = last_gi + 1;
		}

		if( ruletype == RULE_LC )
		{
			rule->vcode_NumOfGlyf[0] = 0;
		}
	}
	else
	{
		switch( jasotype )
		{
			case JASO_FC:
			{
				if( oldjasonum == 1 )
				{
					for( i = srcgroup ; i != 0 ; i--)
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								if( i == srcgroup )
								{
									tmplsb[tmpindex] = rule->Lsb[srcgroup][j][k];
									tmp[tmpindex] = rule->GlyfWidthMap[srcgroup][j][k];
									tmpindex++;
								}
								rule->Lsb[i][j][k] = rule->Lsb[i-1][j][k];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i-1][j][k];
							}
						}
					}

					tmpindex = 0;
					for( j = 0 ; j < nog_mv ; j++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							rule->Lsb[dstgroup][j][k] = tmplsb[tmpindex];
							rule->GlyfWidthMap[dstgroup][j][k] = tmp[tmpindex];
							tmpindex++;
						}
					}
				}
				else
				{
					for( i = nog_fc ; i != 0 ; i-- )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->Lsb[i][j][k] = rule->Lsb[i-1][j][k];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i-1][j][k];
							}
						}
					}

					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->Lsb[dstgroup][j][k] = rule->Lsb[srcgroup+1][j][k];
								rule->GlyfWidthMap[dstgroup][j][k] = rule->GlyfWidthMap[srcgroup+1][j][k];
							}
						}
					}
				}
			}
			break;
			case JASO_MV:
			{
				if( oldjasonum == 1 )
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = srcgroup ; j != 0 ; j--)
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								if( j == srcgroup )
								{
									tmplsb[tmpindex] = rule->Lsb[i][srcgroup][k];
									tmp[tmpindex] = rule->GlyfWidthMap[i][srcgroup][k];
									tmpindex++;
								}
								rule->Lsb[i][j][k] = rule->Lsb[i][j-1][k];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j-1][k];
							}
						}
					}

					tmpindex = 0;
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( k = 0 ; k < nog_lc ; k++ )
						{
							rule->Lsb[i][dstgroup][k] = tmplsb[tmpindex];
							rule->GlyfWidthMap[i][dstgroup][k] = tmp[tmpindex];
							tmpindex++;
						}
					}
				}
				else
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = nog_mv ; j != 0 ; j-- )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->Lsb[i][j][k] = rule->Lsb[i][j-1][k];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j-1][k];
							}
						}
					}

					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->Lsb[i][dstgroup][k] = rule->Lsb[i][srcgroup+1][k];
								rule->GlyfWidthMap[i][dstgroup][k] = rule->GlyfWidthMap[i][srcgroup+1][k];
							}
						}
					}
				}
			}
			break;
			case JASO_LC:
			{
				if( oldjasonum == 1 )
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = srcgroup ; k != 0 ; k-- )
							{
								if( k == srcgroup )
								{
									tmplsb[tmpindex] = rule->Lsb[i][j][srcgroup];
									tmp[tmpindex] = rule->GlyfWidthMap[i][j][srcgroup];
									tmpindex++;
								}
								rule->Lsb[i][j][k] = rule->Lsb[i][j][k-1];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j][k-1];
							}
						}
					}

					tmpindex = 0;
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							rule->Lsb[i][j][dstgroup] = tmplsb[tmpindex];
							rule->GlyfWidthMap[i][j][dstgroup] = tmp[tmpindex];
							tmpindex++;
						}
					}
				}
				else
				{
					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = nog_lc ; k != 0 ; k-- )
							{
								rule->Lsb[i][j][k] = rule->Lsb[i][j][k-1];
								rule->GlyfWidthMap[i][j][k] = rule->GlyfWidthMap[i][j][k-1];
							}
						}
					}

					for( i = 0 ; i < nog_fc ; i++ )
					{
						for( j = 0 ; j < nog_mv ; j++ )
						{
							for( k = 0 ; k < nog_lc ; k++ )
							{
								rule->Lsb[i][j][dstgroup] = rule->Lsb[i][j][srcgroup+1];
								rule->GlyfWidthMap[i][j][dstgroup] = rule->GlyfWidthMap[i][j][srcgroup+1];
							}
						}
					}
				}
			}
			break;
		}
	}
}

void HY_CheckJohapIndexMaxValue( JohapRule * johap )
{
	RULE * rule;
	int i, j, nog_fc, nog_mv, nog_lc;

	for( i = 0 ; i < NUM_COMBI_RULE - 1 ; i++ )
	{
		rule = johap->rule[i];
		switch( i )
		{
			case RULE_FC:
			{
				nog_fc = rule->grouplist[JASO_FC]->nGroups;
				rule->old_maxval = calloc( nog_fc, sizeof( int ) );
				for( j = 0 ; j < nog_fc ; j++ )
				{
					rule->old_maxval[j] = HY_GetMaxJohapIndexValue( johap, i, j );
				}
			}
			break;
			case RULE_MV:
			{
				nog_mv = rule->grouplist[JASO_MV]->nGroups;
				rule->old_maxval = calloc( nog_mv, sizeof( int ) );
				for( j = 0 ; j < nog_mv ; j++ )
				{
					rule->old_maxval[j] = HY_GetMaxJohapIndexValue( johap, i, j );
				}
			}
			break;
			case RULE_LC:
			{
				nog_lc = rule->grouplist[JASO_LC]->nGroups;
				rule->old_maxval = calloc( nog_lc, sizeof( int ) );
				for( j = 0 ; j < nog_lc ; j++ )
				{
					rule->old_maxval[j] = HY_GetMaxJohapIndexValue( johap, i, j );
				}
			}
			break;
		}
	}
}

static void HY_JasoGroupChange( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	RULE * rule = johap->rule[johap->ruletype];
	int srcgroupindex = johap->curSelBeginRow;
	int dstgroupindex = johap->curSelEndRow;
	int srcjasoindex = johap->curSelBeginCol;
	int jasotype	= johap->rule[johap->ruletype]->jasotype;
	JasoGroupList * grouplist = rule->grouplist[jasotype];

	if( srcgroupindex < 0 || srcgroupindex >= grouplist->nGroups )
	{
		return;
	}

	JasoGroupItem * src_jasoitem = HY_GetJasoItem_By_GroupIndex( grouplist, srcgroupindex );

	if( srcgroupindex == dstgroupindex )
	{
		return;
	}

	if( srcjasoindex < 0 || srcjasoindex >= src_jasoitem->numJaso)
	{
		return;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	if( dstgroupindex < 0 )
	{
		// Jaso Group Added to Head
		if( src_jasoitem->numJaso == 1 )
		{
			return;
		}
		dstgroupindex = 0;
		HY_JasoGroupChange_atHead( johap, rule, grouplist, src_jasoitem, srcgroupindex, dstgroupindex, srcjasoindex );
	}
	else if( dstgroupindex >= grouplist->nGroups )
	{
		// Jaso Group Added to Tail
		if( src_jasoitem->numJaso == 1 )
		{
			return;
		}
		dstgroupindex = grouplist->nGroups;
		HY_JasoGroupChange_atTail( johap, rule, grouplist, src_jasoitem, srcgroupindex, dstgroupindex, srcjasoindex );
	}
	else
	{
		// Jaso Group Added to Middle
		HY_JasoGroupChange_inMidl( johap, rule, grouplist, src_jasoitem, srcgroupindex, dstgroupindex, srcjasoindex );
	}
	HY_SetupJohapIndexView( johap, johap->ruletype );

	// Save Johap Index Max Value
	HY_CheckJohapIndexMaxValue( johap );
}

static void HY_RuleGroupMouseDown( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	int	ruletype		= johap->ruletype;
	int	jasotype	= johap->rule[ruletype]->jasotype;
	int	selectCol	= ( event->u.mouse.x - GVIEW_LEFT_BORDER ) / XSPACE_JASO_IMG;
	int	selectRow	= ( event->u.mouse.y - GVIEW_TOP_BORDER ) / YSPACE_JASO_IMG;
	int	dropCol		= ( event->u.mouse.x - GVIEW_LEFT_BORDER ) / XSPACE_JASO_IMG;
	int	dropRow	= ( event->u.mouse.y - GVIEW_TOP_BORDER ) / YSPACE_JASO_IMG;
	int i, j;

	if( event->u.mouse.x < GVIEW_LEFT_BORDER || event->u.mouse.y < GVIEW_TOP_BORDER )
	{
		johap->bPressed = false;
		return;
	}

	if( event->u.mouse.button == 1 )
	{
		johap->bPressed = true;
		JasoGroupList *	grouplist = johap->rule[ruletype]->grouplist[jasotype];
		JasoGroupItem *	jasoitem = grouplist->jasoitem;
		johap->curSelBeginCol	= selectCol;
		johap->curSelBeginRow	= selectRow;

		for( i = 0 ; i < grouplist->nGroups ; i++ )
		{
			for( j = 0 ; j < jasoitem->numJaso ; j++ )
			{
				if( i == selectRow && j == selectCol )
				{
					GDrawSetCursor( pixmap, ct_hand );
					jasoitem->bSelect[j] = true;

					johap->dstX			= event->u.mouse.x;
					johap->dstY			= event->u.mouse.y;
					johap->OldMouseX	= event->u.mouse.x;
					johap->OldMouseY	= event->u.mouse.y;

					break;
				}
				else
				{
					jasoitem->bSelect[j] = false;
				}
			}
			jasoitem = jasoitem->next;
		}
		GDrawRequestExpose( pixmap, NULL, false );
	}
}

static void HY_RuleGroupMouseUp( GWindow pixmap, GEvent * event, JohapRule * johap )
{
	int	ruletype		= johap->ruletype;
	int	jasotype	= johap->rule[ruletype]->jasotype;
	int	dropCol		= ( event->u.mouse.x - GVIEW_LEFT_BORDER ) / XSPACE_JASO_IMG;
	int	dropRow	= ( event->u.mouse.y - GVIEW_TOP_BORDER ) / YSPACE_JASO_IMG;
	int	i, j;

	if( event->u.mouse.y < GVIEW_TOP_BORDER )
	{
		dropRow = -1;
	}

	if( johap->bPressed )
	{
		JasoGroupList *	grouplist = johap->rule[ruletype]->grouplist[jasotype];
		JasoGroupItem *	jasoitem = grouplist->jasoitem;

		for( i = 0 ; i < grouplist->nGroups ; i++ )
		{
			for( j = 0 ; j < jasoitem->numJaso ; j++ )
			{
				jasoitem->bSelect[j] = false;
			}
			jasoitem = jasoitem->next;
		}
		johap->curSelEndCol	= dropCol;
		johap->curSelEndRow	= dropRow;
		johap->bPressed		= false;

		if( johap->curSelBeginRow != johap->curSelEndRow )
		{
			// Change Jaso Group
			HY_JasoGroupChange( pixmap, event, johap );
		}
		GDrawSetCursor( pixmap, ct_mypointer );
		GDrawRequestExpose( pixmap, NULL, false );
	}
}

static int groupview_e_h( GWindow pixmap, GEvent *event )
{
	JohapRule * johap = GDrawGetUserData( pixmap );

	switch( event->type )
	{
		case et_expose:
		{
			HY_GroupViewExpose( pixmap, event, johap );
		}
		break;
		case et_mousedown:
		{
			HY_RuleGroupMouseDown( pixmap, event, johap );
		}
		break;
		case et_mouseup:
		{
			HY_RuleGroupMouseUp( pixmap, event, johap );
		}
		break;
	}
	return( true );
}

static int rule_e_h( GWindow pixmap, GEvent *event )
{
	switch( event->type )
	{
		case et_close:
		{
			JohapRule * johap = GDrawGetUserData( pixmap );
			HY_RollBackRuleJoahp( johap );
			GDrawDestroyWindow( pixmap );
		}
	}
	return( true );	
}

void HY_SetupJohapIndexView( JohapRule * johap, int ruletype )
{
	int nog_fc, nog_mv, nog_lc;

	nog_fc = johap->rule[ruletype]->grouplist[JASO_FC]->nGroups;
	nog_mv = johap->rule[ruletype]->grouplist[JASO_MV]->nGroups;
	nog_lc = johap->rule[ruletype]->grouplist[JASO_LC]->nGroups;

	johap->rule[ruletype]->IndexArr_RowCnt = nog_lc * nog_fc;
	if( ruletype != RULE_WIDTH )
	{
		johap->rule[ruletype]->IndexArr_ColCnt = nog_mv;
	}
	else
	{
		johap->rule[ruletype]->IndexArr_ColCnt = nog_mv * 2;
	}
}

JohapRule * HY_InitJohapRule( SplineFont * sf, JohapRule * johap )
{
	int ruleindex, jasotype;
	int jasoindex, groupindex;
	int nog_fc, nog_mv, nog_lc;
	int cho, ju, jo;

	johap->ruletype = 0;
	for( ruleindex = 0 ; ruleindex < NUM_COMBI_RULE ; ruleindex++ )
	{
		johap->rule[ruleindex] = calloc( 1, sizeof( RULE *) );
		for( jasotype = 0 ; jasotype < NUM_JASO_TYPES ; jasotype++ )
		{
			johap->rule[ruleindex]->grouplist[jasotype] = calloc( 1, sizeof( JasoGroupList ) );
			johap->rule[ruleindex]->grouplist[jasotype]->nGroups = 1;

			for( jasoindex = 0 ; jasoindex < MAX_JASO_PER_JASOTYPE ; jasoindex++ )
			{
				if( johap->rule[ruleindex]->grouplist[jasotype]->nGroups )
				{
					johap->rule[ruleindex]->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
					johap->rule[ruleindex]->grouplist[jasotype]->JasoGroupMap[jasoindex] = 0;
				}
			}

			JasoGroupList * grouplist = johap->rule[ruleindex]->grouplist[jasotype];
			JasoGroupItem * jasoitem_prev = NULL;
			JasoGroupItem * jasoitem = ( JasoGroupItem * )calloc( 1, sizeof( JasoGroupItem ) );
			grouplist->jasoitem = jasoitem;

			for( groupindex = 0 ; groupindex < grouplist->nGroups ; groupindex++ )
			{
				switch( jasotype )
				{
					case JASO_FC:
					{
						jasoitem->numJaso = NUM_JASO_FC;
					}
					break;
					case JASO_MV:
					{
						jasoitem->numJaso = NUM_JASO_MV;
					}
					break;
					case JASO_LC:
					{
						jasoitem->numJaso = NUM_JASO_LC;
					}
					break;
				}

				for( jasoindex = 0 ; jasoindex < jasoitem->numJaso ; jasoindex++ )
				{
					johap->rule[ruleindex]->vcode_NumOfGlyf[jasoindex] = 1;
					jasoitem->bSelect[jasoindex] = false;
					jasoitem->JasoVCode[jasoindex] = ( uint8 )jasoindex;
				}

				jasoitem_prev = jasoitem;
				jasoitem->next = ( JasoGroupItem * ) calloc( 1, sizeof( JasoGroupItem ) );
				jasoitem = jasoitem->next;
			}
		}

		nog_fc	= johap->rule[ruleindex]->grouplist[JASO_FC]->nGroups;
		nog_mv	= johap->rule[ruleindex]->grouplist[JASO_MV]->nGroups;
		nog_lc	= johap->rule[ruleindex]->grouplist[JASO_LC]->nGroups;

		for( cho = 0 ; cho < nog_fc ; cho++ )
		{
			for( ju = 0 ; ju < nog_mv ; ju++ )
			{
				for( jo = 0 ; jo < nog_lc ; jo++ )
				{
					if( ruleindex == RULE_WIDTH )
					{
						johap->rule[ruleindex]->Lsb[cho][ju][jo] = 0;
						johap->rule[ruleindex]->GlyfWidthMap[cho][ju][jo] = 0;
						johap->rule[ruleindex]->tmplsb[cho][ju][jo] = 0;
						johap->rule[ruleindex]->tmpwidth[cho][ju][jo] = 0;
					}
					else
					{
						johap->rule[ruleindex]->tmpgindex[cho][ju][jo] = johap->rule[ruleindex]->GlyfIndexMap[cho][ju][jo] = 0;
					}
				}
			}
		}
	}
	return johap;
}

static void HY_MakeRollbackFile( FILE *tmpfp, struct compositionrules_t *rules )
{
	int ruletype;
	int jasoindex;
	int remapsize;
	int lsbmapsize;

	if( rules == NULL )
	{
		return;
	}

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE ; ++ruletype )
	{
		// JasoGroupMap
		for( jasoindex = 0 ; jasoindex < NUM_JASO_FC ; ++jasoindex )
		{
			fprintf( tmpfp, "%d ", rules->chosung_groups[ruletype][jasoindex]);
		}
		putc('\n',tmpfp);

		for( jasoindex = 0 ; jasoindex < NUM_JASO_MV ; ++jasoindex )
		{
			fprintf( tmpfp, "%d ", rules->jungsung_groups[ruletype][jasoindex]);
		}
		putc('\n',tmpfp);

		for( jasoindex = 0 ; jasoindex < NUM_JASO_LC ; ++jasoindex )
		{
			fprintf( tmpfp, "%d ", rules->jongsung_groups[ruletype][jasoindex]);
		}
		putc('\n',tmpfp);

		// nGroups
		fprintf( tmpfp, "%d %d %d\n", rules->num_groups[ruletype][0], rules->num_groups[ruletype][1], rules->num_groups[ruletype][2]);
	}

	/********************************************************
						GlyfIndexMap
	********************************************************/
	int remapindex = 0;
	int lsbmapindex = 0;

	remapsize = rules->num_groups[0][0] * rules->num_groups[0][1] * rules->num_groups[0][2];
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fprintf( tmpfp, "%d ", rules->chosung_remap[remapindex] );
	}
	putc('\n',tmpfp);

	remapsize = rules->num_groups[1][0] * rules->num_groups[1][1] * rules->num_groups[1][2];
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fprintf( tmpfp, "%d ", rules->jungsung_remap[remapindex] );
	}
	putc('\n',tmpfp);

	remapsize = rules->num_groups[2][0] * rules->num_groups[2][1] * rules->num_groups[2][2];
	for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
	{
		fprintf( tmpfp, "%d ", rules->jongsung_remap[remapindex] );
	}
	putc('\n',tmpfp);

	fprintf( tmpfp, "%d\n", rules->width_map != NULL );

	
	/********************************************************
						WidthMap
	********************************************************/
	if ( rules->width_map != NULL )
	{
		remapsize = rules->num_groups[RULE_WIDTH][JASO_FC] * rules->num_groups[RULE_WIDTH][JASO_MV] * rules->num_groups[RULE_WIDTH][JASO_LC];
		for( remapindex = 0 ; remapindex < remapsize ; ++remapindex )
		{
			fprintf( tmpfp, "%d ", rules->width_map[remapindex] );
		}
		putc('\n',tmpfp);
	}

	if ( rules->lsb_map != NULL )
	{
		lsbmapsize = rules->num_groups[RULE_WIDTH][JASO_FC] * rules->num_groups[RULE_WIDTH][JASO_MV] * rules->num_groups[RULE_WIDTH][JASO_LC];
		for( lsbmapindex = 0 ; lsbmapindex < lsbmapsize ; ++lsbmapindex )
		{
			fprintf( tmpfp, "%d ", rules->lsb_map[lsbmapindex] );
		}
		putc('\n',tmpfp);
	}
}

static int HY_CheckJohapRuleChanged( FILE * tmpfp, JohapRule * johap )
{
	JohapRule * bak_johap;
	int ruletype;
	int jasotype;
	int groupindex, jasoindex;
	int nog_fc, cho;
	int nog_mv, ju;
	int nog_lc, jo;

	bak_johap = calloc( 1, sizeof( JohapRule ) );
	bak_johap = HY_LoadRollbackRuleJohap( tmpfp );

	for( ruletype = 0 ; ruletype < NUM_COMBI_RULE ; ruletype++ )
	{
		if( ruletype != RULE_WIDTH )
		{
			RULE * rule = johap->rule[ruletype];
			RULE * bak_rule = bak_johap->rule[ruletype];

			for( jasotype = 0 ; jasotype < NUM_JASO_TYPES ; jasotype++ )
			{
				JasoGroupList * grplist = johap->rule[ruletype]->grouplist[jasotype];
				JasoGroupList * bak_grplist = bak_johap->rule[ruletype]->grouplist[jasotype];

				nog_fc = johap->rule[ruletype]->grouplist[JASO_FC]->nGroups;
				nog_mv = johap->rule[ruletype]->grouplist[JASO_MV]->nGroups;
				nog_lc = johap->rule[ruletype]->grouplist[JASO_LC]->nGroups;

				if( grplist->nGroups != bak_grplist->nGroups )
				{
					hydebug( "[%s] 1. Rule Group Changed! (%d / %d)\n", __FUNCTION__, ruletype, jasotype );
					return 1;
				}

				for( jasoindex = 0 ; jasoindex < MAX_JASO_PER_JASOTYPE ; jasoindex++ )
				{
					if( grplist->JasoGroupMap[jasoindex] != bak_grplist->JasoGroupMap[jasoindex] )
					{
						hydebug( "[%s] 2. Rule Group Changed! (%d / %d)\n", __FUNCTION__, ruletype, jasotype );
						return 1;
					}
				}

				JasoGroupItem * jtem = grplist->jasoitem;
				JasoGroupItem * bak_jtem = bak_grplist->jasoitem;
				for( groupindex = 0 ; groupindex < grplist->nGroups ; groupindex++ )
				{
					if( jtem->numJaso != bak_jtem->numJaso )
					{
						hydebug( "[%s] 3. Rule Group Changed! (%d / %d)\n", __FUNCTION__, ruletype, jasotype );
						return 1;
					}

					for( jasoindex = 0 ; jasoindex < jtem->numJaso ; jasoindex++ )
					{
						if( jtem->JasoVCode[jasoindex] != bak_jtem->JasoVCode[jasoindex] )
						{
							hydebug( "[%s] 4. Rule Group Changed! (%d / %d)\n", __FUNCTION__, ruletype, jasotype );
							return 1;
						}
					}
					jtem = jtem->next;
					bak_jtem = bak_jtem->next;
				}

				for( jasoindex = 0 ; jasoindex < JasoNum[jasotype] ; jasoindex++ )
				{
					if( johap->rule[ruletype]->vcode_NumOfGlyf[jasoindex] != bak_johap->rule[ruletype]->vcode_NumOfGlyf[jasoindex] )
					{
						hydebug( "[%s] 5. Rule Group Changed! (%d / %d)\n", __FUNCTION__, ruletype, jasotype );
						return 1;
					}
				}

				for( cho = 0 ; cho < nog_fc ; cho++ )
				{
					for( ju = 0 ; ju < nog_mv ; ju++ )
					{
						for( jo = 0 ; jo < nog_lc ; jo++ )
						{
							if( rule->GlyfIndexMap[cho][ju][jo] != bak_rule->GlyfIndexMap[cho][ju][jo] )
							{
								hydebug( "[%s] Johap Index Changed! (%d / %d - GlyfIndexMap[%d][%d][%d]\n", __FUNCTION__, ruletype, jasotype, cho, ju, jo );
								return 1;
							}
						}
					}
				}
			}
		}
	}
	return 0;
}

static int HY_RuleJohapChangeOK( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		hydebug( "[%s]\n", __FUNCTION__ );

		GWindow gw = GGadgetGetWindow( g );
		JohapRule *johap = GDrawGetUserData( gw );
		SplineFont *sf = johap->sf;
		FILE *tmpfp;
		int done = 0;

		GDrawDestroyWindow( gw );

		if( johap->sf->rollbak_fname[0] != '\0' )
		{
			tmpfp = fopen( johap->sf->rollbak_fname, "r" );
			if( tmpfp != NULL )
			{
				if( HY_CheckJohapRuleChanged( tmpfp, johap ) )
				{
					HY_RulesResetVarientCnts( johap );
					if( johap->threepairmethod )
					{
#if RESET_GLYF_TEST
						HY_Reset_GlyfList_ThreepairJohap_New( sf, johap );
#else
						HY_Reset_GlyfList_ThreepairJohap( sf, johap );
#endif
					}
					else
					{
						if( sf->bMMGFont != 0 )
						{
							#if 0
							HY_Reset_GlyfList_MMG( sf, johap );
							#endif
						}
						else
						{
#if RESET_GLYF_TEST
							HY_Reset_GlyfList_Normal_New( sf, johap );
#else
							HY_Reset_GlyfList_Normal( sf, johap );
#endif
						}
					}
				}
				fclose( tmpfp );

				if( access(johap->sf->rollbak_fname, F_OK) == 0 )
				{
					DeleteFile( johap->sf->rollbak_fname );
					memset( johap->sf->rollbak_fname, '\0', PATH_MAX );
				}
				else
				{
					ff_post_error( _("debug"), _("1. file not exist") );
					hydebug( "[%s] 1. file not exist\n", __FUNCTION__ );
				}
			}
			else
			{
				ff_post_error( _("debug"), _("1. Failed to open rule info file") );
				hydebug( "[%s] 1. Failed to open rule info file\n", __FUNCTION__ );
			}
		}
	}
	return( true );
}

static int HY_RuleJohapChangeCancel( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw		= GGadgetGetWindow( g );
		JohapRule *johap	= GDrawGetUserData( gw );

		HY_RollBackRuleJoahp( johap );
		GDrawDestroyWindow(gw);
	}
	return( true );
}

void HY_ModifyRuleJohapDlg( FontView * fv )
{
	SplineFont *			sf = fv->b.sf;
	GWindow				gw;
	GWindowAttrs			wattrs;
	GRect				pos;
	GGadgetCreateData	gView[10];
	GGadgetCreateData	Elems[10];
	GGadgetCreateData	mGcd[15];
	GTabInfo				MainTab[5];
	GTabInfo				SubTab[4];
	GTextInfo			Label[15];
	int					i = 0, k = 0;
	int					labelindex = 0;
	int					gcdindex = 0;

	JohapRule * johap = calloc( 1, sizeof( JohapRule ) );
	if( sf->hasRule )
	{
		johap = sf->johap;
		for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
		{
			johap->rule[i] = sf->johap->rule[i];
		}
		johap->sf = sf;
	}
	else
	{
		johap				= HY_InitJohapRule( sf, johap );
		johap->sf			= sf;
		sf->johap			= johap;
		HY_SetupJohapIndexView( johap, RULE_FC );
		HY_SetupJohapIndexView( johap, RULE_MV );
		HY_SetupJohapIndexView( johap, RULE_LC );
		HY_SetupJohapIndexView( johap, RULE_WIDTH );
	}

	for( i = 0 ; i < NUM_COMBI_RULE ; i++ )
	{
		johap->rule[i]->jasotype = 0;
	}
	johap->ruletype			= 0;
	johap->johapIndexAddOpt	= JOHAP_INDEX_COPY;
	johap->threepairmethod	= 0;
	johap->curSelBeginRow	= -1;
	johap->curSelBeginCol		= -1;
	johap->curSelEndRow		= -1;
	johap->curSelEndCol		= -1;
	johap->bPressed			= 0;
	johap->bDrag				= 0;
	johap->OldMouseX			= 0;
	johap->OldMouseY			= 0;
	johap->dstX				= 0;
	johap->dstY				= 0;
	johap->bChangedGroup	= 0;
	johap->bChangedIndex		= 0;
	johap->bChangedWidth		= 0;

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask				= wam_events | wam_cursor | wam_utf8_wtitle | wam_backcol;
	wattrs.background_color	= COLOR_DEFAULT;
	wattrs.event_masks		= -1;
	wattrs.restrict_input_to_me	= 1;
	wattrs.is_dlg				= true;
	wattrs.undercursor			= 1;
	wattrs.cursor				= ct_pointer;
	if( sf->hasRule )
	{
		wattrs.utf8_window_title	= _("Hangul Johap - Modify Composition");
	}
	else
	{
		wattrs.utf8_window_title	= _("Hangul Johap - New Composition");
	}

	// Get System Resolution ( x, y )
	int resx =  GetSystemMetrics(SM_CXSCREEN);
	int resy =  GetSystemMetrics(SM_CYSCREEN);
	hydebug( "[%s] Screen Resolution: [ %d  %d ]\n", __FUNCTION__, resx, resy );

	pos.x		= 100;
	pos.y		= 100;
	pos.width		= 1000;
	pos.height	= 1000;
	hydebug( "[%s] main window size: [ %d  %d ]\n", __FUNCTION__, pos.width, pos.height );
	gw = johap->group_view = GDrawCreateTopWindow( NULL, &pos, rule_e_h, johap, &wattrs );

	memset( &gView, 0, sizeof(gView) );
	memset( &Elems, 0, sizeof(Elems) );
	memset( &mGcd, 0, sizeof(mGcd) );
	memset( &MainTab, 0, sizeof(MainTab) );
	memset( &SubTab, 0, sizeof(SubTab) );
	memset( &Label, 0, sizeof(Label) );

	gView[0].gd.pos.x				= 5;
	gView[0].gd.pos.y				= 1;
	gView[0].gd.pos.width			= 560;
	gView[0].gd.pos.height			= 700;
	hydebug( "[%s] group view main window size: [ %d  %d ]\n", __FUNCTION__, gView[0].gd.pos.width, gView[0].gd.pos.height );
	gView[0].gd.flags				= gg_visible | gg_enabled;
	gView[0].gd.u.drawable_e_h		= groupview_e_h;
	gView[0].gd.cid				= CID_GroupView;
	gView[0].creator				= GDrawableCreate;

	// Jaso Groups Tab
	SubTab[0].text				= ( unichar_t *) _( "Cho Sung Group" );
	SubTab[0].text_is_1byte		= true;
	SubTab[0].gcd				= gView;
	
	SubTab[1].text				= ( unichar_t *) _( "Jung Sung Group" );
	SubTab[1].text_is_1byte		= true;
	SubTab[1].gcd				= gView;

	SubTab[2].text				= ( unichar_t *) _( "Jong Sung Group" );
	SubTab[2].text_is_1byte		= true;
	SubTab[2].gcd				= gView;

	Elems[k].gd.pos.x				= 0;
	Elems[k].gd.pos.y				= 0;
	Elems[k].gd.pos.width			= 570;
	Elems[k].gd.pos.height			= 720;
	Elems[k].gd.u.tabs				= SubTab;
	hydebug( "[%s] group view sub window size: [ %d  %d ]\n", __FUNCTION__, Elems[k].gd.pos.width, Elems[k].gd.pos.height );
	Elems[k].gd.flags				= gg_visible | gg_enabled;
	Elems[k].gd.handle_controlevent	= HY_JasoTabChanged;
	Elems[k].gd.cid				= CID_SubTabSet;
	Elems[k++].creator			= GTabSetCreate;

	// Rule Tab
	MainTab[0].text				= ( unichar_t *) _( "Cho Sung Rule" );
	MainTab[0].text_is_1byte		= true;
	MainTab[0].gcd				= Elems;

	MainTab[1].text				= ( unichar_t *) _( "Jung Sung Rule" );
	MainTab[1].text_is_1byte		= true;
	MainTab[1].gcd				= Elems;

	MainTab[2].text				= ( unichar_t *) _( "Jong Sung Rule" );
	MainTab[2].text_is_1byte		= true;
	MainTab[2].gcd				= Elems;

	MainTab[3].text				= ( unichar_t *) _( "Width Rule" );
	MainTab[3].text_is_1byte		= true;
	MainTab[3].gcd				= Elems;

	gcdindex = 0;
	mGcd[gcdindex].gd.pos.x				= 10;
	mGcd[gcdindex].gd.pos.y				= 10;
	mGcd[gcdindex].gd.pos.width			= 580;
	mGcd[gcdindex].gd.pos.height			= 730;
	mGcd[gcdindex].gd.u.tabs				= MainTab;
	mGcd[gcdindex].gd.flags				= gg_enabled | gg_visible;
	mGcd[gcdindex].gd.handle_controlevent	= HY_RuleTabChanged;
	mGcd[gcdindex].gd.cid					= CID_MainTabSet;
	mGcd[gcdindex].creator					= GTabSetCreate;
	gcdindex++;

	mGcd[gcdindex].gd.pos.x				= 600;
	mGcd[gcdindex].gd.pos.y				= 25;
	mGcd[gcdindex].gd.pos.width			= 120;
	mGcd[gcdindex].gd.flags				= gg_enabled | gg_visible;
	mGcd[gcdindex].creator					= GLineCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _( "Johap-Index Add Opt:" );
	Label[labelindex].text_is_1byte			= true;
	mGcd[gcdindex].gd.pos.x				= mGcd[gcdindex - 1].gd.pos.x + 5;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + 5;
	mGcd[gcdindex].gd.pos.width			= 100;
	mGcd[gcdindex].gd.pos.height			= 20;
	mGcd[gcdindex].gd.flags				= gg_enabled | gg_visible;
	mGcd[gcdindex].gd.label				= &Label[labelindex++];
	mGcd[gcdindex].gd.cid					= CID_LabelJohapIndexOption;
	mGcd[gcdindex].creator					= GLabelCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _( "New Index" );
	Label[labelindex].text_is_1byte			= true;
	mGcd[gcdindex].gd.pos.x				= mGcd[gcdindex - 1].gd.pos.x + 5;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + 20;
	mGcd[gcdindex].gd.pos.width			= 90;
	mGcd[gcdindex].gd.pos.height			= 20;
	if( johap->johapIndexAddOpt == JOHAP_INDEX_NEW )
	{
		mGcd[gcdindex].gd.flags			= gg_enabled | gg_visible | gg_cb_on;
	}
	else
	{
		mGcd[gcdindex].gd.flags			= gg_enabled | gg_visible;
	}
	mGcd[gcdindex].gd.label				= &Label[labelindex++];
	mGcd[gcdindex].gd.cid					= CID_RadioJohapIndexNew;
	mGcd[gcdindex].gd.handle_controlevent	= HY_JasoGroupAddOptChange;
	mGcd[gcdindex].creator					= GRadioCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _( "Copy Index" );
	Label[labelindex].text_is_1byte			= true;
	mGcd[gcdindex].gd.pos.x				= mGcd[gcdindex - 1].gd.pos.x;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + mGcd[gcdindex - 1].gd.pos.height + 3;
	mGcd[gcdindex].gd.pos.width			= mGcd[gcdindex - 1].gd.pos.width;
	mGcd[gcdindex].gd.pos.height			= mGcd[gcdindex - 1].gd.pos.height;
	if( johap->johapIndexAddOpt == JOHAP_INDEX_COPY )
	{
		mGcd[gcdindex].gd.flags			= gg_enabled | gg_visible | gg_cb_on;
	}
	else
	{
		mGcd[gcdindex].gd.flags			= gg_enabled | gg_visible;
	}
	mGcd[gcdindex].gd.label				= &Label[labelindex++];
	mGcd[gcdindex].gd.cid					= CID_RadioJohapIndexCopy;
	mGcd[gcdindex].gd.handle_controlevent	= HY_JasoGroupAddOptChange;
	mGcd[gcdindex].creator					= GRadioCreate;
	gcdindex++;

	mGcd[gcdindex].gd.pos.x				= 600;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + mGcd[gcdindex - 1].gd.pos.height + 5;
	mGcd[gcdindex].gd.pos.width			= 120;
	mGcd[gcdindex].gd.flags				= gg_enabled | gg_visible;
	mGcd[gcdindex].creator					= GLineCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _("Three Pair Method");
	Label[labelindex].text_is_1byte			= true;
	Label[labelindex].text_in_resource		= true;
	mGcd[gcdindex].gd.pos.x				= mGcd[gcdindex - 1].gd.pos.x + 5;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + 10;
	mGcd[gcdindex].gd.label				= &Label[labelindex++];
	mGcd[gcdindex].gd.flags				= gg_enabled | gg_visible | gg_utf8_popup;
	mGcd[gcdindex].gd.cid					= CID_CheckThreePairMethod;
	mGcd[gcdindex].gd.handle_controlevent	= HY_CheckThreePairMethod;
	mGcd[gcdindex].creator					= GCheckBoxCreate;
	gcdindex++;

	mGcd[gcdindex].gd.pos.x				= 600;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + 25;
	mGcd[gcdindex].gd.pos.width			= 120;
	mGcd[gcdindex].gd.flags				= gg_enabled | gg_visible;
	mGcd[gcdindex].creator					= GLineCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _( "Edit" );
	Label[labelindex].text_is_1byte			= true;
	mGcd[gcdindex].gd.pos.x				= 610;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + 10;
	mGcd[gcdindex].gd.pos.width			= 90;
	mGcd[gcdindex].gd.pos.height			= 20;
	mGcd[gcdindex].gd.flags				= gg_visible | gg_enabled;
	mGcd[gcdindex].gd.label				= &Label[labelindex++];
	mGcd[gcdindex].gd.handle_controlevent	= HY_JohapIndexEdit;
	mGcd[gcdindex].gd.cid					= CID_BtnJoHapIndexEdit;
	mGcd[gcdindex].creator					= GButtonCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _( "OK" );
	Label[labelindex].text_is_1byte			= true;
	mGcd[gcdindex].gd.pos.x				= mGcd[gcdindex - 1].gd.pos.x;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + mGcd[gcdindex - 1].gd.pos.height + 10;
	mGcd[gcdindex].gd.pos.width			= 90;
	mGcd[gcdindex].gd.pos.height			= 20;
	mGcd[gcdindex].gd.flags				= gg_visible | gg_enabled;
	mGcd[gcdindex].gd.label				= &Label[labelindex++];
	mGcd[gcdindex].gd.handle_controlevent	= HY_RuleJohapChangeOK;
	mGcd[gcdindex].gd.cid					= CID_BtnRtfEditOK;
	mGcd[gcdindex].creator					= GButtonCreate;
	gcdindex++;

	Label[labelindex].text					= (unichar_t *) _( "Cancel" );
	Label[labelindex].text_is_1byte			= true;
	mGcd[gcdindex].gd.pos.x				= mGcd[gcdindex - 1].gd.pos.x;
	mGcd[gcdindex].gd.pos.y				= mGcd[gcdindex - 1].gd.pos.y + mGcd[gcdindex - 1].gd.pos.height + 5;
	mGcd[gcdindex].gd.pos.width			= 90;
	mGcd[gcdindex].gd.pos.height			= 20;
	mGcd[gcdindex].gd.flags				= gg_visible | gg_enabled;
	mGcd[gcdindex].gd.label				= &Label[labelindex++]; 
	mGcd[gcdindex].gd.handle_controlevent	= HY_RuleJohapChangeCancel;
	mGcd[gcdindex].gd.cid					= CID_BtnRtfEditCancel;
	mGcd[gcdindex].creator					= GButtonCreate;

	HY_CheckJohapIndexMaxValue( johap );
	GGadgetsCreate( gw, mGcd );
	GDrawSetVisible( gw, true );
}

static void FVMenuHYModifyRuleJohap(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	int i, gid;
	DBounds bb;

	if( sf->hasRule )
	{
		for( i = 0 ; i < sf->map->enccount ; i++ )
		{
			gid = sf->map->map[i];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting(sc) )
			{
				sc->orgwidth = sc->prevwidth = sc->curwidth = sc->width;
				sc->width_off = 0;
				SplineCharFindBounds( sc, &bb );
				sc->orglsb = sc->prevlsb = sc->curlsb = bb.minx;
				sc->lsb_off = 0;
			}
		}

		// make backup rule files
		FILE * tmpfp;
		if( sf->filename != NULL )
		{
			sprintf( sf->rollbak_fname, "%s", sf->filename );
		}
		else
		{
			sprintf( sf->rollbak_fname, "%s", sf->origname );
		}
		
		GFileRemoveExtension( sf->rollbak_fname );
		strcat( sf->rollbak_fname, "-ruleinfo.rollback" );
		hydebug( "[%s] rollback_fname: <%s>\n", __FUNCTION__, sf->rollbak_fname );
		
		tmpfp = fopen( sf->rollbak_fname, "w" );
		if( tmpfp != NULL )
		{
			HY_MakeRollbackFile( tmpfp, HY_ConvertToRollbackFile( sf->johap ) );
			fclose( tmpfp );

			HY_ModifyRuleJohapDlg( fv );
		}
		else
		{
			hydebug( "[%s] Failed to open rollback rule file! <%s>\n", __FUNCTION__, sf->rollbak_fname );
		}
	}
	else
	{
		ff_post_error( _("Error"), _("File has not Rule Table") );
	}
}

#if 1
// New
static void FVMenuHYMMGHangulJohap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	int i, j, gid;
	int cho = 0;
	int jung = 0;
	int jong = 0;
	int cnt = 0xD7A3 - 0xAC00;

	if( !sf->bMMGFont )
	{
		return;
	}

	switch( mi->mid )
	{
		case MID_HYMMGJohap_Hangul_All:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build All Hangul (11,172)..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;

#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif
					if( jungsung_type[jung] == JUNG_TYPE_HORI )
					{
						HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
					}
					else if( jungsung_type[jung] == JUNG_TYPE_VERT )
					{
						HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
					}
					else if( jungsung_type[jung] == JUNG_TYPE_COMP )
					{
						HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Hangul_KSC5601:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build KSC5601 Hangul (2,350)..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				if( HY_GetKSC5601(i) )
				{
					gid = sf->map->map[i];
					if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
					{
						cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
						jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
						jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
						hydebug( "[%s]======================================================================\n", __FUNCTION__ );
						hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
						hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
						hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

						if( jungsung_type[jung] == JUNG_TYPE_HORI )
						{
							HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
						}
						else if( jungsung_type[jung] == JUNG_TYPE_VERT )
						{
							HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
						}
						else if( jungsung_type[jung] == JUNG_TYPE_COMP )
						{
							HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
						}
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Jung_Hori:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Horizontal JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_HORI )
					{
						HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Jung_Vert:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Vertical JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_VERT )
					{
						HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Jung_Comp:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Composit JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_COMP )
					{
						HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		default:
		break;
	}
	sf->bMGJohap = 1;
}
#else
// Old
static void FVMenuHYMMGHangulJohap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	int i, j, gid;
	int cho = 0;
	int jung = 0;
	int jong = 0;
	int cnt = 0xD7A3 - 0xAC00;

	if( !sf->bMMGFont )
	{
		return;
	}

	switch( mi->mid )
	{
		case MID_HYMMGJohap_Hangul_All:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build All Hangul (11,172)..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;

#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif
					if( jungsung_type[jung] == JUNG_TYPE_HORI )
					{
						HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
					}
					else if( jungsung_type[jung] == JUNG_TYPE_VERT )
					{
						HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
					}
					else if( jungsung_type[jung] == JUNG_TYPE_COMP )
					{
						HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Hangul_KSC5601:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build KSC5601 Hangul (2,350)..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				if( HY_GetKSC5601(i) )
				{
					gid = sf->map->map[i];
					if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
					{
						cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
						jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
						jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
						hydebug( "[%s]======================================================================\n", __FUNCTION__ );
						hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
						hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
						hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

						if( jungsung_type[jung] == JUNG_TYPE_HORI )
						{
							HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
						}
						else if( jungsung_type[jung] == JUNG_TYPE_VERT )
						{
							HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
						}
						else if( jungsung_type[jung] == JUNG_TYPE_COMP )
						{
							HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
						}
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Jung_Hori:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Horizontal JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_HORI )
					{
						HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Jung_Vert:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Vertical JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_VERT )
					{
						HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYMMGJohap_Jung_Comp:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Composit JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_COMP )
					{
						HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		default:
		break;
	}
	sf->bMGJohap = 1;
}
#endif

static int nlgetc( FILE *fp )
{
	int ch, ch2;

	ch=getc(fp);
	if ( ch!='\\' )
		return( ch );

	ch2 = getc(fp);
	if ( ch2=='\n' )
		return( nlgetc(fp));

	ungetc(ch2, fp);
	return( ch );
}

static int getreal( FILE *sfd, real *val )
{
	char tokbuf[100];
	int ch;
	char *pt=tokbuf, *end = tokbuf+100-2, *nend;

	while ( isspace(ch = nlgetc(sfd)));
	if ( ch!='e' && ch!='E' )		/* real's can't begin with exponants */
	{
		while ( isdigit(ch) || ch=='-' || ch=='+' || ch=='e' || ch=='E' || ch=='.' || ch==',' )
		{
			if ( pt<end )
			{
				*pt++ = ch;
			}
			ch = nlgetc(sfd);
		}
	}

	*pt='\0';
	ungetc(ch,sfd);
	*val = strtod(tokbuf,&nend);
	/* Beware of different locals! */
	if ( *nend!='\0' )
	{
		if ( *nend=='.' )
		{
			*nend = ',';
		}
		else if ( *nend==',' )
		{
			*nend = '.';
		}
		*val = strtod(tokbuf,&nend);
	}
	return( pt!=tokbuf && *nend=='\0'?1:ch==EOF?-1: 0 );
}

static int getint( FILE *fp, int *val )
{
	char tokbuf[100];
	int ch;
	char *pt=tokbuf, *end = tokbuf + 100 - 2;

	while ( isspace(ch = nlgetc(fp)));
	if ( ch == '-' || ch == '+' )
	{
		*pt++ = ch;
		ch = nlgetc(fp);
	}
	while ( isdigit(ch))
	{
		if ( pt < end )
			*pt++ = ch;
		ch = nlgetc(fp);
	}
	*pt='\0';
	ungetc(ch, fp);
	*val = strtol(tokbuf, NULL, 10);
	return( pt != tokbuf ? 1 : ch == EOF ? -1 : 0 );
}

static int hy_getprotectedname( FILE *fp, char *tokbuf )
{
	char *pt = tokbuf;
	char *end = tokbuf + 100 - 2;
	int ch;

	while ( (ch = nlgetc(fp)) == ' ' || ch == '\t' );
	while ( ch!=EOF && !isspace(ch) && ch != '[' && ch != ']' && ch != '{' && ch != '}' && ch != '<' && ch != '%' )
	{
		if( pt < end )
		{
			*pt++ = ch;
		}
		ch = nlgetc(fp);
	}
	if ( pt == tokbuf && ch != EOF )
	{
		*pt++ = ch;
	}
	else
	{
		ungetc(ch, fp);
	}
	*pt='\0';
	return( pt!=tokbuf ? 1 : ch == EOF ? -1 : 0 );
}

static int hy_getname( FILE *fp, char *tokbuf )
{
	int ch;

	while ( isspace(ch = nlgetc(fp)) );
	ungetc(ch, fp);
	return( hy_getprotectedname(fp, tokbuf) );
}

static int HY_GetChar(FILE * fp)
{
	int ch1;
	int ch2;

	ch1 = fgetc(fp);
	ch2 = fgetc(fp);

	return( (ch2 << 8) | (ch1) );
}

static void HY_FilterBaseChars( FontView *fv )
{
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	FILE * fp;
	int i;
	int gid = -1;
	int ucs;
	int bmatch = 0;

	for ( i = 0xAC00 ; i < 0xD7A4 ; i++ )
	{
		bmatch = 0;
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sc) )
		{
			fp = fopen( "basecharlist.txt", "r" );
			if( fp == NULL )
			{
				return;
			}
			while( (ucs = HY_GetChar(fp)) != EOF )
			{
				// begin 2bytes, line feed, carrage return
				if( ucs == 0Xfeff ) continue;
				if( ucs == 0xd ) continue;
				if( ucs == 0xa ) continue;

				if( i == ucs )
				{
					bmatch = 1;
				}
			}
			if( !bmatch )
			{
				SCClearAll(sf->glyphs[gid], ly_fore);
			}
			fclose(fp);
		}
	}
}

static void FVMenuHYRemoveNoneBaseChar(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);

	HY_FilterBaseChars( fv );
}

static void HY_GetLinkMap( FontView *fv )
{
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	struct splinecharlist *dlist;
	RefChar * ref;
	int i, gid;
	FILE * log;
	int logerr = 0;
	int flags = 0;

	if( flags )
	{
		// uni
		log = fopen( "linkmap-uni.txt", "w" );
		if( log == NULL )
		{
			logerr = 1;
		}
		for( i = 0 ; i < 0xffff ; i++ )
		{
			gid = sf->map->map[i];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting(sc) && sc->unicodeenc != -1)
			{
				if( !logerr )
				{
					fprintf( log, "%d\n", i );
				}
				for( ref = sc->layers[ly_fore].refs ; ref != NULL ; ref = ref->next )
				{
					if( ref->sc->bmaster )
					{
						if( !logerr )
						{
							fprintf( log, "%d %s\n", ref->sc->mmg_type, ref->sc->name );
						}
					}
				}
			}
		}
	}
	else
	{
		// mg
		log = fopen( "linkmap-mg.txt", "w" );
		if( log == NULL )
		{
			logerr = 1;
		}
		for( i = 65536 ; i < sf->map->enccount ; i++ )
		{
			gid = sf->map->map[i];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting(sc) && sc->bmaster )
			{
				if( !logerr )
				{
					fprintf( log, "%d %d\n", sc->mmg_type, i );
				}

				for( dlist = sc->dependents ; dlist != NULL ; dlist = dlist->next )
				{
					fprintf( log, "%d\n", dlist->sc->unicodeenc );
				}
			}
		}
	}
	if( !logerr )
	{
		fclose( log );
	}
}

static void FVMenuHYGetLinkMap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	FILE *fp = NULL;
	FILE *mapfp = NULL;
	int ucs;
	int mgindex = 0;
	int i;

	fp = fopen( "link_chosung.txt", "r" );
	mapfp = fopen( "cho-sung-linkmap.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		mgindex = 65536;
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				mgindex++;
				if( mgindex > 65559 )
				{
					break;
				}
				continue;
			}
			fprintf( mapfp, "\t{%d, 0x%x},\n", mgindex, ucs );
		}
		fclose(fp);
		fclose(mapfp);
	}

	fp = NULL;
	mapfp = NULL;
	
	fp = fopen( "link_jungsung.txt", "r" );
	mapfp = fopen( "jung-sung-linkmap.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		mgindex = 65560;
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				mgindex++;
				if( mgindex > 65580 )
				{
					break;
				}
				continue;
			}
			fprintf( mapfp, "\t{%d, 0x%x},\n", mgindex, ucs );
		}
		fclose(fp);
		fclose(mapfp);
	}

	fp = NULL;
	mapfp = NULL;

	fp = fopen( "link_jongsung.txt", "r" );
	mapfp = fopen( "jong-sung-linkmap.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		mgindex = 65581;
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				mgindex++;
				if( mgindex > 65614 )
				{
					break;
				}
				continue;
			}
			fprintf( mapfp, "\t{%d, 0x%x},\n", mgindex, ucs );
		}
		fclose(fp);
		fclose(mapfp);
	}

#if 0
	fp = fopen( "link_jungsung_comp.txt", "r" );
	mapfp = fopen( "jung-sung-comp-linkmap.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		fprintf( mapfp, "{\n\t//jung-sung-comp linkmap\n" );
		mgindex = 65635;
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				mgindex++;
				if( mgindex > 65664 )
				{
					break;
				}
				continue;
			}
			if( mgindex == 65664 )
			{
				fprintf( mapfp, "\t{%d, 0x%x}\n}", mgindex, ucs );
			}
			else
			{
				fprintf( mapfp, "\t{%d, 0x%x},\n", mgindex, ucs );
			}
		}
		fclose(fp);
		fclose(mapfp);
	}
	fp = NULL;
	mapfp = NULL;
#endif
#if 0
	HY_GetLinkMap( fv, 0 );
#endif
}

static void FVMenuHYGetBaseCharMap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	FILE *fp = NULL;
	FILE *mapfp = NULL;
	int ucs;
	int mgindex = 0;
	int i;
	int flag = 0;

	fp = fopen( "basecharmap1.txt", "r" );
	mapfp = fopen( "basecharmap_hex1.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				continue;
			}
			if( flag == 0 )
			{
				fprintf( mapfp, "\t{ 0x%x, ", ucs );
				flag = 1;
			}
			else
			{
				fprintf( mapfp, "0x%x },\n", ucs );
				flag = 0;
			}
		}
		fclose(fp);
		fclose(mapfp);
	}
	fp = NULL;
	mapfp = NULL;

	fp = fopen( "basecharmap2.txt", "r" );
	mapfp = fopen( "basecharmap_hex2.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				continue;
			}
			if( flag == 0 )
			{
				fprintf( mapfp, "\t{ 0x%x, ", ucs );
				flag = 1;
			}
			else
			{
				fprintf( mapfp, "0x%x },\n", ucs );
				flag = 0;
			}
		}
		fclose(fp);
		fclose(mapfp);
	}
	fp = NULL;
	mapfp = NULL;

	fp = fopen( "basecharmap3.txt", "r" );
	mapfp = fopen( "basecharmap_hex3.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				continue;
			}
			if( flag == 0 )
			{
				fprintf( mapfp, "\t{ 0x%x, ", ucs );
				flag = 1;
			}
			else
			{
				fprintf( mapfp, "0x%x },\n", ucs );
				flag = 0;
			}
		}
		fclose(fp);
		fclose(mapfp);
	}
	fp = NULL;
	mapfp = NULL;

	fp = fopen( "basecharmap4.txt", "r" );
	mapfp = fopen( "basecharmap_hex4.txt", "w" );
	if( fp != NULL && mapfp != NULL )
	{
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				continue;
			}
			if( flag == 0 )
			{
				fprintf( mapfp, "\t{ 0x%x, ", ucs );
				flag = 1;
			}
			else
			{
				fprintf( mapfp, "0x%x },\n", ucs );
				flag = 0;
			}
		}
		fclose(fp);
		fclose(mapfp);
	}
	fp = NULL;
	mapfp = NULL;
}

static void FVMenuHYGetBaseCharUnicode(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc = NULL;
	int i;
	int gid;
	FILE *fp;
	FILE *fp2;
	int ucs;
	int index = 0;

	fp = fopen( "mytest.txt", "r" );
	fp2 = fopen( "mytest2.txt", "w" );
	if( fp != NULL && fp2 != NULL )
	{
		while( (ucs = HY_GetChar(fp)) != EOF )
		{
			if( ucs == 0Xfeff )
			{
				continue;
			}
			if( ucs == 0xd )
			{
				continue;
			}
			if( ucs == 0xa )
			{
				index++;
				continue;
			}

			if( index == 0 )
			{
				fprintf( fp2, "\t{0xba64, 0x%x},\n", ucs );
			}
			else if( index == 1 )
			{
				fprintf( fp2, "\t{0xbab8, 0x%x},\n", ucs );
			}
			else if( index == 2 )
			{
				fprintf( fp2, "\t{0xbad4, 0x%x},\n", ucs );
			}
		}
		fclose(fp);
		fclose(fp2);
	}
}

static void HY_FitHeight( SplineChar *sc, int base_layer, int other_layer )
{
	DBounds bb[2];
	real trans[6] = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	SplineCharLayerFindBounds( sc, base_layer, &bb[0] );
	SplineCharLayerFindBounds( sc, other_layer, &bb[1] );
	trans[0] = 1.0;
	trans[3] = (bb[0].maxy - bb[0].miny) / (bb[1].maxy - bb[1].miny);
	trans[4] = bb[0].minx - bb[1].minx;
	trans[5] = bb[0].maxy - bb[1].maxy;
	SplinePointListTransform( sc->layers[other_layer].splines, trans, tpt_AllPoints );
}

static void HY_FitWidth( SplineChar *sc, int base_layer, int other_layer )
{
	DBounds bb[2];
	real trans[6] = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	SplineCharLayerFindBounds( sc, base_layer, &bb[0] );
	SplineCharLayerFindBounds( sc, other_layer, &bb[1] );
	trans[0] = (bb[0].maxx - bb[0].minx) / (bb[1].maxx - bb[1].minx);
	trans[3] = 1.0;
	trans[4] = 0.0;
	trans[5] = 0.0;
	SplinePointListTransform( sc->layers[other_layer].splines, trans, tpt_AllPoints );
}

static void HY_FitBBox( SplineChar *sc, int base_layer, int other_layer )
{
	DBounds bb[2];
	real trans[6] = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	SplineCharLayerFindBounds( sc, base_layer, &bb[0] );
	SplineCharLayerFindBounds( sc, other_layer, &bb[1] );

	if( bb[0].minx != bb[1].minx )
	{
		trans[4] = bb[0].minx - bb[1].minx;
	}
	if( bb[0].maxy != bb[1].maxy )
	{
		trans[5] = bb[0].maxy - bb[1].maxy;
	}
	SplinePointListTransform( sc->layers[other_layer].splines, trans, tpt_AllPoints );
}

static void HY_TransformLayersCho( SplineFont *sf, SplineChar *sc )
{
	int i = 0;
	int gid = -1;
	int layer = 0;
	DBounds bb[6];
	real trans[6]	= { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
	real emsize = s_emsize;
	real h1, h2;
	real w1, w2;
	real tx, ty;
	real dx, dy;

	for( layer = ly_fore ; layer < sc->layer_cnt ; layer++ )
	{
		switch( layer )
		{
			case ly_fore:
			{
				/*
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_back, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = chobb.minx - bb[0].minx;
				dy = chobb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
				*/
			}
			break;
			case ly_small:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = chobb.minx - bb[0].minx;
				dy = chobb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}

			break;
			case ly_wide:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = chobb.minx - bb[0].minx;
				dy = chobb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				w1 =bb[1].maxx - bb[1].minx;
				w2 = chobb.maxx - chobb.minx;
				tx = w2 / w1;
				ty = 1.0;
				dx = 0.0;
				dy = 0.0;
				trans[0] = tx;
				trans[3] = ty;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				// fit bbox
				HY_FitHeight( sc, ly_small, layer );
				HY_FitBBox( sc, ly_small, layer );
			}
			break;
			case ly_tall:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = chobb.minx - bb[0].minx;
				dy = chobb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				h1 =bb[1].maxy - bb[1].miny;
				h2 = chobb.maxy - chobb.miny;
				tx = 1.0;
				ty = h2 / h1;
				dx = 0.0;
				dy = 0.0;
				trans[0] = tx;
				trans[3] = ty;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				// fit bbox
				HY_FitWidth( sc, ly_small, layer );
				HY_FitBBox( sc, ly_small, layer );
			}
			break;
		}
	}
}

static void HY_TransformLayersJung( SplineFont *sf, SplineChar *sc, int ju_type )
{
	int i = 0;
	int gid = -1;
	int layer = 0;
	DBounds bb[6];
	real trans[6]	= { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
	real emsize = s_emsize;
	real h1, h2;
	real w1, w2;
	real tx, ty;
	real dx, dy;
	DBounds jungbb;

	if( ju_type == 1)		// ㅏ ㅑ ㅓ ㅕ ...
	{
		//static DBounds jungbb_hori	= { 500.0, 900.0, -150.0, 750.0 };
		jungbb.minx = jungbb_hori.minx;		//500.0;
		jungbb.miny = jungbb_hori.miny;		//-150.0;
		jungbb.maxx = jungbb_hori.maxx;		//900.0;
		jungbb.maxy = jungbb_hori.maxy;		//750.0;
	}
	else if( ju_type == 2)	// ㅗ ㅛ ㅜ ㅠ ...
	{
		//static DBounds jungbb_vert	= { 50.0, 950.0, -150.0, 500.0 };
		jungbb.minx = jungbb_vert.minx;		//50.0;
		jungbb.miny = jungbb_vert.miny;		//-150.0;
		jungbb.maxx = jungbb_vert.maxx;		//950.0;
		jungbb.maxy = jungbb_vert.maxy;		//500.0;
	}
	else if( ju_type == 3)	// ㅘ  ㅝ ㅟ ....
	{
		//static DBounds jungbb_comp	= { 50.0, 950.0, -150.0, 750.0 };
		jungbb.minx = jungbb_comp.minx;		//50.0;
		jungbb.miny = jungbb_comp.miny;		//-150.0;
		jungbb.maxx = jungbb_comp.maxx;		//950.0;
		jungbb.maxy = jungbb_comp.maxy;		//750.0;
	}
 
	for( layer = ly_fore ; layer < sc->layer_cnt ; layer++ )
	{
		switch( layer )
		{
			case ly_fore:
			{
				/*
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_back, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jungbb.maxx - bb[0].maxx;
				dy = jungbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
				*/
			}
			break;
			case ly_small:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jungbb.minx - bb[0].minx;
				dy = jungbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}
			break;
			case ly_wide:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jungbb.minx - bb[0].minx;
				dy = jungbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				w1 =bb[1].maxx - bb[1].minx;
				w2 = jungbb.maxx - jungbb.minx;
				tx = w2 / w1;
				ty = 1.0;
				dx = 0.0;
				dy = 0.0;
				trans[0] = tx;
				trans[3] = ty;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}
			break;
			case ly_tall:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jungbb.minx - bb[0].minx;
				dy = jungbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				h1 =bb[1].maxy - bb[1].miny;
				h2 = jungbb.maxy - jungbb.miny;
				tx = 1.0;
				ty = h2 / h1;
				dx = 0.0;
				dy = 0.0;
				trans[0] = tx;
				trans[3] = ty;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}
			break;
		}
	}
}

static void HY_TransformLayersJong( SplineFont *sf, SplineChar *sc )
{
	int i = 0;
	int gid = -1;
	int layer = 0;
	DBounds bb[6];
	real trans[6]	= { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
	real emsize = s_emsize;
	real h1, h2;
	real w1, w2;
	real tx, ty;
	real dx, dy;

	for( layer = ly_fore ; layer < sc->layer_cnt ; layer++ )
	{
		switch( layer )
		{
			case ly_fore:
			{
				/*
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_back, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jongbb.minx - bb[0].minx;
				dy = jongbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );*/
			}
			break;
			case ly_small:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jongbb.minx - bb[0].minx;
				dy = jongbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}
			break;
			case ly_wide:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jongbb.minx - bb[0].minx;
				dy = jongbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				w1 =bb[1].maxx - bb[1].minx;
				w2 = jongbb.maxx - jongbb.minx;
				tx = (w2 / w1);
				ty = 1.0;
				dx = 0.0;
				dy = 0.0;
				trans[0] = tx;
				trans[3] = ty;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}
			break;
			case ly_tall:
			{
				// fit to base point
				SplineCharLayerFindBounds( sc, ly_fore, &bb[0] );
				tx = 1.0;
				ty = 1.0;
				dx = jongbb.minx - bb[0].minx;
				dy = jongbb.maxy - bb[0].maxy;
				trans[0] = trans[3] = 1.0;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );

				SplineCharLayerFindBounds( sc, layer, &bb[1] );
				h1 =bb[1].maxy - bb[1].miny;
				h2 = jongbb.maxy - jongbb.miny;
				tx = 1.0;
				ty = h2 / h1;
				dx = 0.0;
				dy = 0.0;
				trans[0] = tx;
				trans[3] = ty;
				trans[4] = dx;
				trans[5] = dy;
				SplinePointListTransform( sc->layers[layer].splines, trans, tpt_AllPoints );
			}
			break;
		}
	}
}

static void HY_StrokeLayers( SplineFont *sf, SplineChar *sc )
{
	int i = 0;
	int gid = -1;
	int layer = 0;
	StrokeInfo si;
	SplineSet *temp;
	DBounds sbb;
	DBounds bb[2];
	real trans[6]	= { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
	int pentype	= si_caligraphic;
	int linecap	= lc_square;
	int linejoin	= lj_miter;
	real weight	= s_weight;

	for( layer = ly_fore ; layer < sc->layer_cnt ; layer++ )
	{
		switch( layer )
		{
			case ly_fore:
			{
				// stroke
				memset( &si, 0, sizeof(si) );
				si.stroke_type = pentype;
				si.radius = weight;
				si.penangle = 0.0;
				si.minorradius = 0.0;
				si.cap = linecap;
				si.join = linejoin;
				si.removeinternal = 0;
				si.removeexternal = 0;
				temp = HY_SplineSetStroke( sc, sc->layers[layer].splines, &si, sc->layers[layer].order2 );
				SplinePointListsFree( sc->layers[layer].splines );
				sc->layers[layer].splines = temp;
				SCCharChangedUpdate( sc, layer );
			}
			break;
			case ly_small:
			{
				// stroke
				memset( &si, 0, sizeof(si) );
				si.stroke_type = pentype;
				si.radius = weight;
				si.penangle = 0.0;
				si.minorradius = 0.0;
				si.cap = linecap;
				si.join = linejoin;
				si.removeinternal = 0;
				si.removeexternal = 0;
				temp = HY_SplineSetStroke( sc, sc->layers[layer].splines, &si, sc->layers[layer].order2 );
				SplinePointListsFree( sc->layers[layer].splines );
				sc->layers[layer].splines = temp;
				SCCharChangedUpdate( sc, layer );
			}
			break;
			case ly_wide:
			{
				// stroke
				memset( &si, 0, sizeof(si) );
				si.stroke_type = pentype;
				si.radius = weight;
				si.penangle = 0.0;
				si.minorradius = 0.0;
				si.cap = linecap;
				si.join = linejoin;
				si.removeinternal = 0;
				si.removeexternal = 0;
				temp = HY_SplineSetStroke( sc, sc->layers[layer].splines, &si, sc->layers[layer].order2 );
				SplinePointListsFree( sc->layers[layer].splines );
				sc->layers[layer].splines = temp;
				SCCharChangedUpdate( sc, layer );

				// fit bbox
				HY_FitHeight( sc, ly_small, layer );
				HY_FitBBox( sc, ly_small, layer );
			}
			break;
			case ly_tall:
			{
				// stroke
				memset( &si, 0, sizeof(si) );
				si.stroke_type = pentype;
				si.radius = weight;
				si.penangle = 0.0;
				si.minorradius = 0.0;
				si.cap = linecap;
				si.join = linejoin;
				si.removeinternal = 0;
				si.removeexternal = 0;
				temp = HY_SplineSetStroke( sc, sc->layers[layer].splines, &si, sc->layers[layer].order2 );
				SplinePointListsFree( sc->layers[layer].splines );
				sc->layers[layer].splines = temp;
				SCCharChangedUpdate( sc, layer );

				// fit bbox
				HY_FitWidth( sc, ly_small, layer );
				HY_FitBBox( sc, ly_small, layer );
			}
			break;
		}
	}
}

static void HY_MainBaseCharReform( SplineFont *sf )
{
	int baseindex = 0xb9d8;		// 맘
	int baseindex1[5] = { 0xb9f4, 0xba48, 0xba64 };		// 맴, 멈, 멤 -> move
	int startindex1[5] = { 0xb9e5, 0xba39, 0xba55 };		// 맥, 먹, 멕

	int baseindex2[5] = { 0xbab8, 0xbad4 };				// 몸, 뫔	-> scale + move
	int startindex2[5] = { 0xbaa9, 0xbac5 };				// 목, 뫅

	int i, j, gid, gid2;
	SplineChar *sc = NULL;
	SplineChar *sc1 = NULL;
	SplineChar *sc2 = NULL;
	SplineChar *sc3 = NULL;
	RefChar *rf;
	RefChar *rf1;
	RefChar *rf2;
	RefChar *rf3;
	real trans[6] = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	// sc1: 맥, 먹, 멕 : jongsung refchar move transform
	for( i = 0 ; i < 3 ; i++ )
	{
		gid = sf->map->map[baseindex1[i]];
		sc1 = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sc1) )
		{
			for( rf1 = sc1->layers[ly_fore].refs ; rf1 != NULL ; rf1 = rf1->next )
			{
				if( rf1->sc->bmaster && rf1->sc->mmg_type == mg_jong )
				{
					break;
				}
			}

			//if( sc == NULL || sc1 == NULL || rf->sc == NULL || rf1->sc == NULL )
			if( sc == NULL || sc1 == NULL || rf == NULL || rf1 == NULL )
			{
				return;
			}

			if( strcmp( rf->sc->name, rf1->sc->name ) != 0 )
			{
				trans[0] = 1.0;
				trans[1] = 0.0;
				trans[2] = 0.0;
				trans[3] = 1.0;
				trans[4] = rf1->bb.minx - rf->bb.minx;
				trans[5] = rf1->bb.maxy - rf->bb.maxy;
			}

			for( j = 0 ; j < 27 ; j++ )
			{
				if( j == 15 )
				{
					continue;
				}

				gid2 = sf->map->map[startindex1[i]+j];
				sc2 = sf->glyphs[gid2];
				if( gid2 != -1 && SCWorthOutputting(sc2) )
				{
					for( rf2 = sc2->layers[ly_fore].refs ; rf2 != NULL ; rf2 = rf2->next )
					{
						if( rf2->sc->bmaster && rf2->sc->mmg_type == mg_jong )
						{
							memcpy( rf2->transform, trans, sizeof(trans) );
							SCReinstanciateRefChar( sc2, rf2, ly_fore);
							SCUpdateAll( sc2 );
						}
					}
				}
			}
		}
	}

	// sc1: 목, 뫅 : jongsung refchar scale & move transform
	sc1 = NULL;
	for( i = 0 ; i < 2 ; i++ )
	{
		gid = sf->map->map[baseindex2[i]];
		sc1 = sf->glyphs[gid];

		if( gid != -1 && SCWorthOutputting(sc1) )
		{
			for( rf1 = sc1->layers[ly_fore].refs ; rf1 != NULL ; rf1 = rf1->next )
			{
				if( rf1->sc->bmaster && rf1->sc->mmg_type == mg_jong )
				{
					break;
				}
			}
			if( sc == NULL || sc1 == NULL || rf->sc == NULL || rf1->sc == NULL )
			{
				return;
			}

			if( strcmp( rf->sc->name, rf1->sc->name ) != 0 )
			{
				trans[0] = (rf1->bb.maxx - rf1->bb.minx) / (rf->bb.maxx - rf->bb.minx);
				trans[1] = 0.0;
				trans[2] = 0.0;
				trans[3] = (rf1->bb.maxy - rf1->bb.miny) / (rf->bb.maxy - rf->bb.miny);
				trans[4] = rf1->bb.minx - rf->bb.minx;
				trans[5] = rf1->bb.maxy - rf->bb.maxy;
			}

			for( j = 0 ; j < 27 ; j++ )
			{
				if( j == 15 )
				{
					continue;
				}

				gid2 = sf->map->map[startindex2[i]+j];
				sc2 = sf->glyphs[gid2];

				if( gid2 != -1 && SCWorthOutputting(sc2) )
				{
					for( rf2 = sc2->layers[ly_fore].refs ; rf2 != NULL ; rf2 = rf2->next )
					{
						if( rf2->sc->bmaster && rf2->sc->mmg_type == mg_jong )
						{
							memcpy( rf2->transform, trans, sizeof(trans) );
							SCReinstanciateRefChar( sc2, rf2, ly_fore);
							SCUpdateAll( sc2 );
						}
					}
				}
			}
		}
	}
	FontViewReformatAll( sf );
}

static void HY_OtherBaseCharReform( SplineFont *sf )
{
	SplineChar *basesc1;
	SplineChar *basesc2;
	RefChar *baserf1;
	RefChar *baserf2;

	SplineChar *targetsc2;
	SplineChar *targetsc1;
	RefChar *targetrf1;
	RefChar *targetrf2;

	int basemap[4][2] =
	{
		{ 0xb9c8, 0xbaa8 },	// 마, 모
		{ 0xb9c8, 0xbac4 },	// 마, 뫄
		{ 0xb9d8, 0xbab8 },	// 맘, 몸
		{ 0xb9d8, 0xbad4 }	// 맘, 뫔
	};

	real trans[6];
	real tx;
	real ty;
	real dx;
	real dy;
	int i, j;
	int basegid1, basegid2;
	int targetgid1, targetgid2;

	// 마 모 가 고 -> chosung refchar scale & move
	{
		basegid1 = sf->map->map[basemap[0][0]];
		basegid2 = sf->map->map[basemap[0][1]];
		basesc1 = sf->glyphs[basegid1];
		basesc2 = sf->glyphs[basegid2];

		if( basegid1 != -1 && basegid2 != -1 && SCWorthOutputting(basesc1) && SCWorthOutputting(basesc2) )
		{
			for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
			{
				if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
				{
					for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
					{
						if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
						{
							for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
							{
								targetgid1 = sf->map->map[basechar_vert_min_tbl[j][0]];
								targetgid2 = sf->map->map[basechar_vert_min_tbl[j][1]];
								targetsc1 = sf->glyphs[targetgid1];
								targetsc2 = sf->glyphs[targetgid2];
								if( targetgid1 != -1 && targetgid2 != -1 && SCWorthOutputting(targetsc1) && SCWorthOutputting(targetsc2) )
								{
									for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
									{
										if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
										{
											for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
											{
												if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
												{
													tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
													ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
													dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
													dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

													trans[0] = tx;
													trans[1] = 0.0;
													trans[2] = 0.0;
													trans[3] = ty;
													trans[4] = dx;
													trans[5] = dy;

													memcpy( targetrf2->transform, trans, sizeof(trans) );
													SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
													SCUpdateAll( targetsc2 );
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// 마 뫄 가 과 -> chosung refchar scale & move
	{
		basegid1 = sf->map->map[basemap[1][0]];
		basegid2 = sf->map->map[basemap[1][1]];
		basesc1 = sf->glyphs[basegid1];
		basesc2 = sf->glyphs[basegid2];

		if( basegid1 != -1 && basegid2 != -1 && SCWorthOutputting(basesc1) && SCWorthOutputting(basesc2) )
		{
			for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
			{
				if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
				{
					for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
					{
						if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
						{
							for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
							{
								targetgid1 = sf->map->map[basechar_comp_min_tbl[j][0]];
								targetgid2 = sf->map->map[basechar_comp_min_tbl[j][1]];
								targetsc1 = sf->glyphs[targetgid1];
								targetsc2 = sf->glyphs[targetgid2];

								if( targetgid1 != -1 && targetgid2 != -1 && SCWorthOutputting(targetsc1) && SCWorthOutputting(targetsc2) )
								{
									for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
									{
										if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
										{
											for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
											{
												if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
												{
													tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
													ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
													dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
													dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

													trans[0] = tx;
													trans[1] = 0.0;
													trans[2] = 0.0;
													trans[3] = ty;
													trans[4] = dx;
													trans[5] = dy;

													memcpy( targetrf2->transform, trans, sizeof(trans) );
													SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
													SCUpdateAll( targetsc2 );
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// 맘 몸 감 곰 -> chosung rechar scale & move
	{
		basegid1 = sf->map->map[basemap[2][0]];
		basegid2 = sf->map->map[basemap[2][1]];
		basesc1 = sf->glyphs[basegid1];
		basesc2 = sf->glyphs[basegid2];

		if( basegid1 != -1 && basegid2 != -1 && SCWorthOutputting(basesc1) && SCWorthOutputting(basesc2) )
		{
			for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
			{
				if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
				{
					for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
					{
						if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
						{
							for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
							{
								targetgid1 = sf->map->map[basechar_vert_tbl[j][0]];
								targetgid2 = sf->map->map[basechar_vert_tbl[j][1]];
								targetsc1 = sf->glyphs[targetgid1];
								targetsc2 = sf->glyphs[targetgid2];

								if( targetgid1 != -1 && targetgid2 != -1 && SCWorthOutputting(targetsc1) && SCWorthOutputting(targetsc2) )
								{
									for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
									{
										if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
										{
											for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
											{
												if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
												{
													tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
													ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
													dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
													dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

													trans[0] = tx;
													trans[1] = 0.0;
													trans[2] = 0.0;
													trans[3] = ty;
													trans[4] = dx;
													trans[5] = dy;

													memcpy( targetrf2->transform, trans, sizeof(trans) );
													SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
													SCUpdateAll( targetsc2 );
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// 맘 뫔 감 괌 -> chosung rechar scale & move
	{
		basegid1 = sf->map->map[basemap[3][0]];
		basegid2 = sf->map->map[basemap[3][1]];
		basesc1 = sf->glyphs[basegid1];
		basesc2 = sf->glyphs[basegid2];

		if( basegid1 != -1 && basegid2 != -1 && SCWorthOutputting(basesc1) && SCWorthOutputting(basesc2) )
		{
			for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
			{
				if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
				{
					for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
					{
						if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
						{
							for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
							{
								targetgid1 = sf->map->map[basechar_comp_tbl[j][0]];
								targetgid2 = sf->map->map[basechar_comp_tbl[j][1]];
								targetsc1 = sf->glyphs[targetgid1];
								targetsc2 = sf->glyphs[targetgid2];

								if( targetgid1 != -1 && targetgid2 != -1 && SCWorthOutputting(targetsc1) && SCWorthOutputting(targetsc2) )
								{
									for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
									{
										if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
										{
											for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
											{
												if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
												{
													tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
													ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
													dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
													dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

													trans[0] = tx;
													trans[1] = 0.0;
													trans[2] = 0.0;
													trans[3] = ty;
													trans[4] = dx;
													trans[5] = dy;

													memcpy( targetrf2->transform, trans, sizeof(trans) );
													SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
													SCUpdateAll( targetsc2 );
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	FontViewReformatAll( sf );
}

static void HY_JungVertCharReform( SplineFont *sf )
{
	SplineChar *sc;
	RefChar *rf;
	int emsize = 1000;
	int i, j;
	int gid;
	real trans[6];

	// 세로모임 중앙 정렬 - 초성, 종성 중앙 정렬
	for( i = 0 ; i < JUNGSUNG_VERT_COUNT ; i++ )
	{
		for( j = jung_vert_startpos[i] ; j < jung_vert_startpos[i] + 28 ; j++ )
		{
			gid = sf->map->map[j];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting(sc) )
			{
				for( rf = sc->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
				{
					if( rf->sc->bmaster )
					{
						if( rf->sc->mmg_type == mg_cho || rf->sc->mmg_type == mg_jong )
						{
							trans[0] = rf->transform[0];
							trans[1] = rf->transform[1];
							trans[2] = rf->transform[2];
							trans[3] = rf->transform[3];
							trans[4] = rf->transform[4] + ((emsize / 2) - ((rf->bb.maxx + rf->bb.minx) / 2));
							trans[5] = rf->transform[5];
							memcpy( rf->transform, trans, sizeof(trans) );
							SCReinstanciateRefChar( sc, rf, ly_fore);
							SCUpdateAll( sc );
						}
					}
				}
			}
		}
	}
}

typedef struct curlResponseData
{
	char *response;
	size_t size;
} CURL_RES_DATA;

static void initCurlResponseData ( CURL_RES_DATA *res )
{
	res->size = 0;
	res->response = malloc(res->size+1);

	if ( res->response == NULL ) 
	{
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	res->response[0] = '\0';
}

static size_t curlWriteFunction( void *ptr, size_t size, size_t nmemb, CURL_RES_DATA *res )
{
	size_t newLen = res->size + size*nmemb;
	res->response = realloc(res->response, newLen+1);
	if (res->response == NULL)
	{
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(res->response+res->size, ptr, size*nmemb);
	res->response[newLen] = '\0';
	res->size = newLen;
	return size*nmemb;

	return 0;
}

#ifdef _WIN32
static void HY_RegisterFontWin32( char *full_userid, char *userkey )
{
	char param[PATH_MAX];

	sprintf( param, "%s?id=%s&key=%s", REGISTER_URL, full_userid, userkey );
	ShellExecute(NULL, "open", param, NULL, NULL, SW_SHOWDEFAULT);
}
#else
static char browser[1025];
static void HY_RegisterFontOther( void )
{
	char *cmd;

	if ( (cmd = malloc(strlen(REGISTER_URL) + 1)) == NULL )
	{
		return;
	}

	if ( browser[0] == '\0' )
	{
		findbrowser();
	}

	if ( browser[0] == '\0' )
	{
		gwwv_post_error(_("No Browser"),_("Could not find a browser. Set the BROWSER environment variable to point to one"));
		return;
	}

	sprintf( cmd, "%s \"%s\" &", browser, REGISTER_URL );
	//sprintf( cmd, "\"%s\" \"%s\" &", browser, REGISTER_URL );

	system(cmd);
	free(cmd);
}
#endif

static char *HY_DownloadFont( char *filepath, char *userid )
{
	CURL *curl;
	CURLcode res;
	CURL_RES_DATA resData;
	char *tok = "@";
	char param[PATH_MAX];
	FILE *sfd;

	sprintf( param, "command=%s&id=%s", COMMAND_IMPORT_SFD, userid );

	sfd = fopen( filepath, "w" );
	if( sfd != NULL )
	{
		initCurlResponseData(&resData);
		curl = curl_easy_init();
		if(curl) 
		{
			curl_easy_setopt( curl, CURLOPT_URL, BASE_API_URL );
			curl_easy_setopt( curl, CURLOPT_USERAGENT,  "hangultuelApp" );
			curl_easy_setopt( curl, CURLOPT_POSTFIELDSIZE, strlen(param) );
			curl_easy_setopt( curl, CURLOPT_POSTFIELDS, param );
			curl_easy_setopt( curl, CURLOPT_WRITEDATA, &resData );
	 		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, curlWriteFunction );
			res = curl_easy_perform(curl);
			if( res == CURLE_OK )
			{
				fprintf( sfd, "%s\n", resData.response);
			}
			else
			{
				hydebug( "[%s] Failed to download file\n", __FUNCTION__ );
			}
			free(resData.response);
			curl_easy_cleanup(curl);
		}
		fclose( sfd );
	}
	else
	{
		ff_post_error( _("Error"), _("File Download Failed!") );
	}
}

/*
	flag = 0 : download handwriting font
	flag = 1 : register ttf/otf font
*/
static FontView *FontView_Create( SplineFont *sf, int hide );

static void HY_DoHandWriteStroke( SplineFont *sf )
{
	real factor	= 100.0;
	int i, gid;

	ff_progress_start_indicator( 10, _("Hand Write"), _("HandWrite Stroke..."), 0, sf->map->enccount, 1 );
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		if( gid != -1 )
		{
			if( SCWorthOutputting(sf->glyphs[gid]) )
			{
				SCRound2Int( sf->glyphs[gid], ly_fore, factor );
				HY_StrokeLayers( sf, sf->glyphs[gid] );
			}
		}
		if( !ff_progress_next() )
		{
			break;
		}
	}
	ff_progress_end_indicator();
}

static int HY_DoAuthorization( FontView *fv, char *userid, char *passwd, int flag )
{
	//SplineFont *sf = fv->b.sf;
	CURL_RES_DATA resData;
	CURL *curl;
	CURLcode res;
	char param[PATH_MAX];
	char *tok = ",";
	char *code = NULL;
	char *msg = NULL;
	char *key = NULL;
	int ret = 0;

	initCurlResponseData(&resData);
	curl = curl_easy_init();
	if(curl)
	{
		sprintf( param, "command=%s&id=%s&pw=%s", COMMAND_USER_LOGIN, userid, passwd );
		hydebug( "[%s] param: <%s>\n", __FUNCTION__, param );

		curl_easy_setopt( curl, CURLOPT_URL, BASE_API_URL );
		curl_easy_setopt( curl, CURLOPT_USERAGENT,  "hangultuelApp" );
		curl_easy_setopt( curl, CURLOPT_POSTFIELDSIZE, strlen(param) );
		curl_easy_setopt( curl, CURLOPT_POSTFIELDS, param );
		curl_easy_setopt( curl, CURLOPT_WRITEDATA, &resData );
 		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, curlWriteFunction );

		res = curl_easy_perform(curl);
		if( res == CURLE_OK )
		{
			code = strtok( resData.response, tok );
			hydebug( "[%s] resp code: <%s>\n", __FUNCTION__, code );
			if( strcmp( code, "1000" ) == 0 )
			{
				switch( flag )
				{
					case cm_importsfd:
					{
						char *DefaultDir = NULL;
						char filepath[PATH_MAX];
						FILE *newsfd;
						FontView * newfv;
						SplineFont *newsf;
						int i, gid;

						DefaultDir = copy(GFileGetHomeDocumentsDir());
						sprintf( filepath, "%s%s.sfd", DefaultDir, userid );
						hydebug( "[%s] download filepath: <%s>\n", __FUNCTION__, filepath );

						HY_DownloadFont( filepath, userid );

						hydebug( "[%s] <%s> download done.\n", __FUNCTION__, filepath );

						//if( fv->b.sf->new )
						if( 0 )
						{
							newsf = LoadSplineFont( filepath, 0 );

							if( !fv->b.sf->bstroke )
							{
								if( fv->b.sf->bMMGFont )
								{
									HY_DoAutoMakeMG( fv->b.sf );
								}
								else
								{
									HY_DoHandWriteStroke( fv->b.sf );
								}
							}
						}
						else
						{
							newsf = calloc(1, sizeof(SplineFont));
							newsf = LoadSplineFont( filepath, 0 );
							newfv = FontView_Create( newsf, false);
							newsf->bstroke = 0;

							if( !newsf->bstroke )
							{
								if( newsf->bMMGFont )
								{
									HY_DoAutoMakeMG( newsf );
								}
								else
								{
									HY_DoHandWriteStroke( newsf );
								}
							}
						}
					}
					break;
					case cm_registerfont:
					{
						key = strtok( NULL, tok );
						HY_RegisterFontWin32( userid, key );
					}
					break;
				}
			}
			else
			{
				msg = strtok( NULL, tok );
				ff_post_error( _("Error"), _("User Authorization Failed!\n(%s)"), msg );
			}
		}
		free(resData.response);
		curl_easy_cleanup(curl);
	}
	return ret;
}

static int importauth_e_h( GWindow pixmap, GEvent *event )
{
	switch( event->type )
	{
		case et_resize:
		{
		}
		break;
		case et_close:
		{
			GDrawDestroyWindow( pixmap );
		}
		break;
	}
	return( true );	
}

static int HY_ImportUserAuthOK( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw = GGadgetGetWindow( g );
		FontView *fv = GDrawGetUserData(gw);

		FILE *fp;
		CURL *curl;
		CURLcode res;
		CURL_RES_DATA resData;
		char * userid = NULL;
		char *passwd = NULL;
		int ret;

		userid = GGadgetGetTitle8(GWidgetGetControl(gw, CID_TextFieldImportUserID ));
		passwd = GGadgetGetTitle8(GWidgetGetControl(gw, CID_TextFieldImportPasswd ));
		ret = HY_DoAuthorization( fv, userid, passwd, 0 );
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static int HY_ImportUserAuthCancel( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw		= GGadgetGetWindow( g );
		GDrawDestroyWindow(gw);
	}
	return( true );
}

void HY_ImportUserAuthDlg( FontView *fv )
{
	GRect pos;
	GWindow gw;
	GWindowAttrs wattrs;
	GTextInfo label[15];
	GGadgetCreateData gcd[15];
	int k = 0;

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask				= wam_events | wam_cursor | wam_utf8_wtitle | wam_undercursor | wam_isdlg;
	wattrs.event_masks		= ~( 1 << et_charup );
	wattrs.restrict_input_to_me	= 1;
	wattrs.undercursor			= 1;
	wattrs.cursor				= ct_pointer;
	wattrs.utf8_window_title		= _("User Authorization");
	wattrs.is_dlg				= true;
	pos.x = pos.y				= 10;
	pos.width					= 290;
	pos.height				= 120;

	gw = GDrawCreateTopWindow( NULL, &pos, importauth_e_h, fv, &wattrs );
	GDrawSetVisible( gw, true );

	memset( &label, 0, sizeof( label ) );
	memset( &gcd, 0, sizeof( gcd ) );

	label[k].text					= (unichar_t *) _("UserID:");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 1;
	gcd[k].gd.pos.y				= 5;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 17;
	gcd[k].gd.flags				= gg_enabled|gg_visible;
	gcd[k].gd.cid					= CID_LabelImportUserID;
	gcd[k++].creator				= GLabelCreate;

//	label[k].text					= (unichar_t *) _("");
//	label[k].text_is_1byte			= true;
//	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 75;
	gcd[k].gd.pos.y				= 5;
	gcd[k].gd.pos.width			= 120;
	gcd[k].gd.pos.height			= 20;
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_TextFieldImportUserID;
	gcd[k++].creator				= GTextFieldCreate;

	label[k].text					= (unichar_t *) _("Password:");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 1;
	gcd[k].gd.pos.y				= 30;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 17;
	gcd[k].gd.flags				= gg_enabled|gg_visible;
	gcd[k].gd.cid					= CID_LabelImportPasswd;
	gcd[k++].creator				= GLabelCreate;

//	label[k].text					= (unichar_t *) _("");
//	label[k].text_is_1byte			= true;
//	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 75;
	gcd[k].gd.pos.y				= 30;
	gcd[k].gd.pos.width			= 120;
	gcd[k].gd.pos.height			= 20;
	gcd[k].gd.flags				= gg_visible | gg_enabled | /*gg_pos_in_pixels |*/ gg_pos_use0 | gg_text_xim/* | gg_dontcopybox*/;
	gcd[k].gd.cid					= CID_TextFieldImportPasswd;
	gcd[k++].creator				= GPasswordCreate;

	label[k].text					= (unichar_t *)_("OK");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 40;
	gcd[k].gd.pos.y				= 60;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 20; 
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_BtnImportAutoOK;
	gcd[k].gd.handle_controlevent	= HY_ImportUserAuthOK;
	gcd[k++].creator				= GButtonCreate;

	label[k].text					= (unichar_t *)_("Cancel");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 115;
	gcd[k].gd.pos.y				= 60;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 20; 
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_BtnImportAuthCancel;
	gcd[k].gd.handle_controlevent	= HY_ImportUserAuthCancel;
	gcd[k++].creator				= GButtonCreate;

	GGadgetsCreate( gw, gcd );
}

static void FVMenuHYImportHandWrite(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	HY_ImportUserAuthDlg( fv );
}

static int regfontauth_e_h( GWindow pixmap, GEvent *event )
{
	switch( event->type )
	{
		case et_resize:
		{
		}
		break;
		case et_close:
		{
			GDrawDestroyWindow( pixmap );
		}
		break;
	}
	return( true );	
}

static int HY_RegUserAuthOK( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw = GGadgetGetWindow( g );
		FontView *fv = GDrawGetUserData( gw );
		SplineFont *sf = fv->b.sf;
		FILE *fp;
		CURL *curl;
		CURLcode res;
		CURL_RES_DATA resData;
		char * userid = NULL;
		char *passwd = NULL;
		int ret;

		userid = GGadgetGetTitle8(GWidgetGetControl(gw, CID_TextFieldRegUserID ));
		passwd = GGadgetGetTitle8(GWidgetGetControl(gw, CID_TextFieldRegPasswd ));

		ret = HY_DoAuthorization( fv, userid, passwd, 1 );
		GDrawDestroyWindow( gw );
	}
	return( true );
}

static int HY_RegUserAuthCancel( GGadget * g, GEvent * event )
{
	if( event->type == et_controlevent && event->u.control.subtype == et_buttonactivate )
	{
		GWindow gw		= GGadgetGetWindow( g );
		GDrawDestroyWindow(gw);
	}
	return( true );
}

static void HY_DoRegisterFontDlg( SplineFont *sf )
{
	GRect pos;
	GWindow gw;
	GWindowAttrs wattrs;
	GTextInfo label[15];
	GGadgetCreateData gcd[15];
	int k = 0;

	memset( &wattrs, 0, sizeof( wattrs ) );
	wattrs.mask				= wam_events | wam_cursor | wam_utf8_wtitle | wam_undercursor | wam_isdlg;
	wattrs.event_masks		= ~( 1 << et_charup );
	wattrs.restrict_input_to_me	= 1;
	wattrs.undercursor			= 1;
	wattrs.cursor				= ct_pointer;
	wattrs.utf8_window_title		= _("User Authorization");
	wattrs.is_dlg				= true;
	pos.x = pos.y				= 10;
	pos.width					= 290;
	pos.height				= 120;

	gw = GDrawCreateTopWindow( NULL, &pos, regfontauth_e_h, sf, &wattrs );
	GDrawSetVisible( gw, true );

	memset( &label, 0, sizeof( label ) );
	memset( &gcd, 0, sizeof( gcd ) );

	label[k].text					= (unichar_t *) _("UserID:");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 1;
	gcd[k].gd.pos.y				= 5;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 17;
	gcd[k].gd.flags				= gg_enabled|gg_visible;
	gcd[k].gd.cid					= CID_LabelRegUserID;
	gcd[k++].creator				= GLabelCreate;

//	label[k].text					= (unichar_t *) _("");
//	label[k].text_is_1byte			= true;
//	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 75;
	gcd[k].gd.pos.y				= 5;
	gcd[k].gd.pos.width			= 120;
	gcd[k].gd.pos.height			= 20;
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_TextFieldRegUserID;
	gcd[k++].creator				= GTextFieldCreate;

	label[k].text					= (unichar_t *) _("Password:");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 1;
	gcd[k].gd.pos.y				= 30;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 17;
	gcd[k].gd.flags				= gg_enabled|gg_visible;
	gcd[k].gd.cid					= CID_LabelRegPasswd;
	gcd[k++].creator				= GLabelCreate;

//	label[k].text					= (unichar_t *) _("");
//	label[k].text_is_1byte			= true;
//	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 75;
	gcd[k].gd.pos.y				= 30;
	gcd[k].gd.pos.width			= 120;
	gcd[k].gd.pos.height			= 20;
	gcd[k].gd.flags				= gg_visible | gg_enabled | /*gg_pos_in_pixels |*/ gg_pos_use0 | gg_text_xim/* | gg_dontcopybox*/;
	gcd[k].gd.cid					= CID_TextFieldRegPasswd;
	gcd[k++].creator				= GPasswordCreate;

	label[k].text					= (unichar_t *)_("OK");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 40;
	gcd[k].gd.pos.y				= 60;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 20; 
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_BtnRegAutoOK;
	gcd[k].gd.handle_controlevent	= HY_RegUserAuthOK;
	gcd[k++].creator				= GButtonCreate;

	label[k].text					= (unichar_t *)_("Cancel");
	label[k].text_is_1byte			= true;
	label[k].text_in_resource		= true;
	gcd[k].gd.label				= &label[k];
	gcd[k].gd.pos.x				= 115;
	gcd[k].gd.pos.y				= 60;
	gcd[k].gd.pos.width			= 70;
	gcd[k].gd.pos.height			= 20; 
	gcd[k].gd.flags				= gg_enabled | gg_visible;
	gcd[k].gd.cid					= CID_BtnRegAuthCancel;
	gcd[k].gd.handle_controlevent	= HY_RegUserAuthCancel;
	gcd[k++].creator				= GButtonCreate;

	GGadgetsCreate( gw, gcd );
}

static void FVMenuHYRegisterFont(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);

	HY_DoRegisterFontDlg( fv->b.sf );
}

static void HY_DoAutoMakeMG( SplineFont *sf )
{
	SplineChar *sc;
	SplineChar *rsc;
	int i = 0;
	int j = 0;
	int k = 0;
	int gid = -1;
	int gid2 = -1;
	int layer = 0;

	StrokeInfo si;
	DBounds tmpbb;
	DBounds sbb;
	DBounds bb[6];

	real trans[6]	= { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
	real weight	= 20.0;
	int linecap	= lc_square;
	int linejoin	= lj_miter;

	real width	= 0.0;
	real height	= 0.0;
	real factor	= 100.0;

	int from = ly_fore;
	int to;
	int doclear = 1;

	FILE * fp;

	if( sf->bstroke )
	{
		ff_post_error( _("Error"), _("Master Glyph Already made.") );
		return;
	}

	if( !sf->bMMGFont )
	{
		ff_post_error( _("Error"), _("File is not MMGFont.") );
		return;
	}

	fp = fopen( "settings.txt", "r" );
	if( fp != NULL )
	{
		getint( fp, &s_weight );
		getint( fp, &s_emsize );
		fclose(fp);
	}
	else
	{
		s_weight		= 20;
		s_emsize		= 1000;
	}
	weight = weight;
	if( sf->layer_cnt < 5 )
	{
		ff_post_error( _("Error"), _("Invalid Layer Count!") );
		return;
	}

	/*****************************************************************************************************/
	real w1, h1;
	real w2, h2;

	ff_progress_start_indicator( 10 , _("Make MG"),_("Building MMG...") , 0 , sf->map->enccount , 1 );
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sc) && sc->bmaster )
		{
			if( sc->layers[ly_fore].splines != NULL )
			{
				for( layer = ly_back ; layer < 5 ; layer++ )
				{
					from = ly_fore;
					to = layer;
					if( from == to )
					{
						continue;
					}
					SCCopyLayerToLayer( sc, from, to, 1 );
					sc->ticked = true;
				}
			}

			for( layer = ly_back ; layer < 5 ; layer++ )
			{
				SCRound2Int( sc, layer, factor );
			}

			// transform -> stroke
			if( sc->mmg_type == mg_cho )
			{
				HY_TransformLayersCho( sf, sc );
			}
			else if( sc->mmg_type == mg_jung )
			{
				for( j = 0 ; j < 21 ; j++ )
				{
					if( i == mg_jungsung_type[j][0] )
					{
						HY_TransformLayersJung( sf, sc, mg_jungsung_type[j][1] );
					}
				}
			}
			else if( sc->mmg_type == mg_jong )
			{
				HY_TransformLayersJong( sf, sc );
			}

			HY_StrokeLayers( sf, sc );
		}
		if ( !ff_progress_next())
		{
			break;
		}
	}
	ff_progress_end_indicator();
	usleep(1000);

	ff_progress_start_indicator( 10 , _("Make Link"),_("Make Link (ChoSung)") , 0 , CHOSUNG_LINK_COUNT , 1 );
	// link mg to unicode glyphs
	for( i = 0 ; i < CHOSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == chosung_link_tbl[i][0] )
			{
				gid = sf->map->map[chosung_link_tbl[i][0]];
				gid2 = sf->map->map[chosung_link_tbl[i][1]];
				rsc = sf->glyphs[gid];
				sc = sf->glyphs[gid2];

				if( gid != -1 && SCWorthOutputting(rsc) )
				{
					if( gid2 != -1 && SCWorthOutputting(sc) )
					{
						HY_MGAddLink( sc, rsc );
						SCUpdateAll( sc );
					}
					else
					{
						sc = SFMakeChar( sf, sf->map, chosung_link_tbl[i][1] );
						HY_MGAddLink( sc, rsc );
						SCUpdateAll( sc );
					}
				}
			}
		}
		if ( !ff_progress_next())
		{
			break;
		}
	}
	ff_progress_end_indicator();
	usleep(1000);

	ff_progress_start_indicator( 10 , _("Make Link"),_("Make Link (JungSung)") , 0 , JUNGSUNG_LINK_COUNT , 1 );
	for( i = 0 ; i < JUNGSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == jungsung_link_tbl[i][0] )
			{
				gid = sf->map->map[jungsung_link_tbl[i][0]];
				rsc = sf->glyphs[gid];
				gid2 = sf->map->map[jungsung_link_tbl[i][1]];
				sc = sf->glyphs[gid2];

				if( gid != -1 && SCWorthOutputting(rsc) )
				{
					if( gid2 != -1 && SCWorthOutputting(sc) )
					{
						HY_MGAddLink( sc, rsc );
						SCUpdateAll( sc );
					}
					else
					{
						sc = SFMakeChar( sf, sf->map, jungsung_link_tbl[i][1] );
						HY_MGAddLink( sc, rsc );
						SCUpdateAll( sc );
					}
				}
			}
		}
		if ( !ff_progress_next())
		{
			break;
		}
	}
	ff_progress_end_indicator();
	usleep(1000);

	ff_progress_start_indicator( 10 , _("Make Link"),_("Make Link (JongSung)") , 0 , JONGSUNG_LINK_COUNT , 1 );
	for( i = 0 ; i < JONGSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == jongsung_link_tbl[i][0] )
			{
				gid = sf->map->map[jongsung_link_tbl[i][0]];
				rsc = sf->glyphs[gid];
				gid2 = sf->map->map[jongsung_link_tbl[i][1]];
				sc = sf->glyphs[gid2];

				if( gid != -1 && SCWorthOutputting(rsc) )
				{
					if( gid2 != -1 && SCWorthOutputting(sc) )
					{
						HY_MGAddLink( sc, rsc );
						SCUpdateAll( sc );
					}
					else
					{
						sc = SFMakeChar( sf, sf->map, jongsung_link_tbl[i][1] );
						HY_MGAddLink( sc, rsc );
						SCUpdateAll( sc );
					}
				}
			}
		}
		if ( !ff_progress_next())
		{
			break;
		}
	}
	ff_progress_end_indicator();

	HY_MainBaseCharReform( sf );
	HY_OtherBaseCharReform( sf );
	HY_JungVertCharReform( sf );

	sf->bstroke = 1;
}

static void FVMenuHYAutoMakeMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;

	HY_DoAutoMakeMG( sf );
}

static void FVMenuHYMakeMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	SplineChar *rsc;
	int i, j, gid, layer;
	real factor = 100.0;
	int from = ly_fore, to;
	FILE * fp;

	if( sf->bstroke )
	{
		ff_post_error( _("Error"), _("Master Glyph Already made.") );
		return;
	}

#if 0
	if( !sf->bMMGFont )
	{
		ff_post_error( _("Error"), _("File is not MMGFont.") );
		return;
	}

	if( sf->layer_cnt < 5 )
	{
		ff_post_error( _("Error"), _("Invalid Layer Count!") );
		return;
	}
#endif

	fp = fopen( "settings.txt", "r" );
	if( fp != NULL )
	{
		getint( fp, &s_weight );
		getint( fp, &s_emsize );
		fclose(fp);
	}
	else
	{
		s_weight		= 20;
		s_emsize		= 1000;
	}

	/*****************************************************************************************************/
	real w1, h1;
	real w2, h2;

	if( sf->bMMGFont )
	{
		for( i = 0 ; i < sf->map->enccount ; i++ )
		{
			gid = sf->map->map[i];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting(sc) && sc->bmaster )
			{
				if( sc->layers[ly_fore].splines != NULL )
				{
					for( layer = ly_back ; layer < 5 ; layer++ )
					{
						from = ly_fore;
						to = layer;
						if( from == to )
						{
							continue;
						}
						SCCopyLayerToLayer( sc, from, to, 1 );
						sc->ticked = true;
					}
				}

				for( layer = ly_back ; layer < 5 ; layer++ )
				{
					SCRound2Int( sc, layer, factor );
				}

				if( sc->mmg_type == mg_cho )
				{
					HY_TransformLayersCho( sf, sc );
				}
				else if( sc->mmg_type == mg_jung )
				{
					for( j = 0 ; j < 21 ; j++ )
					{
						if( i == mg_jungsung_type[j][0] )
						{
							HY_TransformLayersJung( sf, sc, mg_jungsung_type[j][1] );
						}
					}
				}
				else if( sc->mmg_type == mg_jong )
				{
					HY_TransformLayersJong( sf, sc );
				}
				HY_StrokeLayers( sf, sc );
			}
		}
	}
	else
	{
		ff_progress_start_indicator( 10, _("Hand Write"), _("HandWrite Stroke..."), 0, sf->map->enccount, 1 );
		for( i = 0 ; i < sf->map->enccount ; i++ )
		{
			gid = sf->map->map[i];
			if( gid != -1 )
			{
				if( SCWorthOutputting(sf->glyphs[gid]) )
				{
					SCRound2Int( sf->glyphs[gid], ly_fore, factor );
					HY_StrokeLayers( sf, sf->glyphs[gid] );
				}
			}
			if( !ff_progress_next() )
			{
				break;
			}
		}
		ff_progress_end_indicator();
	}
	sf->bstroke = 1;
}

static void FVMenuHYLinkChoSungMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	SplineChar *rsc;
	int i = 0;
	int j = 0;
	int gid = -1;
	int gid2 = -1;

	for( i = 0 ; i < CHOSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == chosung_link_tbl[i][0] )
			{
				gid = sf->map->map[chosung_link_tbl[i][0]];
				gid2 = sf->map->map[chosung_link_tbl[i][1]];
				rsc = sf->glyphs[gid];
				sc = sf->glyphs[gid2];
				if( gid2 != -1 && SCWorthOutputting(sc) )
				{
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
				else
				{
					sc = SFMakeChar( sf, sf->map, chosung_link_tbl[i][1] );

					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
			}
		}
	}
}

static void FVMenuHYLinkJungSungMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	SplineChar *rsc;
	int i = 0;
	int j = 0;
	int gid = -1;
	int gid2 = -1;

	for( i = 0 ; i < JUNGSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == jungsung_link_tbl[i][0] )
			{
				gid = sf->map->map[jungsung_link_tbl[i][0]];
				rsc = sf->glyphs[gid];
				gid2 = sf->map->map[jungsung_link_tbl[i][1]];
				sc = sf->glyphs[gid2];

				if( gid2 != -1 && SCWorthOutputting(sc) )
				{
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
				else
				{
					sc = SFMakeChar( sf, sf->map, jungsung_link_tbl[i][1] );
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
			}
		}
	}
}

static void FVMenuHYLinkJongSungMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	SplineChar *rsc;
	int i = 0;
	int j = 0;
	int gid = -1;
	int gid2 = -1;

	for( i = 0 ; i < JONGSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == jongsung_link_tbl[i][0] )
			{
				gid = sf->map->map[jongsung_link_tbl[i][0]];
				rsc = sf->glyphs[gid];
				gid2 = sf->map->map[jongsung_link_tbl[i][1]];
				sc = sf->glyphs[gid2];

				if( gid2 != -1 && SCWorthOutputting(sc) )
				{
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
				else
				{
					sc = SFMakeChar( sf, sf->map, jongsung_link_tbl[i][1] );
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
			}
		}
	}
}

static void FVMenuHYAdjustJaso(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;

	HY_MainBaseCharReform( sf );
	HY_OtherBaseCharReform( sf );
	HY_JungVertCharReform( sf );
}

static void FVMenuHYEZ_MMGJohap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	SplineChar *rsc;
	StrokeInfo si;
	DBounds tmpbb;
	DBounds sbb;
	DBounds bb[6];
	real trans[6]	= { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
	real weight	= 20.0;
	int linecap	= lc_square;
	int linejoin	= lj_miter;
	real width	= 0.0;
	real height	= 0.0;
	real factor	= 100.0;
	int layer = 0;
	int from = ly_fore;
	int to;
	int doclear = 1;
	int cnt = 0xD7A3 - 0xAC00;
	int i, j;
	int gid = -1, gid2 = -1;
	int cho = 0;
	int jung = 0;
	int jong = 0;

	if( sf->bstroke )
	{
		ff_post_error( _("Error"), _("Master Glyph Already made.") );
		return;
	}

	if( !sf->bMMGFont )
	{
		ff_post_error( _("Error"), _("File is not MMGFont.") );
		return;
	}

	FILE * fp;
	fp = fopen( "settings.txt", "r" );
	if( fp != NULL )
	{
		getint( fp, &s_weight );
		getint( fp, &s_emsize );
		fclose(fp);
	}
	else
	{
		s_weight		= 20;
		s_emsize		= 1000;
	}
	weight = weight;
	if( sf->layer_cnt < 5 )
	{
		ff_post_error( _("Error"), _("Invalid Layer Count!") );
		return;
	}


	/*****************************************************************************************************/
	real w1, h1;
	real w2, h2;
	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		gid = sf->map->map[i];
		sc = sf->glyphs[gid];
		if( gid != -1 && SCWorthOutputting(sc) && sc->bmaster )
		{
			if( sc->layers[ly_fore].splines != NULL )
			{
				for( layer = ly_back ; layer < 5 ; layer++ )
				{
					from = ly_fore;
					to = layer;
					if( from == to )
					{
						continue;
					}
					SCCopyLayerToLayer( sc, from, to, 1 );
					sc->ticked = true;
				}
			}

			for( layer = ly_back ; layer < 5 ; layer++ )
			{
				SCRound2Int( sc, layer, factor );
			}

			// transform -> stroke
			if( sc->mmg_type == mg_cho )
			{
				HY_TransformLayersCho( sf, sc );
			}
			else if( sc->mmg_type == mg_jung )
			{
				for( j = 0 ; j < 21 ; j++ )
				{
					if( i == mg_jungsung_type[j][0] )
					{
						HY_TransformLayersJung( sf, sc, mg_jungsung_type[j][1] );
					}
				}
			}
			else if( sc->mmg_type == mg_jong )
			{
				HY_TransformLayersJong( sf, sc );
			}
			HY_StrokeLayers( sf, sc );
		}
	}

	for( i = 0 ; i < CHOSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == chosung_link_tbl[i][0] )
			{
				gid = sf->map->map[chosung_link_tbl[i][0]];
				gid2 = sf->map->map[chosung_link_tbl[i][1]];
				rsc = sf->glyphs[gid];
				sc = sf->glyphs[gid2];
				if( gid2 != -1 && SCWorthOutputting(sc) )
				{
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
				else
				{
					sc = SFMakeChar( sf, sf->map, chosung_link_tbl[i][1] );
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
			}
		}
	}

	for( i = 0 ; i < JUNGSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == jungsung_link_tbl[i][0] )
			{
				gid = sf->map->map[jungsung_link_tbl[i][0]];
				rsc = sf->glyphs[gid];
				gid2 = sf->map->map[jungsung_link_tbl[i][1]];
				sc = sf->glyphs[gid2];

				if( gid2 != -1 && SCWorthOutputting(sc) )
				{
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
				else
				{
					sc = SFMakeChar( sf, sf->map, jungsung_link_tbl[i][1] );
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
			}
		}
	}

	for( i = 0 ; i < JONGSUNG_LINK_COUNT ; i++ )
	{
		for( j = 65536 ; j < sf->map->enccount ; j++ )
		{
			if( j == jongsung_link_tbl[i][0] )
			{
				gid = sf->map->map[jongsung_link_tbl[i][0]];
				rsc = sf->glyphs[gid];
				gid2 = sf->map->map[jongsung_link_tbl[i][1]];
				sc = sf->glyphs[gid2];

				if( gid2 != -1 && SCWorthOutputting(sc) )
				{
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
				else
				{
					sc = SFMakeChar( sf, sf->map, jongsung_link_tbl[i][1] );
					HY_MGAddLink( sc, rsc );
					SCUpdateAll( sc );
				}
			}
		}
	}

	// 1차 기본글자 조합 및 보정
	HY_MainBaseCharReform( sf );
	HY_OtherBaseCharReform( sf );

	switch( mi->mid )
	{
		case MID_HYEZJohap_Hangul_All:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build All Hangul (11,172)..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;

#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif
					if( jungsung_type[jung] == JUNG_TYPE_HORI )
					{
						HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
					}
					else if( jungsung_type[jung] == JUNG_TYPE_VERT )
					{
						HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
					}
					else if( jungsung_type[jung] == JUNG_TYPE_COMP )
					{
						HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYEZJohap_Hangul_KSC5601:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build KSC5601 Hangul (2,350)..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				if( HY_GetKSC5601(i) )
				{
					gid = sf->map->map[i];
					if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
					{
						cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
						jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
						jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
						hydebug( "[%s]======================================================================\n", __FUNCTION__ );
						hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
						hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
						hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

						if( jungsung_type[jung] == JUNG_TYPE_HORI )
						{
							HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
						}
						else if( jungsung_type[jung] == JUNG_TYPE_VERT )
						{
							HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
						}
						else if( jungsung_type[jung] == JUNG_TYPE_COMP )
						{
							HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
						}
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYEZJohap_Hangul_Hori:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Horizontal JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_HORI )
					{
						HY_HTypeJohap( sf, i, JUNG_TYPE_HORI, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYEZJohap_Hangul_Vert:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Vertical JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_VERT )
					{
						HY_VTypeJohap( sf, i, JUNG_TYPE_VERT, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		case MID_HYEZJohap_Hangul_Comp:
		{
			ff_progress_start_indicator( 10, _("MMG Johap"), _("Build Composit JungSung Hangul..."), 0, cnt, 1 );
			for( i = 0xAC00 ; i <= 0xD7A3 ; i++ )
			{
				gid = sf->map->map[i];
				if( gid == -1 || !SCWorthOutputting(sf->glyphs[gid]) )
				{
					cho	= (i - 0xAC00) / (NUM_OF_JUNG * NUM_OF_JONG);
					jung	= ((i - 0xAC00) / NUM_OF_JONG) % NUM_OF_JUNG;
					jong	= (i - 0xAC00) % NUM_OF_JONG;
#if MAIN_DBG
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
					hydebug( "[%s] Unicode: <0X%x>\n", __FUNCTION__, i );
					hydebug( "[%s] ChoSung: <%d> / JungSung: <%d> / JongSung: <%d>\n", __FUNCTION__, cho, jung, jong );
					hydebug( "[%s]======================================================================\n", __FUNCTION__ );
#endif

					if( jungsung_type[jung] == JUNG_TYPE_COMP )
					{
						HY_CTypeJohap( sf, i, JUNG_TYPE_COMP, cho, jung, jong );
					}
				}
				if( !ff_progress_next() )
				{
					break;
				}
			}
			ff_progress_end_indicator();
		}
		break;
		default:
		break;
	}
	HY_JungVertCharReform( sf );
	sf->bMGJohap = 1;
}

static void FVMenuHYReformBaseChar1(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;

	int baseindex = 0xb9d8;		// 맘
	int baseindex1[5] = { 0xb9f4, 0xba48, 0xba64 };		// 맴, 멈, 멤 -> move
	int startindex1[5] = { 0xb9e5, 0xba39, 0xba55 };		// 맥, 먹, 멕
	int baseindex2[5] = { 0xbab8, 0xbad4 };				// 몸, 뫔	-> scale + move
	int startindex2[5] = { 0xbaa9, 0xbac5 };				// 목, 뫅

	int i, j;
	SplineChar *sc = NULL;
	SplineChar *sc1 = NULL;
	SplineChar *sc2 = NULL;
	SplineChar *sc3 = NULL;
	RefChar *rf;
	RefChar *rf1;
	RefChar *rf2;
	RefChar *rf3;
	real trans[6] = { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };

	// sc: 맘
	sc = sf->glyphs[sf->map->map[baseindex]];
	for( rf = sc->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
	{
		if( rf->sc->bmaster && rf->sc->mmg_type == mg_jong )
		{
			break;
		}
	}

	// sc1: 맥, 먹, 멕 : move transform
	for( i = 0 ; i < 3 ; i++ )
	{
		sc1 = sf->glyphs[sf->map->map[baseindex1[i]]];
		for( rf1 = sc1->layers[ly_fore].refs ; rf1 != NULL ; rf1 = rf1->next )
		{
			if( rf1->sc->bmaster && rf1->sc->mmg_type == mg_jong )
			{
				break;
			}
		}
		if( sc == NULL || sc1 == NULL || rf->sc == NULL || rf1->sc == NULL )
		{
			return;
		}

		if( strcmp( rf->sc->name, rf1->sc->name ) != 0 )
		{
			trans[0] = 1.0;
			trans[1] = 0.0;
			trans[2] = 0.0;
			trans[3] = 1.0;
			trans[4] = rf1->bb.minx - rf->bb.minx;
			trans[5] = rf1->bb.maxy - rf->bb.maxy;
		}

		for( j = 0 ; j < 27 ; j++ )
		{
			if( j == 15 )
			{
				continue;
			}
			sc2 = sf->glyphs[sf->map->map[startindex1[i]+j]];
			for( rf2 = sc2->layers[ly_fore].refs ; rf2 != NULL ; rf2 = rf2->next )
			{
				if( rf2->sc->bmaster && rf2->sc->mmg_type == mg_jong )
				{
					memcpy( rf2->transform, trans, sizeof(trans) );
					SCReinstanciateRefChar( sc2, rf2, ly_fore);
					SCUpdateAll( sc2 );
				}
			}
		}
	}

	// sc1: 목, 뫅 : scale & move transform
	sc1 = NULL;
	for( i = 0 ; i < 2 ; i++ )
	{
		sc1 = sf->glyphs[sf->map->map[baseindex2[i]]];
		for( rf1 = sc1->layers[ly_fore].refs ; rf1 != NULL ; rf1 = rf1->next )
		{
			if( rf1->sc->bmaster && rf1->sc->mmg_type == mg_jong )
			{
				break;
			}
		}
		if( sc == NULL || sc1 == NULL || rf->sc == NULL || rf1->sc == NULL )
		{
			return;
		}

		if( strcmp( rf->sc->name, rf1->sc->name ) != 0 )
		{
			trans[0] = (rf1->bb.maxx - rf1->bb.minx) / (rf->bb.maxx - rf->bb.minx);
			trans[1] = 0.0;
			trans[2] = 0.0;
			trans[3] = (rf1->bb.maxy - rf1->bb.miny) / (rf->bb.maxy - rf->bb.miny);
			trans[4] = rf1->bb.minx - rf->bb.minx;
			trans[5] = rf1->bb.maxy - rf->bb.maxy;
		}

		for( j = 0 ; j < 27 ; j++ )
		{
			if( j == 15 )
			{
				continue;
			}
			sc2 = sf->glyphs[sf->map->map[startindex2[i]+j]];
			for( rf2 = sc2->layers[ly_fore].refs ; rf2 != NULL ; rf2 = rf2->next )
			{
				if( rf2->sc->bmaster && rf2->sc->mmg_type == mg_jong )
				{
					memcpy( rf2->transform, trans, sizeof(trans) );
					SCReinstanciateRefChar( sc2, rf2, ly_fore);
					SCUpdateAll( sc2 );
				}
			}
		}
	}
	FontViewReformatAll( sf );
}

static void FVMenuHYReformBaseChar2(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;

	SplineChar *basesc1;
	SplineChar *basesc2;
	RefChar *baserf1;
	RefChar *baserf2;

	SplineChar *targetsc2;
	SplineChar *targetsc1;
	RefChar *targetrf1;
	RefChar *targetrf2;

	int basemap[4][2] =
	{
		{ 0xb9c8, 0xbaa8 },	// 마, 모
		{ 0xb9c8, 0xbac4 },	// 마, 뫄
		{ 0xb9d8, 0xbab8 },	// 맘, 몸
		{ 0xb9d8, 0xbad4 }	// 맘, 뫔
	};

	real trans[6];
	real tx;
	real ty;
	real dx;
	real dy;
	int i, j;

	// 마 모 가 고 -> chosung
	{
		basesc1 = sf->glyphs[sf->map->map[basemap[0][0]]];
		basesc2 = sf->glyphs[sf->map->map[basemap[0][1]]];

		for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
		{
			if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
			{
				for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
				{
					if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
					{
						for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
						{
							targetsc1 = sf->glyphs[sf->map->map[basechar_vert_min_tbl[j][0]]];
							targetsc2 = sf->glyphs[sf->map->map[basechar_vert_min_tbl[j][1]]];

							for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
							{
								if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
								{
									for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
									{
										if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
										{
											tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
											ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
											dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
											dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

											trans[0] = tx;
											trans[1] = 0.0;
											trans[2] = 0.0;
											trans[3] = ty;
											trans[4] = dx;
											trans[5] = dy;
											memcpy( targetrf2->transform, trans, sizeof(trans) );
											SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
											SCUpdateAll( targetsc2 );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}


	// 마 뫄 가 과 -> chosung
	{
		basesc1 = sf->glyphs[sf->map->map[basemap[1][0]]];
		basesc2 = sf->glyphs[sf->map->map[basemap[1][1]]];

		for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
		{
			if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
			{
				for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
				{
					if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
					{
						for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
						{
							targetsc1 = sf->glyphs[sf->map->map[basechar_comp_min_tbl[j][0]]];
							targetsc2 = sf->glyphs[sf->map->map[basechar_comp_min_tbl[j][1]]];

							for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
							{
								if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
								{
									for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
									{
										if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
										{
											tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
											ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
											dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
											dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

											trans[0] = tx;
											trans[1] = 0.0;
											trans[2] = 0.0;
											trans[3] = ty;
											trans[4] = dx;
											trans[5] = dy;
											memcpy( targetrf2->transform, trans, sizeof(trans) );
											SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
											SCUpdateAll( targetsc2 );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}


	// 맘 몸 감 곰
	{
		basesc1 = sf->glyphs[sf->map->map[basemap[2][0]]];
		basesc2 = sf->glyphs[sf->map->map[basemap[2][1]]];

		for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
		{
			if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
			{
				for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
				{
					if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
					{
						for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
						{
							targetsc1 = sf->glyphs[sf->map->map[basechar_vert_tbl[j][0]]];
							targetsc2 = sf->glyphs[sf->map->map[basechar_vert_tbl[j][1]]];

							for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
							{
								if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
								{
									for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
									{
										if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
										{
											tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
											ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
											dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
											dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

											trans[0] = tx;
											trans[1] = 0.0;
											trans[2] = 0.0;
											trans[3] = ty;
											trans[4] = dx;
											trans[5] = dy;
											memcpy( targetrf2->transform, trans, sizeof(trans) );
											SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
											SCUpdateAll( targetsc2 );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}


	// 맘 뫔 감 괌 -> chosung
	{
		basesc1 = sf->glyphs[sf->map->map[basemap[3][0]]];
		basesc2 = sf->glyphs[sf->map->map[basemap[3][1]]];

		for( baserf1 = basesc1->layers[ly_fore].refs ; baserf1 != NULL ; baserf1 = baserf1->next )
		{
			if( baserf1->sc->bmaster && baserf1->sc->mmg_type == mg_cho )
			{
				for( baserf2 = basesc2->layers[ly_fore].refs ; baserf2 != NULL ; baserf2 = baserf2->next )
				{
					if( baserf2->sc->bmaster && baserf2->sc->mmg_type == mg_cho )
					{
						for( j = 0 ; j < BASECHAR_MAP_COUNT ; j++ )
						{
							targetsc1 = sf->glyphs[sf->map->map[basechar_comp_tbl[j][0]]];
							targetsc2 = sf->glyphs[sf->map->map[basechar_comp_tbl[j][1]]];

							for( targetrf1 = targetsc1->layers[ly_fore].refs ; targetrf1 != NULL ; targetrf1 = targetrf1->next )
							{
								if( targetrf1->sc->bmaster && targetrf1->sc->mmg_type == mg_cho )
								{
									for( targetrf2 = targetsc2->layers[ly_fore].refs ; targetrf2 != NULL ; targetrf2 = targetrf2->next )
									{
										if( targetrf2->sc->bmaster && targetrf2->sc->mmg_type == mg_cho )
										{
											tx = (baserf2->bb.maxx - baserf2->bb.minx) / (baserf1->bb.maxx - baserf1->bb.minx);
											ty = (baserf2->bb.maxy - baserf2->bb.miny) / (baserf1->bb.maxy - baserf1->bb.miny);
											dx = (baserf2->bb.minx - baserf1->bb.minx) + ((1 - tx) * targetrf1->bb.minx);
											dy = (baserf2->bb.maxy - baserf1->bb.maxy) + ((1 - ty) * targetrf1->bb.maxy);

											trans[0] = tx;
											trans[1] = 0.0;
											trans[2] = 0.0;
											trans[3] = ty;
											trans[4] = dx;
											trans[5] = dy;
											memcpy( targetrf2->transform, trans, sizeof(trans) );
											SCReinstanciateRefChar( targetsc2, targetrf2, ly_fore);
											SCUpdateAll( targetsc2 );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

static void FVMenuHYReformBaseChar3(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	SplineChar *sc;
	RefChar *rf;
	int emsize = 1000;
	int i, j;
	int gid;
	real trans[6];

	// 세로모임 중앙 정렬 - 초성, 종성 중앙 정렬
	for( i = 0 ; i < JUNGSUNG_VERT_COUNT ; i++ )
	{
		for( j = jung_vert_startpos[i] ; j < jung_vert_startpos[i] + 28 ; j++ )
		{
			gid = sf->map->map[j];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting(sc) )
			{
				for( rf = sc->layers[ly_fore].refs ; rf != NULL ; rf = rf->next )
				{
					if( rf->sc->bmaster )
					{
						if( rf->sc->mmg_type == mg_cho || rf->sc->mmg_type == mg_jong )
						{
							trans[0] = rf->transform[0];
							trans[1] = rf->transform[1];
							trans[2] = rf->transform[2];
							trans[3] = rf->transform[3];
							trans[4] = rf->transform[4] + ((emsize / 2) - ((rf->bb.maxx + rf->bb.minx) / 2));
							trans[5] = rf->transform[5];
							memcpy( rf->transform, trans, sizeof(trans) );
							SCReinstanciateRefChar( sc, rf, ly_fore );
							SCUpdateAll( sc );
						}
					}
				}
			}
		}
	}
}

static void FVMenuSaveMMFNormal(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
#if 0
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	int ok;
	char * filename;
	char * fn;
	char * ret;

	hydebug( "[%s]\n", __FUNCTION__ );

	int s2d =	fv->b.cidmaster != NULL ? fv->b.cidmaster->save_to_dir :
			fv->b.sf->mm != NULL ? fv->b.sf->mm->normal->save_to_dir :
			fv->b.sf->save_to_dir;

	GGadgetCreateData gcd;
	GTextInfo label;

	if( fv->b.sf->mm != NULL )
	{
		hydebug( "[%s] MMF!!!!!\n", __FUNCTION__ );
		if( fv->b.sf->mm->normal->fontname == NULL )
		{
			hydebug( "[%s] ===1===\n", __FUNCTION__ );
			fn = def2utf8_copy( fv->b.sf->fontname );
		}
		else
		{
			hydebug( "[%s] ===2===\n", __FUNCTION__ );
			fn = def2utf8_copy( fv->b.sf->mm->normal->fontname );
		}
		hydebug( "fn: <%s>\n", fn );

		filename = malloc( strlen(fn) + 20 );
		strcpy( filename, fn );
		strcat( filename, "MM_normal.sfd" );
		hydebug( "filename: <%s>\n", filename );

		s2d = save_to_dir;

		memset(&gcd,0,sizeof(gcd));
		memset(&label,0,sizeof(label));
		gcd.gd.flags = s2d ? (gg_visible | gg_enabled | gg_cb_on) : (gg_visible | gg_enabled);
		label.text = (unichar_t *) _("Save as _Directory");
		label.text_is_1byte = true;
		label.text_in_resource = true;
		gcd.gd.label = &label;
		gcd.gd.handle_controlevent = SaveAs_FormatChange;
		gcd.data = &s2d;
		gcd.creator = GCheckBoxCreate;

		ret = gwwv_save_filename_with_gadget( _("Save as..."), filename, NULL, &gcd);
		if( ret == NULL )
		{
			return;
		}
		filename = utf82def_copy(ret);
		free(ret);

		FVFlattenAllBitmapSelections(fv);
		fv->b.sf->compression = 0;

		hydebug( "===============================================\n" );
		hydebug( "Do HY_MMGSFDWrite!\n" );
		ok = HY_MMSFDWrite( filename, fv->b.sf, fv->b.map, fv->b.normal, false );
		hydebug( "HY_MMGSFDWrite Done!\n" );
		hydebug( "===============================================\n" );
		if( ok )
		{
			SplineFont *sf = fv->b.sf->mm->normal;
			free(sf->filename);
			sf->filename = filename;
			sf->save_to_dir = false;
			free(sf->origname);
			sf->origname = copy(filename);
			sf->new = false;

			if ( sf->mm!=NULL )
			{
				int i;
				for ( i=0; i<sf->mm->instance_count; ++i )
				{
					free(sf->mm->instances[i]->filename);
					sf->mm->instances[i]->filename = filename;
					free(sf->mm->instances[i]->origname);
					sf->mm->instances[i]->origname = copy(filename);
					sf->mm->instances[i]->new = false;
				}
			}
			SplineFontSetUnChanged(sf);
			FVSetTitles(fv->b.sf);

		}
		free(filename);
	}
#endif
}
#endif

int _FVMenuSave(FontView *fv)
{
	int ret = 0;
	SplineFont *sf = fv->b.cidmaster ? fv->b.cidmaster : fv->b.sf->mm != NULL ? fv->b.sf->mm->normal : fv->b.sf;

	if ( sf->filename == NULL || IsBackupName(sf->filename))
	{
		ret = _FVMenuSaveAs(fv);
	}
	else
	{
		FVFlattenAllBitmapSelections(fv);
		if ( !SFDWriteBak(sf->filename,sf,fv->b.map,fv->b.normal) )
		{
			ff_post_error( _("Error"), _("Save Failed") );
		}
		else
		{
			SplineFontSetUnChanged(sf);
			ret = true;
		}
	}
	return( ret );
}

static void FVMenuSave(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    _FVMenuSave(fv);
}

void _FVCloseWindows(FontView *fv) {
    int i, j;
    BDFFont *bdf;
    MetricsView *mv, *mnext;
    SplineFont *sf = fv->b.cidmaster?fv->b.cidmaster:fv->b.sf->mm!=NULL?fv->b.sf->mm->normal : fv->b.sf;

    PrintWindowClose();
    if ( fv->b.nextsame==NULL && fv->b.sf->fv==&fv->b && fv->b.sf->kcld!=NULL )
	KCLD_End(fv->b.sf->kcld);
    if ( fv->b.nextsame==NULL && fv->b.sf->fv==&fv->b && fv->b.sf->vkcld!=NULL )
	KCLD_End(fv->b.sf->vkcld);

    for ( i=0; i<sf->glyphcnt; ++i ) if ( sf->glyphs[i]!=NULL ) {
	CharView *cv, *next;
	for ( cv = (CharView *) (sf->glyphs[i]->views); cv!=NULL; cv = next ) {
	    next = (CharView *) (cv->b.next);
	    GDrawDestroyWindow(cv->gw);
	}
	if ( sf->glyphs[i]->charinfo )
	    CharInfoDestroy(sf->glyphs[i]->charinfo);
    }
    if ( sf->mm!=NULL ) {
	MMSet *mm = sf->mm;
	for ( j=0; j<mm->instance_count; ++j ) {
	    SplineFont *sf = mm->instances[j];
	    for ( i=0; i<sf->glyphcnt; ++i ) if ( sf->glyphs[i]!=NULL ) {
		CharView *cv, *next;
		for ( cv = (CharView *) (sf->glyphs[i]->views); cv!=NULL; cv = next ) {
		    next = (CharView *) (cv->b.next);
		    GDrawDestroyWindow(cv->gw);
		}
		if ( sf->glyphs[i]->charinfo )
		    CharInfoDestroy(sf->glyphs[i]->charinfo);
	    }
	    for ( mv=sf->metrics; mv!=NULL; mv = mnext ) {
		mnext = mv->next;
		GDrawDestroyWindow(mv->gw);
	    }
	}
    } else if ( sf->subfontcnt!=0 ) {
	for ( j=0; j<sf->subfontcnt; ++j ) {
	    for ( i=0; i<sf->subfonts[j]->glyphcnt; ++i ) if ( sf->subfonts[j]->glyphs[i]!=NULL ) {
		CharView *cv, *next;
		for ( cv = (CharView *) (sf->subfonts[j]->glyphs[i]->views); cv!=NULL; cv = next ) {
		    next = (CharView *) (cv->b.next);
		    GDrawDestroyWindow(cv->gw);
		if ( sf->subfonts[j]->glyphs[i]->charinfo )
		    CharInfoDestroy(sf->subfonts[j]->glyphs[i]->charinfo);
		}
	    }
	    for ( mv=sf->subfonts[j]->metrics; mv!=NULL; mv = mnext ) {
		mnext = mv->next;
		GDrawDestroyWindow(mv->gw);
	    }
	}
    } else {
	for ( mv=sf->metrics; mv!=NULL; mv = mnext ) {
	    mnext = mv->next;
	    GDrawDestroyWindow(mv->gw);
	}
    }
    for ( bdf = sf->bitmaps; bdf!=NULL; bdf=bdf->next ) {
	for ( i=0; i<bdf->glyphcnt; ++i ) if ( bdf->glyphs[i]!=NULL ) {
	    BitmapView *bv, *next;
	    for ( bv = bdf->glyphs[i]->views; bv!=NULL; bv = next ) {
		next = bv->next;
		GDrawDestroyWindow(bv->gw);
	    }
	}
    }
    if ( fv->b.sf->fontinfo!=NULL )
	FontInfoDestroy(fv->b.sf);
    if ( fv->b.sf->valwin!=NULL )
	ValidationDestroy(fv->b.sf);
    SVDetachFV(fv);
}

static int SFAnyChanged(SplineFont *sf) {
    if ( sf->mm!=NULL ) {
	MMSet *mm = sf->mm;
	int i;
	if ( mm->changed )
return( true );
	for ( i=0; i<mm->instance_count; ++i )
	    if ( sf->mm->instances[i]->changed )
return( true );
	/* Changes to the blended font aren't real (for adobe fonts) */
	if ( mm->apple && mm->normal->changed )
return( true );

return( false );
    } else
return( sf->changed );
}

static int _FVMenuClose(FontView *fv)
{
	int i;
	SplineFont *sf = fv->b.cidmaster?fv->b.cidmaster:fv->b.sf;

	if ( !SFCloseAllInstrs(fv->b.sf) )
	{
		return( false );
	}

	if ( fv->b.nextsame!=NULL || fv->b.sf->fv!=&fv->b )
	{
		/* There's another view, can close this one with no problems */
	}
	else if ( SFAnyChanged(sf) )
	{
		i = AskChanged(fv->b.sf);
		if ( i==2 )	/* Cancel */
		{
			return( false );
		}

		if ( i==0 && !_FVMenuSave(fv))		/* Save */
		{
			return(false);
		}
		else
		{
			SFClearAutoSave(sf);		/* if they didn't save it, remove change record */
		}
	}

	_FVCloseWindows(fv);

	if ( sf->filename!=NULL )
	{
		hydebug( "[%s] 1. sf->filename: <%s>\n", __FUNCTION__, sf->filename );
		RecentFilesRemember(sf->filename);
	}
	else if ( sf->origname!=NULL )
	{
		hydebug( "[%s] 2. sf->origname: <%s>\n", __FUNCTION__, sf->origname );
#ifndef CKS	// HYMODIFY :: 2016.03.14
		RecentFilesRemember(sf->origname);
#endif
	}
	GDrawDestroyWindow(fv->gw);
	return( true );
}

void MenuNew(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontNew();
}

static void FVMenuClose(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);

	if ( fv->b.container )
	{
		(fv->b.container->funcs->doClose)(fv->b.container);
	}
	else
	{
		_FVMenuClose(fv);
	}
}

static void FV_ReattachCVs(SplineFont *old,SplineFont *new) {
    int i, j, pos;
    CharView *cv, *cvnext;
    SplineFont *sub;

    for ( i=0; i<old->glyphcnt; ++i ) {
	if ( old->glyphs[i]!=NULL && old->glyphs[i]->views!=NULL ) {
	    if ( new->subfontcnt==0 ) {
		pos = SFFindExistingSlot(new,old->glyphs[i]->unicodeenc,old->glyphs[i]->name);
		sub = new;
	    } else {
		pos = -1;
		for ( j=0; j<new->subfontcnt && pos==-1 ; ++j ) {
		    sub = new->subfonts[j];
		    pos = SFFindExistingSlot(sub,old->glyphs[i]->unicodeenc,old->glyphs[i]->name);
		}
	    }
	    if ( pos==-1 ) {
		for ( cv=(CharView *) (old->glyphs[i]->views); cv!=NULL; cv = cvnext ) {
		    cvnext = (CharView *) (cv->b.next);
		    GDrawDestroyWindow(cv->gw);
		}
	    } else {
		for ( cv=(CharView *) (old->glyphs[i]->views); cv!=NULL; cv = cvnext ) {
		    cvnext = (CharView *) (cv->b.next);
		    CVChangeSC(cv,sub->glyphs[pos]);
		    cv->b.layerheads[dm_grid] = &new->grid;
		}
	    }
	    GDrawProcessPendingEvents(NULL);		/* Don't want to many destroy_notify events clogging up the queue */
	}
    }
}

static void FVMenuRevert(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontViewBase *fv = (FontViewBase *) GDrawGetUserData(gw);
    FVRevert(fv);
}

static void FVMenuRevertBackup(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontViewBase *fv = (FontViewBase *) GDrawGetUserData(gw);
    FVRevertBackup(fv);
}

static void FVMenuRevertGlyph(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVRevertGlyph((FontViewBase *) fv);
}

static void FVMenuClearSpecialData(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVClearSpecialData((FontViewBase *) fv);
}

void MenuPrefs(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    DoPrefs();
}

void MenuXRes(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    DoXRes();
}

void MenuSaveAll(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv;

    for ( fv = fv_list; fv!=NULL; fv = (FontView *) (fv->b.next) ) {
	if ( SFAnyChanged(fv->b.sf) && !_FVMenuSave(fv))
return;
    }
}

static void _MenuExit(void *UNUSED(junk)) {

    FontView *fv, *next;

    if( collabclient_haveLocalServer() )
    {
	AskAndMaybeCloseLocalCollabServers();
    }
#ifndef _NO_PYTHON
    python_call_onClosingFunctions();
#endif

    LastFonts_Save();
    for ( fv = fv_list; fv!=NULL; fv = next )
    {
	next = (FontView *) (fv->b.next);
	if ( !_FVMenuClose(fv))
	    return;
	if ( fv->b.nextsame!=NULL || fv->b.sf->fv!=&fv->b )
	{
	    GDrawSync(NULL);
	    GDrawProcessPendingEvents(NULL);
	}
    }
    GDrawSync(NULL);
    GDrawProcessPendingEvents(NULL);
    exit(0);
}

static void FVMenuExit(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    _MenuExit(NULL);
}

void MenuExit(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *e) {
    if ( e==NULL )	/* Not from the menu directly, but a shortcut */
	_MenuExit(NULL);
    else
	DelayEvent(_MenuExit,NULL);
}

char *GetPostScriptFontName(char *dir, int mult)
{
	unichar_t *ret;
	char *u_dir;
	char *temp;

	u_dir = def2utf8_copy(dir);
	ret = FVOpenFont(_("Open Font"), u_dir,mult);
	temp = u2def_copy(ret);

	free(ret);
	return( temp );
}

void MergeKernInfo(SplineFont *sf,EncMap *map) {
#ifndef __Mac
    static char wild[] = "*.{afm,tfm,ofm,pfm,bin,hqx,dfont,feature,feat,fea}";
    static char wild2[] = "*.{afm,amfm,tfm,ofm,pfm,bin,hqx,dfont,feature,feat,fea}";
#else
    static char wild[] = "*";	/* Mac resource files generally don't have extensions */
    static char wild2[] = "*";
#endif
    char *ret = gwwv_open_filename(_("Merge Feature Info"),NULL,
	    sf->mm!=NULL?wild2:wild,NULL);
    char *temp;

    if ( ret==NULL )
return;				/* Cancelled */
    temp = utf82def_copy(ret);

    if ( !LoadKerningDataFromMetricsFile(sf,temp,map))
	ff_post_error( _("Load of Kerning Metrics Failed"), _("Failed to load kern data from %s"), temp);
    free(ret); free(temp);
}

static void FVMenuMergeKern(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    MergeKernInfo(fv->b.sf,fv->b.map);
}

void _FVMenuOpen(FontView *fv)
{
	char *temp;
	char *eod, *fpt, *file, *full;
	FontView *test; int fvcnt, fvtest;

	char* OpenDir = NULL, *DefaultDir = NULL, *NewDir = NULL;
#if defined(__MINGW32__)
#ifdef CKS	// HYMODIFY :: 2016.01.11
	int i;
	for( i = 0 ; i < RECENT_MAX ; i++ )
	{
		if( RecentFiles[i] != NULL )
		{
			DefaultDir = GFileDirNameEx( RecentFiles[i], true );
			break;
		}
		else
		{
			DefaultDir = copy(GFileGetHomeDocumentsDir());
		}
	}

	if (fv && fv->b.sf && fv->b.sf->filename)
	{
		free(DefaultDir);
		DefaultDir = GFileDirNameEx(fv->b.sf->filename, true);
	}
#else
	DefaultDir = copy(GFileGetHomeDocumentsDir()); //Default value
	if (fv && fv->b.sf && fv->b.sf->filename)
	{
		free(DefaultDir);
		DefaultDir = GFileDirNameEx(fv->b.sf->filename, true);
	}
#endif
#endif

	for ( fvcnt=0, test=fv_list; test!=NULL; ++fvcnt, test=(FontView *) (test->b.next) );

	do
	{
		if (NewDir != NULL)
		{
			if (OpenDir != DefaultDir)
			{
				free(OpenDir);
			}

			OpenDir = NewDir;
			NewDir = NULL;
		}
		else if (OpenDir != DefaultDir)
		{
			free(OpenDir);
			OpenDir = DefaultDir;
		}

		temp = GetPostScriptFontName(OpenDir,true);
		if ( temp == NULL )
		{
			return;
		}

		//Make a copy of the folder; may be needed later if opening fails.
		NewDir = GFileDirName(temp);
		if (!GFileExists(NewDir))
		{
			free(NewDir);
			NewDir = NULL;
		}

		eod = strrchr(temp,'/');
		if (eod != NULL)
		{
			*eod = '\0';
			file = eod+1;
            
			if (*file)
			{
				do
				{
					fpt = strstr(file,"; ");
					if ( fpt!=NULL )
					{
						*fpt = '\0';
					}
					full = malloc(strlen(temp)+1+strlen(file)+1);
					strcpy(full,temp);
					strcat(full,"/");
					strcat(full,file);
					ViewPostScriptFont(full,0);
					file = fpt+2;
					free(full);
				} while ( fpt!=NULL );
			}
		}
		free(temp);
		for ( fvtest=0, test=fv_list; test!=NULL; ++fvtest, test=(FontView *) (test->b.next) );
	} while ( fvtest==fvcnt );	/* did the load fail for some reason? try again */
    
	free( NewDir );
	free( OpenDir );
	if (OpenDir != DefaultDir)
	{
		free( DefaultDir );
	}
}

static void FVMenuOpen(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView*) GDrawGetUserData(gw);
    _FVMenuOpen(fv);
}

#ifdef CKS	// HYMODIFY :: 2016.02.11
#include <gresource.h>
#include <windows.h>
void hy_help(char *file)
{
	ShellExecute( NULL, "open", "hanyang.pdf", NULL, NULL, SW_SHOWDEFAULT );
}

static void FVMenuHYManual(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	hy_help("hanyang.pdf");
}
#endif

static void FVMenuContextualHelp(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    help("fontview.html");
}

void MenuHelp(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    help("overview.html");
}

void MenuIndex(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    help("IndexFS.html");
}

void MenuLicense(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    help("license.html");
}

void MenuAbout(GWindow UNUSED(base), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    ShowAboutScreen();
}

static void FVMenuImport(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int empty = fv->b.sf->onlybitmaps && fv->b.sf->bitmaps==NULL;
    BDFFont *bdf;
    FVImport(fv);
    if ( empty && fv->b.sf->bitmaps!=NULL ) {
	for ( bdf= fv->b.sf->bitmaps; bdf->next!=NULL; bdf = bdf->next );
	FVChangeDisplayBitmap((FontViewBase *) fv,bdf);
    }
}

static int FVSelCount(FontView *fv) {
    int i, cnt=0;

    for ( i=0; i<fv->b.map->enccount; ++i )
	if ( fv->b.selected[i] ) ++cnt;
    if ( cnt>10 ) {
	char *buts[3];
	buts[0] = _("_OK");
	buts[1] = _("_Cancel");
	buts[2] = NULL;
	if ( gwwv_ask(_("Many Windows"),(const char **) buts,0,1,_("This involves opening more than 10 windows.\nIs that really what you want?"))==1 )
return( false );
    }
return( true );
}

static void FVMenuOpenOutline(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i;
    SplineChar *sc;

    if ( !FVSelCount(fv))
return;
    if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
return;

    for ( i=0; i<fv->b.map->enccount; ++i )
	if ( fv->b.selected[i] ) {
	    sc = FVMakeChar(fv,i);
	    CharViewCreate(sc,fv,i);
	}
}

static void FVMenuOpenBitmap(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i;
    SplineChar *sc;

    if ( fv->b.cidmaster==NULL ? (fv->b.sf->bitmaps==NULL) : (fv->b.cidmaster->bitmaps==NULL) )
return;
    if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
return;
    if ( !FVSelCount(fv))
return;
    for ( i=0; i<fv->b.map->enccount; ++i )
	if ( fv->b.selected[i] ) {
	    sc = FVMakeChar(fv,i);
	    if ( sc!=NULL )
		BitmapViewCreatePick(i,fv);
	}
}

void _MenuWarnings(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    ShowErrorWindow();
}

static void FVMenuOpenMetrics(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
return;
    MetricsViewCreate(fv,NULL,fv->filled==fv->show?NULL:fv->show);
}

static void FVMenuPrint(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
return;
    PrintFFDlg(fv,NULL,NULL);
}

#if !defined(_NO_FFSCRIPT) || !defined(_NO_PYTHON)
static void FVMenuExecute(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    ScriptDlg(fv,NULL);
}
#endif

static void FVMenuFontInfo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
return;
    FontMenuFontInfo(fv);
}

static void FVMenuMATHInfo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFMathDlg(fv->b.sf,fv->b.active_layer);
}

static void FVMenuFindProblems(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FindProblems(fv,NULL,NULL);
}

static void FVMenuValidate(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFValidationWindow(fv->b.sf,fv->b.active_layer,ff_none);
}

static void FVMenuSetExtremumBound(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    char buffer[40], *end, *ret;
    int val;

    sprintf( buffer, "%d", fv->b.sf->extrema_bound<=0 ?
	    (int) rint((fv->b.sf->ascent+fv->b.sf->descent)/100.0) :
	    fv->b.sf->extrema_bound );
    ret = gwwv_ask_string(_("Extremum bound..."),buffer,_("Adobe says that \"big\" splines should not have extrema.\nBut they don't define what big means.\nIf the distance between the spline's end-points is bigger than this value, then the spline is \"big\" to fontforge."));
    if ( ret==NULL )
return;
    val = (int) rint(strtod(ret,&end));
    if ( *end!='\0' )
	ff_post_error( _("Bad Number"), _("Bad Number") );
    else {
	fv->b.sf->extrema_bound = val;
	if ( !fv->b.sf->changed ) {
	    fv->b.sf->changed = true;
	    FVSetTitles(fv->b.sf);
	}
    }
    free(ret);
}

static void FVMenuEmbolden(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    EmboldenDlg(fv,NULL);
}

static void FVMenuItalic(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    ItalicDlg(fv,NULL);
}

static void FVMenuSmallCaps(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    GlyphChangeDlg(fv,NULL,gc_smallcaps);
}

static void FVMenuChangeXHeight(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    ChangeXHeightDlg(fv,NULL);
}

static void FVMenuChangeGlyph(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    GlyphChangeDlg(fv,NULL,gc_generic);
}

static void FVMenuSubSup(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    GlyphChangeDlg(fv,NULL,gc_subsuper);
    /*AddSubSupDlg(fv);*/
}

static void FVMenuOblique(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    ObliqueDlg(fv,NULL);
}

static void FVMenuCondense(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    CondenseExtendDlg(fv,NULL);
}

#define MID_24	2001
#define MID_36	2002
#define MID_48	2004
#define MID_72	2014
#define MID_96	2015
#define MID_128	2018
#define MID_AntiAlias	2005
#define MID_Next	2006
#define MID_Prev	2007
#define MID_NextDef	2012
#define MID_PrevDef	2013
#define MID_ShowHMetrics 2016
#define MID_ShowVMetrics 2017
#define MID_Ligatures	2020
#define MID_KernPairs	2021
#define MID_AnchorPairs	2022
#define MID_FitToBbox	2023
#define MID_DisplaySubs	2024
#define MID_32x8	2025
#define MID_16x4	2026
#define MID_8x2		2027
#define MID_BitmapMag	2028
#define MID_Layers	2029
#define MID_FontInfo	2200
#define MID_CharInfo	2201
#define MID_Transform	2202
#define MID_Stroke	2203
#define MID_RmOverlap	2204
#define MID_Simplify	2205
#define MID_Correct	2206
#define MID_BuildAccent	2208
#define MID_AvailBitmaps	2210
#define MID_RegenBitmaps	2211
#define MID_Autotrace	2212
#define MID_Round	2213
#define MID_MergeFonts	2214
#define MID_InterpolateFonts	2215
#define MID_FindProblems 2216
#define MID_Embolden	2217
#define MID_Condense	2218
#define MID_ShowDependentRefs	2222
#define MID_AddExtrema	2224
#define MID_CleanupGlyph	2225
#define MID_TilePath	2226
#define MID_BuildComposite	2227
#define MID_NLTransform	2228
#define MID_Intersection	2229
#define MID_FindInter	2230
#define MID_Styles	2231
#define MID_SimplifyMore	2233
#define MID_ShowDependentSubs	2234
#define MID_DefaultATT	2235
#define MID_POV		2236
#define MID_BuildDuplicates	2237
#define MID_StrikeInfo	2238
#define MID_FontCompare	2239
#define MID_CanonicalStart	2242
#define MID_CanonicalContours	2243
#define MID_RemoveBitmaps	2244
#define MID_Validate		2245
#define MID_MassRename		2246
#define MID_Italic		2247
#define MID_SmallCaps		2248
#define MID_SubSup		2249
#define MID_ChangeXHeight	2250
#define MID_ChangeGlyph	2251
#define MID_SetColor	2252
#define MID_SetExtremumBound	2253
#define MID_Center	2600
#define MID_Thirds	2601
#define MID_SetWidth	2602
#define MID_SetLBearing	2603
#define MID_SetRBearing	2604
#define MID_SetVWidth	2605
#define MID_RmHKern	2606
#define MID_RmVKern	2607
#define MID_VKernByClass	2608
#define MID_VKernFromH	2609
#define MID_SetBearings	2610
#define MID_AutoHint	2501
#define MID_ClearHints	2502
#define MID_ClearWidthMD	2503
#define MID_AutoInstr	2504
#define MID_EditInstructions	2505
#define MID_Editfpgm	2506
#define MID_Editprep	2507
#define MID_ClearInstrs	2508
#define MID_HStemHist	2509
#define MID_VStemHist	2510
#define MID_BlueValuesHist	2511
#define MID_Editcvt	2512
#define MID_HintSubsPt	2513
#define MID_AutoCounter	2514
#define MID_DontAutoHint	2515
#define MID_RmInstrTables	2516
#define MID_Editmaxp	2517
#define MID_Deltas	2518
#define MID_OpenBitmap	2700
#define MID_OpenOutline	2701
#define MID_Revert	2702
#define MID_Recent	2703
#define MID_Print	2704
#define MID_ScriptMenu	2705
#define MID_RevertGlyph	2707
#define MID_RevertToBackup 2708
#define MID_GenerateTTC 2709
#define MID_OpenMetrics	2710
#define MID_ClearSpecialData 2711
#define MID_Cut		2101
#define MID_Copy	2102
#define MID_Paste	2103
#define MID_Clear	2104
#define MID_SelAll	2106
#define MID_CopyRef	2107
#define MID_UnlinkRef	2108
#define MID_Undo	2109
#define MID_Redo	2110
#define MID_CopyWidth	2111
#define MID_UndoFontLevel	2112
#define MID_AllFonts		2122
#define MID_DisplayedFont	2123
#define	MID_CharName		2124
#define MID_RemoveUndoes	2114
#define MID_CopyFgToBg	2115
#define MID_ClearBackground	2116
#define MID_CopyLBearing	2125
#define MID_CopyRBearing	2126
#define MID_CopyVWidth	2127
#define MID_Join	2128
#define MID_PasteInto	2129
#define MID_SameGlyphAs	2130
#define MID_RplRef	2131
#define MID_PasteAfter	2132
#define	MID_TTFInstr	2134
#define	MID_CopyLookupData	2135
#define MID_CopyL2L	2136
#define MID_CorrectRefs	2137
#define MID_Convert2CID	2800
#define MID_Flatten	2801
#define MID_InsertFont	2802
#define MID_InsertBlank	2803
#define MID_CIDFontInfo	2804
#define MID_RemoveFromCID 2805
#define MID_ConvertByCMap	2806
#define MID_FlattenByCMap	2807
#define MID_ChangeSupplement	2808
#define MID_Reencode		2830
#define MID_ForceReencode	2831
#define MID_AddUnencoded	2832
#define MID_RemoveUnused	2833
#define MID_DetachGlyphs	2834
#define MID_DetachAndRemoveGlyphs	2835
#define MID_LoadEncoding	2836
#define MID_MakeFromFont	2837
#define MID_RemoveEncoding	2838
#define MID_DisplayByGroups	2839
#define MID_Compact	2840
#define MID_SaveNamelist	2841
#define MID_RenameGlyphs	2842
#define MID_NameGlyphs		2843
#define MID_HideNoGlyphSlots	2844
#define MID_CreateMM	2900
#define MID_MMInfo	2901
#define MID_MMValid	2902
#define MID_ChangeMMBlend	2903
#define MID_BlendToNew	2904
#define MID_ModifyComposition	20902
#define MID_BuildSyllables	20903
#define MID_CollabStart         22000
#define MID_CollabConnect       22001
#define MID_CollabDisconnect    22002
#define MID_CollabCloseLocalServer  22003
#define MID_CollabConnectToExplicitAddress 22004


#define MID_Warnings	3000


/* returns -1 if nothing selected, if exactly one char return it, -2 if more than one */
static int FVAnyCharSelected(FontView *fv) {
    int i, val=-1;

    for ( i=0; i<fv->b.map->enccount; ++i ) {
	if ( fv->b.selected[i]) {
	    if ( val==-1 )
		val = i;
	    else
return( -2 );
	}
    }
return( val );
}

static int FVAllSelected(FontView *fv) {
    int i, any = false;
    /* Is everything real selected? */

    for ( i=0; i<fv->b.sf->glyphcnt; ++i ) if ( SCWorthOutputting(fv->b.sf->glyphs[i])) {
	if ( !fv->b.selected[fv->b.map->backmap[i]] )
return( false );
	any = true;
    }
return( any );
}

static void FVMenuCopyFrom(GWindow UNUSED(gw), struct gmenuitem *mi, GEvent *UNUSED(e)) {
    /*FontView *fv = (FontView *) GDrawGetUserData(gw);*/

    if ( mi->mid==MID_CharName )
	copymetadata = !copymetadata;
    else if ( mi->mid==MID_TTFInstr )
	copyttfinstr = !copyttfinstr;
    else
	onlycopydisplayed = (mi->mid==MID_DisplayedFont);
    SavePrefs(true);
}

static void FVMenuCopy(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( FVAnyCharSelected(fv)==-1 )
return;
    FVCopy((FontViewBase *) fv,ct_fullcopy);
}

static void FVMenuCopyLookupData(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( FVAnyCharSelected(fv)==-1 )
return;
    FVCopy((FontViewBase *) fv,ct_lookups);
}

static void FVMenuCopyRef(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( FVAnyCharSelected(fv)==-1 )
return;
    FVCopy((FontViewBase *) fv,ct_reference);
}

static void FVMenuCopyWidth(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( FVAnyCharSelected(fv)==-1 )
return;
    if ( mi->mid==MID_CopyVWidth && !fv->b.sf->hasvmetrics )
return;
    FVCopyWidth((FontViewBase *) fv,
		   mi->mid==MID_CopyWidth?ut_width:
		   mi->mid==MID_CopyVWidth?ut_vwidth:
		   mi->mid==MID_CopyLBearing?ut_lbearing:
					 ut_rbearing);
}

static void FVMenuPaste(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( FVAnyCharSelected(fv)==-1 )
return;
    PasteIntoFV((FontViewBase *) fv,false,NULL);
}

static void FVMenuPasteInto(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( FVAnyCharSelected(fv)==-1 )
return;
    PasteIntoFV((FontViewBase *) fv,true,NULL);
}

static void FVMenuPasteAfter(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int pos = FVAnyCharSelected(fv);
    if ( pos<0 )
return;
    PasteIntoFV(&fv->b,2,NULL);
}

static void FVMenuSameGlyphAs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVSameGlyphAs((FontViewBase *) fv);
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuCopyFgBg(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVCopyFgtoBg( (FontViewBase *) fv );
}

static void FVMenuCopyL2L(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVCopyLayerToLayer( fv );
}

static void FVMenuCompareL2L(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVCompareLayerToLayer( fv );
}

static void FVMenuClear(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVClear( (FontViewBase *) fv );

#ifdef HYMODIFY
    HY_CheckMMGFont( fv->b.sf );
#endif
}

static void FVMenuClearBackground(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVClearBackground( (FontViewBase *) fv );
}

static void FVMenuJoin(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVJoin( (FontViewBase *) fv );
}

#ifdef HYMODIFY
static void FVMenuHYSetMG(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);

	HY_SetMasterGlyphDlg( fv );
}

static void FVMenuHYUnsetMG(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView * fv = (FontView *) GDrawGetUserData(gw);
	SplineFont * sf = fv->b.sf;
	int i, gid;

	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		if( fv->b.selected[i] && (gid = sf->map->map[i]) != -1 && SCWorthOutputting(sf->glyphs[gid]) )
		{
			sf->glyphs[gid]->bmaster = 0;
		}
	}
	GDrawRequestExpose(fv->v, NULL, false);
	GDrawRequestExpose(fv->gw, NULL, false);
}

static void FVMenuHYSetMGType(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView * fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	int i, gid;

	for( i = 0 ; i < sf->map->enccount ; i++ )
	{
		if(fv->b.selected[i])
		{
			if( (gid = sf->map->map[i] ) != -1 && SCWorthOutputting(sf->glyphs[gid]) && sf->glyphs[gid]->bmaster )
			{
				if( mi->mid == MID_HY_SetMGTypeCho )
				{
					sf->glyphs[gid]->mmg_type = MG_TYPE_CHO;
				}
				else if( mi->mid == MID_HY_SetMGTypeJung )
				{
					sf->glyphs[gid]->mmg_type = MG_TYPE_JUNG;
				}
				else if( mi->mid == MID_HY_SetMGTypeJong )
				{
					sf->glyphs[gid]->mmg_type = MG_TYPE_JONG;
				}
				else if( mi->mid == MID_HY_UnsetMGType )
				{
					sf->glyphs[gid]->mmg_type = MG_TYPE_NONE;
				}
			}
		}
	}
}

static void FVMenuHYCopyMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView * fv = (FontView *) GDrawGetUserData(gw);

	if( FVAnyCharSelected( fv ) == -1 )
	{
		return;
	}

	switch( mi->mid )
	{
		case MID_HY_CopyMG:
		{
			HY_MGCopy( (FontViewBase *) fv, 0 );
		}
		break;
		case MID_HY_MoveMG:
		{
			HY_MGCopy( (FontViewBase *) fv, 1 );
		}
		break;
	}
}

static void FVMenuHYPasteMG(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView * fv = (FontView *) GDrawGetUserData(gw);

	if( FVAnyCharSelected(fv) == -1 )
	{
		return;
	}

	HY_MGPaste( (FontViewBase *)fv, false, NULL );
}

static void FVMenuHYClearMG(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont *sf = fv->b.sf;
	int i;

	if( mi->mid == MID_HY_ClearMG )
	{
		HY_ClearMasterGlyphs( (FontViewBase *) fv );
	}
	else if( mi->mid == MID_HY_ClearAllMG )
	{
		HY_ClearMasterGlyphAll( (FontViewBase *) fv );
	}
	HY_CheckMMGFont( fv->b.sf );
}

static void FVMenuHYFitMGBBox(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	SplineFont * sf = fv->b.sf;
	SplineChar * sc;
	int i, gid;

	if( sf->bMMGFont )
	{
		for( i = 0 ; i < sf->map->enccount ; i++ )
		{
			gid = sf->map->map[i];
			sc = sf->glyphs[gid];
			if( gid != -1 && SCWorthOutputting( sc ) )
			{
				if( sc->bmaster )
				{
					HY_FitAllMMGBounds( sc );
				}
			}
		}
	}
}
#endif

static void FVMenuUnlinkRef(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVUnlinkRef( (FontViewBase *) fv );
}

static void FVMenuRemoveUndoes(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFRemoveUndoes(fv->b.sf,fv->b.selected,fv->b.map);
}

static void FVMenuUndo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVUndo((FontViewBase *) fv);
}

static void FVMenuRedo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVRedo((FontViewBase *) fv);
}

static void FVMenuUndoFontLevel(GWindow gw,struct gmenuitem *mi,GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FontViewBase * fvb = (FontViewBase *) fv;
    SplineFont *sf = fvb->sf;

    if( !sf->undoes )
	return;

    struct sfundoes *undo = sf->undoes;
    printf("font level undo msg:%s\n", undo->msg );
    SFUndoPerform( undo, sf );
    SFUndoRemoveAndFree( sf, undo );
}

static void FVMenuCut(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVCopy(&fv->b,ct_fullcopy);
    FVClear(&fv->b);
}

static void FVMenuSelectAll(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVSelectAll(fv);
}

static void FVMenuInvertSelection(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVInvertSelection(fv);
}

static void FVMenuDeselectAll(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVDeselectAll(fv);
}

enum merge_type { mt_set=0, mt_merge=4, mt_or=mt_merge, mt_restrict=8, mt_and=12 };
    /* Index array by merge_type(*4) + selection*2 + doit */
const uint8 mergefunc[] = {
/* mt_set */
	0, 1,
	0, 1,
/* mt_merge */
	0, 1,
	1, 1,
/* mt_restrict */
	0, 0,
	1, 0,
/* mt_and */
	0, 0,
	0, 1,
};

static enum merge_type SelMergeType(GEvent *e) {
    if ( e->type!=et_mouseup )
return( mt_set );

return( ((e->u.mouse.state&ksm_shift)?mt_merge:0) |
	((e->u.mouse.state&ksm_control)?mt_restrict:0) );
}

static char *SubMatch(char *pattern, char *eop, char *name,int ignorecase) {
    char ch, *ppt, *npt, *ept, *eon;

    while ( pattern<eop && ( ch = *pattern)!='\0' ) {
	if ( ch=='*' ) {
	    if ( pattern[1]=='\0' )
return( name+strlen(name));
	    for ( npt=name; ; ++npt ) {
		if ( (eon = SubMatch(pattern+1,eop,npt,ignorecase))!= NULL )
return( eon );
		if ( *npt=='\0' )
return( NULL );
	    }
	} else if ( ch=='?' ) {
	    if ( *name=='\0' )
return( NULL );
	    ++name;
	} else if ( ch=='[' ) {
	    /* [<char>...] matches the chars
	     * [<char>-<char>...] matches any char within the range (inclusive)
	     * the above may be concattenated and the resultant pattern matches
	     *		anything thing which matches any of them.
	     * [^<char>...] matches any char which does not match the rest of
	     *		the pattern
	     * []...] as a special case a ']' immediately after the '[' matches
	     *		itself and does not end the pattern
	     */
	    int found = 0, not=0;
	    ++pattern;
	    if ( pattern[0]=='^' ) { not = 1; ++pattern; }
	    for ( ppt = pattern; (ppt!=pattern || *ppt!=']') && *ppt!='\0' ; ++ppt ) {
		ch = *ppt;
		if ( ppt[1]=='-' && ppt[2]!=']' && ppt[2]!='\0' ) {
		    char ch2 = ppt[2];
		    if ( (*name>=ch && *name<=ch2) ||
			    (ignorecase && islower(ch) && islower(ch2) &&
				    *name>=toupper(ch) && *name<=toupper(ch2)) ||
			    (ignorecase && isupper(ch) && isupper(ch2) &&
				    *name>=tolower(ch) && *name<=tolower(ch2))) {
			if ( !not ) {
			    found = 1;
	    break;
			}
		    } else {
			if ( not ) {
			    found = 1;
	    break;
			}
		    }
		    ppt += 2;
		} else if ( ch==*name || (ignorecase && tolower(ch)==tolower(*name)) ) {
		    if ( !not ) {
			found = 1;
	    break;
		    }
		} else {
		    if ( not ) {
			found = 1;
	    break;
		    }
		}
	    }
	    if ( !found )
return( NULL );
	    while ( *ppt!=']' && *ppt!='\0' ) ++ppt;
	    pattern = ppt;
	    ++name;
	} else if ( ch=='{' ) {
	    /* matches any of a comma separated list of substrings */
	    for ( ppt = pattern+1; *ppt!='\0' ; ppt = ept ) {
		for ( ept=ppt; *ept!='}' && *ept!=',' && *ept!='\0'; ++ept );
		npt = SubMatch(ppt,ept,name,ignorecase);
		if ( npt!=NULL ) {
		    char *ecurly = ept;
		    while ( *ecurly!='}' && ecurly<eop && *ecurly!='\0' ) ++ecurly;
		    if ( (eon=SubMatch(ecurly+1,eop,npt,ignorecase))!=NULL )
return( eon );
		}
		if ( *ept=='}' )
return( NULL );
		if ( *ept==',' ) ++ept;
	    }
	} else if ( ch==*name ) {
	    ++name;
	} else if ( ignorecase && tolower(ch)==tolower(*name)) {
	    ++name;
	} else
return( NULL );
	++pattern;
    }
return( name );
}

/* Handles *?{}[] wildcards */
static int WildMatch(char *pattern, char *name,int ignorecase) {
    char *eop = pattern + strlen(pattern);

    if ( pattern==NULL )
return( true );

    name = SubMatch(pattern,eop,name,ignorecase);
    if ( name==NULL )
return( false );
    if ( *name=='\0' )
return( true );

return( false );
}

static int SS_ScriptChanged(GGadget *g, GEvent *e) {

    if ( e->type==et_controlevent && e->u.control.subtype != et_textfocuschanged ) {
	char *txt = GGadgetGetTitle8(g);
	char buf[8];
	int i;
	extern GTextInfo scripts[];

	for ( i=0; scripts[i].text!=NULL; ++i ) {
	    if ( strcmp((char *) scripts[i].text,txt)==0 )
	break;
	}
	free(txt);
	if ( scripts[i].text==NULL )
return( true );
	buf[0] = ((intpt) scripts[i].userdata)>>24;
	buf[1] = ((intpt) scripts[i].userdata)>>16;
	buf[2] = ((intpt) scripts[i].userdata)>>8 ;
	buf[3] = ((intpt) scripts[i].userdata)    ;
	buf[4] = 0;
	GGadgetSetTitle8(g,buf);
    }
return( true );
}

static int SS_OK(GGadget *g, GEvent *e) {

    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	int *done = GDrawGetUserData(GGadgetGetWindow(g));
	*done = 2;
    }
return( true );
}

static int SS_Cancel(GGadget *g, GEvent *e) {

    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	int *done = GDrawGetUserData(GGadgetGetWindow(g));
	*done = true;
    }
return( true );
}

static int ss_e_h(GWindow gw, GEvent *event) {
    int *done = GDrawGetUserData(gw);

    switch ( event->type ) {
      case et_char:
return( false );
      case et_close:
	*done = true;
      break;
    }
return( true );
}

static void FVSelectByScript(FontView *fv,int merge) {
    int j, gid;
    SplineChar *sc;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    extern GTextInfo scripts[];
    GRect pos;
    GWindow gw;
    GWindowAttrs wattrs;
    GGadgetCreateData gcd[10], *hvarray[21][2], *barray[8], boxes[3];
    GTextInfo label[10];
    int i,k;
    int done = 0, doit;
    char tagbuf[4];
    uint32 tag;
    const unichar_t *ret;
    int lc_k, uc_k, select_k;
    int only_uc=0, only_lc=0;

    LookupUIInit();

    memset(&wattrs,0,sizeof(wattrs));
    memset(&gcd,0,sizeof(gcd));
    memset(&label,0,sizeof(label));
    memset(&boxes,0,sizeof(boxes));

    wattrs.mask = wam_events|wam_cursor|wam_utf8_wtitle|wam_undercursor|wam_isdlg|wam_restrict|wam_isdlg;
    wattrs.event_masks = ~(1<<et_charup);
    wattrs.restrict_input_to_me = false;
    wattrs.is_dlg = 1;
    wattrs.undercursor = 1;
    wattrs.cursor = ct_pointer;
    wattrs.utf8_window_title = _("Select by Script");
    wattrs.is_dlg = true;
    pos.x = pos.y = 0;
    pos.width = 100;
    pos.height = 100;
    gw = GDrawCreateTopWindow(NULL,&pos,ss_e_h,&done,&wattrs);

    k = i = 0;

    gcd[k].gd.flags = gg_visible|gg_enabled ;
    gcd[k].gd.u.list = scripts;
    gcd[k].gd.handle_controlevent = SS_ScriptChanged;
    gcd[k++].creator = GListFieldCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("All glyphs");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup|gg_cb_on;
    gcd[k].gd.popup_msg = (unichar_t *) _("Set the selection of the font view to all glyphs in the script.");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;
    hvarray[i][0] = GCD_HPad10; hvarray[i++][1] = NULL;

    uc_k = k;
    label[k].text = (unichar_t *) _("Only upper case");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Set the selection of the font view to any upper case glyphs in the script.");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    lc_k = k;
    label[k].text = (unichar_t *) _("Only lower case");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Set the selection of the font view to any lower case glyphs in the script.");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;
    hvarray[i][0] = GCD_HPad10; hvarray[i++][1] = NULL;

    select_k = k;
    label[k].text = (unichar_t *) _("Select Results");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup|gg_rad_startnew;
    gcd[k].gd.popup_msg = (unichar_t *) _("Set the selection of the font view to the glyphs\nwhich match");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Merge Results");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Expand the selection of the font view to include\nall the glyphs which match");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Restrict Selection");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Remove matching glyphs from the selection." );
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Logical And with Selection");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Remove glyphs which do not match from the selection." );
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;
    gcd[k-4 + merge/4].gd.flags |= gg_cb_on;

    hvarray[i][0] = GCD_Glue; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("_OK");
    label[k].text_is_1byte = true;
    label[k].text_in_resource = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_visible|gg_enabled | gg_but_default;
    gcd[k].gd.handle_controlevent = SS_OK;
    gcd[k++].creator = GButtonCreate;

    label[k].text = (unichar_t *) _("_Cancel");
    label[k].text_is_1byte = true;
    label[k].text_in_resource = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_visible|gg_enabled | gg_but_cancel;
    gcd[k].gd.handle_controlevent = SS_Cancel;
    gcd[k++].creator = GButtonCreate;

    barray[0] = barray[2] = barray[3] = barray[4] = barray[6] = GCD_Glue; barray[7] = NULL;
    barray[1] = &gcd[k-2]; barray[5] = &gcd[k-1];
    hvarray[i][0] = &boxes[2]; hvarray[i++][1] = NULL;
    hvarray[i][0] = NULL;

    memset(boxes,0,sizeof(boxes));
    boxes[0].gd.pos.x = boxes[0].gd.pos.y = 2;
    boxes[0].gd.flags = gg_enabled|gg_visible;
    boxes[0].gd.u.boxelements = hvarray[0];
    boxes[0].creator = GHVGroupCreate;

    boxes[2].gd.flags = gg_enabled|gg_visible;
    boxes[2].gd.u.boxelements = barray;
    boxes[2].creator = GHBoxCreate;

    GGadgetsCreate(gw,boxes);
    GHVBoxSetExpandableCol(boxes[2].ret,gb_expandgluesame);
    GHVBoxSetExpandableRow(boxes[0].ret,gb_expandglue);


    GHVBoxFitWindow(boxes[0].ret);

    GDrawSetVisible(gw,true);
    ret = NULL;
    while ( !done ) {
	GDrawProcessOneEvent(NULL);
	if ( done==2 ) {
	    ret = _GGadgetGetTitle(gcd[0].ret);
	    if ( *ret=='\0' ) {
		ff_post_error( _("No Script"), _("Please specify a script") );
		done = 0;
	    } else if ( u_strlen(ret)>4 ) {
		ff_post_error( _("Bad Script"), _("Scripts are 4 letter tags") );
		done = 0;
	    }
	}
    }
    memset(tagbuf,' ',4);
    if ( done==2 && ret!=NULL ) {
	tagbuf[0] = *ret;
	if ( ret[1]!='\0' ) {
	    tagbuf[1] = ret[1];
	    if ( ret[2]!='\0' ) {
		tagbuf[2] = ret[2];
		if ( ret[3]!='\0' )
		    tagbuf[3] = ret[3];
	    }
	}
    }
    merge = GGadgetIsChecked(gcd[select_k+0].ret) ? mt_set :
	    GGadgetIsChecked(gcd[select_k+1].ret) ? mt_merge :
	    GGadgetIsChecked(gcd[select_k+2].ret) ? mt_restrict :
						    mt_and;
    only_uc = GGadgetIsChecked(gcd[uc_k+0].ret);
    only_lc = GGadgetIsChecked(gcd[lc_k+0].ret);

    GDrawDestroyWindow(gw);
    if ( done==1 )
return;
    tag = (tagbuf[0]<<24) | (tagbuf[1]<<16) | (tagbuf[2]<<8) | tagbuf[3];

    for ( j=0; j<map->enccount; ++j ) if ( (gid=map->map[j])!=-1 && (sc=sf->glyphs[gid])!=NULL ) {
	doit = ( SCScriptFromUnicode(sc)==tag );
	if ( doit ) {
	    if ( only_uc && (sc->unicodeenc==-1 || sc->unicodeenc>0xffff ||
		    !isupper(sc->unicodeenc)) )
		doit = false;
	    else if ( only_lc && (sc->unicodeenc==-1 || sc->unicodeenc>0xffff ||
		    !islower(sc->unicodeenc)) )
		doit = false;
	}
	fv->b.selected[j] = mergefunc[ merge + (fv->b.selected[j]?2:0) + doit ];
    } else if ( merge==mt_set )
	fv->b.selected[j] = false;

    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSelectByScript(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVSelectByScript(fv,SelMergeType(e));
}

static void FVSelectColor(FontView *fv, uint32 col, int merge) {
    int i, doit;
    uint32 sccol;
    SplineChar **glyphs = fv->b.sf->glyphs;

    for ( i=0; i<fv->b.map->enccount; ++i ) {
	int gid = fv->b.map->map[i];
	sccol =  ( gid==-1 || glyphs[gid]==NULL ) ? COLOR_DEFAULT : glyphs[gid]->color;
	doit = sccol==col;
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSelectColor(GWindow gw, struct gmenuitem *mi, GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    Color col = (Color) (intpt) (mi->ti.userdata);
    if ( (intpt) mi->ti.userdata == (intpt) -10 ) {
	struct hslrgb retcol, font_cols[6];
	retcol = GWidgetColor(_("Pick a color"),NULL,SFFontCols(fv->b.sf,font_cols));
	if ( !retcol.rgb )
return;
	col = (((int) rint(255.*retcol.r))<<16 ) |
		    (((int) rint(255.*retcol.g))<<8 ) |
		    (((int) rint(255.*retcol.b)) );
    }
    FVSelectColor(fv,col,SelMergeType(e));
}

static int FVSelectByName(FontView *fv, char *ret, int merge) {
    int j, gid, doit;
    char *end;
    SplineChar *sc;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    struct altuni *alt;

    if ( !merge )
	FVDeselectAll(fv);
    if (( *ret=='0' && ( ret[1]=='x' || ret[1]=='X' )) ||
	    ((*ret=='u' || *ret=='U') && ret[1]=='+' )) {
	int uni = (int) strtol(ret+2,&end,16);
	int vs= -2;
	if ( *end=='.' ) {
	    ++end;
	    if (( *end=='0' && ( end[1]=='x' || end[1]=='X' )) ||
		    ((*end=='u' || *end=='U') && end[1]=='+' ))
		end += 2;
	    vs = (int) strtoul(end,&end,16);
	}
	if ( *end!='\0' || uni<0 || uni>=0x110000 ) {
	    ff_post_error( _("Bad Number"), _("Bad Number") );
return( false );
	}
	for ( j=0; j<map->enccount; ++j ) if ( (gid=map->map[j])!=-1 && (sc=sf->glyphs[gid])!=NULL ) {
	    if ( vs==-2 ) {
		for ( alt=sc->altuni; alt!=NULL && (alt->unienc!=uni || alt->fid!=0); alt=alt->next );
	    } else {
		for ( alt=sc->altuni; alt!=NULL && (alt->unienc!=uni || alt->vs!=vs || alt->fid!=0); alt=alt->next );
	    }
	    doit = (sc->unicodeenc == uni && vs<0) || alt!=NULL;
	    fv->b.selected[j] = mergefunc[ merge + (fv->b.selected[j]?2:0) + doit ];
	} else if ( merge==mt_set )
	    fv->b.selected[j] = false;
    } else {
	for ( j=0; j<map->enccount; ++j ) if ( (gid=map->map[j])!=-1 && (sc=sf->glyphs[gid])!=NULL ) {
	    doit = WildMatch(ret,sc->name,false);
	    fv->b.selected[j] = mergefunc[ merge + (fv->b.selected[j]?2:0) + doit ];
	} else if ( merge==mt_set )
	    fv->b.selected[j] = false;
    }
    GDrawRequestExpose(fv->v,NULL,false);
    fv->sel_index = 1;
return( true );
}

static void FVMenuSelectByName(GWindow _gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(_gw);
    GRect pos;
    GWindow gw;
    GWindowAttrs wattrs;
    GGadgetCreateData gcd[8], *hvarray[12][2], *barray[8], boxes[3];
    GTextInfo label[8];
    int merge = SelMergeType(e);
    int done=0,k,i;

    memset(&wattrs,0,sizeof(wattrs));
    memset(&gcd,0,sizeof(gcd));
    memset(&label,0,sizeof(label));
    memset(&boxes,0,sizeof(boxes));

    wattrs.mask = wam_events|wam_cursor|wam_utf8_wtitle|wam_undercursor|wam_isdlg|wam_restrict;
    wattrs.event_masks = ~(1<<et_charup);
    wattrs.restrict_input_to_me = false;
    wattrs.undercursor = 1;
    wattrs.cursor = ct_pointer;
    wattrs.utf8_window_title = _("Select by Name");
    wattrs.is_dlg = false;
    pos.x = pos.y = 0;
    pos.width = 100;
    pos.height = 100;
    gw = GDrawCreateTopWindow(NULL,&pos,ss_e_h,&done,&wattrs);

    k = i = 0;

    label[k].text = (unichar_t *) _("Enter either a wildcard pattern (to match glyph names)\n or a unicode encoding like \"U+0065\".");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_visible | gg_enabled | gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _(
	"Unix style wildcarding is accepted:\n"
	"Most characters match themselves\n"
	"A \"?\" will match any single character\n"
	"A \"*\" will match an arbitrary number of characters (including none)\n"
	"An \"[abd]\" set of characters within square brackets will match any (single) character\n"
	"A \"{scmp,c2sc}\" set of strings within curly brackets will match any string\n"
	"So \"a.*\" would match \"a.\" or \"a.sc\" or \"a.swash\"\n"
	"While \"a.{scmp,c2sc}\" would match \"a.scmp\" or \"a.c2sc\"\n"
	"And \"a.[abd]\" would match \"a.a\" or \"a.b\" or \"a.d\"");
    gcd[k++].creator = GLabelCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    gcd[k].gd.flags = gg_visible | gg_enabled | gg_utf8_popup;
    gcd[k].gd.popup_msg = gcd[k-1].gd.popup_msg;
    gcd[k++].creator = GTextFieldCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Select Results");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Set the selection of the font view to the glyphs\nwhich match");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Merge Results");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Expand the selection of the font view to include\nall the glyphs which match");
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Restrict Selection");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Remove matching glyphs from the selection." );
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("Logical And with Selection");
    label[k].text_is_1byte = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_enabled|gg_visible|gg_utf8_popup;
    gcd[k].gd.popup_msg = (unichar_t *) _("Remove glyphs which do not match from the selection." );
    gcd[k++].creator = GRadioCreate;
    hvarray[i][0] = &gcd[k-1]; hvarray[i++][1] = NULL;
    gcd[k-4 + merge/4].gd.flags |= gg_cb_on;

    hvarray[i][0] = GCD_Glue; hvarray[i++][1] = NULL;

    label[k].text = (unichar_t *) _("_OK");
    label[k].text_is_1byte = true;
    label[k].text_in_resource = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_visible|gg_enabled | gg_but_default;
    gcd[k].gd.handle_controlevent = SS_OK;
    gcd[k++].creator = GButtonCreate;

    label[k].text = (unichar_t *) _("_Cancel");
    label[k].text_is_1byte = true;
    label[k].text_in_resource = true;
    gcd[k].gd.label = &label[k];
    gcd[k].gd.flags = gg_visible|gg_enabled | gg_but_cancel;
    gcd[k].gd.handle_controlevent = SS_Cancel;
    gcd[k++].creator = GButtonCreate;

    barray[0] = barray[2] = barray[3] = barray[4] = barray[6] = GCD_Glue; barray[7] = NULL;
    barray[1] = &gcd[k-2]; barray[5] = &gcd[k-1];
    hvarray[i][0] = &boxes[2]; hvarray[i++][1] = NULL;
    hvarray[i][0] = NULL;

    memset(boxes,0,sizeof(boxes));
    boxes[0].gd.pos.x = boxes[0].gd.pos.y = 2;
    boxes[0].gd.flags = gg_enabled|gg_visible;
    boxes[0].gd.u.boxelements = hvarray[0];
    boxes[0].creator = GHVGroupCreate;

    boxes[2].gd.flags = gg_enabled|gg_visible;
    boxes[2].gd.u.boxelements = barray;
    boxes[2].creator = GHBoxCreate;

    GGadgetsCreate(gw,boxes);
    GHVBoxSetExpandableCol(boxes[2].ret,gb_expandgluesame);
    GHVBoxSetExpandableRow(boxes[0].ret,gb_expandglue);


    GHVBoxFitWindow(boxes[0].ret);

    GDrawSetVisible(gw,true);
    while ( !done ) {
	GDrawProcessOneEvent(NULL);
	if ( done==2 ) {
	    char *str = GGadgetGetTitle8(gcd[1].ret);
	    int merge = GGadgetIsChecked(gcd[2].ret) ? mt_set :
			GGadgetIsChecked(gcd[3].ret) ? mt_merge :
			GGadgetIsChecked(gcd[4].ret) ? mt_restrict :
						       mt_and;
	    int ret = FVSelectByName(fv,str,merge);
	    free(str);
	    if ( !ret )
		done = 0;
	}
    }
    GDrawDestroyWindow(gw);
}

static void FVMenuSelectWorthOutputting(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		SCWorthOutputting(sf->glyphs[gid]) );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuGlyphsRefs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);
    int layer = fv->b.active_layer;

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		sf->glyphs[gid]->layers[layer].refs!=NULL &&
		sf->glyphs[gid]->layers[layer].splines==NULL );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuGlyphsSplines(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);
    int layer = fv->b.active_layer;

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		sf->glyphs[gid]->layers[layer].refs==NULL &&
		sf->glyphs[gid]->layers[layer].splines!=NULL );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuGlyphsBoth(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);
    int layer = fv->b.active_layer;

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		sf->glyphs[gid]->layers[layer].refs!=NULL &&
		sf->glyphs[gid]->layers[layer].splines!=NULL );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuGlyphsWhite(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);
    int layer = fv->b.active_layer;

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		sf->glyphs[gid]->layers[layer].refs==NULL &&
		sf->glyphs[gid]->layers[layer].splines==NULL );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSelectChanged(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL && sf->glyphs[gid]->changed );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }

    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSelectHintingNeeded(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int order2 = sf->layers[fv->b.active_layer].order2;
    int merge = SelMergeType(e);

    for ( i=0; i< map->enccount; ++i ) {
	doit = ( (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		((!order2 && sf->glyphs[gid]->changedsincelasthinted ) ||
		 ( order2 && sf->glyphs[gid]->layers[fv->b.active_layer].splines!=NULL &&
		     sf->glyphs[gid]->ttf_instrs_len<=0 ) ||
		 ( order2 && sf->glyphs[gid]->instructions_out_of_date )) );
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSelectAutohintable(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid, doit;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    int merge = SelMergeType(e);

    for ( i=0; i< map->enccount; ++i ) {
	doit = (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL &&
		!sf->glyphs[gid]->manualhints;
	fv->b.selected[i] = mergefunc[ merge + (fv->b.selected[i]?2:0) + doit ];
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSelectByPST(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVSelectByPST(fv);
}

static void FVMenuFindRpl(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    SVCreate(fv);
}

static void FVMenuReplaceWithRef(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVReplaceOutlineWithReference(fv,.001);
}

static void FVMenuCorrectRefs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontViewBase *fv = (FontViewBase *) GDrawGetUserData(gw);

    FVCorrectReferences(fv);
}

static void FVMenuCharInfo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int pos = FVAnyCharSelected(fv);
    if ( pos<0 )
return;
    if ( fv->b.cidmaster!=NULL &&
	    (fv->b.map->map[pos]==-1 || fv->b.sf->glyphs[fv->b.map->map[pos]]==NULL ))
return;
    SCCharInfo(SFMakeChar(fv->b.sf,fv->b.map,pos),fv->b.active_layer,fv->b.map,pos);
}

static void FVMenuBDFInfo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( fv->b.sf->bitmaps==NULL )
return;
    if ( fv->show!=fv->filled )
	SFBdfProperties(fv->b.sf,fv->b.map,fv->show);
    else
	SFBdfProperties(fv->b.sf,fv->b.map,NULL);
}

static void FVMenuBaseHoriz(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *sf = fv->b.cidmaster == NULL ? fv->b.sf : fv->b.cidmaster;
    sf->horiz_base = SFBaselines(sf,sf->horiz_base,false);
    SFBaseSort(sf);
}

static void FVMenuBaseVert(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *sf = fv->b.cidmaster == NULL ? fv->b.sf : fv->b.cidmaster;
    sf->vert_base = SFBaselines(sf,sf->vert_base,true);
    SFBaseSort(sf);
}

static void FVMenuJustify(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *sf = fv->b.cidmaster == NULL ? fv->b.sf : fv->b.cidmaster;
    JustifyDlg(sf);
}

static void FVMenuMassRename(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVMassGlyphRename(fv);
}

static void FVSetColor(FontView *fv, uint32 col) {
    int i;

    for ( i=0; i<fv->b.map->enccount; ++i ) if ( fv->b.selected[i] ) {
	SplineChar *sc = SFMakeChar(fv->b.sf,fv->b.map,i);
	sc->color = col;
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuSetColor(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    Color col = (Color) (intpt) (mi->ti.userdata);
    if ( (intpt) mi->ti.userdata == (intpt) -10 ) {
	struct hslrgb retcol, font_cols[6];
	retcol = GWidgetColor(_("Pick a color"),NULL,SFFontCols(fv->b.sf,font_cols));
	if ( !retcol.rgb )
return;
	col = (((int) rint(255.*retcol.r))<<16 ) |
		    (((int) rint(255.*retcol.g))<<8 ) |
		    (((int) rint(255.*retcol.b)) );
    }
    FVSetColor(fv,col);
}

static void FVMenuShowDependentRefs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int pos = FVAnyCharSelected(fv);
    SplineChar *sc;

    if ( pos<0 || fv->b.map->map[pos]==-1 )
return;
    sc = fv->b.sf->glyphs[fv->b.map->map[pos]];
    if ( sc==NULL || sc->dependents==NULL )
return;
    SCRefBy(sc);
}

static void FVMenuShowDependentSubs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int pos = FVAnyCharSelected(fv);
    SplineChar *sc;

    if ( pos<0 || fv->b.map->map[pos]==-1 )
return;
    sc = fv->b.sf->glyphs[fv->b.map->map[pos]];
    if ( sc==NULL )
return;
    SCSubBy(sc);
}

static int getorigin(void *UNUSED(d), BasePoint *base, int index) {
    /*FontView *fv = (FontView *) d;*/

    base->x = base->y = 0;
    switch ( index ) {
      case 0:		/* Character origin */
	/* all done */
      break;
      case 1:		/* Center of selection */
	/*CVFindCenter(cv,base,!CVAnySel(cv,NULL,NULL,NULL,NULL));*/
      break;
      default:
return( false );
    }
return( true );
}

static void FVDoTransform(FontView *fv) {
    enum transdlg_flags flags=tdf_enableback|tdf_enablekerns;
    if ( FVAnyCharSelected(fv)==-1 )
return;
    if ( FVAllSelected(fv))
	flags=tdf_enableback|tdf_enablekerns|tdf_defaultkerns;
    TransformDlgCreate(fv,FVTransFunc,getorigin,flags,cvt_none);
}

static void FVMenuTransform(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVDoTransform(fv);
}

static void FVMenuPOV(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    struct pov_data pov_data;
    if ( FVAnyCharSelected(fv)==-1 || fv->b.sf->onlybitmaps )
return;
    if ( PointOfViewDlg(&pov_data,fv->b.sf,false)==-1 )
return;
    FVPointOfView((FontViewBase *) fv,&pov_data);
}

static void FVMenuNLTransform(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( FVAnyCharSelected(fv)==-1 )
return;
    NonLinearDlg(fv,NULL);
}

static void FVMenuBitmaps(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    BitmapDlg(fv,NULL,mi->mid==MID_RemoveBitmaps?-1:(mi->mid==MID_AvailBitmaps) );
}

static void FVMenuStroke(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVStroke(fv);
}

#  ifdef FONTFORGE_CONFIG_TILEPATH
static void FVMenuTilePath(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVTile(fv);
}

static void FVMenuPatternTile(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVPatternTile(fv);
}
#endif

static void FVMenuOverlap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( fv->b.sf->onlybitmaps )
return;

    /* We know it's more likely that we'll find a problem in the overlap code */
    /*  than anywhere else, so let's save the current state against a crash */
    DoAutoSaves();

    FVOverlap(&fv->b,mi->mid==MID_RmOverlap ? over_remove :
		 mi->mid==MID_Intersection ? over_intersect :
		      over_findinter);
}

static void FVMenuInline(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    OutlineDlg(fv,NULL,NULL,true);
}

static void FVMenuOutline(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    OutlineDlg(fv,NULL,NULL,false);
}

static void FVMenuShadow(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    ShadowDlg(fv,NULL,NULL,false);
}

static void FVMenuWireframe(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    ShadowDlg(fv,NULL,NULL,true);
}

static void FVSimplify(FontView *fv,int type) {
    static struct simplifyinfo smpls[] = {
	    { sf_normal, 0, 0, 0, 0, 0, 0 },
	    { sf_normal,.75,.05,0,-1, 0, 0 },
	    { sf_normal,.75,.05,0,-1, 0, 0 }};
    struct simplifyinfo *smpl = &smpls[type+1];

    if ( smpl->linelenmax==-1 || (type==0 && !smpl->set_as_default)) {
	smpl->err = (fv->b.sf->ascent+fv->b.sf->descent)/1000.;
	smpl->linelenmax = (fv->b.sf->ascent+fv->b.sf->descent)/100.;
    }

    if ( type==1 ) {
	if ( !SimplifyDlg(fv->b.sf,smpl))
return;
	if ( smpl->set_as_default )
	    smpls[1] = *smpl;
    }
    _FVSimplify((FontViewBase *) fv,smpl);
}

static void FVMenuSimplify(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVSimplify( (FontView *) GDrawGetUserData(gw),false );
}

static void FVMenuSimplifyMore(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVSimplify( (FontView *) GDrawGetUserData(gw),true );
}

static void FVMenuCleanup(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVSimplify( (FontView *) GDrawGetUserData(gw),-1 );
}

static void FVMenuCanonicalStart(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVCanonicalStart( (FontViewBase *) GDrawGetUserData(gw) );
}

static void FVMenuCanonicalContours(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVCanonicalContours( (FontViewBase *) GDrawGetUserData(gw) );
}

static void FVMenuAddExtrema(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVAddExtrema( (FontViewBase *) GDrawGetUserData(gw) , false);
}

static void FVMenuCorrectDir(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVCorrectDir((FontViewBase *) fv);
}

static void FVMenuRound2Int(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVRound2Int( (FontViewBase *) GDrawGetUserData(gw), 1.0 );
}

static void FVMenuRound2Hundredths(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVRound2Int( (FontViewBase *) GDrawGetUserData(gw),100.0 );
}

static void FVMenuCluster(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVCluster( (FontViewBase *) GDrawGetUserData(gw));
}

static void FVMenuAutotrace(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    GCursor ct=0;

    if ( fv->v!=NULL ) {
	ct = GDrawGetCursor(fv->v);
	GDrawSetCursor(fv->v,ct_watch);
	GDrawSync(NULL);
	GDrawProcessPendingEvents(NULL);
    }
    FVAutoTrace(&fv->b,e!=NULL && (e->u.mouse.state&ksm_shift));
    if ( fv->v!=NULL )
	GDrawSetCursor(fv->v,ct);
}

static void FVMenuBuildAccent(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVBuildAccent( (FontViewBase *) GDrawGetUserData(gw), true );
}

static void FVMenuBuildComposite(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVBuildAccent( (FontViewBase *) GDrawGetUserData(gw), false );
}

static void FVMenuBuildDuplicate(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FVBuildDuplicate( (FontViewBase *) GDrawGetUserData(gw));
}

#ifdef KOREAN
static void FVMenuShowGroup(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    ShowGroup( ((FontView *) GDrawGetUserData(gw))->sf );
}
#endif

#if HANYANG
static void FVMenuModifyComposition(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( fv->b.sf->rules!=NULL )
	SFModifyComposition(fv->b.sf);
}

static void FVMenuBuildSyllables(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    if ( fv->b.sf->rules!=NULL )
	SFBuildSyllables(fv->b.sf);
}
#endif

static void FVMenuCompareFonts(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FontCompareDlg(fv);
}

static void FVMenuMergeFonts(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVMergeFonts(fv);
}

static void FVMenuInterpFonts(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVInterpolateFonts(fv);
}

static void FVShowInfo(FontView *fv);

void FVChangeChar(FontView *fv,int i) {

    if ( i!=-1 ) {
	FVDeselectAll(fv);
	fv->b.selected[i] = true;
	fv->sel_index = 1;
	fv->end_pos = fv->pressed_pos = i;
	FVToggleCharSelected(fv,i);
	FVScrollToChar(fv,i);
	FVShowInfo(fv);
    }
}

void FVScrollToChar(FontView *fv,int i) {

    if ( fv->v==NULL || fv->colcnt==0 )	/* Can happen in scripts */
return;

    if ( i!=-1 ) {
	if ( i/fv->colcnt<fv->rowoff || i/fv->colcnt >= fv->rowoff+fv->rowcnt ) {
	    fv->rowoff = i/fv->colcnt;
	    if ( fv->rowcnt>= 3 )
		--fv->rowoff;
	    if ( fv->rowoff+fv->rowcnt>=fv->rowltot )
		fv->rowoff = fv->rowltot-fv->rowcnt;
	    if ( fv->rowoff<0 ) fv->rowoff = 0;
	    GScrollBarSetPos(fv->vsb,fv->rowoff);
	    GDrawRequestExpose(fv->v,NULL,false);
	}
    }
}

static void FVScrollToGID(FontView *fv,int gid) {
    FVScrollToChar(fv,fv->b.map->backmap[gid]);
}

static void FV_ChangeGID(FontView *fv,int gid) {
    FVChangeChar(fv,fv->b.map->backmap[gid]);
}

static void _FVMenuChangeChar(FontView *fv,int mid ) {
    SplineFont *sf = fv->b.sf;
    EncMap *map = fv->b.map;
    int pos = FVAnyCharSelected(fv);

    if ( pos>=0 ) {
	if ( mid==MID_Next )
	    ++pos;
	else if ( mid==MID_Prev )
	    --pos;
	else if ( mid==MID_NextDef ) {
	    for ( ++pos; pos<map->enccount &&
		    (map->map[pos]==-1 || !SCWorthOutputting(sf->glyphs[map->map[pos]]) ||
			(fv->show!=fv->filled && fv->show->glyphs[map->map[pos]]==NULL ));
		    ++pos );
	    if ( pos>=map->enccount ) {
		int selpos = FVAnyCharSelected(fv);
		char *iconv_name = map->enc->iconv_name ? map->enc->iconv_name :
			map->enc->enc_name;
		if ( strstr(iconv_name,"2022")!=NULL && selpos<0x2121 )
		    pos = 0x2121;
		else if ( strstr(iconv_name,"EUC")!=NULL && selpos<0xa1a1 )
		    pos = 0xa1a1;
		else if ( map->enc->is_tradchinese ) {
		    if ( strstrmatch(map->enc->enc_name,"HK")!=NULL &&
			    selpos<0x8140 )
			pos = 0x8140;
		    else
			pos = 0xa140;
		} else if ( map->enc->is_japanese ) {
		    if ( strstrmatch(iconv_name,"SJIS")!=NULL ||
			    (strstrmatch(iconv_name,"JIS")!=NULL && strstrmatch(iconv_name,"SHIFT")!=NULL )) {
			if ( selpos<0x8100 )
			    pos = 0x8100;
			else if ( selpos<0xb000 )
			    pos = 0xb000;
		    }
		} else if ( map->enc->is_korean ) {
		    if ( strstrmatch(iconv_name,"JOHAB")!=NULL ) {
			if ( selpos<0x8431 )
			    pos = 0x8431;
		    } else {	/* Wansung, EUC-KR */
			if ( selpos<0xa1a1 )
			    pos = 0xa1a1;
		    }
		} else if ( map->enc->is_simplechinese ) {
		    if ( strmatch(iconv_name,"EUC-CN")==0 && selpos<0xa1a1 )
			pos = 0xa1a1;
		}
		if ( pos>=map->enccount )
return;
	    }
	} else if ( mid==MID_PrevDef ) {
	    for ( --pos; pos>=0 &&
		    (map->map[pos]==-1 || !SCWorthOutputting(sf->glyphs[map->map[pos]]) ||
			(fv->show!=fv->filled && fv->show->glyphs[map->map[pos]]==NULL ));
		    --pos );
	    if ( pos<0 )
return;
	}
    }
    if ( pos<0 ) pos = map->enccount-1;
    else if ( pos>= map->enccount ) pos = 0;
    if ( pos>=0 && pos<map->enccount )
	FVChangeChar(fv,pos);
}

static void FVMenuChangeChar(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    _FVMenuChangeChar(fv,mi->mid);
}

static void FVShowSubFont(FontView *fv,SplineFont *new) {
    MetricsView *mv, *mvnext;
    BDFFont *newbdf;
    int wascompact = fv->b.normal!=NULL;
    extern int use_freetype_to_rasterize_fv;

    for ( mv=fv->b.sf->metrics; mv!=NULL; mv = mvnext ) {
	/* Don't bother trying to fix up metrics views, just not worth it */
	mvnext = mv->next;
	GDrawDestroyWindow(mv->gw);
    }
    if ( wascompact ) {
	EncMapFree(fv->b.map);
	if (fv->b.map == fv->b.sf->map) { fv->b.sf->map = fv->b.normal; }
	fv->b.map = fv->b.normal;
	fv->b.normal = NULL;
	fv->b.selected = realloc(fv->b.selected,fv->b.map->enccount);
	memset(fv->b.selected,0,fv->b.map->enccount);
    }
    CIDSetEncMap((FontViewBase *) fv,new);
    if ( wascompact ) {
	fv->b.normal = EncMapCopy(fv->b.map);
	CompactEncMap(fv->b.map,fv->b.sf);
	FontViewReformatOne(&fv->b);
	FVSetTitle(&fv->b);
    }
    newbdf = SplineFontPieceMeal(fv->b.sf,fv->b.active_layer,fv->filled->pixelsize,72,
	    (fv->antialias?pf_antialias:0)|(fv->bbsized?pf_bbsized:0)|
		(use_freetype_to_rasterize_fv && !fv->b.sf->strokedfont && !fv->b.sf->multilayer?pf_ft_nohints:0),
	    NULL);
    BDFFontFree(fv->filled);
    if ( fv->filled == fv->show )
	fv->show = newbdf;
    fv->filled = newbdf;
    GDrawRequestExpose(fv->v,NULL,true);
}

static void FVMenuGotoChar(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int merge_with_selection = false;
    int pos = GotoChar(fv->b.sf,fv->b.map,&merge_with_selection);
    if ( fv->b.cidmaster!=NULL && pos!=-1 && !fv->b.map->enc->is_compact ) {
	SplineFont *cidmaster = fv->b.cidmaster;
	int k, hadk= cidmaster->subfontcnt;
	for ( k=0; k<cidmaster->subfontcnt; ++k ) {
	    SplineFont *sf = cidmaster->subfonts[k];
	    if ( pos<sf->glyphcnt && sf->glyphs[pos]!=NULL )
	break;
	    if ( pos<sf->glyphcnt )
		hadk = k;
	}
	if ( k==cidmaster->subfontcnt && pos>=fv->b.sf->glyphcnt )
	    k = hadk;
	if ( k!=cidmaster->subfontcnt && cidmaster->subfonts[k] != fv->b.sf )
	    FVShowSubFont(fv,cidmaster->subfonts[k]);
	if ( pos>=fv->b.sf->glyphcnt )
	    pos = -1;
    }
    if ( !merge_with_selection )
	FVChangeChar(fv,pos);
    else {
	if ( !fv->b.selected[pos] ) {
	    fv->b.selected[pos] = ++fv->sel_index;
	    FVToggleCharSelected(fv,pos);
	}
	fv->end_pos = fv->pressed_pos = pos;
	FVScrollToChar(fv,pos);
	FVShowInfo(fv);
    }
}

static void FVMenuLigatures(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFShowLigatures(fv->b.sf,NULL);
}

static void FVMenuKernPairs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFKernClassTempDecompose(fv->b.sf,false);
    SFShowKernPairs(fv->b.sf,NULL,NULL,fv->b.active_layer);
    SFKernCleanup(fv->b.sf,false);
}

static void FVMenuAnchorPairs(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFShowKernPairs(fv->b.sf,NULL,mi->ti.userdata,fv->b.active_layer);
}

static void FVMenuShowAtt(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    ShowAtt(fv->b.sf,fv->b.active_layer);
}

static void FVMenuDisplaySubs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( fv->cur_subtable!=0 ) {
	fv->cur_subtable = NULL;
    } else {
	SplineFont *sf = fv->b.sf;
	OTLookup *otf;
	struct lookup_subtable *sub;
	int cnt, k;
	char **names = NULL;
	if ( sf->cidmaster ) sf=sf->cidmaster;
	for ( k=0; k<2; ++k ) {
	    cnt = 0;
	    for ( otf = sf->gsub_lookups; otf!=NULL; otf=otf->next ) {
		if ( otf->lookup_type==gsub_single ) {
		    for ( sub=otf->subtables; sub!=NULL; sub=sub->next ) {
			if ( names )
			    names[cnt] = sub->subtable_name;
			++cnt;
		    }
		}
	    }
	    if ( cnt==0 )
	break;
	    if ( names==NULL )
		names = malloc((cnt+3) * sizeof(char *));
	    else {
		names[cnt++] = "-";
		names[cnt++] = _("New Lookup Subtable...");
		names[cnt] = NULL;
	    }
	}
	sub = NULL;
	if ( names!=NULL ) {
	    int ret = gwwv_choose(_("Display Substitution..."), (const char **) names, cnt, 0,
		    _("Pick a substitution to display in the window."));
	    if ( ret!=-1 )
		sub = SFFindLookupSubtable(sf,names[ret]);
	    free(names);
	    if ( ret==-1 )
return;
	}
	if ( sub==NULL )
	    sub = SFNewLookupSubtableOfType(sf,gsub_single,NULL,fv->b.active_layer);
	if ( sub!=NULL )
	    fv->cur_subtable = sub;
    }
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVChangeDisplayFont(FontView *fv,BDFFont *bdf) {
    int samesize=0;
    int rcnt, ccnt;
    int oldr, oldc;
    int first_time = fv->show==NULL;

    if ( fv->v==NULL )			/* Can happen in scripts */
return;

    if ( fv->show!=bdf ) {
	oldc = fv->cbw*fv->colcnt;
	oldr = fv->cbh*fv->rowcnt;

	fv->show = bdf;
	fv->b.active_bitmap = bdf==fv->filled ? NULL : bdf;
	if ( fv->user_requested_magnify!=-1 )
	    fv->magnify=fv->user_requested_magnify;
	else if ( bdf->pixelsize<20 ) {
	    if ( bdf->pixelsize<=9 )
		fv->magnify = 3;
	    else
		fv->magnify = 2;
	    samesize = ( fv->show && fv->cbw == (bdf->pixelsize*fv->magnify)+1 );
	} else
	    fv->magnify = 1;
	if ( !first_time && fv->cbw == fv->magnify*bdf->pixelsize+1 )
	    samesize = true;
	fv->cbw = (bdf->pixelsize*fv->magnify)+1;
	fv->cbh = (bdf->pixelsize*fv->magnify)+1+fv->lab_height+1;
	fv->resize_expected = !samesize;
	ccnt = fv->b.sf->desired_col_cnt;
	rcnt = fv->b.sf->desired_row_cnt;
	if ((( bdf->pixelsize<=fv->b.sf->display_size || bdf->pixelsize<=-fv->b.sf->display_size ) &&
		 fv->b.sf->top_enc!=-1 /* Not defaulting */ ) ||
		bdf->pixelsize<=48 ) {
	    /* use the desired sizes */
	} else {
	    if ( bdf->pixelsize>48 ) {
		ccnt = 8;
		rcnt = 2;
	    } else if ( bdf->pixelsize>=96 ) {
		ccnt = 4;
		rcnt = 1;
	    }
	    if ( !first_time ) {
		if ( ccnt < oldc/fv->cbw )
		    ccnt = oldc/fv->cbw;
		if ( rcnt < oldr/fv->cbh )
		    rcnt = oldr/fv->cbh;
	    }
	}
	if ( samesize ) {
	    GDrawRequestExpose(fv->v,NULL,false);
	} else if ( fv->b.container!=NULL && fv->b.container->funcs->doResize!=NULL ) {
	    (fv->b.container->funcs->doResize)(fv->b.container,&fv->b,
		    ccnt*fv->cbw+1+GDrawPointsToPixels(fv->gw,_GScrollBar_Width),
		    rcnt*fv->cbh+1+fv->mbh+fv->infoh);
	} else {
	    GDrawResize(fv->gw,
		    ccnt*fv->cbw+1+GDrawPointsToPixels(fv->gw,_GScrollBar_Width),
		    rcnt*fv->cbh+1+fv->mbh+fv->infoh);
	}
    }
}

struct md_data {
    int done;
    int ish;
    FontView *fv;
};

static int md_e_h(GWindow gw, GEvent *e) {
    if ( e->type==et_close ) {
	struct md_data *d = GDrawGetUserData(gw);
	d->done = true;
    } else if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	struct md_data *d = GDrawGetUserData(gw);
	static int masks[] = { fvm_baseline, fvm_origin, fvm_advanceat, fvm_advanceto, -1 };
	int i, metrics;
	if ( GGadgetGetCid(e->u.control.g)==10 ) {
	    metrics = 0;
	    for ( i=0; masks[i]!=-1 ; ++i )
		if ( GGadgetIsChecked(GWidgetGetControl(gw,masks[i])))
		    metrics |= masks[i];
	    if ( d->ish )
		default_fv_showhmetrics = d->fv->showhmetrics = metrics;
	    else
		default_fv_showvmetrics = d->fv->showvmetrics = metrics;
	}
	d->done = true;
    } else if ( e->type==et_char ) {
return( false );
    }
return( true );
}

static void FVMenuShowMetrics(GWindow fvgw,struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(fvgw);
    GRect pos;
    GWindow gw;
    GWindowAttrs wattrs;
    struct md_data d;
    GGadgetCreateData gcd[7];
    GTextInfo label[6];
    int metrics = mi->mid==MID_ShowHMetrics ? fv->showhmetrics : fv->showvmetrics;

    d.fv = fv;
    d.done = 0;
    d.ish = mi->mid==MID_ShowHMetrics;

    memset(&wattrs,0,sizeof(wattrs));
    wattrs.mask = wam_events|wam_cursor|wam_utf8_wtitle|wam_undercursor|wam_restrict;
    wattrs.event_masks = ~(1<<et_charup);
    wattrs.restrict_input_to_me = 1;
    wattrs.undercursor = 1;
    wattrs.cursor = ct_pointer;
    wattrs.utf8_window_title = d.ish?_("Show H. Metrics"):_("Show V. Metrics");
    pos.x = pos.y = 0;
    pos.width =GDrawPointsToPixels(NULL,GGadgetScale(170));
    pos.height = GDrawPointsToPixels(NULL,130);
    gw = GDrawCreateTopWindow(NULL,&pos,md_e_h,&d,&wattrs);

    memset(&label,0,sizeof(label));
    memset(&gcd,0,sizeof(gcd));

    label[0].text = (unichar_t *) _("Baseline");
    label[0].text_is_1byte = true;
    gcd[0].gd.label = &label[0];
    gcd[0].gd.pos.x = 8; gcd[0].gd.pos.y = 8;
    gcd[0].gd.flags = gg_enabled|gg_visible|(metrics&fvm_baseline?gg_cb_on:0);
    gcd[0].gd.cid = fvm_baseline;
    gcd[0].creator = GCheckBoxCreate;

    label[1].text = (unichar_t *) _("Origin");
    label[1].text_is_1byte = true;
    gcd[1].gd.label = &label[1];
    gcd[1].gd.pos.x = 8; gcd[1].gd.pos.y = gcd[0].gd.pos.y+16;
    gcd[1].gd.flags = gg_enabled|gg_visible|(metrics&fvm_origin?gg_cb_on:0);
    gcd[1].gd.cid = fvm_origin;
    gcd[1].creator = GCheckBoxCreate;

    label[2].text = (unichar_t *) _("Advance Width as a Line");
    label[2].text_is_1byte = true;
    gcd[2].gd.label = &label[2];
    gcd[2].gd.pos.x = 8; gcd[2].gd.pos.y = gcd[1].gd.pos.y+16;
    gcd[2].gd.flags = gg_enabled|gg_visible|gg_utf8_popup|(metrics&fvm_advanceat?gg_cb_on:0);
    gcd[2].gd.cid = fvm_advanceat;
    gcd[2].gd.popup_msg = (unichar_t *) _("Display the advance width as a line\nperpendicular to the advance direction");
    gcd[2].creator = GCheckBoxCreate;

    label[3].text = (unichar_t *) _("Advance Width as a Bar");
    label[3].text_is_1byte = true;
    gcd[3].gd.label = &label[3];
    gcd[3].gd.pos.x = 8; gcd[3].gd.pos.y = gcd[2].gd.pos.y+16;
    gcd[3].gd.flags = gg_enabled|gg_visible|gg_utf8_popup|(metrics&fvm_advanceto?gg_cb_on:0);
    gcd[3].gd.cid = fvm_advanceto;
    gcd[3].gd.popup_msg = (unichar_t *) _("Display the advance width as a bar under the glyph\nshowing the extent of the advance");
    gcd[3].creator = GCheckBoxCreate;

    label[4].text = (unichar_t *) _("_OK");
    label[4].text_is_1byte = true;
    label[4].text_in_resource = true;
    gcd[4].gd.label = &label[4];
    gcd[4].gd.pos.x = 20-3; gcd[4].gd.pos.y = GDrawPixelsToPoints(NULL,pos.height)-35-3;
    gcd[4].gd.pos.width = -1; gcd[4].gd.pos.height = 0;
    gcd[4].gd.flags = gg_visible | gg_enabled | gg_but_default;
    gcd[4].gd.cid = 10;
    gcd[4].creator = GButtonCreate;

    label[5].text = (unichar_t *) _("_Cancel");
    label[5].text_is_1byte = true;
    label[5].text_in_resource = true;
    gcd[5].gd.label = &label[5];
    gcd[5].gd.pos.x = -20; gcd[5].gd.pos.y = gcd[4].gd.pos.y+3;
    gcd[5].gd.pos.width = -1; gcd[5].gd.pos.height = 0;
    gcd[5].gd.flags = gg_visible | gg_enabled | gg_but_cancel;
    gcd[5].creator = GButtonCreate;

    GGadgetsCreate(gw,gcd);

    GDrawSetVisible(gw,true);
    while ( !d.done )
	GDrawProcessOneEvent(NULL);
    GDrawDestroyWindow(gw);

    SavePrefs(true);
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FV_ChangeDisplayBitmap(FontView *fv,BDFFont *bdf) {
    FVChangeDisplayFont(fv,bdf);
    if (fv->show != NULL) {
        fv->b.sf->display_size = fv->show->pixelsize;
    } else {
        fv->b.sf->display_size = 1;
    }
}

static void FVMenuSize(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	int dspsize = fv->filled->pixelsize;
	int changedmodifier = false;
	extern int use_freetype_to_rasterize_fv;

	fv->magnify = 1;
	fv->user_requested_magnify = -1;
	if ( mi->mid == MID_24 )
	{
		default_fv_font_size = dspsize = 24;
	}
	else if ( mi->mid == MID_36 )
	{
		default_fv_font_size = dspsize = 36;
	}
	else if ( mi->mid == MID_48 )
	{
		default_fv_font_size = dspsize = 48;
	}
	else if ( mi->mid == MID_72 )
	{
		default_fv_font_size = dspsize = 72;
	}
	else if ( mi->mid == MID_96 )
	{
		default_fv_font_size = dspsize = 96;
	}
	else if ( mi->mid == MID_128 )
	{
		default_fv_font_size = dspsize = 128;
	}
	else if ( mi->mid == MID_FitToBbox )
	{
		default_fv_bbsized = fv->bbsized = !fv->bbsized;
		fv->b.sf->display_bbsized = fv->bbsized;
		changedmodifier = true;
	}
	else
	{
		default_fv_antialias = fv->antialias = !fv->antialias;
		fv->b.sf->display_antialias = fv->antialias;
		changedmodifier = true;
	}

	SavePrefs(true);
	if ( fv->filled!=fv->show || fv->filled->pixelsize != dspsize || changedmodifier )
	{
		BDFFont *new, *old;
		old = fv->filled;
		new = SplineFontPieceMeal(fv->b.sf,fv->b.active_layer,dspsize,72, (fv->antialias?pf_antialias:0)|(fv->bbsized?pf_bbsized:0)| (use_freetype_to_rasterize_fv && !fv->b.sf->strokedfont && !fv->b.sf->multilayer?pf_ft_nohints:0), NULL);
		fv->filled = new;
		FVChangeDisplayFont(fv,new);
		BDFFontFree(old);
		fv->b.sf->display_size = -dspsize;
		if ( fv->b.cidmaster!=NULL )
		{
			int i;
			for ( i=0; i<fv->b.cidmaster->subfontcnt; ++i )
			{
				fv->b.cidmaster->subfonts[i]->display_size = -dspsize;
			}
		}
	}
}

void FVSetUIToMatch(FontView *destfv,FontView *srcfv) {
    extern int use_freetype_to_rasterize_fv;

    if ( destfv->filled==NULL || srcfv->filled==NULL )
return;
    if ( destfv->magnify!=srcfv->magnify ||
	    destfv->user_requested_magnify!=srcfv->user_requested_magnify ||
	    destfv->bbsized!=srcfv->bbsized ||
	    destfv->antialias!=srcfv->antialias ||
	    destfv->filled->pixelsize != srcfv->filled->pixelsize ) {
	BDFFont *new, *old;
	destfv->magnify = srcfv->magnify;
	destfv->user_requested_magnify = srcfv->user_requested_magnify;
	destfv->bbsized = srcfv->bbsized;
	destfv->antialias = srcfv->antialias;
	old = destfv->filled;
	new = SplineFontPieceMeal(destfv->b.sf,destfv->b.active_layer,srcfv->filled->pixelsize,72,
	    (destfv->antialias?pf_antialias:0)|(destfv->bbsized?pf_bbsized:0)|
		(use_freetype_to_rasterize_fv && !destfv->b.sf->strokedfont && !destfv->b.sf->multilayer?pf_ft_nohints:0),
	    NULL);
	destfv->filled = new;
	FVChangeDisplayFont(destfv,new);
	BDFFontFree(old);
    }
}

static void FV_LayerChanged( FontView *fv ) {
    extern int use_freetype_to_rasterize_fv;
    BDFFont *new, *old;

    fv->magnify = 1;
    fv->user_requested_magnify = -1;

    old = fv->filled;
    new = SplineFontPieceMeal(fv->b.sf,fv->b.active_layer,fv->filled->pixelsize,72,
	(fv->antialias?pf_antialias:0)|(fv->bbsized?pf_bbsized:0)|
	    (use_freetype_to_rasterize_fv && !fv->b.sf->strokedfont && !fv->b.sf->multilayer?pf_ft_nohints:0),
	NULL);
    fv->filled = new;
    FVChangeDisplayFont(fv,new);
    fv->b.sf->display_size = -fv->filled->pixelsize;
    BDFFontFree(old);
}

static void FVMenuChangeLayer(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    fv->b.active_layer = mi->mid;
    fv->b.sf->display_layer = mi->mid;
    FV_LayerChanged(fv);
}

static void FVMenuMagnify(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int magnify = fv->user_requested_magnify!=-1 ? fv->user_requested_magnify : fv->magnify;
    char def[20], *end, *ret;
    int val;
    BDFFont *show = fv->show;

    sprintf( def, "%d", magnify );
    ret = gwwv_ask_string(_("Bitmap Magnification..."),def,_("Please specify a bitmap magnification factor."));
    if ( ret==NULL )
return;
    val = strtol(ret,&end,10);
    if ( val<1 || val>5 || *end!='\0' )
	ff_post_error( _("Bad Number"), _("Bad Number") );
    else {
	fv->user_requested_magnify = val;
	fv->show = fv->filled;
	fv->b.active_bitmap = NULL;
	FVChangeDisplayFont(fv,show);
    }
    free(ret);
}

static void FVMenuWSize(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int h,v;
    extern int default_fv_col_count, default_fv_row_count;

    if ( mi->mid == MID_32x8 ) {
	h = 32; v=8;
    } else if ( mi->mid == MID_16x4 ) {
	h = 16; v=4;
    } else {
	h = 8; v=2;
    }
    GDrawResize(fv->gw,
	    h*fv->cbw+1+GDrawPointsToPixels(fv->gw,_GScrollBar_Width),
	    v*fv->cbh+1+fv->mbh+fv->infoh);
    fv->b.sf->desired_col_cnt = default_fv_col_count = h;
    fv->b.sf->desired_row_cnt = default_fv_row_count = v;

    SavePrefs(true);
}

static void FVMenuGlyphLabel(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    default_fv_glyphlabel = fv->glyphlabel = mi->mid;

    GDrawRequestExpose(fv->v,NULL,false);

    SavePrefs(true);
}

static void FVMenuShowBitmap(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    BDFFont *bdf = mi->ti.userdata;

    FV_ChangeDisplayBitmap(fv,bdf);		/* Let's not change any of the others */
}

static void FV_ShowFilled(FontView *fv) {

    fv->magnify = 1;
    fv->user_requested_magnify = 1;
    if ( fv->show!=fv->filled )
	FVChangeDisplayFont(fv,fv->filled);
    fv->b.sf->display_size = -fv->filled->pixelsize;
    fv->b.active_bitmap = NULL;
}

static void FVMenuCenter(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontViewBase *fv = (FontViewBase *) GDrawGetUserData(gw);
    FVMetricsCenter(fv,mi->mid==MID_Center);
}

static void FVMenuSetWidth(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( FVAnyCharSelected(fv)==-1 )
return;
    if ( mi->mid == MID_SetVWidth && !fv->b.sf->hasvmetrics )
return;
    FVSetWidth(fv,mi->mid==MID_SetWidth   ?wt_width:
		  mi->mid==MID_SetLBearing?wt_lbearing:
		  mi->mid==MID_SetRBearing?wt_rbearing:
		  mi->mid==MID_SetBearings?wt_bearings:
		  wt_vwidth);
}

static void FVMenuAutoWidth(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVAutoWidth2(fv);
}

static void FVMenuKernByClasses(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    ShowKernClasses(fv->b.sf,NULL,fv->b.active_layer,false);
}

static void FVMenuVKernByClasses(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    ShowKernClasses(fv->b.sf,NULL,fv->b.active_layer,true);
}

static void FVMenuRemoveKern(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVRemoveKerns(&fv->b);
}

static void FVMenuRemoveVKern(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVRemoveVKerns(&fv->b);
}

static void FVMenuKPCloseup(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i;

    for ( i=0; i<fv->b.map->enccount; ++i )
	if ( fv->b.selected[i] )
    break;
    KernPairD(fv->b.sf,i==fv->b.map->enccount?NULL:
		    fv->b.map->map[i]==-1?NULL:
		    fv->b.sf->glyphs[fv->b.map->map[i]],NULL,fv->b.active_layer,
		    false);
}

static void FVMenuVKernFromHKern(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    FVVKernFromHKern(&fv->b);
}

static void FVMenuAutoHint(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

#if 0	//def CKS	// HYMODIFY :: 2016.02.04
	struct timeval start, stop, running_time;
	double operating_time;

	gettimeofday( &start, NULL );
#endif

    FVAutoHint( &fv->b );

#if 0	//def CKS	// HYMODIFY :: 2016.02.04
	gettimeofday( &stop, NULL );

	running_time.tv_sec = stop.tv_sec - start.tv_sec;
	running_time.tv_usec = stop.tv_usec - start.tv_usec;

	if( running_time.tv_usec < 0 ) 
	{
		running_time.tv_sec -= 1;
		running_time.tv_usec += 1000000;
	}
	operating_time = (double)(stop.tv_sec) + (double)(stop.tv_usec) / 1000000.0 - (double)(start.tv_sec) - (double)(start.tv_usec) / 1000000.0;

	ff_post_notice(_("Auto Hint Time Check"),_("Operating time is %f sec"), operating_time );
#endif
}

static void FVMenuAutoHintSubs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVAutoHintSubs( &fv->b );
}

static void FVMenuAutoCounter(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVAutoCounter( &fv->b );
}

static void FVMenuDontAutoHint(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVDontAutoHint( &fv->b );
}

static void FVMenuDeltas(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( !hasFreeTypeDebugger())
return;
    DeltaSuggestionDlg(fv,NULL);
}

static void FVMenuAutoInstr(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

#if 0	//def CKS	// HYMODIFY :: 2016.02.04
	struct timeval start, stop, running_time;
	double operating_time;

	gettimeofday( &start, NULL );
#endif

    FVAutoInstr( &fv->b );

#if 0	//def CKS	// HYMODIFY :: 2016.02.04
	gettimeofday( &stop, NULL );

	running_time.tv_sec = stop.tv_sec - start.tv_sec;
	running_time.tv_usec = stop.tv_usec - start.tv_usec;

	if( running_time.tv_usec < 0 ) 
	{
		running_time.tv_sec -= 1;
		running_time.tv_usec += 1000000;
	}
	operating_time = (double)(stop.tv_sec) + (double)(stop.tv_usec) / 1000000.0 - (double)(start.tv_sec) - (double)(start.tv_usec) / 1000000.0;

	ff_post_notice(_("Auto Instr Time Check"),_("Operating time is %f sec"), operating_time );
#endif
}

static void FVMenuEditInstrs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int index = FVAnyCharSelected(fv);
    SplineChar *sc;
    if ( index<0 )
return;
    sc = SFMakeChar(fv->b.sf,fv->b.map,index);
    SCEditInstructions(sc);
}

static void FVMenuEditTable(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFEditTable(fv->b.sf,
	    mi->mid==MID_Editprep?CHR('p','r','e','p'):
	    mi->mid==MID_Editfpgm?CHR('f','p','g','m'):
	    mi->mid==MID_Editmaxp?CHR('m','a','x','p'):
				  CHR('c','v','t',' '));
}

static void FVMenuRmInstrTables(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    TtfTablesFree(fv->b.sf->ttf_tables);
    fv->b.sf->ttf_tables = NULL;
    if ( !fv->b.sf->changed ) {
	fv->b.sf->changed = true;
	FVSetTitles(fv->b.sf);
    }
}

static void FVMenuClearInstrs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVClearInstrs(&fv->b);
}

static void FVMenuClearHints(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVClearHints(&fv->b);
}

static void FVMenuHistograms(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SFHistogram(fv->b.sf, fv->b.active_layer, NULL,
			FVAnyCharSelected(fv)!=-1?fv->b.selected:NULL,
			fv->b.map,
			mi->mid==MID_HStemHist ? hist_hstem :
			mi->mid==MID_VStemHist ? hist_vstem :
				hist_blues);
}


static void FontViewSetTitle(FontView *fv)
{
	unichar_t *title, *ititle, *temp;
	char *file=NULL;
	char *enc;
	int len;

	if ( fv->gw==NULL )	/* In scripting */
	{
		return;
	}

	const char* collabStateString = "";
	if( collabclient_inSessionFV( &fv->b ))
	{
		printf("collabclient_getState( fv ) %d %d\n", fv->b.collabState, collabclient_getState( &fv->b ));
		collabStateString = collabclient_stateToString(collabclient_getState( &fv->b ));
	}

	enc = SFEncodingName(fv->b.sf,fv->b.normal?fv->b.normal:fv->b.map);
	len = strlen(fv->b.sf->fontname)+1 + strlen(enc)+6;
	if ( fv->b.normal )
	{
		len += strlen(_("Compact"))+1;
	}
	if ( fv->b.cidmaster!=NULL )
	{
		if ( (file = fv->b.cidmaster->filename)==NULL )
		{
			file = fv->b.cidmaster->origname;
		}
	}
	else
	{
		if ( (file = fv->b.sf->filename)==NULL )
		{
			file = fv->b.sf->origname;
		}
	}
	len += strlen(collabStateString);
	if ( file!=NULL )
	{
		len += 2+strlen(file);
	}
	title = malloc((len+1)*sizeof(unichar_t));
	uc_strcpy(title,"");

	if(*collabStateString)
	{
		uc_strcat(title, collabStateString);
		uc_strcat(title, " - ");
	}
	uc_strcat(title,fv->b.sf->fontname);
	if ( fv->b.sf->changed )
	{
		uc_strcat(title,"*");
	}
	if ( file!=NULL )
	{
		uc_strcat(title,"  ");
		temp = def2u_copy(GFileNameTail(file));
		u_strcat(title,temp);
		free(temp);
	}
	uc_strcat(title, " (" );
	if ( fv->b.normal )
	{
		utf82u_strcat(title,_("Compact"));
		uc_strcat(title," ");
	}
	uc_strcat(title,enc);
	uc_strcat(title, ")" );
	free(enc);

	ititle = uc_copy(fv->b.sf->fontname);
	GDrawSetWindowTitles(fv->gw,title,ititle);
	free(title);
	free(ititle);
}

void FVTitleUpdate(FontViewBase *fv)
{
    FontViewSetTitle( (FontView*)fv );
}

static void FontViewSetTitles(SplineFont *sf) {
    FontView *fv;

    for ( fv = (FontView *) (sf->fv); fv!=NULL; fv=(FontView *) (fv->b.nextsame))
	FontViewSetTitle(fv);
}

static void FVMenuShowSubFont(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *new = mi->ti.userdata;
    FVShowSubFont(fv,new);
}

static void FVMenuConvert2CID(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;
    struct cidmap *cidmap;

    if ( cidmaster!=NULL )
return;
    SFFindNearTop(fv->b.sf);
    cidmap = AskUserForCIDMap();
    if ( cidmap==NULL )
return;
    MakeCIDMaster(fv->b.sf,fv->b.map,false,NULL,cidmap);
    SFRestoreNearTop(fv->b.sf);
}

static enum fchooserret CMapFilter(GGadget *g,GDirEntry *ent,
	const unichar_t *dir) {
    enum fchooserret ret = GFileChooserDefFilter(g,ent,dir);
    char buf2[256];
    FILE *file;
    static char *cmapflag = "%!PS-Adobe-3.0 Resource-CMap";

    if ( ret==fc_show && !ent->isdir ) {
	int len = 3*(u_strlen(dir)+u_strlen(ent->name)+5);
	char *filename = malloc(len);
	u2def_strncpy(filename,dir,len);
	strcat(filename,"/");
	u2def_strncpy(buf2,ent->name,sizeof(buf2));
	strcat(filename,buf2);
	file = fopen(filename,"r");
	if ( file==NULL )
	    ret = fc_hide;
	else {
	    if ( fgets(buf2,sizeof(buf2),file)==NULL ||
		    strncmp(buf2,cmapflag,strlen(cmapflag))!=0 )
		ret = fc_hide;
	    fclose(file);
	}
	free(filename);
    }
return( ret );
}

static void FVMenuConvertByCMap(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;
    char *cmapfilename;

    if ( cidmaster!=NULL )
return;
    cmapfilename = gwwv_open_filename(_("Find an adobe CMap file..."),NULL,NULL,CMapFilter);
    if ( cmapfilename==NULL )
return;
    MakeCIDMaster(fv->b.sf,fv->b.map,true,cmapfilename,NULL);
    free(cmapfilename);
}

static void FVMenuFlatten(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;

    if ( cidmaster==NULL )
return;
    SFFlatten(cidmaster);
}

static void FVMenuFlattenByCMap(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;
    char *cmapname;

    if ( cidmaster==NULL )
return;
    cmapname = gwwv_open_filename(_("Find an adobe CMap file..."),NULL,NULL,CMapFilter);
    if ( cmapname==NULL )
return;
    SFFindNearTop(fv->b.sf);
    SFFlattenByCMap(cidmaster,cmapname);
    SFRestoreNearTop(fv->b.sf);
    free(cmapname);
}

static void FVMenuInsertFont(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;
    SplineFont *new;
    struct cidmap *map;
    char *filename;
    extern NameList *force_names_when_opening;

    if ( cidmaster==NULL || cidmaster->subfontcnt>=255 )	/* Open type allows 1 byte to specify the fdselect */
return;

    filename = GetPostScriptFontName(NULL,false);
    if ( filename==NULL )
return;
    new = LoadSplineFont(filename,0);
    free(filename);
    if ( new==NULL )
return;
    if ( new->fv == &fv->b )		/* Already part of us */
return;
    if ( new->fv != NULL ) {
	if ( ((FontView *) (new->fv))->gw!=NULL )
	    GDrawRaise( ((FontView *) (new->fv))->gw);
	ff_post_error( _("Please close font"), _("Please close %s before inserting it into a CID font"),new->origname);
return;
    }
    EncMapFree(new->map);
    if ( force_names_when_opening!=NULL )
	SFRenameGlyphsToNamelist(new,force_names_when_opening );

    map = FindCidMap(cidmaster->cidregistry,cidmaster->ordering,cidmaster->supplement,cidmaster);
    SFEncodeToMap(new,map);
    if ( !PSDictHasEntry(new->private,"lenIV"))
	PSDictChangeEntry(new->private,"lenIV","1");		/* It's 4 by default, in CIDs the convention seems to be 1 */
    new->display_antialias = fv->b.sf->display_antialias;
    new->display_bbsized = fv->b.sf->display_bbsized;
    new->display_size = fv->b.sf->display_size;
    FVInsertInCID((FontViewBase *) fv,new);
    CIDMasterAsDes(new);
}

static void FVMenuInsertBlank(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster, *sf;
    struct cidmap *map;

    if ( cidmaster==NULL || cidmaster->subfontcnt>=255 )	/* Open type allows 1 byte to specify the fdselect */
return;
    map = FindCidMap(cidmaster->cidregistry,cidmaster->ordering,cidmaster->supplement,cidmaster);
    sf = SplineFontBlank(MaxCID(map));
    sf->glyphcnt = sf->glyphmax;
    sf->cidmaster = cidmaster;
    sf->display_antialias = fv->b.sf->display_antialias;
    sf->display_bbsized = fv->b.sf->display_bbsized;
    sf->display_size = fv->b.sf->display_size;
    sf->private = calloc(1,sizeof(struct psdict));
    PSDictChangeEntry(sf->private,"lenIV","1");		/* It's 4 by default, in CIDs the convention seems to be 1 */
    FVInsertInCID((FontViewBase *) fv,sf);
}

static void FVMenuRemoveFontFromCID(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    char *buts[3];
    SplineFont *cidmaster = fv->b.cidmaster, *sf = fv->b.sf, *replace;
    int i;
    MetricsView *mv, *mnext;
    FontView *fvs;

    if ( cidmaster==NULL || cidmaster->subfontcnt<=1 )	/* Can't remove last font */
return;
    buts[0] = _("_Remove"); buts[1] = _("_Cancel"); buts[2] = NULL;
    if ( gwwv_ask(_("_Remove Font"),(const char **) buts,0,1,_("Are you sure you wish to remove sub-font %1$.40s from the CID font %2$.40s"),
	    sf->fontname,cidmaster->fontname)==1 )
return;

    for ( i=0; i<sf->glyphcnt; ++i ) if ( sf->glyphs[i]!=NULL ) {
	CharView *cv, *next;
	for ( cv = (CharView *) (sf->glyphs[i]->views); cv!=NULL; cv = next ) {
	    next = (CharView *) (cv->b.next);
	    GDrawDestroyWindow(cv->gw);
	}
    }
    GDrawProcessPendingEvents(NULL);
    for ( mv=fv->b.sf->metrics; mv!=NULL; mv = mnext ) {
	mnext = mv->next;
	GDrawDestroyWindow(mv->gw);
    }
    GDrawSync(NULL);
    GDrawProcessPendingEvents(NULL);
    /* Just in case... */
    GDrawSync(NULL);
    GDrawProcessPendingEvents(NULL);

    for ( i=0; i<cidmaster->subfontcnt; ++i )
	if ( cidmaster->subfonts[i]==sf )
    break;
    replace = i==0?cidmaster->subfonts[1]:cidmaster->subfonts[i-1];
    while ( i<cidmaster->subfontcnt-1 ) {
	cidmaster->subfonts[i] = cidmaster->subfonts[i+1];
	++i;
    }
    --cidmaster->subfontcnt;

    for ( fvs=(FontView *) (fv->b.sf->fv); fvs!=NULL; fvs=(FontView *) (fvs->b.nextsame) ) {
	if ( fvs->b.sf==sf )
	    CIDSetEncMap((FontViewBase *) fvs,replace);
    }
    FontViewReformatAll(fv->b.sf);
    SplineFontFree(sf);
}

static void FVMenuCIDFontInfo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;

    if ( cidmaster==NULL )
return;
    FontInfo(cidmaster,fv->b.active_layer,-1,false);
}

static void FVMenuChangeSupplement(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *cidmaster = fv->b.cidmaster;
    struct cidmap *cidmap;
    char buffer[20];
    char *ret, *end;
    int supple;

    if ( cidmaster==NULL )
return;
    sprintf(buffer,"%d",cidmaster->supplement);
    ret = gwwv_ask_string(_("Change Supplement..."),buffer,_("Please specify a new supplement for %.20s-%.20s"),
	    cidmaster->cidregistry,cidmaster->ordering);
    if ( ret==NULL )
return;
    supple = strtol(ret,&end,10);
    if ( *end!='\0' || supple<=0 ) {
	free(ret);
	ff_post_error( _("Bad Number"),_("Bad Number") );
return;
    }
    free(ret);
    if ( supple!=cidmaster->supplement ) {
	    /* this will make noises if it can't find an appropriate cidmap */
	cidmap = FindCidMap(cidmaster->cidregistry,cidmaster->ordering,supple,cidmaster);
	cidmaster->supplement = supple;
	FontViewSetTitle(fv);
    }
}

static SplineChar *FVFindACharInDisplay(FontView *fv) {
    int start, end, enc, gid;
    EncMap *map = fv->b.map;
    SplineFont *sf = fv->b.sf;
    SplineChar *sc;

    start = fv->rowoff*fv->colcnt;
    end = start + fv->rowcnt*fv->colcnt;
    for ( enc = start; enc<end && enc<map->enccount; ++enc ) {
	if ( (gid=map->map[enc])!=-1 && (sc=sf->glyphs[gid])!=NULL )
return( sc );
    }
return( NULL );
}

static void FVMenuReencode(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    Encoding *enc = NULL;
    SplineChar *sc;

    sc = FVFindACharInDisplay(fv);
    enc = FindOrMakeEncoding(mi->ti.userdata);
    if ( enc==NULL ) {
	IError("Known encoding could not be found");
return;
    }
    FVReencode((FontViewBase *) fv,enc);
    if ( sc!=NULL ) {
	int enc = fv->b.map->backmap[sc->orig_pos];
	if ( enc!=-1 )
	    FVScrollToChar(fv,enc);
    }
}

static void FVMenuForceEncode(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    Encoding *enc = NULL;
    int oldcnt = fv->b.map->enccount;

    enc = FindOrMakeEncoding(mi->ti.userdata);
    if ( enc==NULL ) {
	IError("Known encoding could not be found");
return;
    }
    SFForceEncoding(fv->b.sf,fv->b.map,enc);
    if ( oldcnt < fv->b.map->enccount ) {
	fv->b.selected = realloc(fv->b.selected,fv->b.map->enccount);
	memset(fv->b.selected+oldcnt,0,fv->b.map->enccount-oldcnt);
    }
    if ( fv->b.normal!=NULL ) {
	EncMapFree(fv->b.normal);
	if (fv->b.normal == fv->b.sf->map) { fv->b.sf->map = NULL; }
	fv->b.normal = NULL;
    }
    SFReplaceEncodingBDFProps(fv->b.sf,fv->b.map);
    FontViewSetTitle(fv);
    FontViewReformatOne(&fv->b);
}

static void FVMenuDisplayByGroups(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    DisplayGroups(fv);
}

static void FVMenuDefineGroups(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    DefineGroups(fv);
}

static void FVMenuMMValid(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    MMSet *mm = fv->b.sf->mm;

    if ( mm==NULL )
return;
    MMValid(mm,true);
}

static void FVMenuCreateMM(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    MMWizard(NULL);
}

static void FVMenuMMInfo(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    MMSet *mm = fv->b.sf->mm;

    if ( mm==NULL )
return;
    MMWizard(mm);
}

static void FVMenuChangeMMBlend(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    MMSet *mm = fv->b.sf->mm;

    if ( mm==NULL || mm->apple )
return;
    MMChangeBlend(mm,fv,false);
}

static void FVMenuBlendToNew(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    MMSet *mm = fv->b.sf->mm;

    if ( mm==NULL )
return;
    MMChangeBlend(mm,fv,true);
}

static void cflistcheck(GWindow UNUSED(gw), struct gmenuitem *mi, GEvent *UNUSED(e)) {
    /*FontView *fv = (FontView *) GDrawGetUserData(gw);*/

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_AllFonts:
	    mi->ti.checked = !onlycopydisplayed;
	  break;
	  case MID_DisplayedFont:
	    mi->ti.checked = onlycopydisplayed;
	  break;
	  case MID_CharName:
	    mi->ti.checked = copymetadata;
	  break;
	  case MID_TTFInstr:
	    mi->ti.checked = copyttfinstr;
	  break;
	}
    }
}

static void sllistcheck(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    fv = fv;
}

static void htlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);
    int multilayer = fv->b.sf->multilayer;

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_AutoHint:
	    mi->ti.disabled = anychars==-1 || multilayer;
	  break;
	  case MID_HintSubsPt:
	    mi->ti.disabled = fv->b.sf->layers[fv->b.active_layer].order2 || anychars==-1 || multilayer;
	    if ( fv->b.sf->mm!=NULL && fv->b.sf->mm->apple )
		mi->ti.disabled = true;
	  break;
	  case MID_AutoCounter: case MID_DontAutoHint:
	    mi->ti.disabled = fv->b.sf->layers[fv->b.active_layer].order2 || anychars==-1 || multilayer;
	  break;
	  case MID_AutoInstr: case MID_EditInstructions: case MID_Deltas:
	    mi->ti.disabled = !fv->b.sf->layers[fv->b.active_layer].order2 || anychars==-1 || multilayer;
	  break;
	  case MID_RmInstrTables:
	    mi->ti.disabled = fv->b.sf->ttf_tables==NULL;
	  break;
	  case MID_Editfpgm: case MID_Editprep: case MID_Editcvt: case MID_Editmaxp:
	    mi->ti.disabled = !fv->b.sf->layers[fv->b.active_layer].order2 || multilayer;
	  break;
	  case MID_ClearHints: case MID_ClearWidthMD: case MID_ClearInstrs:
	    mi->ti.disabled = anychars==-1;
	  break;
	}
    }
}

static void fllistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e))
{
	FontView *fv = (FontView *) GDrawGetUserData(gw);
	int anychars = FVAnyCharSelected(fv);
	FontView *fvs;
	int in_modal = (fv->b.container!=NULL && fv->b.container->funcs->is_modal);

	for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi )
	{
		switch ( mi->mid )
		{
			case MID_GenerateTTC:
			{
				for ( fvs=fv_list; fvs!=NULL; fvs=(FontView *) (fvs->b.next) )
				{
					if ( fvs!=fv )
					{
						break;
					}
				}
				mi->ti.disabled = fvs==NULL;
			}
			break;
			case MID_Revert:
			{
				mi->ti.disabled = fv->b.sf->origname==NULL || fv->b.sf->new;
			}
			break;
			case MID_RevertToBackup:
			{
				/* We really do want to use filename here and origname above */
				mi->ti.disabled = true;
				if ( fv->b.sf->filename!=NULL )
				{
					if ( fv->b.sf->backedup == bs_dontknow )
					{
						char *buf = malloc(strlen(fv->b.sf->filename)+20);
						strcpy(buf,fv->b.sf->filename);
						if ( fv->b.sf->compression!=0 )
						{
							strcat(buf,compressors[fv->b.sf->compression-1].ext);
						}
						strcat(buf,"~");
						if ( access(buf,F_OK)==0 )
						{
							fv->b.sf->backedup = bs_backedup;
						}
						else
						{
							fv->b.sf->backedup = bs_not;
						}
						free(buf);
					}
					if ( fv->b.sf->backedup == bs_backedup )
					{
						mi->ti.disabled = false;
					}
				}
			}
			break;
			case MID_RevertGlyph:
			{
				mi->ti.disabled = fv->b.sf->origname==NULL || fv->b.sf->sfd_version<2 || anychars==-1 || fv->b.sf->compression!=0;
			}
			break;
			case MID_Recent:
			{
				mi->ti.disabled = !RecentFilesAny();
			}
			break;
			case MID_ScriptMenu:
			{
				mi->ti.disabled = script_menu_names[0]==NULL;
			}
			break;
			case MID_Print:
			{
				mi->ti.disabled = fv->b.sf->onlybitmaps || in_modal;
			}
			break;
		}
	}
}

static void edlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int pos = FVAnyCharSelected(fv), i, gid;
    int not_pasteable = pos==-1 ||
		    (!CopyContainsSomething() &&
#ifndef _NO_LIBPNG
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/png") &&
#endif
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/svg+xml") &&
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/svg-xml") &&
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/svg") &&
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/bmp") &&
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/eps") &&
		    !GDrawSelectionHasType(fv->gw,sn_clipboard,"image/ps"));
    RefChar *base = CopyContainsRef(fv->b.sf);
    int base_enc = base!=NULL ? fv->b.map->backmap[base->orig_pos] : -1;


    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Paste: case MID_PasteInto:
	    mi->ti.disabled = not_pasteable;
	  break;
	  case MID_PasteAfter:
	    mi->ti.disabled = not_pasteable || pos<0;
	  break;
	  case MID_SameGlyphAs:
	    mi->ti.disabled = not_pasteable || base==NULL || fv->b.cidmaster!=NULL ||
		    base_enc==-1 ||
		    fv->b.selected[base_enc];	/* Can't be self-referential */
	  break;
	  case MID_Join:
	  case MID_Cut: case MID_Copy: case MID_Clear:
	  case MID_CopyWidth: case MID_CopyLBearing: case MID_CopyRBearing:
	  case MID_CopyRef: case MID_UnlinkRef:
	  case MID_RemoveUndoes: case MID_CopyFgToBg: case MID_CopyL2L:
	    mi->ti.disabled = pos==-1;
	  break;
	  case MID_RplRef:
	  case MID_CorrectRefs:
	    mi->ti.disabled = pos==-1 || fv->b.cidmaster!=NULL || fv->b.sf->multilayer;
	  break;
	  case MID_CopyLookupData:
	    mi->ti.disabled = pos==-1 || (fv->b.sf->gpos_lookups==NULL && fv->b.sf->gsub_lookups==NULL);
	  break;
	  case MID_CopyVWidth:
	    mi->ti.disabled = pos==-1 || !fv->b.sf->hasvmetrics;
	  break;
	  case MID_ClearBackground:
	    mi->ti.disabled = true;
	    if ( pos!=-1 && !( onlycopydisplayed && fv->filled!=fv->show )) {
		for ( i=0; i<fv->b.map->enccount; ++i )
		    if ( fv->b.selected[i] && (gid = fv->b.map->map[i])!=-1 &&
			    fv->b.sf->glyphs[gid]!=NULL )
			if ( fv->b.sf->glyphs[gid]->layers[ly_back].images!=NULL ||
				fv->b.sf->glyphs[gid]->layers[ly_back].splines!=NULL ) {
			    mi->ti.disabled = false;
		break;
			}
	    }
	  break;
	  case MID_Undo:
	    for ( i=0; i<fv->b.map->enccount; ++i )
		if ( fv->b.selected[i] && (gid = fv->b.map->map[i])!=-1 &&
			fv->b.sf->glyphs[gid]!=NULL )
		    if ( fv->b.sf->glyphs[gid]->layers[fv->b.active_layer].undoes!=NULL )
	    break;
	    mi->ti.disabled = i==fv->b.map->enccount;
	  break;
	  case MID_Redo:
	    for ( i=0; i<fv->b.map->enccount; ++i )
		if ( fv->b.selected[i] && (gid = fv->b.map->map[i])!=-1 &&
			fv->b.sf->glyphs[gid]!=NULL )
		    if ( fv->b.sf->glyphs[gid]->layers[fv->b.active_layer].redoes!=NULL )
	    break;
	    mi->ti.disabled = i==fv->b.map->enccount;
	  break;
	case MID_UndoFontLevel:
	    mi->ti.disabled = dlist_isempty( (struct dlistnode **)&fv->b.sf->undoes );
	    break;
	}
    }
}

static void trlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Transform:
	    mi->ti.disabled = anychars==-1;
	  break;
	  case MID_NLTransform: case MID_POV:
	    mi->ti.disabled = anychars==-1 || fv->b.sf->onlybitmaps;
	  break;
	}
    }
}

static void validlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_FindProblems:
	    mi->ti.disabled = anychars==-1;
	  break;
	  case MID_Validate:
	    mi->ti.disabled = fv->b.sf->strokedfont || fv->b.sf->multilayer;
	  break;
        }
    }
}

static void ellistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv), gid;
    int anybuildable, anytraceable;
    int in_modal = (fv->b.container!=NULL && fv->b.container->funcs->is_modal);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_FontInfo:
	    mi->ti.disabled = in_modal;
	  break;
	  case MID_CharInfo:
	    mi->ti.disabled = anychars<0 || (gid = fv->b.map->map[anychars])==-1 ||
		    (fv->b.cidmaster!=NULL && fv->b.sf->glyphs[gid]==NULL) ||
		    in_modal;
	  break;
	  case MID_Transform:
	    mi->ti.disabled = anychars==-1;
	    /* some Transformations make sense on bitmaps now */
	  break;
	  case MID_AddExtrema:
	    mi->ti.disabled = anychars==-1 || fv->b.sf->onlybitmaps;
	  break;
	  case MID_Simplify:
	  case MID_Stroke: case MID_RmOverlap:
	    mi->ti.disabled = anychars==-1 || fv->b.sf->onlybitmaps;
	  break;
	  case MID_Styles:
	    mi->ti.disabled = anychars==-1 || fv->b.sf->onlybitmaps;
	  break;
	  case MID_Round: case MID_Correct:
	    mi->ti.disabled = anychars==-1 || fv->b.sf->onlybitmaps;
	  break;
#ifdef FONTFORGE_CONFIG_TILEPATH
	  case MID_TilePath:
	    mi->ti.disabled = anychars==-1 || fv->b.sf->onlybitmaps;
	  break;
#endif
	  case MID_AvailBitmaps:
	    mi->ti.disabled = fv->b.sf->mm!=NULL;
	  break;
	  case MID_RegenBitmaps: case MID_RemoveBitmaps:
	    mi->ti.disabled = fv->b.sf->bitmaps==NULL || fv->b.sf->onlybitmaps ||
		    fv->b.sf->mm!=NULL;
	  break;
	  case MID_BuildAccent:
	    anybuildable = false;
	    if ( anychars!=-1 ) {
		int i;
		for ( i=0; i<fv->b.map->enccount; ++i ) if ( fv->b.selected[i] ) {
		    SplineChar *sc=NULL, dummy;
		    gid = fv->b.map->map[i];
		    if ( gid!=-1 )
			sc = fv->b.sf->glyphs[gid];
		    if ( sc==NULL )
			sc = SCBuildDummy(&dummy,fv->b.sf,fv->b.map,i);
		    if ( SFIsSomethingBuildable(fv->b.sf,sc,fv->b.active_layer,false) ||
			    SFIsDuplicatable(fv->b.sf,sc)) {
			anybuildable = true;
		break;
		    }
		}
	    }
	    mi->ti.disabled = !anybuildable;
	  break;
	  case MID_Autotrace:
	    anytraceable = false;
	    if ( FindAutoTraceName()!=NULL && anychars!=-1 ) {
		int i;
		for ( i=0; i<fv->b.map->enccount; ++i )
		    if ( fv->b.selected[i] && (gid = fv->b.map->map[i])!=-1 &&
			    fv->b.sf->glyphs[gid]!=NULL &&
			    fv->b.sf->glyphs[gid]->layers[ly_back].images!=NULL ) {
			anytraceable = true;
		break;
		    }
	    }
	    mi->ti.disabled = !anytraceable;
	  break;
	  case MID_MergeFonts:
	    mi->ti.disabled = fv->b.sf->bitmaps!=NULL && fv->b.sf->onlybitmaps;
	  break;
	  case MID_FontCompare:
	    mi->ti.disabled = fv_list->b.next==NULL;
	  break;
	  case MID_InterpolateFonts:
	    mi->ti.disabled = fv->b.sf->onlybitmaps;
	  break;
	}
    }
}

static void mtlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Center: case MID_Thirds: case MID_SetWidth:
	  case MID_SetLBearing: case MID_SetRBearing: case MID_SetBearings:
	    mi->ti.disabled = anychars==-1;
	  break;
	  case MID_SetVWidth:
	    mi->ti.disabled = anychars==-1 || !fv->b.sf->hasvmetrics;
	  break;
	  case MID_VKernByClass:
	  case MID_VKernFromH:
	  case MID_RmVKern:
	    mi->ti.disabled = !fv->b.sf->hasvmetrics;
	  break;
	}
    }
}

#if HANYANG
static void hglistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
        if ( mi->mid==MID_BuildSyllables || mi->mid==MID_ModifyComposition )
	    mi->ti.disabled = fv->b.sf->rules==NULL;
    }
}

static GMenuItem2 hglist[] = {
    { { (unichar_t *) N_("_New Composition..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'N' }, H_("New Composition...|No Shortcut"), NULL, NULL, MenuNewComposition },
    { { (unichar_t *) N_("_Modify Composition..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("Modify Composition...|No Shortcut"), NULL, NULL, FVMenuModifyComposition, MID_ModifyComposition },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, }},
    { { (unichar_t *) N_("_Build Syllables"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Build Syllables|No Shortcut"), NULL, NULL, FVMenuBuildSyllables, MID_BuildSyllables },
    { NULL }
};
#endif

static void balistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e))
{
	FontView *fv = (FontView *) GDrawGetUserData(gw);

	for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi )
	{
		if ( mi->mid==MID_BuildAccent || mi->mid==MID_BuildComposite )
		{
			int anybuildable = false;
			int onlyaccents = mi->mid==MID_BuildAccent;
			int i, gid;
			for ( i=0; i<fv->b.map->enccount; ++i )
			{
				if ( fv->b.selected[i] )
				{
					SplineChar *sc=NULL, dummy;
					if ( (gid=fv->b.map->map[i])!=-1 )
					{
						sc = fv->b.sf->glyphs[gid];
					}
					if ( sc==NULL )
					{
						sc = SCBuildDummy(&dummy,fv->b.sf,fv->b.map,i);
					}
					if ( SFIsSomethingBuildable(fv->b.sf,sc,fv->b.active_layer,onlyaccents))
					{
						anybuildable = true;
						break;
					}
				}
			}
			mi->ti.disabled = !anybuildable;
		}
		else if ( mi->mid==MID_BuildDuplicates )
		{
			int anybuildable = false;
			int i, gid;
			for ( i=0; i<fv->b.map->enccount; ++i )
			{
				if ( fv->b.selected[i] )
				{
					SplineChar *sc=NULL, dummy;
					if ( (gid=fv->b.map->map[i])!=-1 )
					{
					sc = fv->b.sf->glyphs[gid];
					}
					if ( sc==NULL )
					{
					sc = SCBuildDummy(&dummy,fv->b.sf,fv->b.map,i);
					}
					if ( SFIsDuplicatable(fv->b.sf,sc))
					{
					anybuildable = true;
					break;
					}
				}
			}
			mi->ti.disabled = !anybuildable;
		}
	}
}

static void delistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i = FVAnyCharSelected(fv);
    int gid = i<0 ? -1 : fv->b.map->map[i];

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_ShowDependentRefs:
	    mi->ti.disabled = gid<0 || fv->b.sf->glyphs[gid]==NULL ||
		    fv->b.sf->glyphs[gid]->dependents == NULL;
	  break;
	  case MID_ShowDependentSubs:
	    mi->ti.disabled = gid<0 || fv->b.sf->glyphs[gid]==NULL ||
		    !SCUsedBySubs(fv->b.sf->glyphs[gid]);
	  break;
	}
    }
}

static void infolistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_StrikeInfo:
	    mi->ti.disabled = fv->b.sf->bitmaps==NULL;
	  break;
	  case MID_MassRename:
	    mi->ti.disabled = anychars==-1;
	  break;
	  case MID_SetColor:
	    mi->ti.disabled = anychars==-1;
	  break;
	}
    }
}

static GMenuItem2 dummyitem[] = {
    { { (unichar_t *) N_("Font|_New"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'N' }, NULL, NULL, NULL, NULL, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 fllist[] = {
    { { (unichar_t *) N_("Font|_New"), (GImage *) "filenew.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'N' }, H_("New|No Shortcut"), NULL, NULL, MenuNew, 0 },
#if HANYANG
    { { (unichar_t *) N_("_Hangul"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Hangul|No Shortcut"), hglist, hglistcheck, NULL, 0 },
#endif
    { { (unichar_t *) N_("_Open"), (GImage *) "fileopen.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'O' }, H_("Open|No Shortcut"), NULL, NULL, FVMenuOpen, 0 },
    { { (unichar_t *) N_("Recen_t"), (GImage *) "filerecent.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 't' }, H_("Recent|No Shortcut"), dummyitem, MenuRecentBuild, NULL, MID_Recent },
    { { (unichar_t *) N_("_Close"), (GImage *) "fileclose.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Close|No Shortcut"), NULL, NULL, FVMenuClose, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */

#ifdef HYMODIFY
    { { (unichar_t *) N_("Hanyang Rule Import"), (GImage *) "ImportHanyangRule.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Hanyang Rule Import|No Shortcut"), NULL, NULL, FVMenuHYImportRuleJohap },
    { { (unichar_t *) N_("Hanyang Rule Modify"), (GImage *) "ModifyHanyangRule.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Hanyang Rule Modify|No Shortcut"), NULL, NULL, FVMenuHYModifyRuleJohap },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#if USE_HANDWRITE
    { { (unichar_t *) N_("HandWrite Data Import"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("HandWrite Data Import|No Shortcut"), NULL, NULL, FVMenuHYImportHandWrite, MID_HYImportHandWrite },
#endif
#if REGIST_FONT
    { { (unichar_t *) N_("Register Font"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Register Font|No Shortcut"), NULL, NULL, FVMenuHYRegisterFont, MID_HYRegisterFont },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
#if USE_MG_JOHAP
    { { (unichar_t *) N_("MMG All Hangul(11,172) Johap"), (GImage *) "MMGAllHangulJohap.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MMG All Hangul(11,172) Johap|No Shortcut"), NULL, NULL, FVMenuHYMMGHangulJohap, MID_HYMMGJohap_Hangul_All },
    { { (unichar_t *) N_("MMG Jungsung Hori Johap"), (GImage *) "MMGJungsungHoriJohap.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MMG Jungsung Hori Johap|No Shortcut"), NULL, NULL, FVMenuHYMMGHangulJohap, MID_HYMMGJohap_Jung_Hori },
    { { (unichar_t *) N_("MMG Jungsung Vert Johap"), (GImage *) "MMGJungsungVertJohap.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MMG Jungsung Vert Johap|No Shortcut"), NULL, NULL, FVMenuHYMMGHangulJohap, MID_HYMMGJohap_Jung_Vert},
    { { (unichar_t *) N_("MMG Jungsung Comp Johap"), (GImage *) "MMGJungsungCompJohap.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MMG Jungsung Comp Johap|No Shortcut"), NULL, NULL, FVMenuHYMMGHangulJohap, MID_HYMMGJohap_Jung_Comp },
    { { (unichar_t *) N_("MMG KSC5601 Hangul(2,350) Johap"), (GImage *) "MMGKSC5601HangulJohap.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MMG KSC5601 Hangul(2,350) Johap|No Shortcut"), NULL, NULL, FVMenuHYMMGHangulJohap, MID_HYMMGJohap_Hangul_KSC5601 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
#if AUTO_MAKE_MG_TEST
#if 1
    { { (unichar_t *) N_("Make MG"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Make MG|No Shortcut"), NULL, NULL, FVMenuHYMakeMG, MID_HYTest },
    { { (unichar_t *) N_("Link ChoSung"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Link ChoSung|No Shortcut"), NULL, NULL, FVMenuHYLinkChoSungMG, MID_HYTest },
    { { (unichar_t *) N_("Link JungSung"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Link JungSung|No Shortcut"), NULL, NULL, FVMenuHYLinkJungSungMG, MID_HYTest },
    { { (unichar_t *) N_("Link JongSung"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Link JongSung|No Shortcut"), NULL, NULL, FVMenuHYLinkJongSungMG, MID_HYTest },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Reform BaseChar 1"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Reform BaseChar 1|No Shortcut"), NULL, NULL, FVMenuHYReformBaseChar1, MID_HYTest },
    { { (unichar_t *) N_("Reform BaseChar 2"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Reform BaseChar 2|No Shortcut"), NULL, NULL, FVMenuHYReformBaseChar2, MID_HYTest },
    { { (unichar_t *) N_("Reform BaseChar 3"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Reform BaseChar 3|No Shortcut"), NULL, NULL, FVMenuHYReformBaseChar3, MID_HYTest },
//    { { (unichar_t *) N_("Adjust Jaso"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Adjust Jaso|No Shortcut"), NULL, NULL, FVMenuHYAdjustJaso, MID_HYTest },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#if 1
    { { (unichar_t *) N_("Auto Make MG"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Auto Make MG|No Shortcut"), NULL, NULL, FVMenuHYAutoMakeMG, MID_HYAutoMakeMG },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
#endif
#if 0
    { { (unichar_t *) N_("EZ MMG Johap (11,172)"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("EZ MMG Johap (11,172)|No Shortcut"), NULL, NULL, FVMenuHYEZ_MMGJohap, MID_HYEZJohap_Hangul_All },
    { { (unichar_t *) N_("EZ MMG Johap (KSC5601)"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("EZ MMG Johap (KSC5601)|No Shortcut"), NULL, NULL, FVMenuHYEZ_MMGJohap, MID_HYEZJohap_Hangul_KSC5601 },
    { { (unichar_t *) N_("EZ MMG Johap (Hori)"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("EZ MMG Johap (Hori)|No Shortcut"), NULL, NULL, FVMenuHYEZ_MMGJohap, MID_HYEZJohap_Hangul_Hori },
    { { (unichar_t *) N_("EZ MMG Johap (Vert)"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("EZ MMG Johap (Vert)|No Shortcut"), NULL, NULL, FVMenuHYEZ_MMGJohap, MID_HYEZJohap_Hangul_Vert },
    { { (unichar_t *) N_("EZ MMG Johap (Comp)"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("EZ MMG Johap (Comp)|No Shortcut"), NULL, NULL, FVMenuHYEZ_MMGJohap, MID_HYEZJohap_Hangul_Comp },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
#if 0
    { { (unichar_t *) N_("Get LinkMap"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Get LinkMap|No Shortcut"), NULL, NULL, FVMenuHYGetLinkMap, MID_HYGetBaseCharLinkMap },
    { { (unichar_t *) N_("Get BaseChar Hex Map"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Get BaseChar Hax Map|No Shortcut"), NULL, NULL, FVMenuHYGetBaseCharMap, MID_HYGetBaseCharToHex },
    { { (unichar_t *) N_("Get BaseChar Unicode"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Get BaseChar Unicode|No Shortcut"), NULL, NULL, FVMenuHYGetBaseCharUnicode, MID_HYGetBaseCharToUni },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Detach None BaseChar"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Detach None BaseChar|No Shortcut"), NULL, NULL, FVMenuHYRemoveNoneBaseChar, MID_HYTest },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
#endif
#endif

    { { (unichar_t *) N_("_Save"), (GImage *) "filesave.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'S' }, H_("Save|No Shortcut"), NULL, NULL, FVMenuSave, 0 },
    { { (unichar_t *) N_("S_ave as..."), (GImage *) "filesaveas.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'a' }, H_("Save as...|No Shortcut"), NULL, NULL, FVMenuSaveAs, 0 },
    { { (unichar_t *) N_("Save A_ll"), (GImage *) "filesaveall.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Save All|No Shortcut"), NULL, NULL, MenuSaveAll, 0 },

#ifdef HYMODIFY
#if USE_MMG
#if 0
    { { (unichar_t *) N_("Save MMF"), (GImage *) "hanyang.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Save MMF|No Shortcut"), NULL, NULL, FVMenuSaveMMFNormal, 0 },
#endif
#endif

#endif
    { { (unichar_t *) N_("_Generate Fonts..."), (GImage *) "filegenerate.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'G' }, H_("Generate Fonts...|No Shortcut"), NULL, NULL, FVMenuGenerate, 0 },
    { { (unichar_t *) N_("Generate Mac _Family..."), (GImage *) "filegeneratefamily.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Generate Mac Family...|No Shortcut"), NULL, NULL, FVMenuGenerateFamily, 0 },
    { { (unichar_t *) N_("Generate TTC..."), (GImage *) "filegeneratefamily.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Generate TTC...|No Shortcut"), NULL, NULL, FVMenuGenerateTTC, MID_GenerateTTC },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Import..."), (GImage *) "fileimport.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Import...|No Shortcut"), NULL, NULL, FVMenuImport, 0 },
    { { (unichar_t *) N_("_Merge Feature Info..."), (GImage *) "filemergefeature.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("Merge Kern Info...|No Shortcut"), NULL, NULL, FVMenuMergeKern, 0 },
    { { (unichar_t *) N_("_Revert File"), (GImage *) "filerevert.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Revert File|No Shortcut"), NULL, NULL, FVMenuRevert, MID_Revert },
    { { (unichar_t *) N_("Revert To _Backup"), (GImage *) "filerevertbackup.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Revert To Backup|No Shortcut"), NULL, NULL, FVMenuRevertBackup, MID_RevertToBackup },
    { { (unichar_t *) N_("Revert Gl_yph"), (GImage *) "filerevertglyph.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Revert Glyph|No Shortcut"), NULL, NULL, FVMenuRevertGlyph, MID_RevertGlyph },
    { { (unichar_t *) N_("Clear Special Data"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Clear Special Data|No Shortcut"), NULL, NULL, FVMenuClearSpecialData, MID_ClearSpecialData },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Print..."), (GImage *) "fileprint.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Print...|No Shortcut"), NULL, NULL, FVMenuPrint, MID_Print },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#if !defined(_NO_PYTHON)
    { { (unichar_t *) N_("E_xecute Script..."), (GImage *) "python.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'x' }, H_("Execute Script...|No Shortcut"), NULL, NULL, FVMenuExecute, 0 },
#elif !defined(_NO_FFSCRIPT)
    { { (unichar_t *) N_("E_xecute Script..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'x' }, H_("Execute Script...|No Shortcut"), NULL, NULL, FVMenuExecute, 0 },
#endif
#if !defined(_NO_FFSCRIPT)
    { { (unichar_t *) N_("Script Menu"), (GImage *) "fileexecute.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'r' }, H_("Script Menu|No Shortcut"), dummyitem, MenuScriptsBuild, NULL, MID_ScriptMenu },
#endif
#if !defined(_NO_FFSCRIPT) || !defined(_NO_PYTHON)
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
    { { (unichar_t *) N_("Pr_eferences..."), (GImage *) "fileprefs.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'e' }, H_("Preferences...|No Shortcut"), NULL, NULL, MenuPrefs, 0 },
    { { (unichar_t *) N_("_X Resource Editor..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'e' }, H_("X Resource Editor...|No Shortcut"), NULL, NULL, MenuXRes, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Quit"), (GImage *) "filequit.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'Q' }, H_("Quit|Ctl+Q"), /* WARNING: THIS BINDING TO PROPERLY INITIALIZE KEYBOARD INPUT */
      NULL, NULL, FVMenuExit, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 cflist[] = {
    { { (unichar_t *) N_("_All Fonts"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'A' }, H_("All Fonts|No Shortcut"), NULL, NULL, FVMenuCopyFrom, MID_AllFonts },
    { { (unichar_t *) N_("_Displayed Font"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'D' }, H_("Displayed Font|No Shortcut"), NULL, NULL, FVMenuCopyFrom, MID_DisplayedFont },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Glyph _Metadata"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'N' }, H_("Glyph Metadata|No Shortcut"), NULL, NULL, FVMenuCopyFrom, MID_CharName },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_TrueType Instructions"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'N' }, H_("TrueType Instructions|No Shortcut"), NULL, NULL, FVMenuCopyFrom, MID_TTFInstr },
    GMENUITEM2_EMPTY
};

static GMenuItem2 sclist[] = {
    { { (unichar_t *) N_("Color|Choose..."), (GImage *)"colorwheel.png", COLOR_DEFAULT, COLOR_DEFAULT, (void *) -10, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Color Choose|No Shortcut"), NULL, NULL, FVMenuSelectColor, 0 },
    { { (unichar_t *)  N_("Color|Default"), &def_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) COLOR_DEFAULT, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Default|No Shortcut"), NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &white_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xffffff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &red_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xff0000, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &green_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0x00ff00, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &blue_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0x0000ff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &yellow_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xffff00, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &cyan_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0x00ffff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    { { NULL, &magenta_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xff00ff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSelectColor, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 sllist[] = {
    { { (unichar_t *) N_("Select _All"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'A' }, H_("Select All|No Shortcut"), NULL, NULL, FVMenuSelectAll, 0 },
    { { (unichar_t *) N_("_Invert Selection"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Invert Selection|No Shortcut"), NULL, NULL, FVMenuInvertSelection, 0 },
    { { (unichar_t *) N_("_Deselect All"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Deselect All|Escape"), NULL, NULL, FVMenuDeselectAll, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Select by _Color"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Select by Color|No Shortcut"), sclist, NULL, NULL, 0 },
    { { (unichar_t *) N_("Select by _Wildcard..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Select by Wildcard...|No Shortcut"), NULL, NULL, FVMenuSelectByName, 0 },
    { { (unichar_t *) N_("Select by _Script..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Select by Script...|No Shortcut"), NULL, NULL, FVMenuSelectByScript, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Glyphs Worth Outputting"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Glyphs Worth Outputting|No Shortcut"), NULL,NULL, FVMenuSelectWorthOutputting, 0 },
    { { (unichar_t *) N_("Glyphs with only _References"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Glyphs with only References|No Shortcut"), NULL,NULL, FVMenuGlyphsRefs, 0 },
    { { (unichar_t *) N_("Glyphs with only S_plines"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Glyphs with only Splines|No Shortcut"), NULL,NULL, FVMenuGlyphsSplines, 0 },
    { { (unichar_t *) N_("Glyphs with both"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Glyphs with both|No Shortcut"), NULL,NULL, FVMenuGlyphsBoth, 0 },
    { { (unichar_t *) N_("W_hitespace Glyphs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Whitespace Glyphs|No Shortcut"), NULL,NULL, FVMenuGlyphsWhite, 0 },
    { { (unichar_t *) N_("_Changed Glyphs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Changed Glyphs|No Shortcut"), NULL,NULL, FVMenuSelectChanged, 0 },
    { { (unichar_t *) N_("_Hinting Needed"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Hinting Needed|No Shortcut"), NULL,NULL, FVMenuSelectHintingNeeded, 0 },
    { { (unichar_t *) N_("Autohinta_ble"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Autohintable|No Shortcut"), NULL,NULL, FVMenuSelectAutohintable, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#ifndef CKS	// HYMODIFY :: 2016.02.01
    { { (unichar_t *) N_("Hold [Shift] key to merge"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 1, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, NULL, NULL, NULL, NULL, 0 },
    { { (unichar_t *) N_("Hold [Control] key to restrict"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 1, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, NULL, NULL, NULL, NULL, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
    { { (unichar_t *) N_("Selec_t By Lookup Subtable..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("Select By Lookup Subtable...|No Shortcut"), NULL, NULL, FVMenuSelectByPST, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 edlist[] = {
    { { (unichar_t *) N_("_Undo"), (GImage *) "editundo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'U' }, H_("Undo|No Shortcut"), NULL, NULL, FVMenuUndo, MID_Undo },
    { { (unichar_t *) N_("_Redo"), (GImage *) "editredo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Redo|No Shortcut"), NULL, NULL, FVMenuRedo, MID_Redo},
    { { (unichar_t *) N_("Undo Fontlevel"), (GImage *) "editundo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'U' }, H_("Undo Fontlevel|No Shortcut"), NULL, NULL, FVMenuUndoFontLevel, MID_UndoFontLevel },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Cu_t"), (GImage *) "editcut.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 't' }, H_("Cut|No Shortcut"), NULL, NULL, FVMenuCut, MID_Cut },
    { { (unichar_t *) N_("_Copy"), (GImage *) "editcopy.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Copy|No Shortcut"), NULL, NULL, FVMenuCopy, MID_Copy },
    { { (unichar_t *) N_("C_opy Reference"), (GImage *) "editcopyref.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Copy Reference|No Shortcut"), NULL, NULL, FVMenuCopyRef, MID_CopyRef },
    { { (unichar_t *) N_("Copy _Lookup Data"), (GImage *) "editcopylookupdata.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Copy Lookup Data|No Shortcut"), NULL, NULL, FVMenuCopyLookupData, MID_CopyLookupData },
    { { (unichar_t *) N_("Copy _Width"), (GImage *) "editcopywidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'W' }, H_("Copy Width|No Shortcut"), NULL, NULL, FVMenuCopyWidth, MID_CopyWidth },
    { { (unichar_t *) N_("Copy _VWidth"), (GImage *) "editcopyvwidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, H_("Copy VWidth|No Shortcut"), NULL, NULL, FVMenuCopyWidth, MID_CopyVWidth },
    { { (unichar_t *) N_("Co_py LBearing"), (GImage *) "editcopylbearing.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'p' }, H_("Copy LBearing|No Shortcut"), NULL, NULL, FVMenuCopyWidth, MID_CopyLBearing },
    { { (unichar_t *) N_("Copy RBearin_g"), (GImage *) "editcopyrbearing.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'g' }, H_("Copy RBearing|No Shortcut"), NULL, NULL, FVMenuCopyWidth, MID_CopyRBearing },
    { { (unichar_t *) N_("_Paste"), (GImage *) "editpaste.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Paste|No Shortcut"), NULL, NULL, FVMenuPaste, MID_Paste },
    { { (unichar_t *) N_("Paste Into"), (GImage *) "editpasteinto.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Paste Into|No Shortcut"), NULL, NULL, FVMenuPasteInto, MID_PasteInto },
    { { (unichar_t *) N_("Paste After"), (GImage *) "editpasteafter.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Paste After|No Shortcut"), NULL, NULL, FVMenuPasteAfter, MID_PasteAfter },
    { { (unichar_t *) N_("Sa_me Glyph As"), (GImage *) "editsameas.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'm' }, H_("Same Glyph As|No Shortcut"), NULL, NULL, FVMenuSameGlyphAs, MID_SameGlyphAs },
    { { (unichar_t *) N_("C_lear"), (GImage *) "editclear.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Clear|No Shortcut"), NULL, NULL, FVMenuClear, MID_Clear },
    { { (unichar_t *) N_("Clear _Background"), (GImage *) "editclearback.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Clear Background|No Shortcut"), NULL, NULL, FVMenuClearBackground, MID_ClearBackground },
    { { (unichar_t *) N_("Copy _Fg To Bg"), (GImage *) "editcopyfg2bg.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Copy Fg To Bg|No Shortcut"), NULL, NULL, FVMenuCopyFgBg, MID_CopyFgToBg },
    { { (unichar_t *) N_("Copy Layer To Layer"), (GImage *) "editcopylayer2layer.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Copy Layer To Layer|No Shortcut"), NULL, NULL, FVMenuCopyL2L, MID_CopyL2L },
    { { (unichar_t *) N_("_Join"), (GImage *) "editjoin.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'J' }, H_("Join|No Shortcut"), NULL, NULL, FVMenuJoin, MID_Join },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#ifdef HYMODIFY
#if USE_MMG
    { { (unichar_t *) N_("Set MG"), (GImage *) "SetMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Set MG|No Shortcut"), NULL, NULL, FVMenuHYSetMG, MID_HY_SetMG },
    { { (unichar_t *) N_("Unset MG"), (GImage *) "UnsetMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Set MG|No Shortcut"), NULL, NULL, FVMenuHYUnsetMG, MID_HY_UnsetMG },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Set MG ChoSung"), (GImage *) "SetMGChoSung.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Set MG ChoSung|No Shortcut"), NULL, NULL, FVMenuHYSetMGType, MID_HY_SetMGTypeCho },
    { { (unichar_t *) N_("Set MG JungSung"), (GImage *) "SetMGJungSung.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Set MG JungSung|No Shortcut"), NULL, NULL, FVMenuHYSetMGType, MID_HY_SetMGTypeJung },
    { { (unichar_t *) N_("Set MG JongSung"), (GImage *) "SetMGJongSung.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Set MG JongSung|No Shortcut"), NULL, NULL, FVMenuHYSetMGType, MID_HY_SetMGTypeJong },
    { { (unichar_t *) N_("Unset MMGType"), (GImage *) "UnsetMMGType .png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Unset MMGType|No Shortcut"), NULL, NULL, FVMenuHYSetMGType, MID_HY_UnsetMGType },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Copy MG"), (GImage *) "CopyMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Copy MG|Shft+C"), NULL, NULL, FVMenuHYCopyMG, MID_HY_CopyMG },
//    { { (unichar_t *) N_("Move MG"), (GImage *) "MoveMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Move MG|Shft+M"), NULL, NULL, FVMenuHYCopyMG, MID_HY_MoveMG },
    { { (unichar_t *) N_("Paste MG"), (GImage *) "PasteMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Paste MG|Shft+V"), NULL, NULL, FVMenuHYPasteMG, MID_HY_PasteMG },
    { { (unichar_t *) N_("Clear MG"), (GImage *) "ClearMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Clear MG|No Shortcut"), NULL, NULL, FVMenuHYClearMG, MID_HY_ClearMG },
    { { (unichar_t *) N_("Clear All MG"), (GImage *) "ClearALLMG.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Clear All MG|No Shortcut"), NULL, NULL, FVMenuHYClearMG, MID_HY_ClearAllMG },
    { { (unichar_t *) N_("Fit MG BBox"), (GImage *) "FitMGBBox.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Fit MG BBox|No Shortcut"), NULL, NULL, FVMenuHYFitMGBBox, MID_HY_FitMGBBox },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
#endif
    { { (unichar_t *) N_("_Select"), (GImage *) "editselect.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'S' }, H_("Select|No Shortcut"), sllist, sllistcheck, NULL, 0 },
    { { (unichar_t *) N_("F_ind / Replace..."), (GImage *) "editfind.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'i' }, H_("Find / Replace...|No Shortcut"), NULL, NULL, FVMenuFindRpl, 0 },
    { { (unichar_t *) N_("Replace with Reference"), (GImage *) "editrplref.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'i' }, H_("Replace with Reference|No Shortcut"), NULL, NULL, FVMenuReplaceWithRef, MID_RplRef },
    { { (unichar_t *) N_("Correct References"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'i' }, H_("Correct References|No Shortcut"), NULL, NULL, FVMenuCorrectRefs, MID_CorrectRefs },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("U_nlink Reference"), (GImage *) "editunlink.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'U' }, H_("Unlink Reference|No Shortcut"), NULL, NULL, FVMenuUnlinkRef, MID_UnlinkRef },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Copy _From"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Copy From|No Shortcut"), cflist, cflistcheck, NULL, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Remo_ve Undoes"), (GImage *) "editrmundoes.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'e' }, H_("Remove Undoes|No Shortcut"), NULL, NULL, FVMenuRemoveUndoes, MID_RemoveUndoes },
    GMENUITEM2_EMPTY
};

static GMenuItem2 smlist[] = {
    { { (unichar_t *) N_("_Simplify"), (GImage *) "elementsimplify.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'S' }, H_("Simplify|No Shortcut"), NULL, NULL, FVMenuSimplify, MID_Simplify },
    { { (unichar_t *) N_("Simplify More..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("Simplify More...|No Shortcut"), NULL, NULL, FVMenuSimplifyMore, MID_SimplifyMore },
    { { (unichar_t *) N_("Clea_nup Glyph"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'n' }, H_("Cleanup Glyph|No Shortcut"), NULL, NULL, FVMenuCleanup, MID_CleanupGlyph },
    { { (unichar_t *) N_("Canonical Start _Point"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'n' }, H_("Canonical Start Point|No Shortcut"), NULL, NULL, FVMenuCanonicalStart, MID_CanonicalStart },
    { { (unichar_t *) N_("Canonical _Contours"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'n' }, H_("Canonical Contours|No Shortcut"), NULL, NULL, FVMenuCanonicalContours, MID_CanonicalContours },
    GMENUITEM2_EMPTY
};

static GMenuItem2 rmlist[] = {
    { { (unichar_t *) N_("_Remove Overlap"), (GImage *) "overlaprm.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, 'O' }, H_("Remove Overlap|No Shortcut"), NULL, NULL, FVMenuOverlap, MID_RmOverlap },
    { { (unichar_t *) N_("_Intersect"), (GImage *) "overlapintersection.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Intersect|No Shortcut"), NULL, NULL, FVMenuOverlap, MID_Intersection },
    { { (unichar_t *) N_("_Find Intersections"), (GImage *) "overlapfindinter.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, 'O' }, H_("Find Intersections|No Shortcut"), NULL, NULL, FVMenuOverlap, MID_FindInter },
    GMENUITEM2_EMPTY
};

static GMenuItem2 eflist[] = {
    { { (unichar_t *) N_("Change _Weight..."), (GImage *) "styleschangeweight.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Change Weight...|No Shortcut"), NULL, NULL, FVMenuEmbolden, MID_Embolden },
    { { (unichar_t *) N_("_Italic..."), (GImage *) "stylesitalic.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Italic...|No Shortcut"), NULL, NULL, FVMenuItalic, MID_Italic },
    { { (unichar_t *) N_("Obli_que..."), (GImage *) "stylesoblique.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Oblique...|No Shortcut"), NULL, NULL, FVMenuOblique, 0 },
    { { (unichar_t *) N_("_Condense/Extend..."), (GImage *) "stylesextendcondense.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Condense...|No Shortcut"), NULL, NULL, FVMenuCondense, MID_Condense },
    { { (unichar_t *) N_("Change _X-Height..."), (GImage *) "styleschangexheight.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Change XHeight...|No Shortcut"), NULL, NULL, FVMenuChangeXHeight, MID_ChangeXHeight },
    { { (unichar_t *) N_("Change _Glyph..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Change Glyph...|No Shortcut"), NULL, NULL, FVMenuChangeGlyph, MID_ChangeGlyph },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Add _Small Capitals..."), (GImage *) "stylessmallcaps.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Add Small Caps...|No Shortcut"), NULL, NULL, FVMenuSmallCaps, MID_SmallCaps },
    { { (unichar_t *) N_("Add Subscripts/Superscripts..."), (GImage *) "stylessubsuper.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Add Subscripts/Superscripts...|No Shortcut"), NULL, NULL, FVMenuSubSup, MID_SubSup },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("In_line..."), (GImage *) "stylesinline.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Inline|No Shortcut"), NULL, NULL, FVMenuInline, 0 },
    { { (unichar_t *) N_("_Outline..."), (GImage *) "stylesoutline.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Outline|No Shortcut"), NULL, NULL, FVMenuOutline, 0 },
    { { (unichar_t *) N_("S_hadow..."), (GImage *) "stylesshadow.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Shadow|No Shortcut"), NULL, NULL, FVMenuShadow, 0 },
    { { (unichar_t *) N_("_Wireframe..."), (GImage *) "styleswireframe.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, true, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Wireframe|No Shortcut"), NULL, NULL, FVMenuWireframe, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 balist[] = {
    { { (unichar_t *) N_("_Build Accented Glyph"), (GImage *) "elementbuildaccent.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Build Accented Glyph|No Shortcut"), NULL, NULL, FVMenuBuildAccent, MID_BuildAccent },
    { { (unichar_t *) N_("Build _Composite Glyph"), (GImage *) "elementbuildcomposite.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Build Composite Glyph|No Shortcut"), NULL, NULL, FVMenuBuildComposite, MID_BuildComposite },
    { { (unichar_t *) N_("Buil_d Duplicate Glyph"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Build Duplicate Glyph|No Shortcut"), NULL, NULL, FVMenuBuildDuplicate, MID_BuildDuplicates },
#ifdef KOREAN
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) _STR_ShowGrp, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, NULL, NULL, NULL, FVMenuShowGroup },
#endif
    GMENUITEM2_EMPTY
};

static GMenuItem2 delist[] = {
    { { (unichar_t *) N_("_References..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'u' }, H_("References...|No Shortcut"), NULL, NULL, FVMenuShowDependentRefs, MID_ShowDependentRefs },
    { { (unichar_t *) N_("_Substitutions..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Substitutions...|No Shortcut"), NULL, NULL, FVMenuShowDependentSubs, MID_ShowDependentSubs },
    GMENUITEM2_EMPTY
};

static GMenuItem2 trlist[] = {
    { { (unichar_t *) N_("_Transform..."), (GImage *) "elementtransform.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("Transform...|No Shortcut"), NULL, NULL, FVMenuTransform, MID_Transform },
    { { (unichar_t *) N_("_Point of View Projection..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("Point of View Projection...|No Shortcut"), NULL, NULL, FVMenuPOV, MID_POV },
    { { (unichar_t *) N_("_Non Linear Transform..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("Non Linear Transform...|No Shortcut"), NULL, NULL, FVMenuNLTransform, MID_NLTransform },
    GMENUITEM2_EMPTY
};

static GMenuItem2 rndlist[] = {
    { { (unichar_t *) N_("To _Int"), (GImage *) "elementround.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("To Int|No Shortcut"), NULL, NULL, FVMenuRound2Int, MID_Round },
    { { (unichar_t *) N_("To _Hundredths"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("To Hundredths|No Shortcut"), NULL, NULL, FVMenuRound2Hundredths, 0 },
    { { (unichar_t *) N_("_Cluster"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Cluster|No Shortcut"), NULL, NULL, FVMenuCluster, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 scollist[] = {
    { { (unichar_t *) N_("Color|Choose..."), (GImage *)"colorwheel.png", COLOR_DEFAULT, COLOR_DEFAULT, (void *) -10, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Color Choose|No Shortcut"), NULL, NULL, FVMenuSetColor, 0 },
    { { (unichar_t *)  N_("Color|Default"), &def_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) COLOR_DEFAULT, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Default|No Shortcut"), NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &white_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xffffff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &red_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xff0000, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &green_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0x00ff00, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &blue_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0x0000ff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &yellow_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xffff00, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &cyan_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0x00ffff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    { { NULL, &magenta_image, COLOR_DEFAULT, COLOR_DEFAULT, (void *) 0xff00ff, NULL, 0, 1, 0, 0, 0, 0, 0, 0, 0, '\0' }, NULL, NULL, NULL, FVMenuSetColor, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 infolist[] = {
    { { (unichar_t *) N_("_MATH Info..."), (GImage *) "elementmathinfo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MATH Info...|No Shortcut"), NULL, NULL, FVMenuMATHInfo, 0 },
    { { (unichar_t *) N_("_BDF Info..."), (GImage *) "elementbdfinfo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("BDF Info...|No Shortcut"), NULL, NULL, FVMenuBDFInfo, MID_StrikeInfo },
    { { (unichar_t *) N_("_Horizontal Baselines..."), (GImage *) "elementhbaselines.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Horizontal Baselines...|No Shortcut"), NULL, NULL, FVMenuBaseHoriz, 0 },
    { { (unichar_t *) N_("_Vertical Baselines..."), (GImage *) "elementvbaselines.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Vertical Baselines...|No Shortcut"), NULL, NULL, FVMenuBaseVert, 0 },
    { { (unichar_t *) N_("_Justification..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Justification...|No Shortcut"), NULL, NULL, FVMenuJustify, 0 },
    { { (unichar_t *) N_("Show _Dependent"), (GImage *) "elementshowdep.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Show Dependent|No Shortcut"), delist, delistcheck, NULL, 0 },
    { { (unichar_t *) N_("Mass Glyph _Rename..."), (GImage *) "elementrenameglyph.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Mass Glyph Rename...|No Shortcut"), NULL, NULL, FVMenuMassRename, MID_MassRename },
    { { (unichar_t *) N_("Set _Color"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Set Color|No Shortcut"), scollist, NULL, NULL, MID_SetColor },
    GMENUITEM2_EMPTY
};

static GMenuItem2 validlist[] = {
    { { (unichar_t *) N_("Find Pr_oblems..."), (GImage *) "elementfindprobs.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Find Problems...|No Shortcut"), NULL, NULL, FVMenuFindProblems, MID_FindProblems },
    { { (unichar_t *) N_("_Validate..."), (GImage *) "elementvalidate.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Validate...|No Shortcut"), NULL, NULL, FVMenuValidate, MID_Validate },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Set E_xtremum Bound..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Set Extremum bound...|No Shortcut"), NULL, NULL, FVMenuSetExtremumBound, MID_SetExtremumBound },
    GMENUITEM2_EMPTY
};

static GMenuItem2 ellist[] = {
    { { (unichar_t *) N_("_Font Info..."), (GImage *) "elementfontinfo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Font Info...|No Shortcut"), NULL, NULL, FVMenuFontInfo, MID_FontInfo },
    { { (unichar_t *) N_("_Glyph Info..."), (GImage *) "elementglyphinfo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Glyph Info...|No Shortcut"), NULL, NULL, FVMenuCharInfo, MID_CharInfo },
    { { (unichar_t *) N_("Other Info"), (GImage *) "elementotherinfo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Other Info|No Shortcut"), infolist, infolistcheck, NULL, 0 },
    { { (unichar_t *) N_("_Validation"), (GImage *) "elementvalidate.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Validation|No Shortcut"), validlist, validlistcheck, NULL, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Bitm_ap Strikes Available..."), (GImage *) "elementbitmapsavail.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'A' }, H_("Bitmap Strikes Available...|No Shortcut"), NULL, NULL, FVMenuBitmaps, MID_AvailBitmaps },
    { { (unichar_t *) N_("Regenerate _Bitmap Glyphs..."), (GImage *) "elementregenbitmaps.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Regenerate Bitmap Glyphs...|No Shortcut"), NULL, NULL, FVMenuBitmaps, MID_RegenBitmaps },
    { { (unichar_t *) N_("Remove Bitmap Glyphs..."), (GImage *) "elementremovebitmaps.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Remove Bitmap Glyphs...|No Shortcut"), NULL, NULL, FVMenuBitmaps, MID_RemoveBitmaps },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("St_yle"), (GImage *) "elementstyles.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'S' }, H_("Style|No Shortcut"), eflist, NULL, NULL, MID_Styles },
    { { (unichar_t *) N_("_Transformations"), (GImage *) "elementtransform.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("Transformations|No Shortcut"), trlist, trlistcheck, NULL, MID_Transform },
    { { (unichar_t *) N_("_Expand Stroke..."), (GImage *) "elementexpandstroke.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'E' }, H_("Expand Stroke...|No Shortcut"), NULL, NULL, FVMenuStroke, MID_Stroke },
#ifdef FONTFORGE_CONFIG_TILEPATH
    { { (unichar_t *) N_("Tile _Path..."), (GImage *) "elementtilepath.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Tile Path...|No Shortcut"), NULL, NULL, FVMenuTilePath, MID_TilePath },
    { { (unichar_t *) N_("Tile Pattern..."), (GImage *) "elementtilepattern.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Tile Pattern...|No Shortcut"), NULL, NULL, FVMenuPatternTile, 0 },
#endif
    { { (unichar_t *) N_("O_verlap"), (GImage *) "overlaprm.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'O' }, H_("Overlap|No Shortcut"), rmlist, NULL, NULL, MID_RmOverlap },
    { { (unichar_t *) N_("_Simplify"), (GImage *) "elementsimplify.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'S' }, H_("Simplify|No Shortcut"), smlist, NULL, NULL, MID_Simplify },
    { { (unichar_t *) N_("Add E_xtrema"), (GImage *) "elementaddextrema.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'x' }, H_("Add Extrema|No Shortcut"), NULL, NULL, FVMenuAddExtrema, MID_AddExtrema },
    { { (unichar_t *) N_("Roun_d"), (GImage *) "elementround.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Round|No Shortcut"), rndlist, NULL, NULL, MID_Round },
    { { (unichar_t *) N_("Autot_race"), (GImage *) "elementautotrace.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'r' }, H_("Autotrace|No Shortcut"), NULL, NULL, FVMenuAutotrace, MID_Autotrace },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Correct Direction"), (GImage *) "elementcorrectdir.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'D' }, H_("Correct Direction|No Shortcut"), NULL, NULL, FVMenuCorrectDir, MID_Correct },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("B_uild"), (GImage *) "elementbuildaccent.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Build|No Shortcut"), balist, balistcheck, NULL, MID_BuildAccent },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Merge Fonts..."), (GImage *) "elementmergefonts.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("Merge Fonts...|No Shortcut"), NULL, NULL, FVMenuMergeFonts, MID_MergeFonts },
    { { (unichar_t *) N_("Interpo_late Fonts..."), (GImage *) "elementinterpolatefonts.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'p' }, H_("Interpolate Fonts...|No Shortcut"), NULL, NULL, FVMenuInterpFonts, MID_InterpolateFonts },
    { { (unichar_t *) N_("Compare Fonts..."), (GImage *) "elementcomparefonts.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'p' }, H_("Compare Fonts...|No Shortcut"), NULL, NULL, FVMenuCompareFonts, MID_FontCompare },
    { { (unichar_t *) N_("Compare Layers..."), (GImage *) "elementcomparelayers.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'p' }, H_("Compare Layers...|No Shortcut"), NULL, NULL, FVMenuCompareL2L, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 dummyall[] = {
    { { (unichar_t *) N_("All"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 1, 0, 'K' }, H_("All|No Shortcut"), NULL, NULL, NULL, 0 },
    GMENUITEM2_EMPTY
};

/* Builds up a menu containing all the anchor classes */
static void aplistbuild(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    GMenuItemArrayFree(mi->sub);
    mi->sub = NULL;

    _aplistbuild(mi,fv->b.sf,FVMenuAnchorPairs);
}

static GMenuItem2 cblist[] = {
    { { (unichar_t *) N_("_Kern Pairs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'K' }, H_("Kern Pairs|No Shortcut"), NULL, NULL, FVMenuKernPairs, MID_KernPairs },
    { { (unichar_t *) N_("_Anchored Pairs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'K' }, H_("Anchored Pairs|No Shortcut"), dummyall, aplistbuild, NULL, MID_AnchorPairs },
    { { (unichar_t *) N_("_Ligatures"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'L' }, H_("Ligatures|No Shortcut"), NULL, NULL, FVMenuLigatures, MID_Ligatures },
    GMENUITEM2_EMPTY
};

static void cblistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *sf = fv->b.sf;
    int i, anyligs=0, anykerns=0, gid;
    PST *pst;

    if ( sf->kerns ) anykerns=true;
    for ( i=0; i<fv->b.map->enccount; ++i ) if ( (gid = fv->b.map->map[i])!=-1 && sf->glyphs[gid]!=NULL ) {
	for ( pst=sf->glyphs[gid]->possub; pst!=NULL; pst=pst->next ) {
	    if ( pst->type==pst_ligature ) {
		anyligs = true;
		if ( anykerns )
    break;
	    }
	}
	if ( sf->glyphs[gid]->kerns!=NULL ) {
	    anykerns = true;
	    if ( anyligs )
    break;
	}
    }

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Ligatures:
	    mi->ti.disabled = !anyligs;
	  break;
	  case MID_KernPairs:
	    mi->ti.disabled = !anykerns;
	  break;
	  case MID_AnchorPairs:
	    mi->ti.disabled = sf->anchor==NULL;
	  break;
	}
    }
}


static GMenuItem2 gllist[] = {
    { { (unichar_t *) N_("_Glyph Image"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'K' }, H_("Glyph Image|No Shortcut"), NULL, NULL, FVMenuGlyphLabel, gl_glyph },
    { { (unichar_t *) N_("_Name"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'K' }, H_("Name|No Shortcut"), NULL, NULL, FVMenuGlyphLabel, gl_name },
    { { (unichar_t *) N_("_Unicode"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'L' }, H_("Unicode|No Shortcut"), NULL, NULL, FVMenuGlyphLabel, gl_unicode },
    { { (unichar_t *) N_("_Encoding Hex"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'L' }, H_("Encoding Hex|No Shortcut"), NULL, NULL, FVMenuGlyphLabel, gl_encoding },
    GMENUITEM2_EMPTY
};

static void gllistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	mi->ti.checked = fv->glyphlabel == mi->mid;
    }
}

static GMenuItem2 emptymenu[] = {
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0},
    GMENUITEM2_EMPTY
};

static void FVEncodingMenuBuild(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( mi->sub!=NULL ) {
	GMenuItemArrayFree(mi->sub);
	mi->sub = NULL;
    }
    mi->sub = GetEncodingMenu(FVMenuReencode,fv->b.map->enc);
}

static void FVMenuAddUnencoded(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    char *ret, *end;
    int cnt;

    ret = gwwv_ask_string(_("Add Encoding Slots..."),"1",fv->b.cidmaster?_("How many CID slots do you wish to add?"):_("How many unencoded glyph slots do you wish to add?"));
    if ( ret==NULL )
return;
    cnt = strtol(ret,&end,10);
    if ( *end!='\0' || cnt<=0 ) {
	free(ret);
	ff_post_error( _("Bad Number"),_("Bad Number") );
return;
    }
    free(ret);
    FVAddUnencoded((FontViewBase *) fv, cnt);
}

static void FVMenuRemoveUnused(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVRemoveUnused((FontViewBase *) fv);
}

static void FVMenuCompact(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineChar *sc;

    sc = FVFindACharInDisplay(fv);
    FVCompact((FontViewBase *) fv);
    if ( sc!=NULL ) {
	int enc = fv->b.map->backmap[sc->orig_pos];
	if ( enc!=-1 )
	    FVScrollToChar(fv,enc);
    }
}

static void FVMenuDetachGlyphs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    FVDetachGlyphs((FontViewBase *) fv);
}

static void FVMenuDetachAndRemoveGlyphs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    char *buts[3];

    buts[0] = _("_Remove");
    buts[1] = _("_Cancel");
    buts[2] = NULL;

    if ( gwwv_ask(_("Detach & Remove Glyphs"),(const char **) buts,0,1,_("Are you sure you wish to remove these glyphs? This operation cannot be undone."))==1 )
return;

    FVDetachAndRemoveGlyphs((FontViewBase *) fv);
}

static void FVForceEncodingMenuBuild(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if ( mi->sub!=NULL ) {
	GMenuItemArrayFree(mi->sub);
	mi->sub = NULL;
    }
    mi->sub = GetEncodingMenu(FVMenuForceEncode,fv->b.map->enc);
}

static void FVMenuAddEncodingName(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    char *ret;
    Encoding *enc;

    /* Search the iconv database for the named encoding */
    ret = gwwv_ask_string(_("Add Encoding Name..."),NULL,_("Please provide the name of an encoding in the iconv database which you want in the menu."));
    if ( ret==NULL )
return;
    enc = FindOrMakeEncoding(ret);
    if ( enc==NULL )
	ff_post_error(_("Invalid Encoding"),_("Invalid Encoding"));
    free(ret);
}

static void FVMenuLoadEncoding(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    LoadEncodingFile();
}

static void FVMenuMakeFromFont(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    (void) MakeEncoding(fv->b.sf,fv->b.map);
}

static void FVMenuRemoveEncoding(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    RemoveEncoding();
}

static void FVMenuMakeNamelist(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    char buffer[1025];
    char *filename, *temp;
    FILE *file;

    snprintf(buffer, sizeof(buffer),"%s/%s.nam", getFontForgeUserDir(Config), fv->b.sf->fontname );
    temp = def2utf8_copy(buffer);
    filename = gwwv_save_filename(_("Make Namelist"), temp,"*.nam");
    free(temp);
    if ( filename==NULL )
return;
    temp = utf82def_copy(filename);
    file = fopen(temp,"w");
    free(temp);
    if ( file==NULL ) {
	ff_post_error(_("Namelist creation failed"),_("Could not write %s"), filename);
	free(filename);
return;
    }
    FVB_MakeNamelist((FontViewBase *) fv, file);
    fclose(file);
}

static void FVMenuLoadNamelist(GWindow UNUSED(gw), struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    /* Read in a name list and copy it into the prefs dir so that we'll find */
    /*  it in the future */
    /* Be prepared to update what we've already got if names match */
    char buffer[1025];
    char *ret = gwwv_open_filename(_("Load Namelist"),NULL,
	    "*.nam",NULL);
    char *temp, *pt;
    char *buts[3];
    FILE *old, *new;
    int ch, ans;
    NameList *nl;

    if ( ret==NULL )
return;				/* Cancelled */
    temp = utf82def_copy(ret);
    pt = strrchr(temp,'/');
    if ( pt==NULL )
	pt = temp;
    else
	++pt;
    snprintf(buffer,sizeof(buffer),"%s/%s", getFontForgeUserDir(Config), pt);
    if ( access(buffer,F_OK)==0 ) {
	buts[0] = _("_Replace");
	buts[1] = _("_Cancel");
	buts[2] = NULL;
	ans = gwwv_ask( _("Replace"),(const char **) buts,0,1,_("A name list with this name already exists. Replace it?"));
	if ( ans==1 ) {
	    free(temp);
	    free(ret);
return;
	}
    }

    old = fopen( temp,"r");
    if ( old==NULL ) {
	ff_post_error(_("No such file"),_("Could not read %s"), ret );
	free(ret); free(temp);
return;
    }
    if ( (nl = LoadNamelist(temp))==NULL ) {
	ff_post_error(_("Bad namelist file"),_("Could not parse %s"), ret );
	free(ret); free(temp);
        fclose(old);
return;
    }
    free(ret); free(temp);
    if ( nl->uses_unicode ) {
	if ( nl->a_utf8_name!=NULL )
	    ff_post_notice(_("Non-ASCII glyphnames"),_("This namelist contains at least one non-ASCII glyph name, namely: %s"), nl->a_utf8_name );
	else
	    ff_post_notice(_("Non-ASCII glyphnames"),_("This namelist is based on a namelist which contains non-ASCII glyph names"));
    }

    new = fopen( buffer,"w");
    if ( new==NULL ) {
	ff_post_error(_("Create failed"),_("Could not write %s"), buffer );
        fclose(old);
return;
    }

    while ( (ch=getc(old))!=EOF )
	putc(ch,new);
    fclose(old);
    fclose(new);
}

static void FVMenuRenameByNamelist(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    char **namelists = AllNamelistNames();
    int i;
    int ret;
    NameList *nl;
    extern int allow_utf8_glyphnames;

    for ( i=0; namelists[i]!=NULL; ++i );
    ret = gwwv_choose(_("Rename by NameList"),(const char **) namelists,i,0,_("Rename the glyphs in this font to the names found in the selected namelist"));
    if ( ret==-1 )
return;
    nl = NameListByName(namelists[ret]);
    if ( nl==NULL ) {
	IError("Couldn't find namelist");
return;
    } else if ( nl!=NULL && nl->uses_unicode && !allow_utf8_glyphnames) {
	ff_post_error(_("Namelist contains non-ASCII names"),_("Glyph names should be limited to characters in the ASCII character set, but there are names in this namelist which use characters outside that range."));
return;
    }
    SFRenameGlyphsToNamelist(fv->b.sf,nl);
    GDrawRequestExpose(fv->v,NULL,false);
}

static void FVMenuNameGlyphs(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    /* Read a file containing a list of names, and add an unencoded glyph for */
    /*  each name */
    char buffer[33];
    char *ret = gwwv_open_filename(_("Load glyph names"),NULL, "*",NULL);
    char *temp, *pt;
    FILE *file;
    int ch;
    SplineChar *sc;
    FontView *fvs;

    if ( ret==NULL )
return;				/* Cancelled */
    temp = utf82def_copy(ret);

    file = fopen( temp,"r");
    if ( file==NULL ) {
	ff_post_error(_("No such file"),_("Could not read %s"), ret );
	free(ret); free(temp);
return;
    }
    pt = buffer;
    for (;;) {
	ch = getc(file);
	if ( ch!=EOF && !isspace(ch)) {
	    if ( pt<buffer+sizeof(buffer)-1 )
		*pt++ = ch;
	} else {
	    if ( pt!=buffer ) {
		*pt = '\0';
		sc = NULL;
		for ( fvs=(FontView *) (fv->b.sf->fv); fvs!=NULL; fvs=(FontView *) (fvs->b.nextsame) ) {
		    EncMap *map = fvs->b.map;
		    if ( map->enccount+1>=map->encmax )
			map->map = realloc(map->map,(map->encmax += 20)*sizeof(int));
		    map->map[map->enccount] = -1;
		    fvs->b.selected = realloc(fvs->b.selected,(map->enccount+1));
		    memset(fvs->b.selected+map->enccount,0,1);
		    ++map->enccount;
		    if ( sc==NULL ) {
			sc = SFMakeChar(fv->b.sf,map,map->enccount-1);
			free(sc->name);
			sc->name = copy(buffer);
			sc->comment = copy(".");	/* Mark as something for sfd file */
		    }
		    map->map[map->enccount-1] = sc->orig_pos;
		    map->backmap[sc->orig_pos] = map->enccount-1;
		}
		pt = buffer;
	    }
	    if ( ch==EOF )
    break;
	}
    }
    fclose(file);
    free(ret); free(temp);
    FontViewReformatAll(fv->b.sf);
}

static GMenuItem2 enlist[] = {
    { { (unichar_t *) N_("_Reencode"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'E' }, H_("Reencode|No Shortcut"), emptymenu, FVEncodingMenuBuild, NULL, MID_Reencode },
    { { (unichar_t *) N_("_Compact"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'C' }, H_("Compact|No Shortcut"), NULL, NULL, FVMenuCompact, MID_Compact },
    { { (unichar_t *) N_("_Force Encoding"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Force Encoding|No Shortcut"), emptymenu, FVForceEncodingMenuBuild, NULL, MID_ForceReencode },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Add Encoding Slots..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Add Encoding Slots...|No Shortcut"), NULL, NULL, FVMenuAddUnencoded, MID_AddUnencoded },
    { { (unichar_t *) N_("Remove _Unused Slots"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Remove Unused Slots|No Shortcut"), NULL, NULL, FVMenuRemoveUnused, MID_RemoveUnused },
    { { (unichar_t *) N_("_Detach Glyphs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Detach Glyphs|No Shortcut"), NULL, NULL, FVMenuDetachGlyphs, MID_DetachGlyphs },
    { { (unichar_t *) N_("Detach & Remo_ve Glyphs..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Detach & Remove Glyphs...|No Shortcut"), NULL, NULL, FVMenuDetachAndRemoveGlyphs, MID_DetachAndRemoveGlyphs },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Add E_ncoding Name..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Add Encoding Name...|No Shortcut"), NULL, NULL, FVMenuAddEncodingName, 0 },
    { { (unichar_t *) N_("_Load Encoding..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Load Encoding...|No Shortcut"), NULL, NULL, FVMenuLoadEncoding, MID_LoadEncoding },
    { { (unichar_t *) N_("Ma_ke From Font..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Make From Font...|No Shortcut"), NULL, NULL, FVMenuMakeFromFont, MID_MakeFromFont },
    { { (unichar_t *) N_("Remove En_coding..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Remove Encoding...|No Shortcut"), NULL, NULL, FVMenuRemoveEncoding, MID_RemoveEncoding },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Display By _Groups..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Display By Groups...|No Shortcut"), NULL, NULL, FVMenuDisplayByGroups, MID_DisplayByGroups },
    { { (unichar_t *) N_("D_efine Groups..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Define Groups...|No Shortcut"), NULL, NULL, FVMenuDefineGroups, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Save Namelist of Font..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Save Namelist of Font...|No Shortcut"), NULL, NULL, FVMenuMakeNamelist, MID_SaveNamelist },
    { { (unichar_t *) N_("L_oad Namelist..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Load Namelist...|No Shortcut"), NULL, NULL, FVMenuLoadNamelist, 0 },
    { { (unichar_t *) N_("Rename Gl_yphs..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Rename Glyphs...|No Shortcut"), NULL, NULL, FVMenuRenameByNamelist, MID_RenameGlyphs },
    { { (unichar_t *) N_("Cre_ate Named Glyphs..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Create Named Glyphs...|No Shortcut"), NULL, NULL, FVMenuNameGlyphs, MID_NameGlyphs },
    GMENUITEM2_EMPTY
};

static void enlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, gid;
    SplineFont *sf = fv->b.sf;
    EncMap *map = fv->b.map;
    int anyglyphs = false;

    for ( i=map->enccount-1; i>=0 ; --i )
	if ( fv->b.selected[i] && (gid=map->map[i])!=-1 )
	    anyglyphs = true;

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Compact:
	    mi->ti.checked = fv->b.normal!=NULL;
	  break;
	case MID_HideNoGlyphSlots:
	    break;
	  case MID_Reencode: case MID_ForceReencode:
	    mi->ti.disabled = fv->b.cidmaster!=NULL;
	  break;
	  case MID_DetachGlyphs: case MID_DetachAndRemoveGlyphs:
	    mi->ti.disabled = !anyglyphs;
	  break;
	  case MID_RemoveUnused:
	    gid = map->enccount>0 ? map->map[map->enccount-1] : -1;
	    mi->ti.disabled = gid!=-1 && SCWorthOutputting(sf->glyphs[gid]);
	  break;
	  case MID_MakeFromFont:
	    mi->ti.disabled = fv->b.cidmaster!=NULL || map->enccount>1024 || map->enc!=&custom;
	  break;
	  case MID_RemoveEncoding:
	  break;
	  case MID_DisplayByGroups:
	    mi->ti.disabled = fv->b.cidmaster!=NULL || group_root==NULL;
	  break;
	  case MID_NameGlyphs:
	    mi->ti.disabled = fv->b.normal!=NULL || fv->b.cidmaster!=NULL;
	  break;
	  case MID_RenameGlyphs: case MID_SaveNamelist:
	    mi->ti.disabled = fv->b.cidmaster!=NULL;
	  break;
	}
    }
}

static GMenuItem2 lylist[] = {
    { { (unichar_t *) N_("Layer|Foreground"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, 1, 0, '\0' }, NULL, NULL, NULL, FVMenuChangeLayer, ly_fore },
    GMENUITEM2_EMPTY
};

static void lylistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    SplineFont *sf = fv->b.sf;
    int ly;
    GMenuItem *sub;

    sub = calloc(sf->layer_cnt+1,sizeof(GMenuItem));
    for ( ly=ly_fore; ly<sf->layer_cnt; ++ly ) {
	sub[ly-1].ti.text = utf82u_copy(sf->layers[ly].name);
	sub[ly-1].ti.checkable = true;
	sub[ly-1].ti.checked = ly == fv->b.active_layer;
	sub[ly-1].invoke = FVMenuChangeLayer;
	sub[ly-1].mid = ly;
	sub[ly-1].ti.fg = sub[ly-1].ti.bg = COLOR_DEFAULT;
    }
    GMenuItemArrayFree(mi->sub);
    mi->sub = sub;
}

static GMenuItem2 vwlist[] = {
    { { (unichar_t *) N_("_Next Glyph"), (GImage *) "viewnext.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'N' }, H_("Next Glyph|No Shortcut"), NULL, NULL, FVMenuChangeChar, MID_Next },
    { { (unichar_t *) N_("_Prev Glyph"), (GImage *) "viewprev.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Prev Glyph|No Shortcut"), NULL, NULL, FVMenuChangeChar, MID_Prev },
    { { (unichar_t *) N_("Next _Defined Glyph"), (GImage *) "viewnextdef.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'D' }, H_("Next Defined Glyph|No Shortcut"), NULL, NULL, FVMenuChangeChar, MID_NextDef },
    { { (unichar_t *) N_("Prev Defined Gl_yph"), (GImage *) "viewprevdef.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'a' }, H_("Prev Defined Glyph|No Shortcut"), NULL, NULL, FVMenuChangeChar, MID_PrevDef },
    { { (unichar_t *) N_("_Goto"), (GImage *) "viewgoto.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'G' }, H_("Goto|No Shortcut"), NULL, NULL, FVMenuGotoChar, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Layers"), (GImage *) "viewlayers.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Layers|No Shortcut"), lylist, lylistcheck, NULL, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Show ATT"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'S' }, H_("Show ATT|No Shortcut"), NULL, NULL, FVMenuShowAtt, 0 },
    { { (unichar_t *) N_("Display S_ubstitutions..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'u' }, H_("Display Substitutions...|No Shortcut"), NULL, NULL, FVMenuDisplaySubs, MID_DisplaySubs },
    { { (unichar_t *) N_("Com_binations"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'b' }, H_("Combinations|No Shortcut"), cblist, cblistcheck, NULL, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Label Gl_yph By"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'b' }, H_("Label Glyph By|No Shortcut"), gllist, gllistcheck, NULL, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("S_how H. Metrics..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Show H. Metrics...|No Shortcut"), NULL, NULL, FVMenuShowMetrics, MID_ShowHMetrics },
    { { (unichar_t *) N_("Show _V. Metrics..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, H_("Show V. Metrics...|No Shortcut"), NULL, NULL, FVMenuShowMetrics, MID_ShowVMetrics },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("32x8 cell window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '2' }, H_("32x8 cell window|No Shortcut"), NULL, NULL, FVMenuWSize, MID_32x8 },
    { { (unichar_t *) N_("_16x4 cell window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '3' }, H_("16x4 cell window|No Shortcut"), NULL, NULL, FVMenuWSize, MID_16x4 },
    { { (unichar_t *) N_("_8x2  cell window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '3' }, H_("8x2  cell window|No Shortcut"), NULL, NULL, FVMenuWSize, MID_8x2 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_24 pixel outline"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '2' }, H_("24 pixel outline|No Shortcut"), NULL, NULL, FVMenuSize, MID_24 },
    { { (unichar_t *) N_("_36 pixel outline"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '3' }, H_("36 pixel outline|No Shortcut"), NULL, NULL, FVMenuSize, MID_36 },
    { { (unichar_t *) N_("_48 pixel outline"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '4' }, H_("48 pixel outline|No Shortcut"), NULL, NULL, FVMenuSize, MID_48 },
    { { (unichar_t *) N_("_72 pixel outline"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '4' }, H_("72 pixel outline|No Shortcut"), NULL, NULL, FVMenuSize, MID_72 },
    { { (unichar_t *) N_("_96 pixel outline"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '4' }, H_("96 pixel outline|No Shortcut"), NULL, NULL, FVMenuSize, MID_96 },
    { { (unichar_t *) N_("_128 pixel outline"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, '4' }, H_("128 pixel outline|No Shortcut"), NULL, NULL, FVMenuSize, MID_128 },
    { { (unichar_t *) N_("_Anti Alias"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'A' }, H_("Anti Alias|No Shortcut"), NULL, NULL, FVMenuSize, MID_AntiAlias },
    { { (unichar_t *) N_("_Fit to font bounding box"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'F' }, H_("Fit to em|No Shortcut"), NULL, NULL, FVMenuSize, MID_FitToBbox },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Bitmap _Magnification..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 1, 0, 0, 0, 1, 1, 0, 'F' }, H_("Bitmap Magnification...|No Shortcut"), NULL, NULL, FVMenuMagnify, MID_BitmapMag },
    GMENUITEM2_EMPTY,			/* Some extra room to show bitmaps */
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY
};

static void vwlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);
    int i, base;
    BDFFont *bdf;
    char buffer[50];
    int pos;
    SplineFont *sf = fv->b.sf;
    SplineFont *master = sf->cidmaster ? sf->cidmaster : sf;
    EncMap *map = fv->b.map;
    OTLookup *otl;

    for ( i=0; vwlist[i].ti.text==NULL || strcmp((char *) vwlist[i].ti.text, _("Bitmap _Magnification..."))!=0; ++i );
    base = i+1;
    for ( i=base; vwlist[i].ti.text!=NULL; ++i ) {
	free( vwlist[i].ti.text);
	vwlist[i].ti.text = NULL;
    }

    vwlist[base-1].ti.disabled = true;
    if ( master->bitmaps!=NULL ) {
	for ( bdf = master->bitmaps, i=base;
		i<sizeof(vwlist)/sizeof(vwlist[0])-1 && bdf!=NULL;
		++i, bdf = bdf->next ) {
	    if ( BDFDepth(bdf)==1 )
		sprintf( buffer, _("%d pixel bitmap"), bdf->pixelsize );
	    else
		sprintf( buffer, _("%d@%d pixel bitmap"),
			bdf->pixelsize, BDFDepth(bdf) );
	    vwlist[i].ti.text = (unichar_t *) utf82u_copy(buffer);
	    vwlist[i].ti.checkable = true;
	    vwlist[i].ti.checked = bdf==fv->show;
	    vwlist[i].ti.userdata = bdf;
	    vwlist[i].invoke = FVMenuShowBitmap;
	    vwlist[i].ti.fg = vwlist[i].ti.bg = COLOR_DEFAULT;
	    if ( bdf==fv->show )
		vwlist[base-1].ti.disabled = false;
	}
    }
    GMenuItemArrayFree(mi->sub);
    mi->sub = GMenuItem2ArrayCopy(vwlist,NULL);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Next: case MID_Prev:
	    mi->ti.disabled = anychars<0;
	  break;
	  case MID_NextDef:
	    pos = anychars+1;
	    if ( anychars<0 ) pos = map->enccount;
	    for ( ; pos<map->enccount &&
		    (map->map[pos]==-1 || !SCWorthOutputting(sf->glyphs[map->map[pos]]));
		    ++pos );
	    mi->ti.disabled = pos==map->enccount;
	  break;
	  case MID_PrevDef:
	    for ( pos = anychars-1; pos>=0 &&
		    (map->map[pos]==-1 || !SCWorthOutputting(sf->glyphs[map->map[pos]]));
		    --pos );
	    mi->ti.disabled = pos<0;
	  break;
	  case MID_DisplaySubs: { SplineFont *_sf = sf;
	    mi->ti.checked = fv->cur_subtable!=NULL;
	    if ( _sf->cidmaster ) _sf = _sf->cidmaster;
	    for ( otl=_sf->gsub_lookups; otl!=NULL; otl=otl->next )
		if ( otl->lookup_type == gsub_single && otl->subtables!=NULL )
	    break;
	    mi->ti.disabled = otl==NULL;
	  } break;
	  case MID_ShowHMetrics:
	  break;
	  case MID_ShowVMetrics:
	    mi->ti.disabled = !sf->hasvmetrics;
	  break;
	  case MID_32x8:
	    mi->ti.checked = (fv->rowcnt==8 && fv->colcnt==32);
	    mi->ti.disabled = fv->b.container!=NULL;
	  break;
	  case MID_16x4:
	    mi->ti.checked = (fv->rowcnt==4 && fv->colcnt==16);
	    mi->ti.disabled = fv->b.container!=NULL;
	  break;
	  case MID_8x2:
	    mi->ti.checked = (fv->rowcnt==2 && fv->colcnt==8);
	    mi->ti.disabled = fv->b.container!=NULL;
	  break;
	  case MID_24:
	    mi->ti.checked = (fv->show!=NULL && fv->show==fv->filled && fv->show->pixelsize==24);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_36:
	    mi->ti.checked = (fv->show!=NULL && fv->show==fv->filled && fv->show->pixelsize==36);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_48:
	    mi->ti.checked = (fv->show!=NULL && fv->show==fv->filled && fv->show->pixelsize==48);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_72:
	    mi->ti.checked = (fv->show!=NULL && fv->show==fv->filled && fv->show->pixelsize==72);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_96:
	    mi->ti.checked = (fv->show!=NULL && fv->show==fv->filled && fv->show->pixelsize==96);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_128:
	    mi->ti.checked = (fv->show!=NULL && fv->show==fv->filled && fv->show->pixelsize==128);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_AntiAlias:
	    mi->ti.checked = (fv->show!=NULL && fv->show->clut!=NULL);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_FitToBbox:
	    mi->ti.checked = (fv->show!=NULL && fv->show->bbsized);
	    mi->ti.disabled = sf->onlybitmaps && fv->show!=fv->filled;
	  break;
	  case MID_Layers:
	    mi->ti.disabled = sf->layer_cnt<=2 || sf->multilayer;
	  break;
	}
    }
}

static GMenuItem2 histlist[] = {
    { { (unichar_t *) N_("_HStem"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("HStem|No Shortcut"), NULL, NULL, FVMenuHistograms, MID_HStemHist },
    { { (unichar_t *) N_("_VStem"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, H_("VStem|No Shortcut"), NULL, NULL, FVMenuHistograms, MID_VStemHist },
    { { (unichar_t *) N_("BlueValues"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("BlueValues|No Shortcut"), NULL, NULL, FVMenuHistograms, MID_BlueValuesHist },
    GMENUITEM2_EMPTY
};

static GMenuItem2 htlist[] = {
    { { (unichar_t *) N_("Auto_Hint"), (GImage *) "hintsautohint.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("AutoHint|No Shortcut"), NULL, NULL, FVMenuAutoHint, MID_AutoHint },
    { { (unichar_t *) N_("Hint _Substitution Pts"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Hint Substitution Pts|No Shortcut"), NULL, NULL, FVMenuAutoHintSubs, MID_HintSubsPt },
    { { (unichar_t *) N_("Auto _Counter Hint"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Auto Counter Hint|No Shortcut"), NULL, NULL, FVMenuAutoCounter, MID_AutoCounter },
    { { (unichar_t *) N_("_Don't AutoHint"), (GImage *) "hintsdontautohint.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Don't AutoHint|No Shortcut"), NULL, NULL, FVMenuDontAutoHint, MID_DontAutoHint },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Auto_Instr"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("AutoInstr|No Shortcut"), NULL, NULL, FVMenuAutoInstr, MID_AutoInstr },
    { { (unichar_t *) N_("_Edit Instructions..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Edit Instructions...|No Shortcut"), NULL, NULL, FVMenuEditInstrs, MID_EditInstructions },
    { { (unichar_t *) N_("Edit 'fpgm'..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Edit 'fpgm'...|No Shortcut"), NULL, NULL, FVMenuEditTable, MID_Editfpgm },
    { { (unichar_t *) N_("Edit 'prep'..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Edit 'prep'...|No Shortcut"), NULL, NULL, FVMenuEditTable, MID_Editprep },
    { { (unichar_t *) N_("Edit 'maxp'..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Edit 'maxp'...|No Shortcut"), NULL, NULL, FVMenuEditTable, MID_Editmaxp },
    { { (unichar_t *) N_("Edit 'cvt '..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Edit 'cvt '...|No Shortcut"), NULL, NULL, FVMenuEditTable, MID_Editcvt },
    { { (unichar_t *) N_("Remove Instr Tables"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Remove Instr Tables|No Shortcut"), NULL, NULL, FVMenuRmInstrTables, MID_RmInstrTables },
    { { (unichar_t *) N_("S_uggest Deltas..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Suggest Deltas|No Shortcut"), NULL, NULL, FVMenuDeltas, MID_Deltas },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Clear Hints"), (GImage *) "hintsclearvstems.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Clear Hints|No Shortcut"), NULL, NULL, FVMenuClearHints, MID_ClearHints },
    { { (unichar_t *) N_("Clear Instructions"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Clear Instructions|No Shortcut"), NULL, NULL, FVMenuClearInstrs, MID_ClearInstrs },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Histograms"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("Histograms|No Shortcut"), histlist, NULL, NULL, 0 },
    GMENUITEM2_EMPTY
};

static GMenuItem2 mtlist[] = {
    { { (unichar_t *) N_("New _Metrics Window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("New Metrics Window|No Shortcut"), NULL, NULL, FVMenuOpenMetrics, MID_OpenMetrics },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Center in Width"), (GImage *) "metricscenter.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Center in Width|No Shortcut"), NULL, NULL, FVMenuCenter, MID_Center },
    { { (unichar_t *) N_("_Thirds in Width"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, H_("Thirds in Width|No Shortcut"), NULL, NULL, FVMenuCenter, MID_Thirds },
    { { (unichar_t *) N_("Set _Width..."), (GImage *) "metricssetwidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'W' }, H_("Set Width...|No Shortcut"), NULL, NULL, FVMenuSetWidth, MID_SetWidth },
    { { (unichar_t *) N_("Set _LBearing..."), (GImage *) "metricssetlbearing.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'L' }, H_("Set LBearing...|No Shortcut"), NULL, NULL, FVMenuSetWidth, MID_SetLBearing },
    { { (unichar_t *) N_("Set _RBearing..."), (GImage *) "metricssetrbearing.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Set RBearing...|No Shortcut"), NULL, NULL, FVMenuSetWidth, MID_SetRBearing },
    { { (unichar_t *) N_("Set Both Bearings..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Set Both Bearings...|No Shortcut"), NULL, NULL, FVMenuSetWidth, MID_SetBearings },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Set _Vertical Advance..."), (GImage *) "metricssetvwidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, H_("Set Vertical Advance...|No Shortcut"), NULL, NULL, FVMenuSetWidth, MID_SetVWidth },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Auto Width..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'A' }, H_("Auto Width...|No Shortcut"), NULL, NULL, FVMenuAutoWidth, 0 },
    { { (unichar_t *) N_("Ker_n By Classes..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'K' }, H_("Kern By Classes...|No Shortcut"), NULL, NULL, FVMenuKernByClasses, 0 },
    { { (unichar_t *) N_("Remove All Kern _Pairs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Remove All Kern Pairs|No Shortcut"), NULL, NULL, FVMenuRemoveKern, MID_RmHKern },
    { { (unichar_t *) N_("Kern Pair Closeup..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Kern Pair Closeup...|No Shortcut"), NULL, NULL, FVMenuKPCloseup, 0 },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("VKern By Classes..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'K' }, H_("VKern By Classes...|No Shortcut"), NULL, NULL, FVMenuVKernByClasses, MID_VKernByClass },
    { { (unichar_t *) N_("VKern From HKern"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("VKern From HKern|No Shortcut"), NULL, NULL, FVMenuVKernFromHKern, MID_VKernFromH },
    { { (unichar_t *) N_("Remove All VKern Pairs"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, H_("Remove All VKern Pairs|No Shortcut"), NULL, NULL, FVMenuRemoveVKern, MID_RmVKern },
    GMENUITEM2_EMPTY
};

static GMenuItem2 cdlist[] = {
    { { (unichar_t *) N_("_Convert to CID"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Convert to CID|No Shortcut"), NULL, NULL, FVMenuConvert2CID, MID_Convert2CID },
    { { (unichar_t *) N_("Convert By C_Map"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("Convert By CMap|No Shortcut"), NULL, NULL, FVMenuConvertByCMap, MID_ConvertByCMap },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Flatten"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("Flatten|No Shortcut"), NULL, NULL, FVMenuFlatten, MID_Flatten },
    { { (unichar_t *) N_("Fl_attenByCMap"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("FlattenByCMap|No Shortcut"), NULL, NULL, FVMenuFlattenByCMap, MID_FlattenByCMap },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Insert F_ont..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, H_("Insert Font...|No Shortcut"), NULL, NULL, FVMenuInsertFont, MID_InsertFont },
    { { (unichar_t *) N_("Insert _Blank"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("Insert Blank|No Shortcut"), NULL, NULL, FVMenuInsertBlank, MID_InsertBlank },
    { { (unichar_t *) N_("_Remove Font"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'R' }, H_("Remove Font|No Shortcut"), NULL, NULL, FVMenuRemoveFontFromCID, MID_RemoveFromCID },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Change Supplement..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Change Supplement...|No Shortcut"), NULL, NULL, FVMenuChangeSupplement, MID_ChangeSupplement },
    { { (unichar_t *) N_("C_ID Font Info..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("CID Font Info...|No Shortcut"), NULL, NULL, FVMenuCIDFontInfo, MID_CIDFontInfo },
    GMENUITEM2_EMPTY,				/* Extra room to show sub-font names */
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY, GMENUITEM2_EMPTY,
    GMENUITEM2_EMPTY
};

static void cdlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, base, j;
    SplineFont *sub, *cidmaster = fv->b.cidmaster;

    for ( i=0; cdlist[i].mid!=MID_CIDFontInfo; ++i );
    base = i+2;
    for ( i=base; cdlist[i].ti.text!=NULL; ++i ) {
	free( cdlist[i].ti.text);
	cdlist[i].ti.text = NULL;
    }

    cdlist[base-1].ti.fg = cdlist[base-1].ti.bg = COLOR_DEFAULT;
    if ( cidmaster==NULL ) {
	cdlist[base-1].ti.line = false;
    } else {
	cdlist[base-1].ti.line = true;
	for ( j = 0, i=base;
		i<sizeof(cdlist)/sizeof(cdlist[0])-1 && j<cidmaster->subfontcnt;
		++i, ++j ) {
	    sub = cidmaster->subfonts[j];
	    cdlist[i].ti.text = uc_copy(sub->fontname);
	    cdlist[i].ti.checkable = true;
	    cdlist[i].ti.checked = sub==fv->b.sf;
	    cdlist[i].ti.userdata = sub;
	    cdlist[i].invoke = FVMenuShowSubFont;
	    cdlist[i].ti.fg = cdlist[i].ti.bg = COLOR_DEFAULT;
	}
    }
    GMenuItemArrayFree(mi->sub);
    mi->sub = GMenuItem2ArrayCopy(cdlist,NULL);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_Convert2CID: case MID_ConvertByCMap:
	    mi->ti.disabled = cidmaster!=NULL || fv->b.sf->mm!=NULL;
	  break;
	  case MID_InsertFont: case MID_InsertBlank:
	    /* OpenType allows at most 255 subfonts (PS allows more, but why go to the effort to make safe font check that? */
	    mi->ti.disabled = cidmaster==NULL || cidmaster->subfontcnt>=255;
	  break;
	  case MID_RemoveFromCID:
	    mi->ti.disabled = cidmaster==NULL || cidmaster->subfontcnt<=1;
	  break;
	  case MID_Flatten: case MID_FlattenByCMap: case MID_CIDFontInfo:
	  case MID_ChangeSupplement:
	    mi->ti.disabled = cidmaster==NULL;
	  break;
	}
    }
}

static GMenuItem2 mmlist[] = {
/* GT: Here (and following) MM means "MultiMaster" */
    { { (unichar_t *) N_("_Create MM..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Create MM...|No Shortcut"), NULL, NULL, FVMenuCreateMM, MID_CreateMM },
    { { (unichar_t *) N_("MM _Validity Check"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("MM Validity Check|No Shortcut"), NULL, NULL, FVMenuMMValid, MID_MMValid },
    { { (unichar_t *) N_("MM _Info..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("MM Info...|No Shortcut"), NULL, NULL, FVMenuMMInfo, MID_MMInfo },
    { { (unichar_t *) N_("_Blend to New Font..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Blend to New Font...|No Shortcut"), NULL, NULL, FVMenuBlendToNew, MID_BlendToNew },
    { { (unichar_t *) N_("MM Change Default _Weights..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("MM Change Default Weights...|No Shortcut"), NULL, NULL, FVMenuChangeMMBlend, MID_ChangeMMBlend },
    GMENUITEM2_EMPTY,				/* Extra room to show sub-font names */
};

static void mmlistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e)) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int i, base, j;
    MMSet *mm = fv->b.sf->mm;
    SplineFont *sub;
    GMenuItem2 *mml;

    for ( i=0; mmlist[i].mid!=MID_ChangeMMBlend; ++i );
    base = i+2;
    if ( mm==NULL )
	mml = mmlist;
    else {
	mml = calloc(base+mm->instance_count+2,sizeof(GMenuItem2));
	memcpy(mml,mmlist,sizeof(mmlist));
	mml[base-1].ti.fg = mml[base-1].ti.bg = COLOR_DEFAULT;
	mml[base-1].ti.line = true;
	for ( j = 0, i=base; j<mm->instance_count+1; ++i, ++j ) {
	    if ( j==0 )
		sub = mm->normal;
	    else
		sub = mm->instances[j-1];
	    mml[i].ti.text = uc_copy(sub->fontname);
	    mml[i].ti.checkable = true;
	    mml[i].ti.checked = sub==fv->b.sf;
	    mml[i].ti.userdata = sub;
	    mml[i].invoke = FVMenuShowSubFont;
	    mml[i].ti.fg = mml[i].ti.bg = COLOR_DEFAULT;
	}
    }
    GMenuItemArrayFree(mi->sub);
    mi->sub = GMenuItem2ArrayCopy(mml,NULL);
    if ( mml!=mmlist ) {
	for ( i=base; mml[i].ti.text!=NULL; ++i )
	    free( mml[i].ti.text);
	free(mml);
    }

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi ) {
	switch ( mi->mid ) {
	  case MID_CreateMM:
	    mi->ti.disabled = false;
	  break;
	  case MID_MMInfo: case MID_MMValid: case MID_BlendToNew:
	    mi->ti.disabled = mm==NULL;
	  break;
	  case MID_ChangeMMBlend:
	    mi->ti.disabled = mm==NULL || mm->apple;
	  break;
	}
    }
}

static GMenuItem2 wnmenu[] = {
    { { (unichar_t *) N_("New O_utline Window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'u' }, H_("New Outline Window|No Shortcut"), NULL, NULL, FVMenuOpenOutline, MID_OpenOutline },
    { { (unichar_t *) N_("New _Bitmap Window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'B' }, H_("New Bitmap Window|No Shortcut"), NULL, NULL, FVMenuOpenBitmap, MID_OpenBitmap },
    { { (unichar_t *) N_("New _Metrics Window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("New Metrics Window|No Shortcut"), NULL, NULL, FVMenuOpenMetrics, MID_OpenMetrics },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#ifndef CKS	// HYMODIFY :: 2016.02016
    { { (unichar_t *) N_("Warnings"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("Warnings|No Shortcut"), NULL, NULL, _MenuWarnings, MID_Warnings },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, NULL, NULL, NULL, NULL, 0 }, /* line */
#endif
    GMENUITEM2_EMPTY
};

static void FVWindowMenuBuild(GWindow gw, struct gmenuitem *mi, GEvent *e) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    int anychars = FVAnyCharSelected(fv);
    struct gmenuitem *wmi;
    int in_modal = (fv->b.container!=NULL && fv->b.container->funcs->is_modal);

    WindowMenuBuild(gw,mi,e);
    for ( wmi = mi->sub; wmi->ti.text!=NULL || wmi->ti.line ; ++wmi ) {
	switch ( wmi->mid ) {
	  case MID_OpenOutline:
	    wmi->ti.disabled = anychars==-1 || in_modal;
	  break;
	  case MID_OpenBitmap:
	    wmi->ti.disabled = anychars==-1 || fv->b.sf->bitmaps==NULL || in_modal;
	  break;
	  case MID_OpenMetrics:
	    wmi->ti.disabled = in_modal;
	  break;
	  case MID_Warnings:
	    wmi->ti.disabled = ErrorWindowExists();
	  break;
	}
    }
}

#ifdef BUILD_COLLAB
static void FVMenuCollabStart(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e))
{
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    printf("connecting to server and sending initial SFD to it...\n");

    int port_default = 5556;
    int port = port_default;
    char address[IPADDRESS_STRING_LENGTH_T];
    if( !getNetworkAddress( address ))
    {
	snprintf( address, IPADDRESS_STRING_LENGTH_T-1,
		  "%s", HostPortPack( "127.0.0.1", port ));
    }
    else
    {
	snprintf( address, IPADDRESS_STRING_LENGTH_T-1,
		  "%s", HostPortPack( address, port ));
    }

    printf("host address:%s\n",address);

    char* res = gwwv_ask_string(
	"Starting Collab Server",
	address,
	"FontForge has determined that your computer can be accessed"
	" using the below address. Share that address with other people"
	" who you wish to collaborate with...\n\nPress OK to start the collaboration server...");

    if( res )
    {
	printf("res:%s\n", res );
	strncpy( address, res, IPADDRESS_STRING_LENGTH_T );
	HostPortUnpack( address, &port, port_default );

	printf("address:%s\n", address );
	printf("port:%d\n", port );

	void* cc = collabclient_new( address, port );
	fv->b.collabClient = cc;
	collabclient_sessionStart( cc, fv );
	printf("connecting to server...sent the sfd for session start.\n");
    }
}
#endif

static int collab_MakeChoicesArray( GHashTable* peers, char** choices, int choices_sz, int localOnly )
{
    GHashTableIter iter;
    gpointer key, value;
    int lastidx = 0;
    memset( choices, 0, sizeof(char*) * choices_sz );

    int i=0;
    int maxUserNameLength = 1;
    g_hash_table_iter_init (&iter, peers);
    for( i=0; g_hash_table_iter_next (&iter, &key, &value); i++ )
    {
	beacon_announce_t* ba = (beacon_announce_t*)value;
	maxUserNameLength = imax( maxUserNameLength, strlen(ba->username) );
    }

    g_hash_table_iter_init (&iter, peers);
    for( i=0; g_hash_table_iter_next (&iter, &key, &value); i++ )
    {
	beacon_announce_t* ba = (beacon_announce_t*)value;
	if( localOnly && !collabclient_isAddressLocal( ba->ip ))
	    continue;

	printf("user:%s\n", ba->username );
	printf("mach:%s\n", ba->machinename );

	char buf[101];
	if( localOnly )
	{
	    snprintf( buf, 100, "%s", ba->fontname );
	}
	else
	{
	    char format[50];
	    sprintf( format, "%s %%%d", "%s", maxUserNameLength );
	    strcat( format, "s %s");
	    snprintf( buf, 100, format, ba->fontname, ba->username, ba->machinename );
	}
	choices[i] = copy( buf );
	if( i >= choices_sz )
	    break;
	lastidx++;
    }

    return lastidx;
}

static beacon_announce_t* collab_getBeaconFromChoicesArray( GHashTable* peers, int choice, int localOnly )
{
    GHashTableIter iter;
    gpointer key, value;
    int i=0;

    g_hash_table_iter_init (&iter, peers);
    for( i=0; g_hash_table_iter_next (&iter, &key, &value); i++ )
    {
	beacon_announce_t* ba = (beacon_announce_t*)value;
	if( localOnly && !collabclient_isAddressLocal( ba->ip ))
	    continue;
	if( i != choice )
	    continue;
	return ba;
    }
    return 0;
}


#ifdef BUILD_COLLAB
static void FVMenuCollabConnect(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e))
{
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    printf("connecting to server...\n");

    {
	int choices_sz = 100;
	char* choices[101];
	memset( choices, 0, sizeof(choices));

	collabclient_trimOldBeaconInformation( 0 );
	GHashTable* peers = collabclient_getServersFromBeaconInfomration();
	int localOnly = 0;
	int max = collab_MakeChoicesArray( peers, choices, choices_sz, localOnly );
	int choice = gwwv_choose(_("Connect to Collab Server"),(const char **) choices, max,
				 0,_("Select a collab server to connect to..."));
	printf("you wanted %d\n", choice );
	if( choice <= max )
	{
	    beacon_announce_t* ba = collab_getBeaconFromChoicesArray( peers, choice, localOnly );

	    if( ba )
	    {
		int port = ba->port;
		char address[IPADDRESS_STRING_LENGTH_T];
		strncpy( address, ba->ip, IPADDRESS_STRING_LENGTH_T-1 );
		void* cc = collabclient_new( address, port );
		fv->b.collabClient = cc;
		collabclient_sessionJoin( cc, fv );
	    }
	}
    }

    printf("FVMenuCollabConnect(done)\n");
}

static void FVMenuCollabConnectToExplicitAddress(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e))
{
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    printf("********** connecting to server... explicit address... p:%p\n", pref_collab_last_server_connected_to);

    char* default_server = "localhost";
    if( pref_collab_last_server_connected_to ) {
	default_server = pref_collab_last_server_connected_to;
    }
    
    char* res = gwwv_ask_string(
    	"Connect to Collab Server",
    	default_server,
    	"Please enter the network location of the Collab server you wish to connect to...");
    if( res )
    {
	if( pref_collab_last_server_connected_to ) {
	    free( pref_collab_last_server_connected_to );
	}
	pref_collab_last_server_connected_to = copy( res );
	SavePrefs(true);
	
    	int port_default = 5556;
    	int port = port_default;
    	char address[IPADDRESS_STRING_LENGTH_T];
    	strncpy( address, res, IPADDRESS_STRING_LENGTH_T-1 );
    	HostPortUnpack( address, &port, port_default );

    	void* cc = collabclient_new( address, port );
    	fv->b.collabClient = cc;
    	collabclient_sessionJoin( cc, fv );
    }

    printf("FVMenuCollabConnect(done)\n");
}

static void FVMenuCollabDisconnect(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e))
{
    FontView *fv = (FontView *) GDrawGetUserData(gw);
    collabclient_sessionDisconnect( &fv->b );
}
#endif

static void AskAndMaybeCloseLocalCollabServers()
{
    char *buts[3];
    buts[0] = _("_OK");
    buts[1] = _("_Cancel");
    buts[2] = NULL;

    int i=0;
    int choices_sz = 100;
    char* choices[101];
    collabclient_trimOldBeaconInformation( 0 );
    GHashTable* peers = collabclient_getServersFromBeaconInfomration();
    if( !peers )
	return;
    
    int localOnly = 1;
    int max = collab_MakeChoicesArray( peers, choices, choices_sz, localOnly );
    if( !max )
	return;

    char sel[101];
    memset( sel, 1, max );
    int choice = gwwv_choose_multiple(_("Close Collab Server(s)"),
				      (const char **) choices, sel, max,
				      buts, _("Select which servers you wish to close..."));

    int allServersSelected = 1;
    printf("you wanted %d\n", choice );
    for( i=0; i < max; i++ )
    {
	printf("sel[%d] is %d\n", i, sel[i] );
	beacon_announce_t* ba = collab_getBeaconFromChoicesArray( peers, choice, localOnly );

	if( sel[i] && ba )
	{
	    int port = ba->port;

	    if( sel[i] )
	    {
		FontViewBase* fv = FontViewFind( FontViewFind_byCollabBasePort, (void*)(intptr_t)port );
		if( fv )
		    collabclient_sessionDisconnect( fv );
		printf("CLOSING port:%d fv:%p\n", port, fv );
		collabclient_closeLocalServer( port );
	    }
	}
	else
	{
	    allServersSelected = 0;
	}
    }

    printf("allServersSelected:%d\n", allServersSelected );
    if( allServersSelected )
	collabclient_closeAllLocalServersForce();
}

static void FVMenuCollabCloseLocalServer(GWindow gw, struct gmenuitem *UNUSED(mi), GEvent *UNUSED(e))
{
    AskAndMaybeCloseLocalCollabServers();
}


#if defined(__MINGW32__)
//
// This is an imperfect implemenation of kill() for windows.
//
static int kill( int pid, int sig )
{
    HANDLE hHandle;
    hHandle = OpenProcess( PROCESS_ALL_ACCESS, 0, pid );
    TerminateProcess( hHandle, 0 );
}
#endif



static void collablistcheck(GWindow gw, struct gmenuitem *mi, GEvent *UNUSED(e))
{
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    for ( mi = mi->sub; mi->ti.text!=NULL || mi->ti.line ; ++mi )
    {
	switch ( mi->mid )
	{
	case MID_CollabDisconnect:
	{
	    enum collabState_t st = collabclient_getState( &fv->b );
	    mi->ti.disabled = ( st < cs_server );
	    break;
	}
	case MID_CollabCloseLocalServer:
	    printf("can close local server: %d\n", collabclient_haveLocalServer() );
	    mi->ti.disabled = !collabclient_haveLocalServer();
	    break;
	}
    }
}

#ifdef BUILD_COLLAB

static GMenuItem2 collablist[] = {
    { { (unichar_t *) N_("_Start Session..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Start Session...|No Shortcut"), NULL, NULL, FVMenuCollabStart, MID_CollabStart },
    { { (unichar_t *) N_("_Connect to Session..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Connect to Session...|No Shortcut"), NULL, NULL, FVMenuCollabConnect, MID_CollabConnect },
    { { (unichar_t *) N_("_Connect to Session (ip:port)..."), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Connect to Session (ip:port)...|No Shortcut"), NULL, NULL, FVMenuCollabConnectToExplicitAddress, MID_CollabConnectToExplicitAddress },
    { { (unichar_t *) N_("_Disconnect"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Disconnect|No Shortcut"), NULL, NULL, FVMenuCollabDisconnect, MID_CollabDisconnect },
    GMENUITEM2_LINE,
    { { (unichar_t *) N_("Close local server"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Close local server|No Shortcut"), NULL, NULL, FVMenuCollabCloseLocalServer, MID_CollabCloseLocalServer },

    GMENUITEM2_EMPTY,				/* Extra room to show sub-font names */
};
#endif

GMenuItem2 helplist[] = {
    { { (unichar_t *) N_("_Help"), (GImage *) "helphelp.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Help|F1"), NULL, NULL, FVMenuContextualHelp, 0 },
    { { (unichar_t *) N_("_Overview"), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Overview|Shft+F1"), NULL, NULL, MenuHelp, 0 },
    { { (unichar_t *) N_("_Index"), (GImage *) "helpindex.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, H_("Index|No Shortcut"), NULL, NULL, MenuIndex, 0 },
    { { (unichar_t *) N_("_About..."), (GImage *) "helpabout.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'A' }, H_("About...|No Shortcut"), NULL, NULL, MenuAbout, 0 },
    { { (unichar_t *) N_("_License..."), (GImage *) "menuempty.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'A' }, H_("License...|No Shortcut"), NULL, NULL, MenuLicense, 0 },
    GMENUITEM2_EMPTY
};

GMenuItem fvpopupmenu[] = {
    { { (unichar_t *) N_("New O_utline Window"), 0, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'u' }, '\0', ksm_control, NULL, NULL, FVMenuOpenOutline, MID_OpenOutline },
    GMENUITEM_LINE,
    { { (unichar_t *) N_("Cu_t"), (GImage *) "editcut.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 't' }, '\0', ksm_control, NULL, NULL, FVMenuCut, MID_Cut },
    { { (unichar_t *) N_("_Copy"), (GImage *) "editcopy.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, '\0', ksm_control, NULL, NULL, FVMenuCopy, MID_Copy },
    { { (unichar_t *) N_("C_opy Reference"), (GImage *) "editcopyref.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'o' }, '\0', ksm_control, NULL, NULL, FVMenuCopyRef, MID_CopyRef },
    { { (unichar_t *) N_("Copy _Width"), (GImage *) "editcopywidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'W' }, '\0', ksm_control, NULL, NULL, FVMenuCopyWidth, MID_CopyWidth },
    { { (unichar_t *) N_("_Paste"), (GImage *) "editpaste.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'P' }, '\0', ksm_control, NULL, NULL, FVMenuPaste, MID_Paste },
    { { (unichar_t *) N_("C_lear"), (GImage *) "editclear.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, 0, 0, NULL, NULL, FVMenuClear, MID_Clear },
    { { (unichar_t *) N_("Copy _Fg To Bg"), (GImage *) "editcopyfg2bg.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuCopyFgBg, MID_CopyFgToBg },
    { { (unichar_t *) N_("U_nlink Reference"), (GImage *) "editunlink.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'U' }, '\0', ksm_control, NULL, NULL, FVMenuUnlinkRef, MID_UnlinkRef },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, '\0', 0, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Glyph _Info..."), (GImage *) "elementglyphinfo.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, '\0', ksm_control, NULL, NULL, FVMenuCharInfo, MID_CharInfo },
    { { (unichar_t *) N_("_Transform..."), (GImage *) "elementtransform.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'T' }, '\0', ksm_control, NULL, NULL, FVMenuTransform, MID_Transform },
    { { (unichar_t *) N_("_Expand Stroke..."), (GImage *) "elementexpandstroke.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'E' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuStroke, MID_Stroke },
    { { (unichar_t *) N_("To _Int"), (GImage *) "elementround.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'I' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuRound2Int, MID_Round },
    { { (unichar_t *) N_("_Correct Direction"), (GImage *) "elementcorrectdir.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'D' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuCorrectDir, MID_Correct },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, '\0', 0, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("Auto_Hint"), (GImage *) "hintsautohint.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuAutoHint, MID_AutoHint },
    { { NULL, NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 1, 0, 0, 0, '\0' }, '\0', 0, NULL, NULL, NULL, 0 }, /* line */
    { { (unichar_t *) N_("_Center in Width"), (GImage *) "metricscenter.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, '\0', ksm_control, NULL, NULL, FVMenuCenter, MID_Center },
    { { (unichar_t *) N_("Set _Width..."), (GImage *) "metricssetwidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'W' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuSetWidth, MID_SetWidth },
    { { (unichar_t *) N_("Set _Vertical Advance..."), (GImage *) "metricssetvwidth.png", COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, '\0', ksm_control|ksm_shift, NULL, NULL, FVMenuSetWidth, MID_SetVWidth },
    GMENUITEM_EMPTY
};

static GMenuItem2 mblist[] = {
    { { (unichar_t *) N_("_File"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'F' }, H_("File|No Shortcut"), fllist, fllistcheck, NULL, 0 },
    { { (unichar_t *) N_("_Edit"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'E' }, H_("Edit|No Shortcut"), edlist, edlistcheck, NULL, 0 },
    { { (unichar_t *) N_("E_lement"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Element|No Shortcut"), ellist, ellistcheck, NULL, 0 },
#ifndef _NO_PYTHON
    { { (unichar_t *) N_("_Tools"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 1, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Tools|No Shortcut"), NULL, fvpy_tllistcheck, NULL, 0 },
#endif
#ifdef NATIVE_CALLBACKS
    { { (unichar_t *) N_("Tools_2"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 1, 1, 0, 0, 0, 0, 1, 1, 0, 'l' }, H_("Tools 2|No Shortcut"), NULL, fv_tl2listcheck, NULL, 0 },
#endif
    { { (unichar_t *) N_("H_ints"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'i' }, H_("Hints|No Shortcut"), htlist, htlistcheck, NULL, 0 },
    { { (unichar_t *) N_("E_ncoding"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, H_("Encoding|No Shortcut"), enlist, enlistcheck, NULL, 0 },
    { { (unichar_t *) N_("_View"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'V' }, H_("View|No Shortcut"), vwlist, vwlistcheck, NULL, 0 },
    { { (unichar_t *) N_("_Metrics"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'M' }, H_("Metrics|No Shortcut"), mtlist, mtlistcheck, NULL, 0 },
    { { (unichar_t *) N_("_CID"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'C' }, H_("CID|No Shortcut"), cdlist, cdlistcheck, NULL, 0 },
/* GT: Here (and following) MM means "MultiMaster" */
    { { (unichar_t *) N_("MM"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, '\0' }, H_("MM|No Shortcut"), mmlist, mmlistcheck, NULL, 0 },
    { { (unichar_t *) N_("_Window"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'W' }, H_("Window|No Shortcut"), wnmenu, FVWindowMenuBuild, NULL, 0 },
#ifdef BUILD_COLLAB
    { { (unichar_t *) N_("C_ollaborate"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'W' }, H_("Collaborate|No Shortcut"), collablist, collablistcheck, NULL, 0 },
#endif
    { { (unichar_t *) N_("_Help"), NULL, COLOR_DEFAULT, COLOR_DEFAULT, NULL, NULL, 0, 1, 0, 0, 0, 0, 1, 1, 0, 'H' }, H_("Help|No Shortcut"), helplist, NULL, NULL, 0 },
    GMENUITEM2_EMPTY
};

void FVRefreshChar(FontView *fv,int gid) {
    BDFChar *bdfc;
    int i, j, enc;
    MetricsView *mv;

    /* Can happen in scripts */ /* Can happen if we do an AutoHint when generating a tiny font for freetype context */
    if ( fv->v==NULL || fv->colcnt==0 || fv->b.sf->glyphs[gid]== NULL )
return;
    for ( fv=(FontView *) (fv->b.sf->fv); fv!=NULL; fv = (FontView *) (fv->b.nextsame) ) {
	if( !fv->colcnt )
	    continue;

	for ( mv=fv->b.sf->metrics; mv!=NULL; mv=mv->next )
	    MVRefreshChar(mv,fv->b.sf->glyphs[gid]);
	if ( fv->show==fv->filled )
	    bdfc = BDFPieceMealCheck(fv->show,gid);
	else
	    bdfc = fv->show->glyphs[gid];
	if ( bdfc==NULL )
	    bdfc = BDFPieceMeal(fv->show,gid);
	/* A glyph may be encoded in several places, all need updating */
	for ( enc = 0; enc<fv->b.map->enccount; ++enc ) if ( fv->b.map->map[enc]==gid ) {
	    i = enc / fv->colcnt;
	    j = enc - i*fv->colcnt;
	    i -= fv->rowoff;
	    if ( i>=0 && i<fv->rowcnt )
		FVDrawGlyph(fv->v,fv,enc,true);
	}
    }
}

void FVRegenChar(FontView *fv,SplineChar *sc) {
    struct splinecharlist *dlist;
    MetricsView *mv;

    if ( fv->v==NULL )			/* Can happen in scripts */
return;

    if ( sc->orig_pos<fv->filled->glyphcnt ) {
	BDFCharFree(fv->filled->glyphs[sc->orig_pos]);
	fv->filled->glyphs[sc->orig_pos] = NULL;
    }
    /* FVRefreshChar does NOT do this for us */
    for ( mv=fv->b.sf->metrics; mv!=NULL; mv=mv->next )
	MVRegenChar(mv,sc);

    FVRefreshChar(fv,sc->orig_pos);
#if HANYANG
    if ( sc->compositionunit && fv->b.sf->rules!=NULL )
	Disp_RefreshChar(fv->b.sf,sc);
#endif

    for ( dlist=sc->dependents; dlist!=NULL; dlist=dlist->next )
	FVRegenChar(fv,dlist->sc);
}

static void AddSubPST(SplineChar *sc,struct lookup_subtable *sub,char *variant) {
    PST *pst;

    pst = chunkalloc(sizeof(PST));
    pst->type = pst_substitution;
    pst->subtable = sub;
    pst->u.alt.components = copy(variant);
    pst->next = sc->possub;
    sc->possub = pst;
}

SplineChar *FVMakeChar(FontView *fv,int enc) {
    SplineFont *sf = fv->b.sf;
    SplineChar *base_sc = SFMakeChar(sf,fv->b.map,enc), *feat_sc = NULL;
    int feat_gid = FeatureTrans(fv,enc);

    if ( fv->cur_subtable==NULL )
return( base_sc );

    if ( feat_gid==-1 ) {
	int uni = -1;
	FeatureScriptLangList *fl = fv->cur_subtable->lookup->features;

	if ( base_sc->unicodeenc>=0x600 && base_sc->unicodeenc<=0x6ff &&
		fl!=NULL &&
		(fl->featuretag == CHR('i','n','i','t') ||
		 fl->featuretag == CHR('m','e','d','i') ||
		 fl->featuretag == CHR('f','i','n','a') ||
		 fl->featuretag == CHR('i','s','o','l')) ) {
	    uni = fl->featuretag == CHR('i','n','i','t') ? ArabicForms[base_sc->unicodeenc-0x600].initial  :
		  fl->featuretag == CHR('m','e','d','i') ? ArabicForms[base_sc->unicodeenc-0x600].medial   :
		  fl->featuretag == CHR('f','i','n','a') ? ArabicForms[base_sc->unicodeenc-0x600].final    :
		  fl->featuretag == CHR('i','s','o','l') ? ArabicForms[base_sc->unicodeenc-0x600].isolated :
		  -1;
	    feat_sc = SFGetChar(sf,uni,NULL);
	    if ( feat_sc!=NULL )
return( feat_sc );
	}
	feat_sc = SFSplineCharCreate(sf);
	feat_sc->unicodeenc = uni;
	if ( uni!=-1 ) {
	    feat_sc->name = malloc(8);
	    feat_sc->unicodeenc = uni;
	    sprintf( feat_sc->name,"uni%04X", uni );
	} else if ( fv->cur_subtable->suffix!=NULL ) {
	    feat_sc->name = malloc(strlen(base_sc->name)+strlen(fv->cur_subtable->suffix)+2);
	    sprintf( feat_sc->name, "%s.%s", base_sc->name, fv->cur_subtable->suffix );
	} else if ( fl==NULL ) {
	    feat_sc->name = strconcat(base_sc->name,".unknown");
	} else if ( fl->ismac ) {
	    /* mac feature/setting */
	    feat_sc->name = malloc(strlen(base_sc->name)+14);
	    sprintf( feat_sc->name,"%s.m%d_%d", base_sc->name,
		    (int) (fl->featuretag>>16),
		    (int) ((fl->featuretag)&0xffff) );
	} else {
	    /* OpenType feature tag */
	    feat_sc->name = malloc(strlen(base_sc->name)+6);
	    sprintf( feat_sc->name,"%s.%c%c%c%c", base_sc->name,
		    (int) (fl->featuretag>>24),
		    (int) ((fl->featuretag>>16)&0xff),
		    (int) ((fl->featuretag>>8)&0xff),
		    (int) ((fl->featuretag)&0xff) );
	}
	SFAddGlyphAndEncode(sf,feat_sc,fv->b.map,fv->b.map->enccount);
	AddSubPST(base_sc,fv->cur_subtable,feat_sc->name);
return( feat_sc );
    } else
return( base_sc );
}

/* we style some glyph names differently, see FVExpose() */
#define _uni_italic	0x2
#define _uni_vertical	(1<<2)
#define _uni_fontmax	(2<<2)

static GFont *FVCheckFont(FontView *fv,int type) {
    FontRequest rq;

    if ( fv->fontset[type]==NULL ) {
	memset(&rq,0,sizeof(rq));
	rq.utf8_family_name = fv_fontnames;
	rq.point_size = fv_fontsize;
	rq.weight = 400;
	rq.style = 0;
	if (type&_uni_italic)
	    rq.style |= fs_italic;
	if (type&_uni_vertical)
	    rq.style |= fs_vertical;
	fv->fontset[type] = GDrawInstanciateFont(fv->v,&rq);
    }
return( fv->fontset[type] );
}

extern unichar_t adobes_pua_alts[0x200][3];

static void do_Adobe_Pua(unichar_t *buf,int sob,int uni) {
    int i, j;

    for ( i=j=0; j<sob-1 && i<3; ++i ) {
	int ch = adobes_pua_alts[uni-0xf600][i];
	if ( ch==0 )
    break;
	if ( ch>=0xf600 && ch<=0xf7ff && adobes_pua_alts[ch-0xf600]!=0 ) {
	    do_Adobe_Pua(buf+j,sob-j,ch);
	    while ( buf[j]!=0 ) ++j;
	} else
	    buf[j++] = ch;
    }
    buf[j] = 0;
}

static void FVExpose( FontView *fv, GWindow pixmap, GEvent *event )
{
	int i, j, y, width, gid;
	int changed;
	GRect old, old2, r;
	GClut clut;
	struct _GImage base;
	GImage gi;
	SplineChar dummy;
	int styles, laststyles=0;
	Color bg, def_fg;
	int fgxor;

	def_fg = GDrawGetDefaultForeground(NULL);
	memset(&gi,'\0',sizeof(gi));
	memset(&base,'\0',sizeof(base));
	if ( fv->show->clut!=NULL )
	{
		gi.u.image = &base;
		base.image_type = it_index;
		base.clut = fv->show->clut;
		GDrawSetDither(NULL, false);
		base.trans = -1;
	}
	else
	{
		memset(&clut,'\0',sizeof(clut));
		gi.u.image = &base;
		base.image_type = it_mono;
		base.clut = &clut;
		clut.clut_len = 2;
		clut.clut[0] = view_bgcol;
	}

	GDrawSetFont(pixmap,fv->fontset[0]);
	GDrawSetLineWidth(pixmap,0);
	GDrawPushClip(pixmap,&event->u.expose.rect,&old);
	GDrawFillRect(pixmap,NULL,view_bgcol);

	for ( i=0; i<=fv->rowcnt; ++i )
	{
		GDrawDrawLine(pixmap,0,i*fv->cbh,fv->width,i*fv->cbh,def_fg);
		GDrawDrawLine(pixmap,0,i*fv->cbh+fv->lab_height,fv->width,i*fv->cbh+fv->lab_height,0x808080);
	}

	for ( i=0; i<=fv->colcnt; ++i )
	{
		GDrawDrawLine(pixmap,i*fv->cbw,0,i*fv->cbw,fv->height,def_fg);
	}

	for ( i=event->u.expose.rect.y/fv->cbh; i<=fv->rowcnt && (event->u.expose.rect.y+event->u.expose.rect.height+fv->cbh-1)/fv->cbh; ++i )
	{
		for ( j=0; j<fv->colcnt; ++j )
		{
			int index = (i+fv->rowoff)*fv->colcnt+j;
			SplineChar *sc;
			styles = 0;
			if ( index < fv->b.map->enccount && index!=-1 )
			{
				unichar_t buf[60];
				char cbuf[8];
				char utf8_buf[8];
				int use_utf8 = false;
				Color fg;
				extern const int amspua[];
				int uni;
				struct cidmap *cidmap = NULL;
				sc = (gid=fv->b.map->map[index])!=-1 ? fv->b.sf->glyphs[gid]: NULL;

				if ( fv->b.cidmaster!=NULL )
				{
					cidmap = FindCidMap(fv->b.cidmaster->cidregistry,fv->b.cidmaster->ordering,fv->b.cidmaster->supplement,fv->b.cidmaster);
				}

				if ( ( fv->b.map->enc==&custom && index<256 ) || ( fv->b.map->enc!=&custom && index<fv->b.map->enc->char_cnt ) || ( cidmap!=NULL && index<MaxCID(cidmap) ))
				{
					fg = def_fg;
				}
				else
				{
					fg = 0x505050;
				}

				if ( sc==NULL )
				{
					sc = SCBuildDummy(&dummy,fv->b.sf,fv->b.map,index);
				}
				uni = sc->unicodeenc;
				buf[0] = buf[1] = 0;

				if ( fv->b.sf->uni_interp==ui_ams && uni>=0xe000 && uni<=0xf8ff && amspua[uni-0xe000]!=0 )
				{
					uni = amspua[uni-0xe000];
				}

				switch ( fv->glyphlabel )
				{
					case gl_name:
					{
						uc_strncpy(buf,sc->name,sizeof(buf)/sizeof(buf[0]));
					}
					break;
					case gl_unicode:
					{
						if ( sc->unicodeenc != -1 )
						{
							sprintf(cbuf,"%04x",sc->unicodeenc);
							uc_strcpy(buf,cbuf);
						}
						else
						{
#ifdef CKS	// HYMODIFY :: 2016.02.04
							////////////////////////////////////////////////////////////
							struct splinecharlist *dlist;
							SplineChar * depsc;
							char * jasobuf = utf8_buf;
							use_utf8 = true;

							if( sc != NULL )
							{
								if( sc->bCompositionUnit )
								{
									if( sc->unicodeenc == -1 )
									{
										if( HY_GetSCDependentCount(sc) )
										{
											if( HY_GetSCDependentCount(sc) )
											{
												for( dlist = sc->dependents ; dlist != NULL ; dlist = dlist->next )
												{
													depsc = dlist->sc;
												}
												jasobuf = utf8_idpb( jasobuf, depsc->unicodeenc, 0 );
											}
											else
											{
												jasobuf = utf8_idpb( jasobuf, -1, 0 );
											}
											strcat( jasobuf, " - ");
											*jasobuf = '\0';

											if( fv->filled->pixelsize >= 48 )
											{

												int vcode = sc->VCode;
												if( vcode < NUM_JASO_FC )
												{
													buf[0] = 0x3131 + ChoSungIdx[vcode];
												}
												else if( vcode < NUM_JASO_FC + NUM_JASO_MV )
												{
													buf[0] = 0x314f + JungSungIdx[vcode - NUM_JASO_FC];
												}
												else
												{
													buf[0] = 0x3131 + JongSungIdx[vcode - ( NUM_JASO_FC + NUM_JASO_MV + 1 )];
												}
												buf[1] = 0;
												uc_strcat(buf," : ");
												sprintf(cbuf,"%d",sc->Varient);
												uc_strcat(buf,cbuf);
											}
										}
										else
										{
											buf[0] = '?';
										}
										fg = 0xff0000;
									}
								}
								else
								{
									if( sc->unicodeenc == -1 )
									{
										if( HY_GetSCDependentCount(sc) )
										{
											if( HY_GetSCDependentCount(sc) )
											{
												for( dlist = sc->dependents ; dlist != NULL ; dlist = dlist->next )
												{
													depsc = dlist->sc;
												}
												jasobuf = utf8_idpb( jasobuf, depsc->unicodeenc, 0 );
											}
											else
											{
												jasobuf = utf8_idpb( jasobuf, -1, 0 );
											}
											*jasobuf = '\0';
										}
										else
										{
											buf[0] = '?';
										}
										fg = 0x0000ff;
									}
								}
							}
							else
							{
								uc_strcpy(buf,"?");
							}
#else
							uc_strcpy(buf,"?");
#endif

						}
					}
					break;
					case gl_encoding:
					{
						if ( fv->b.map->enc->only_1byte || (fv->b.map->enc->has_1byte && index<256))
						{
							sprintf(cbuf,"%02x",index);
						}
						else
						{
							sprintf(cbuf,"%04x",index);
						}
						uc_strcpy(buf,cbuf);
					}
					break;
					case gl_glyph:
					{
						if ( uni==0xad )
						{
							buf[0] = '-';
						}
						else if ( fv->b.sf->uni_interp==ui_adobe && uni>=0xf600 && uni<=0xf7ff && adobes_pua_alts[uni-0xf600]!=0 )
						{
							use_utf8 = false;
							do_Adobe_Pua(buf,sizeof(buf),uni);
						}
						else if ( uni>=0xe0020 && uni<=0xe007e )
						{
							buf[0] = uni-0xe0000;	/* A map of Ascii for language names */
#if HANYANG
						}
						else if ( sc->compositionunit )
						{
							if ( sc->jamo<19 )
							{
								buf[0] = 0x1100+sc->jamo;
							}
							else if ( sc->jamo<19+21 )
							{
								buf[0] = 0x1161 + sc->jamo-19;
							}
							else	/* Leave a hole for the blank char */
							{
								buf[0] = 0x11a8 + sc->jamo-(19+21+1);
							}
#endif
						}
						else if ( uni>0 && uni<unicode4_size )
						{
							char *pt = utf8_buf;
							use_utf8 = true;
							*pt = '\0'; // We terminate the string in case the appendage (?) fails.
							pt = utf8_idpb(pt,uni,0);
							if (pt)
							{
								*pt = '\0';
							}
							else
							{
								fprintf(stderr, "Invalid Unicode alert.\n");
							}
						}
						else
						{
							char *pt = strchr(sc->name,'.');
#ifdef CKS	// HYMODIFY :: 2016.02.04
							////////////////////////////////////////////////////////////
							struct splinecharlist *dlist;
							SplineChar * depsc;
							char * jasobuf = utf8_buf;
							use_utf8 = true;

							if( sc != NULL )
							{
								if( sc->bCompositionUnit )
								{
									if( sc->unicodeenc == -1 )
									{
										if( HY_GetSCDependentCount(sc) )
										{
											if( HY_GetSCDependentCount(sc) )
											{
												for( dlist = sc->dependents ; dlist != NULL ; dlist = dlist->next )
												{
													depsc = dlist->sc;
												}
												jasobuf = utf8_idpb( jasobuf, depsc->unicodeenc, 0 );
											}
											else
											{
												jasobuf = utf8_idpb( jasobuf, -1, 0 );
											}
											strcat( jasobuf, " - ");
											*jasobuf = '\0';

											if( fv->filled->pixelsize >= 48 )
											{
												int vcode = sc->VCode;
												if( vcode < NUM_JASO_FC )
												{
													buf[0] = 0x3131 + ChoSungIdx[vcode];
												}
												else if( vcode < NUM_JASO_FC + NUM_JASO_MV )
												{
													buf[0] = 0x314f + JungSungIdx[vcode - NUM_JASO_FC];
												}
												else
												{
													buf[0] = 0x3131 + JongSungIdx[vcode - ( NUM_JASO_FC + NUM_JASO_MV + 1 )];
												}
												buf[1] = 0;
												uc_strcat(buf," : ");
												sprintf(cbuf,"%d",sc->Varient);
												uc_strcat(buf,cbuf);
											}
										}
										else
										{
											buf[0] = '?';
										}
										fg = 0xff0000;
									}
								}
								else
								{
									if( sc->unicodeenc == -1 )
									{
										if( HY_GetSCDependentCount(sc) )
										{
											if( HY_GetSCDependentCount(sc) )
											{
												for( dlist = sc->dependents ; dlist != NULL ; dlist = dlist->next )
												{
													depsc = dlist->sc;
												}
												jasobuf = utf8_idpb( jasobuf, depsc->unicodeenc, 0 );
											}
											else
											{
												jasobuf = utf8_idpb( jasobuf, -1, 0 );
											}
											*jasobuf = '\0';
										}
										else
										{
											buf[0] = '?';
										}
										fg = 0x0000ff;
									}
								}
							}
#else
							buf[0] = '?';
							fg = 0xff0000;
#endif
							if ( pt!=NULL )
							{
								int i, n = pt-sc->name;
								char *end;
								SplineFont *cm = fv->b.sf->cidmaster;
								if ( n==7 && sc->name[0]=='u' && sc->name[1]=='n' && sc->name[2]=='i' && (i=strtol(sc->name+3,&end,16), end-sc->name==7))
								{
									buf[0] = i;
								}
								else if ( n>=5 && n<=7 && sc->name[0]=='u' && (i=strtol(sc->name+1,&end,16), end-sc->name==n))
								{
									buf[0] = i;
								}
								else if ( cm!=NULL && (i=CIDFromName(sc->name,cm))!=-1 )
								{
									int uni;
									uni = CID2Uni(FindCidMap(cm->cidregistry,cm->ordering,cm->supplement,cm), i);
									if ( uni!=-1 )
									{
										buf[0] = uni;
									}
								}
								else
								{
									int uni;
									*pt = '\0';
									uni = UniFromName(sc->name,fv->b.sf->uni_interp,fv->b.map->enc);
									if ( uni!=-1 )
									{
										buf[0] = uni;
									}
									*pt = '.';
								}
								if ( strstr(pt,".vert")!=NULL )
								{
									styles = _uni_vertical;
								}
								if ( buf[0]!='?' )
								{
									fg = def_fg;
									if ( strstr(pt,".italic")!=NULL )
									{
										styles = _uni_italic;
									}
								}
							}
							else if ( strncmp(sc->name,"hwuni",5)==0 )
							{
								int uni=-1;
								sscanf(sc->name,"hwuni%x", (unsigned *) &uni );
								if ( uni!=-1 )
								{
									buf[0] = uni;
								}
							}
							else if ( strncmp(sc->name,"italicuni",9)==0 )
							{
								int uni=-1;
								sscanf(sc->name,"italicuni%x", (unsigned *) &uni );
								if ( uni!=-1 )
								{
									buf[0] = uni; styles=_uni_italic;
								}
								fg = def_fg;
							}
							else if ( strncmp(sc->name,"vertcid_",8)==0 || strncmp(sc->name,"vertuni",7)==0 )
							{
								styles = _uni_vertical;
							}
						}
						break;
					}
				}
				r.x = j*fv->cbw+1; r.width = fv->cbw-1;
				r.y = i*fv->cbh+1; r.height = fv->lab_height-1;
				bg = view_bgcol;
				fgxor = 0x000000;
				changed = sc->changed;
				if ( fv->b.sf->onlybitmaps && gid<fv->show->glyphcnt )
				{
					changed = gid==-1 || fv->show->glyphs[gid]==NULL? false : fv->show->glyphs[gid]->changed;
				}
				if ( changed || sc->layers[ly_back].splines!=NULL || sc->layers[ly_back].images!=NULL || sc->color!=COLOR_DEFAULT )
				{
					if ( sc->layers[ly_back].splines!=NULL || sc->layers[ly_back].images!=NULL || sc->color!=COLOR_DEFAULT )
					{
						bg = sc->color!=COLOR_DEFAULT?sc->color:0x808080;
					}
					if ( sc->changed )
					{
						fgxor = bg ^ fvchangedcol;
						bg = fvchangedcol;
					}
					GDrawFillRect(pixmap,&r,bg);
				}
				if ( (!fv->b.sf->layers[fv->b.active_layer].order2 && sc->changedsincelasthinted ) || ( fv->b.sf->layers[fv->b.active_layer].order2 && sc->layers[fv->b.active_layer].splines!=NULL &&
					sc->ttf_instrs_len<=0 ) || ( fv->b.sf->layers[fv->b.active_layer].order2 && sc->instructions_out_of_date ) )
				{
					Color hintcol = fvhintingneededcol;
					if ( fv->b.sf->layers[fv->b.active_layer].order2 && sc->instructions_out_of_date && sc->ttf_instrs_len>0 )
					{
						hintcol = 0xff0000;
					}
					GDrawDrawLine(pixmap,r.x,r.y,r.x,r.y+r.height-1,hintcol);
					GDrawDrawLine(pixmap,r.x+1,r.y,r.x+1,r.y+r.height-1,hintcol);
					GDrawDrawLine(pixmap,r.x+2,r.y,r.x+2,r.y+r.height-1,hintcol);
					GDrawDrawLine(pixmap,r.x+r.width-1,r.y,r.x+r.width-1,r.y+r.height-1,hintcol);
					GDrawDrawLine(pixmap,r.x+r.width-2,r.y,r.x+r.width-2,r.y+r.height-1,hintcol);
					GDrawDrawLine(pixmap,r.x+r.width-3,r.y,r.x+r.width-3,r.y+r.height-1,hintcol);
				}
				if ( use_utf8 && sc->unicodeenc!=-1 &&			/* Pango complains if we try to draw non characters */
															/* These two are guaranteed "NOT A UNICODE CHARACTER" in all planes */
					((sc->unicodeenc&0xffff)==0xfffe || (sc->unicodeenc&0xffff)==0xffff ||
					(sc->unicodeenc>=0xfdd0 && sc->unicodeenc<=0xfdef) ||		/* noncharacters */
					(sc->unicodeenc>=0xfe00 && sc->unicodeenc<=0xfe0f) ||		/* variation selectors */
					(sc->unicodeenc>=0xe0110 && sc->unicodeenc<=0xe01ff) ||	/* variation selectors */
																			/*  The surrogates in BMP aren't valid either */
					(sc->unicodeenc>=0xd800 && sc->unicodeenc<=0xdfff)))		/* surrogates */
				{
					GDrawDrawLine(pixmap,r.x,r.y,r.x+r.width-1,r.y+r.height-1,0x000000);
					GDrawDrawLine(pixmap,r.x,r.y+r.height-1,r.x+r.width-1,r.y,0x000000);
				}
				else if ( use_utf8 )
				{
					GTextBounds size;
					if ( styles!=laststyles )
					{
						GDrawSetFont(pixmap,FVCheckFont(fv,styles));
					}
					width = GDrawGetText8Bounds(pixmap,utf8_buf,-1,&size);
					if ( size.lbearing==0 && size.rbearing==0 )
					{
						utf8_buf[0] = 0xe0 | (0xfffd>>12);
						utf8_buf[1] = 0x80 | ((0xfffd>>6)&0x3f);
						utf8_buf[2] = 0x80 | (0xfffd&0x3f);
						utf8_buf[3] = 0;
						width = GDrawGetText8Bounds(pixmap,utf8_buf,-1,&size);
					}
					width = size.rbearing - size.lbearing+1;
					if ( width >= fv->cbw-1 )
					{
						GDrawPushClip(pixmap,&r,&old2);
						width = fv->cbw-1;
					}
					if ( sc->unicodeenc<0x80 || sc->unicodeenc>=0xa0 )
					{
						y = i*fv->cbh+fv->lab_as+1;
						/* move rotated glyph up a bit to center it */
						if (styles&_uni_vertical)
						{
							y -= fv->lab_as/2;
						}
#ifdef HYMODIFY
						if( sc->bmaster )
						{
							if( sc->bmaster && sc->mmg_type == MG_TYPE_CHO )
							{
								GDrawDrawText8(pixmap,j*fv->cbw+(fv->cbw-1-width)/2-size.lbearing,y,utf8_buf,-1,chomg_col);
							}
							else if( sc->bmaster && sc->mmg_type == MG_TYPE_JUNG )
							{
								GDrawDrawText8(pixmap,j*fv->cbw+(fv->cbw-1-width)/2-size.lbearing,y,utf8_buf,-1,jumg_col);
							}
							else if( sc->bmaster && sc->mmg_type == MG_TYPE_JONG )
							{
								GDrawDrawText8(pixmap,j*fv->cbw+(fv->cbw-1-width)/2-size.lbearing,y,utf8_buf,-1,jomg_col);
							}
							else
							{
								GDrawDrawText8(pixmap,j*fv->cbw+(fv->cbw-1-width)/2-size.lbearing,y,utf8_buf,-1,mg_col);
							}
						}
						else
						{
							#ifdef CKS	// HYMODIFY :: 2016.02.04 ~
							if( sc->bCompositionUnit )
							{
								if( fv->filled->pixelsize >= 48 )
								{
									GDrawDrawText8( pixmap, (j * fv->cbw) + (fv->cbw * 0.3), y, utf8_buf, -1, fg^fgxor );
									GDrawDrawText( pixmap, (j * fv->cbw) + (fv->cbw * 0.3) + GDrawGetText8Width(pixmap, utf8_buf, -1), y, buf, -1, fg^fgxor );
								}
								else
								{
									GDrawDrawText8( pixmap, (j * fv->cbw) + (fv->cbw * 0.2) + size.lbearing, y, utf8_buf, -1, fg^fgxor );
								}
							}
							#else
							{
								GDrawDrawText8(pixmap,j*fv->cbw+(fv->cbw-1-width)/2-size.lbearing,y,utf8_buf,-1,fg^fgxor);
							}
							#endif
						}
#else
						GDrawDrawText8(pixmap,j*fv->cbw+(fv->cbw-1-width)/2-size.lbearing,y,utf8_buf,-1,fg^fgxor);
#endif
					}
					if ( width >= fv->cbw-1 )
					{
						GDrawPopClip(pixmap,&old2);
					}
					laststyles = styles;
				}
				else
				{
					if ( styles!=laststyles )
					{
						GDrawSetFont(pixmap,FVCheckFont(fv,styles));
					}
					width = GDrawGetTextWidth(pixmap,buf,-1);
					if ( width >= fv->cbw-1 )
					{
						GDrawPushClip(pixmap,&r,&old2);
						width = fv->cbw-1;
					}
					if ( sc->unicodeenc<0x80 || sc->unicodeenc>=0xa0 )
					{
						y = i*fv->cbh+fv->lab_as+1;
						/* move rotated glyph up a bit to center it */
						if (styles&_uni_vertical)
						{
							y -= fv->lab_as/2;
						}
#ifdef HYMODIFY
						if( sc->bmaster )
						{
							if( sc->bmaster && sc->mmg_type == MG_TYPE_CHO )
							{
								GDrawDrawText(pixmap,j*fv->cbw+(fv->cbw-1-width)/2,y,buf,-1,chomg_col);
							}
							else if( sc->bmaster && sc->mmg_type == MG_TYPE_JUNG )
							{
								GDrawDrawText(pixmap,j*fv->cbw+(fv->cbw-1-width)/2,y,buf,-1,jumg_col);
							}
							else if( sc->bmaster && sc->mmg_type == MG_TYPE_JONG )
							{
								GDrawDrawText(pixmap,j*fv->cbw+(fv->cbw-1-width)/2,y,buf,-1,jomg_col);
							}
							else
							{
								GDrawDrawText(pixmap,j*fv->cbw+(fv->cbw-1-width)/2,y,buf,-1,mg_col);
							}
						}
						else
						{
							GDrawDrawText(pixmap,j*fv->cbw+(fv->cbw-1-width)/2,y,buf,-1,fg^fgxor);
						}
#else
						GDrawDrawText(pixmap,j*fv->cbw+(fv->cbw-1-width)/2,y,buf,-1,fg^fgxor);
#endif
					}

					if ( width >= fv->cbw-1 )
					{
						GDrawPopClip(pixmap,&old2);
					}
					laststyles = styles;
				}
			}
			FVDrawGlyph(pixmap,fv,index,false);
		}
	}

	if ( fv->showhmetrics&fvm_baseline )
	{
		for ( i=0; i<=fv->rowcnt; ++i )
		{
			GDrawDrawLine(pixmap,0,i*fv->cbh+fv->lab_height+fv->magnify*fv->show->ascent+1,fv->width,i*fv->cbh+fv->lab_height+fv->magnify*fv->show->ascent+1,METRICS_BASELINE);
		}
	}
	GDrawPopClip(pixmap,&old);
	GDrawSetDither(NULL, true);
}

void FVDrawInfo( FontView *fv, GWindow pixmap, GEvent *event )
{
	GRect old, r;
	Color bg = GDrawGetDefaultBackground(GDrawGetDisplayOfWindow(pixmap));
	Color fg = fvglyphinfocol;
	SplineChar *sc, dummy;
	SplineFont *sf = fv->b.sf;
	EncMap *map = fv->b.map;
	int gid, uni, localenc;
	GString *output = g_string_new( "" );
	gchar *uniname = NULL;

	if ( event->u.expose.rect.y+event->u.expose.rect.height<=fv->mbh )
	{
		g_string_free( output, TRUE ); output = NULL;
		return;
	}

	GDrawSetFont(pixmap,fv->fontset[0]);
	GDrawPushClip(pixmap,&event->u.expose.rect,&old);

	r.x = 0; r.width = fv->width; r.y = fv->mbh; r.height = fv->infoh;
	GDrawFillRect(pixmap,&r,bg);
	if ( fv->end_pos>=map->enccount || fv->pressed_pos>=map->enccount || fv->end_pos<0 || fv->pressed_pos<0 )
	{
		fv->end_pos = fv->pressed_pos = -1;	/* Can happen after reencoding */
	}
	if ( fv->end_pos == -1 )
	{
		g_string_free( output, TRUE ); output = NULL;
		GDrawPopClip(pixmap,&old);
		return;
	}

	localenc = fv->end_pos;
	if ( map->remap!=NULL )
	{
		struct remap *remap = map->remap;
		while ( remap->infont!=-1 )
		{
			if ( localenc>=remap->infont && localenc<=remap->infont+(remap->lastenc-remap->firstenc) )
			{
				localenc += remap->firstenc-remap->infont;
				break;
			}
			++remap;
		}
	}
	g_string_printf( output, "%d (0x%x) ", localenc, localenc );

#ifdef CKS	// HYMODIFY :: 2016.02.23
	g_string_append_printf( output, " <map->map[%d]: %d> ", fv->end_pos, fv->b.map->map[fv->end_pos] );
	if( fv->b.map->map[fv->end_pos] != -1 )
	{
		g_string_append_printf( output, " <map->backmap[%d]: %d> ", fv->b.map->map[fv->end_pos], fv->b.map->backmap[fv->b.map->map[fv->end_pos]] );
	}
	else
	{
		g_string_append_printf( output, " <map->backmap[%d]: N/A> ", fv->b.map->map[fv->end_pos] );
	}
#endif

	sc = (gid=fv->b.map->map[fv->end_pos])!=-1 ? sf->glyphs[gid] : NULL;
	if ( fv->b.cidmaster==NULL || fv->b.normal==NULL || sc==NULL )
	{
		SCBuildDummy(&dummy,sf,fv->b.map,fv->end_pos);
	}
	else
	{
		dummy = *sc;
	}
	if ( sc==NULL ) sc = &dummy;
	uni = dummy.unicodeenc!=-1 ? dummy.unicodeenc : sc->unicodeenc;

	/* last resort at guessing unicode code point from partial name */
	if ( uni == -1 )
	{
		char *pt = strchr( sc->name, '.' );
		if( pt != NULL )
		{
			gchar *buf = g_strndup( (const gchar *) sc->name, pt - sc->name );
			uni = UniFromName( (char *) buf, fv->b.sf->uni_interp, map->enc );
			g_free( buf );
		}
	}

	if ( uni != -1 )
	{
		g_string_append_printf( output, "U+%04X", uni );
	}
	else
	{
		output = g_string_append( output, "U+????" );
	}

	/* postscript name */
	g_string_append_printf( output, " \"%s\" ", sc->name );

	/* code point name or range name */
	if( uni != -1 )
	{
		uniname = (gchar *) unicode_name( uni );
		if ( uniname == NULL )
		{
			uniname = g_strdup( UnicodeRange( uni ) );
		}
	}

	if ( uniname != NULL )
	{
		output = g_string_append( output, uniname );
		g_free( uniname );
	}

	GDrawDrawText8( pixmap, 10, fv->mbh+fv->lab_as, output->str, -1, fg );
	g_string_free( output, TRUE ); output = NULL;
	GDrawPopClip( pixmap, &old );
	return;
}

static void FVShowInfo(FontView *fv) {
    GRect r;

    if ( fv->v==NULL )			/* Can happen in scripts */
return;

    r.x = 0; r.width = fv->width; r.y = fv->mbh; r.height = fv->infoh;
    GDrawRequestExpose(fv->gw,&r,false);
}

void FVChar(FontView *fv, GEvent *event) {
    int i,pos, cnt, gid;
    extern int navigation_mask;

#if MyMemory
    if ( event->u.chr.keysym == GK_F2 ) {
	fprintf( stderr, "Malloc debug on\n" );
	__malloc_debug(5);
    } else if ( event->u.chr.keysym == GK_F3 ) {
	fprintf( stderr, "Malloc debug off\n" );
	__malloc_debug(0);
    }
#endif

    if ( event->u.chr.keysym=='s' &&
	    (event->u.chr.state&ksm_control) &&
	    (event->u.chr.state&ksm_meta) )
	MenuSaveAll(NULL,NULL,NULL);
    else if ( event->u.chr.keysym=='q' &&
	    (event->u.chr.state&ksm_control) &&
	    (event->u.chr.state&ksm_meta) )
	MenuExit(NULL,NULL,NULL);
    else if ( event->u.chr.keysym=='I' &&
	    (event->u.chr.state&ksm_shift) &&
	    (event->u.chr.state&ksm_meta) )
	FVMenuCharInfo(fv->gw,NULL,NULL);
    else if ( (event->u.chr.keysym=='[' || event->u.chr.keysym==']') &&
	    (event->u.chr.state&ksm_control) ) {
	_FVMenuChangeChar(fv,event->u.chr.keysym=='['?MID_Prev:MID_Next);
    } else if ( (event->u.chr.keysym=='{' || event->u.chr.keysym=='}') &&
	    (event->u.chr.state&ksm_control) ) {
	_FVMenuChangeChar(fv,event->u.chr.keysym=='{'?MID_PrevDef:MID_NextDef);
    } else if ( event->u.chr.keysym=='\\' && (event->u.chr.state&ksm_control) ) {
	/* European keyboards need a funky modifier to get \ */
	FVDoTransform(fv);
#if !defined(_NO_FFSCRIPT) || !defined(_NO_PYTHON)
    } else if ( isdigit(event->u.chr.keysym) && (event->u.chr.state&ksm_control) &&
	    (event->u.chr.state&ksm_meta) ) {
	/* The Script menu isn't always up to date, so we might get one of */
	/*  the shortcuts here */
	int index = event->u.chr.keysym-'1';
	if ( index<0 ) index = 9;
	if ( script_filenames[index]!=NULL )
	    ExecuteScriptFile((FontViewBase *) fv,NULL,script_filenames[index]);
#endif
    } else if ( event->u.chr.keysym == GK_Left ||
	    event->u.chr.keysym == GK_Tab ||
	    event->u.chr.keysym == GK_BackTab ||
	    event->u.chr.keysym == GK_Up ||
	    event->u.chr.keysym == GK_Right ||
	    event->u.chr.keysym == GK_Down ||
	    event->u.chr.keysym == GK_KP_Left ||
	    event->u.chr.keysym == GK_KP_Up ||
	    event->u.chr.keysym == GK_KP_Right ||
	    event->u.chr.keysym == GK_KP_Down ||
	    event->u.chr.keysym == GK_Home ||
	    event->u.chr.keysym == GK_KP_Home ||
	    event->u.chr.keysym == GK_End ||
	    event->u.chr.keysym == GK_KP_End ||
	    event->u.chr.keysym == GK_Page_Up ||
	    event->u.chr.keysym == GK_KP_Page_Up ||
	    event->u.chr.keysym == GK_Prior ||
	    event->u.chr.keysym == GK_Page_Down ||
	    event->u.chr.keysym == GK_KP_Page_Down ||
	    event->u.chr.keysym == GK_Next ) {
	int end_pos = fv->end_pos;
	/* We move the currently selected char. If there is none, then pick */
	/*  something on the screen */
	if ( end_pos==-1 )
	    end_pos = (fv->rowoff+fv->rowcnt/2)*fv->colcnt;
	switch ( event->u.chr.keysym ) {
	  case GK_Tab:
	    pos = end_pos;
	    do {
		if ( event->u.chr.state&ksm_shift )
		    --pos;
		else
		    ++pos;
		if ( pos>=fv->b.map->enccount ) pos = 0;
		else if ( pos<0 ) pos = fv->b.map->enccount-1;
	    } while ( pos!=end_pos &&
		    ((gid=fv->b.map->map[pos])==-1 || !SCWorthOutputting(fv->b.sf->glyphs[gid])));
	    if ( pos==end_pos ) ++pos;
	    if ( pos>=fv->b.map->enccount ) pos = 0;
	  break;
#if GK_Tab!=GK_BackTab
	  case GK_BackTab:
	    pos = end_pos;
	    do {
		--pos;
		if ( pos<0 ) pos = fv->b.map->enccount-1;
	    } while ( pos!=end_pos &&
		    ((gid=fv->b.map->map[pos])==-1 || !SCWorthOutputting(fv->b.sf->glyphs[gid])));
	    if ( pos==end_pos ) --pos;
	    if ( pos<0 ) pos = 0;
	  break;
#endif
	  case GK_Left: case GK_KP_Left:
	    pos = end_pos-1;
	  break;
	  case GK_Right: case GK_KP_Right:
	    pos = end_pos+1;
	  break;
	  case GK_Up: case GK_KP_Up:
	    pos = end_pos-fv->colcnt;
	  break;
	  case GK_Down: case GK_KP_Down:
	    pos = end_pos+fv->colcnt;
	  break;
	  case GK_End: case GK_KP_End:
	    pos = fv->b.map->enccount;
	  break;
	  case GK_Home: case GK_KP_Home:
	    pos = 0;
	    if ( fv->b.sf->top_enc!=-1 && fv->b.sf->top_enc<fv->b.map->enccount )
		pos = fv->b.sf->top_enc;
	    else {
		pos = SFFindSlot(fv->b.sf,fv->b.map,home_char,NULL);
		if ( pos==-1 ) pos = 0;
	    }
	  break;
	  case GK_Page_Up: case GK_KP_Page_Up:
#if GK_Prior!=GK_Page_Up
	  case GK_Prior:
#endif
	    pos = (fv->rowoff-fv->rowcnt+1)*fv->colcnt;
	  break;
	  case GK_Page_Down: case GK_KP_Page_Down:
#if GK_Next!=GK_Page_Down
	  case GK_Next:
#endif
	    pos = (fv->rowoff+fv->rowcnt+1)*fv->colcnt;
	  break;
	}
	if ( pos<0 ) pos = 0;
	if ( pos>=fv->b.map->enccount ) pos = fv->b.map->enccount-1;
	if ( event->u.chr.state&ksm_shift && event->u.chr.keysym!=GK_Tab && event->u.chr.keysym!=GK_BackTab ) {
	    FVReselect(fv,pos);
	} else {
	    FVDeselectAll(fv);
	    fv->b.selected[pos] = true;
	    FVToggleCharSelected(fv,pos);
	    fv->pressed_pos = pos;
	    fv->sel_index = 1;
	}
	fv->end_pos = pos;
	FVShowInfo(fv);
	FVScrollToChar(fv,pos);
    } else if ( event->u.chr.keysym == GK_Help ) {
	MenuHelp(NULL,NULL,NULL);	/* Menu does F1 */
    } else if ( event->u.chr.keysym == GK_Escape ) {
	FVDeselectAll(fv);
    } else if ( event->u.chr.chars[0]=='\r' || event->u.chr.chars[0]=='\n' ) {
	if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
return;
	for ( i=cnt=0; i<fv->b.map->enccount && cnt<10; ++i ) if ( fv->b.selected[i] ) {
	    SplineChar *sc = SFMakeChar(fv->b.sf,fv->b.map,i);
	    if ( fv->show==fv->filled ) {
		CharViewCreate(sc,fv,i);
	    } else {
		BDFFont *bdf = fv->show;
		BitmapViewCreate(BDFMakeGID(bdf,sc->orig_pos),bdf,fv,i);
	    }
	    ++cnt;
	}
    } else if ( (event->u.chr.state&((GMenuMask()|navigation_mask)&~(ksm_shift|ksm_capslock)))==navigation_mask &&
	    event->type == et_char &&
	    event->u.chr.keysym!=0 &&
	    (event->u.chr.keysym<GK_Special/* || event->u.chr.keysym>=0x10000*/)) {
	SplineFont *sf = fv->b.sf;
	int enc = EncFromUni(event->u.chr.keysym,fv->b.map->enc);
	if ( enc==-1 ) {
	    for ( i=0; i<sf->glyphcnt; ++i ) {
		if ( sf->glyphs[i]!=NULL )
		    if ( sf->glyphs[i]->unicodeenc==event->u.chr.keysym )
	    break;
	    }
	    if ( i!=-1 )
		enc = fv->b.map->backmap[i];
	}
	if ( enc<fv->b.map->enccount && enc!=-1 )
	    FVChangeChar(fv,enc);
    }
}

static void utf82u_annot_strncat(unichar_t *to, const char *from, int len) {
    register unichar_t ch;

    to += u_strlen(to);
    while ( (ch = utf8_ildb(&from)) != '\0' && --len>=0 ) {
	if ( ch=='\t' ) {
	    *(to++) = ' ';
	    ch = ' ';
	}
	*(to++) = ch;
    }
    *to = 0;
}

void SCPreparePopup(GWindow gw,SplineChar *sc,struct remap *remap, int localenc,
	int actualuni) {
/* This is for the popup which appears when you hover mouse over a character on main window */
    int upos=-1;
    char *msg, *msg_old;

    /* If a glyph is multiply mapped then the inbuild unicode enc may not be */
    /*  the actual one used to access the glyph */
    if ( remap!=NULL ) {
	while ( remap->infont!=-1 ) {
	    if ( localenc>=remap->infont && localenc<=remap->infont+(remap->lastenc-remap->firstenc) ) {
		localenc += remap->firstenc-remap->infont;
                break;
	    }
	    ++remap;
	}
    }

    if ( actualuni!=-1 )
	upos = actualuni;
    else if ( sc->unicodeenc!=-1 )
	upos = sc->unicodeenc;
#if HANYANG
    else if ( sc->compositionunit ) {
	if ( sc->jamo<19 )
	    upos = 0x1100+sc->jamo;
	else if ( sc->jamo<19+21 )
	    upos = 0x1161 + sc->jamo-19;
	else		/* Leave a hole for the blank char */
	    upos = 0x11a8 + sc->jamo-(19+21+1);
    }
#endif

    if ( upos == -1 ) {
	msg = xasprintf( "%u 0x%x U+???? \"%.25s\" ",
		localenc, localenc,
		(sc->name == NULL) ? "" : sc->name );
    } else {
	/* unicode name or range name */
	char *uniname = unicode_name( upos );
	if( uniname == NULL ) uniname = strdup( UnicodeRange( upos ) );
	msg = xasprintf ( "%u 0x%x U+%04X \"%.25s\" %.100s",
		localenc, localenc, upos,
		(sc->name == NULL) ? "" : sc->name, uniname );
	if ( uniname != NULL ) free( uniname ); uniname = NULL;

	/* annotation */
        char *uniannot = unicode_annot( upos );
        if( uniannot != NULL ) {
            msg_old = msg;
            msg = xasprintf("%s\n%s", msg_old, uniannot);
            free(msg_old);
            free( uniannot );
        }
    }

    /* user comments */
    if ( sc->comment!=NULL ) {
        msg_old = msg;
        msg = xasprintf("%s\n%s", msg_old, sc->comment);
        free(msg_old);
    }

    GGadgetPreparePopup8( gw, msg );
    free(msg);
}

static void noop(void *UNUSED(_fv)) {
}

static void *ddgencharlist(void *_fv,int32 *len) {
    int i,j,cnt, gid;
    FontView *fv = (FontView *) _fv;
    SplineFont *sf = fv->b.sf;
    EncMap *map = fv->b.map;
    char *data;

    for ( i=cnt=0; i<map->enccount; ++i ) if ( fv->b.selected[i] && (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL )
	cnt += strlen(sf->glyphs[gid]->name)+1;
    data = malloc(cnt+1); data[0] = '\0';
    for ( cnt=0, j=1 ; j<=fv->sel_index; ++j ) {
	for ( i=cnt=0; i<map->enccount; ++i )
	    if ( fv->b.selected[i] && (gid=map->map[i])!=-1 && sf->glyphs[gid]!=NULL ) {
		strcpy(data+cnt,sf->glyphs[gid]->name);
		cnt += strlen(sf->glyphs[gid]->name);
		strcpy(data+cnt++," ");
	    }
    }
    if ( cnt>0 )
	data[--cnt] = '\0';
    *len = cnt;
return( data );
}

static void FVMouse(FontView *fv, GEvent *event) {
    int pos = (event->u.mouse.y/fv->cbh + fv->rowoff)*fv->colcnt + event->u.mouse.x/fv->cbw;
    int gid;
    int realpos = pos;
    SplineChar *sc, dummy;
    int dopopup = true;

    if ( event->type==et_mousedown )
	CVPaletteDeactivate();
    if ( pos<0 ) {
	pos = 0;
	dopopup = false;
    } else if ( pos>=fv->b.map->enccount ) {
	pos = fv->b.map->enccount-1;
	if ( pos<0 )		/* No glyph slots in font */
return;
	dopopup = false;
    }

    sc = (gid=fv->b.map->map[pos])!=-1 ? fv->b.sf->glyphs[gid] : NULL;
    if ( sc==NULL )
    {
	sc = SCBuildDummy(&dummy,fv->b.sf,fv->b.map,pos);
    }
    if ( event->type == et_mouseup && event->u.mouse.clicks==2 )
    {
	if ( fv->pressed )
	{
	    GDrawCancelTimer(fv->pressed);
	    fv->pressed = NULL;
	}
	if ( fv->b.container!=NULL && fv->b.container->funcs->is_modal )
	{
		return;
	}
	if ( fv->cur_subtable!=NULL )
	{
	    sc = FVMakeChar(fv,pos);
	    pos = fv->b.map->backmap[sc->orig_pos];
	}
	if ( sc==&dummy )
	{
	    sc = SFMakeChar(fv->b.sf,fv->b.map,pos);
	    gid = fv->b.map->map[pos];
	}
	if ( fv->show==fv->filled )
	{
	    SplineFont *sf = fv->b.sf;
	    gid = -1;
	    if ( !OpenCharsInNewWindow )
	    {
		for ( gid=sf->glyphcnt-1; gid>=0; --gid )
		{
		    if ( sf->glyphs[gid]!=NULL && sf->glyphs[gid]->views!=NULL )
		    {
			break;
		    }
		}
	    }
	    if ( gid!=-1 )
	    {
		CharView *cv = (CharView *) (sf->glyphs[gid]->views);
		printf("calling CVChangeSC() sc:%p %s\n", sc, sc->name );
		CVChangeSC(cv,sc);
		GDrawSetVisible(cv->gw,true);
		GDrawRaise(cv->gw);
	    }
	    else
	    {
		CharViewCreate(sc,fv,pos);
	    }
	} else {
	    BDFFont *bdf = fv->show;
	    BDFChar *bc =BDFMakeGID(bdf,gid);
	    gid = -1;
	    if ( !OpenCharsInNewWindow )
		for ( gid=bdf->glyphcnt-1; gid>=0; --gid )
		    if ( bdf->glyphs[gid]!=NULL && bdf->glyphs[gid]->views!=NULL )
		break;
	    if ( gid!=-1 ) {
		BitmapView *bv = bdf->glyphs[gid]->views;
		BVChangeBC(bv,bc,true);
		GDrawSetVisible(bv->gw,true);
		GDrawRaise(bv->gw);
	    } else
		BitmapViewCreate(bc,bdf,fv,pos);
	}
    } else if ( event->type == et_mousemove ) {
	if ( dopopup )
	    SCPreparePopup(fv->v,sc,fv->b.map->remap,pos,sc==&dummy?dummy.unicodeenc: UniFromEnc(pos,fv->b.map->enc));
    }
    if ( event->type == et_mousedown ) {
	if ( fv->drag_and_drop ) {
	    GDrawSetCursor(fv->v,ct_mypointer);
	    fv->any_dd_events_sent = fv->drag_and_drop = false;
	}
	if ( !(event->u.mouse.state&ksm_shift) && event->u.mouse.clicks<=1 ) {
	    if ( !fv->b.selected[pos] )
		FVDeselectAll(fv);
	    else if ( event->u.mouse.button!=3 ) {
		fv->drag_and_drop = fv->has_dd_no_cursor = true;
		fv->any_dd_events_sent = false;
		GDrawSetCursor(fv->v,ct_prohibition);
		GDrawGrabSelection(fv->v,sn_drag_and_drop);
		GDrawAddSelectionType(fv->v,sn_drag_and_drop,"STRING",fv,0,sizeof(char),
			ddgencharlist,noop);
	    }
	}
	fv->pressed_pos = fv->end_pos = pos;
	FVShowInfo(fv);
	if ( !fv->drag_and_drop ) {
	    if ( !(event->u.mouse.state&ksm_shift))
		fv->sel_index = 1;
	    else if ( fv->sel_index<255 )
		++fv->sel_index;
	    if ( fv->pressed!=NULL ) {
		GDrawCancelTimer(fv->pressed);
		fv->pressed = NULL;
	    } else if ( event->u.mouse.state&ksm_shift ) {
		fv->b.selected[pos] = fv->b.selected[pos] ? 0 : fv->sel_index;
		FVToggleCharSelected(fv,pos);
	    } else if ( !fv->b.selected[pos] ) {
		fv->b.selected[pos] = fv->sel_index;
		FVToggleCharSelected(fv,pos);
	    }
	    if ( event->u.mouse.button==3 )
		GMenuCreatePopupMenuWithName(fv->v,event, "Popup", fvpopupmenu);
	    else
		fv->pressed = GDrawRequestTimer(fv->v,200,100,NULL);
	}
    } else if ( fv->drag_and_drop ) {
	GWindow othergw = GDrawGetPointerWindow(fv->v);

	if ( othergw==fv->v || othergw==fv->gw || othergw==NULL ) {
	    if ( !fv->has_dd_no_cursor ) {
		fv->has_dd_no_cursor = true;
		GDrawSetCursor(fv->v,ct_prohibition);
	    }
	} else {
	    if ( fv->has_dd_no_cursor ) {
		fv->has_dd_no_cursor = false;
		GDrawSetCursor(fv->v,ct_ddcursor);
	    }
	}
	if ( event->type==et_mouseup ) {
	    if ( pos!=fv->pressed_pos ) {
		GDrawPostDragEvent(fv->v,event,event->type==et_mouseup?et_drop:et_drag);
		fv->any_dd_events_sent = true;
	    }
	    fv->drag_and_drop = fv->has_dd_no_cursor = false;
	    GDrawSetCursor(fv->v,ct_mypointer);
	    if ( !fv->any_dd_events_sent )
		FVDeselectAll(fv);
	    fv->any_dd_events_sent = false;
	}
    } else if ( fv->pressed!=NULL ) {
	int showit = realpos!=fv->end_pos;
	FVReselect(fv,realpos);
	if ( showit )
	    FVShowInfo(fv);
	if ( event->type==et_mouseup ) {
	    GDrawCancelTimer(fv->pressed);
	    fv->pressed = NULL;
	}
    }
    if ( event->type==et_mouseup && dopopup )
	SCPreparePopup(fv->v,sc,fv->b.map->remap,pos,sc==&dummy?dummy.unicodeenc: UniFromEnc(pos,fv->b.map->enc));
    if ( event->type==et_mouseup )
	SVAttachFV(fv,2);
}

static void FVResize(FontView *fv, GEvent *event) {
    extern int default_fv_row_count, default_fv_col_count;
    GRect pos,screensize;
    int topchar;

    if ( fv->colcnt!=0 )
	topchar = fv->rowoff*fv->colcnt;
    else if ( fv->b.sf->top_enc!=-1 && fv->b.sf->top_enc<fv->b.map->enccount )
	topchar = fv->b.sf->top_enc;
    else {
	/* Position on 'A' (or whatever they ask for) if it exists */
	topchar = SFFindSlot(fv->b.sf,fv->b.map,home_char,NULL);
	if ( topchar==-1 ) {
	    for ( topchar=0; topchar<fv->b.map->enccount; ++topchar )
		if ( fv->b.map->map[topchar]!=-1 && fv->b.sf->glyphs[fv->b.map->map[topchar]]!=NULL )
	    break;
	    if ( topchar==fv->b.map->enccount )
		topchar = 0;
	}
    }
    if ( !event->u.resize.sized )
	/* WM isn't responding to my resize requests, so no point in trying */;
    else if ( (event->u.resize.size.width-
		GDrawPointsToPixels(fv->gw,_GScrollBar_Width)-1)%fv->cbw!=0 ||
	    (event->u.resize.size.height-fv->mbh-fv->infoh-1)%fv->cbh!=0 ) {
	int cc = (event->u.resize.size.width+fv->cbw/2-
		GDrawPointsToPixels(fv->gw,_GScrollBar_Width)-1)/fv->cbw;
	int rc = (event->u.resize.size.height-fv->mbh-fv->infoh-1)/fv->cbh;
	if ( cc<=0 ) cc = 1;
	if ( rc<=0 ) rc = 1;
	GDrawGetSize(GDrawGetRoot(NULL),&screensize);
	if ( cc*fv->cbw+GDrawPointsToPixels(fv->gw,_GScrollBar_Width)>screensize.width )
	    --cc;
	if ( rc*fv->cbh+fv->mbh+fv->infoh+10>screensize.height )
	    --rc;
	GDrawResize(fv->gw,
		cc*fv->cbw+1+GDrawPointsToPixels(fv->gw,_GScrollBar_Width),
		rc*fv->cbh+1+fv->mbh+fv->infoh);
	/* somehow KDE loses this event of mine so to get even the vague effect */
	/*  we can't just return */
/*return;*/
    }

    pos.width = GDrawPointsToPixels(fv->gw,_GScrollBar_Width);
    pos.height = event->u.resize.size.height-fv->mbh-fv->infoh;
    pos.x = event->u.resize.size.width-pos.width; pos.y = fv->mbh+fv->infoh;
    GGadgetResize(fv->vsb,pos.width,pos.height);
    GGadgetMove(fv->vsb,pos.x,pos.y);
    pos.width = pos.x; pos.x = 0;
    GDrawResize(fv->v,pos.width,pos.height);

    fv->width = pos.width; fv->height = pos.height;
    fv->colcnt = (fv->width-1)/fv->cbw;
    if ( fv->colcnt<1 ) fv->colcnt = 1;
    fv->rowcnt = (fv->height-1)/fv->cbh;
    if ( fv->rowcnt<1 ) fv->rowcnt = 1;
    fv->rowltot = (fv->b.map->enccount+fv->colcnt-1)/fv->colcnt;

    GScrollBarSetBounds(fv->vsb,0,fv->rowltot,fv->rowcnt);
    fv->rowoff = topchar/fv->colcnt;
    if ( fv->rowoff>=fv->rowltot-fv->rowcnt )
        fv->rowoff = fv->rowltot-fv->rowcnt;
    if ( fv->rowoff<0 ) fv->rowoff =0;
    GScrollBarSetPos(fv->vsb,fv->rowoff);
    GDrawRequestExpose(fv->gw,NULL,true);
    GDrawRequestExpose(fv->v,NULL,true);

    if ( fv->rowcnt!=fv->b.sf->desired_row_cnt || fv->colcnt!=fv->b.sf->desired_col_cnt ) {
	default_fv_row_count = fv->rowcnt;
	default_fv_col_count = fv->colcnt;
	fv->b.sf->desired_row_cnt = fv->rowcnt;
	fv->b.sf->desired_col_cnt = fv->colcnt;
	SavePrefs(true);
    }
}

static void FVTimer(FontView *fv, GEvent *event) {

    if ( event->u.timer.timer==fv->pressed ) {
	GEvent e;
	GDrawGetPointerPosition(fv->v,&e);
	if ( e.u.mouse.y<0 || e.u.mouse.y >= fv->height ) {
	    real dy = 0;
	    if ( e.u.mouse.y<0 )
		dy = -1;
	    else if ( e.u.mouse.y>=fv->height )
		dy = 1;
	    if ( fv->rowoff+dy<0 )
		dy = 0;
	    else if ( fv->rowoff+dy+fv->rowcnt > fv->rowltot )
		dy = 0;
	    fv->rowoff += dy;
	    if ( dy!=0 ) {
		GScrollBarSetPos(fv->vsb,fv->rowoff);
		GDrawScroll(fv->v,NULL,0,dy*fv->cbh);
	    }
	}
    } else if ( event->u.timer.timer==fv->resize ) {
	/* It's a delayed resize event (for kde which sends continuous resizes) */
	fv->resize = NULL;
	FVResize(fv,(GEvent *) (event->u.timer.userdata));
    } else if ( event->u.timer.userdata!=NULL ) {
	/* It's a delayed function call */
	void (*func)(FontView *) = (void (*)(FontView *)) (event->u.timer.userdata);
	func(fv);
    }
}

void FVDelay(FontView *fv,void (*func)(FontView *)) {
    GDrawRequestTimer(fv->v,100,0,(void *) func);
}

static int FVScroll(GGadget *g, GEvent *e) {
    FontView *fv = GGadgetGetUserData(g);
    int newpos = fv->rowoff;
    struct sbevent *sb = &e->u.control.u.sb;

    switch( sb->type ) {
      case et_sb_top:
        newpos = 0;
      break;
      case et_sb_uppage:
        newpos -= fv->rowcnt;
      break;
      case et_sb_up:
        --newpos;
      break;
      case et_sb_down:
        ++newpos;
      break;
      case et_sb_downpage:
        newpos += fv->rowcnt;
      break;
      case et_sb_bottom:
        newpos = fv->rowltot-fv->rowcnt;
      break;
      case et_sb_thumb:
      case et_sb_thumbrelease:
        newpos = sb->pos;
      break;
    }
    if ( newpos>fv->rowltot-fv->rowcnt )
        newpos = fv->rowltot-fv->rowcnt;
    if ( newpos<0 ) newpos =0;
    if ( newpos!=fv->rowoff ) {
	int diff = newpos-fv->rowoff;
	fv->rowoff = newpos;
	GScrollBarSetPos(fv->vsb,fv->rowoff);
	GDrawScroll(fv->v,NULL,0,diff*fv->cbh);
    }
return( true );
}

static int v_e_h(GWindow gw, GEvent *event) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if (( event->type==et_mouseup || event->type==et_mousedown ) &&
	    (event->u.mouse.button>=4 && event->u.mouse.button<=7) ) {
return( GGadgetDispatchEvent(fv->vsb,event));
    }

    GGadgetPopupExternalEvent(event);
    switch ( event->type ) {
      case et_expose:
	GDrawSetLineWidth(gw,0);
	FVExpose(fv,gw,event);
      break;
      case et_char:
	if ( fv->b.container!=NULL )
	    (fv->b.container->funcs->charEvent)(fv->b.container,event);
	else
	    FVChar(fv,event);
      break;
      case et_mousemove: case et_mousedown: case et_mouseup:
	if ( event->type==et_mousedown )
	    GDrawSetGIC(gw,fv->gic,0,20);
	if ( fv->notactive && event->type==et_mousedown )
	    (fv->b.container->funcs->activateMe)(fv->b.container,&fv->b);
	FVMouse(fv,event);
      break;
      case et_timer:
	FVTimer(fv,event);
      break;
      case et_focus:
	  printf("fv.et_focus\n");
	if ( event->u.focus.gained_focus )
	    GDrawSetGIC(gw,fv->gic,0,20);
      break;
    }
return( true );
}

static void FontView_ReformatOne(FontView *fv) {
	FontView *fvs;

	if ( fv->v==NULL || fv->colcnt==0 )	/* Can happen in scripts */
	{
		return;
	}

	GDrawSetCursor(fv->v,ct_watch);
	fv->rowltot = (fv->b.map->enccount+fv->colcnt-1)/fv->colcnt;
	GScrollBarSetBounds(fv->vsb,0,fv->rowltot,fv->rowcnt);
	if ( fv->rowoff>fv->rowltot-fv->rowcnt )
	{
		fv->rowoff = fv->rowltot-fv->rowcnt;
		if ( fv->rowoff<0 )
		{
			fv->rowoff =0;
		}
		GScrollBarSetPos(fv->vsb,fv->rowoff);
	}

	for ( fvs=(FontView *) (fv->b.sf->fv); fvs!=NULL; fvs=(FontView *) (fvs->b.nextsame) )
	{
		if ( fvs!=fv && fvs->b.sf==fv->b.sf )
		{
			break;
		}
	}

	GDrawRequestExpose(fv->v,NULL,false);
	GDrawSetCursor(fv->v,ct_pointer);
}

static void FontView_ReformatAll(SplineFont *sf) {
    BDFFont *new, *old, *bdf;
    FontView *fv;
    MetricsView *mvs;
    extern int use_freetype_to_rasterize_fv;

    if ( ((FontView *) (sf->fv))->v==NULL || ((FontView *) (sf->fv))->colcnt==0 )			/* Can happen in scripts */
return;

    for ( fv=(FontView *) (sf->fv); fv!=NULL; fv=(FontView *) (fv->b.nextsame) ) {
	GDrawSetCursor(fv->v,ct_watch);
	old = fv->filled;
				/* In CID fonts fv->b.sf may not be same as sf */
	new = SplineFontPieceMeal(fv->b.sf,fv->b.active_layer,fv->filled->pixelsize,72,
		(fv->antialias?pf_antialias:0)|(fv->bbsized?pf_bbsized:0)|
		    (use_freetype_to_rasterize_fv && !sf->strokedfont && !sf->multilayer?pf_ft_nohints:0),
		NULL);
	fv->filled = new;
	if ( fv->show==old )
	    fv->show = new;
	else {
	    for ( bdf=sf->bitmaps; bdf != NULL &&
		( bdf->pixelsize != fv->show->pixelsize || BDFDepth( bdf ) != BDFDepth( fv->show )); bdf=bdf->next );
	    if ( bdf != NULL ) fv->show = bdf;
	    else fv->show = new;
	}
	BDFFontFree(old);
	fv->rowltot = (fv->b.map->enccount+fv->colcnt-1)/fv->colcnt;
	GScrollBarSetBounds(fv->vsb,0,fv->rowltot,fv->rowcnt);
	if ( fv->rowoff>fv->rowltot-fv->rowcnt ) {
	    fv->rowoff = fv->rowltot-fv->rowcnt;
	    if ( fv->rowoff<0 ) fv->rowoff =0;
	    GScrollBarSetPos(fv->vsb,fv->rowoff);
	}
	GDrawRequestExpose(fv->v,NULL,false);
	GDrawSetCursor(fv->v,ct_pointer);
    }
    for ( mvs=sf->metrics; mvs!=NULL; mvs=mvs->next ) if ( mvs->bdf==NULL ) {
	BDFFontFree(mvs->show);
	mvs->show = SplineFontPieceMeal(sf,mvs->layer,mvs->ptsize,mvs->dpi,
		mvs->antialias?(pf_antialias|pf_ft_recontext):pf_ft_recontext,NULL);
	GDrawRequestExpose(mvs->gw,NULL,false);
    }
}

void FontViewRemove(FontView *fv)
{
	if ( fv_list==fv )
	{
		fv_list = (FontView *) (fv->b.next);
	}
	else
	{
		FontView *n;
		for ( n=fv_list; n->b.next!=&fv->b; n=(FontView *) (n->b.next) );
		n->b.next = fv->b.next;
	}
	FontViewFree(&fv->b);
}

/**
 * In some cases fontview gets an et_selclear event when using copy
 * and paste on the OSX. So this guard lets us quietly ignore that
 * event when we have just done command+c or command+x.
 */
extern int osx_fontview_copy_cut_counter;

static FontView* ActiveFontView = 0;

static int fv_e_h(GWindow gw, GEvent *event) {
    FontView *fv = (FontView *) GDrawGetUserData(gw);

    if (( event->type==et_mouseup || event->type==et_mousedown ) &&
	    (event->u.mouse.button>=4 && event->u.mouse.button<=7) ) {
return( GGadgetDispatchEvent(fv->vsb,event));
    }

    switch ( event->type ) {
      case et_focus:
	  if ( event->u.focus.gained_focus )
	  {
	      ActiveFontView = fv;
	  }
	  else
	  {
	  }
	  break;
      case et_selclear:
#ifdef __Mac
	  // For some reason command + c and command + x wants
	  // to send a clear to us, even if that key was pressed
	  // on a charview.
	  if( osx_fontview_copy_cut_counter )
	  {
	     osx_fontview_copy_cut_counter--;
	     break;
          }
//	  printf("fontview et_selclear\n");
#endif
	ClipboardClear();
      break;
      case et_expose:
	GDrawSetLineWidth(gw,0);
	FVDrawInfo(fv,gw,event);
      break;
      case et_resize:
	/* KDE sends a continuous stream of resize events, and gets very */
	/*  confused if I start resizing the window myself, try to wait for */
	/*  the user to finish before responding to resizes */
	if ( event->u.resize.sized || fv->resize_expected ) {
	    if ( fv->resize )
		GDrawCancelTimer(fv->resize);
	    fv->resize_event = *event;
	    fv->resize = GDrawRequestTimer(fv->v,300,0,(void *) &fv->resize_event);
	    fv->resize_expected = false;
	}
      break;
      case et_char:
	if ( fv->b.container!=NULL )
	    (fv->b.container->funcs->charEvent)(fv->b.container,event);
	else
	    FVChar(fv,event);
      break;
      case et_mousedown:
	GDrawSetGIC(gw,fv->gwgic,0,20);
	if ( fv->notactive )
	    (fv->b.container->funcs->activateMe)(fv->b.container,&fv->b);
      break;
      case et_close:
	FVMenuClose(gw,NULL,NULL);
      break;
      case et_create:
	fv->b.next = (FontViewBase *) fv_list;
	fv_list = fv;
      break;
      case et_destroy:
	if ( fv->qg!=NULL )
	    QGRmFontView(fv->qg,fv);
	FontViewRemove(fv);
      break;
    }
return( true );
}

static void FontViewOpenKids(FontView *fv) {
    int k, i;
    SplineFont *sf = fv->b.sf, *_sf;
#if defined(__Mac)
    int cnt= 0;
#endif

    if ( sf->cidmaster!=NULL )
	sf = sf->cidmaster;

    k=0;
    do {
	_sf = sf->subfontcnt==0 ? sf : sf->subfonts[k];
	for ( i=0; i<_sf->glyphcnt; ++i )
	    if ( _sf->glyphs[i]!=NULL && _sf->glyphs[i]->wasopen ) {
		_sf->glyphs[i]->wasopen = false;
#if defined(__Mac)
		/* If we open a bunch of charviews all at once on the mac, X11*/
		/*  crashes */ /* But opening one seems ok */
		if ( ++cnt==1 )
#endif
		CharViewCreate(_sf->glyphs[i],fv,-1);
	    }
	++k;
    } while ( k<sf->subfontcnt );
}

static FontView *__FontViewCreate( SplineFont *sf )
{
	FontView *fv = calloc(1,sizeof(FontView));
	int i;
	int ps = sf->display_size<0 ? -sf->display_size : sf->display_size==0 ? default_fv_font_size : sf->display_size;

	if ( ps > 200 )
	{
		ps = 128;
	}

	/* Filename != NULL if we opened an sfd file. Sfd files know whether */
	/*  the font is compact or not and should not depend on a global flag */
	/* If a font is new, then compaction will make it vanish completely */
	if ( sf->fv==NULL && compact_font_on_open && sf->filename==NULL && !sf->new )
	{
		sf->compacted = true;
		for ( i=0; i<sf->subfontcnt; ++i )
		{
			sf->subfonts[i]->compacted = true;
		}
	}
	fv->b.nextsame = sf->fv;
	fv->b.active_layer = sf->display_layer;
	sf->fv = (FontViewBase *) fv;
	if ( sf->mm!=NULL )
	{
		sf->mm->normal->fv = (FontViewBase *) fv;
		for ( i = 0; i<sf->mm->instance_count; ++i )
		{
			sf->mm->instances[i]->fv = (FontViewBase *) fv;
		}
	}
	if ( sf->subfontcnt==0 )
	{
		fv->b.sf = sf;
		if ( fv->b.nextsame!=NULL )
		{
			fv->b.map = EncMapCopy(fv->b.nextsame->map);
			fv->b.normal = fv->b.nextsame->normal==NULL ? NULL : EncMapCopy(fv->b.nextsame->normal);
		}
		else if ( sf->compacted )
		{
			fv->b.normal = sf->map;
			fv->b.map = CompactEncMap(EncMapCopy(sf->map),sf);
			sf->map = fv->b.map;
		}
		else
		{
			fv->b.map = sf->map;
			fv->b.normal = NULL;
		}
	}
	else
	{
		fv->b.cidmaster = sf;
		for ( i=0; i<sf->subfontcnt; ++i )
		{
			sf->subfonts[i]->fv = (FontViewBase *) fv;
		}
		for ( i=0; i<sf->subfontcnt; ++i )	/* Search for a subfont that contains more than ".notdef" (most significant in .gai fonts) */
		{
			if ( sf->subfonts[i]->glyphcnt>1 )
			{
				fv->b.sf = sf->subfonts[i];
				break;
			}
		}
		if ( fv->b.sf==NULL )
		{
			fv->b.sf = sf->subfonts[0];
		}
		sf = fv->b.sf;
		if ( fv->b.nextsame==NULL )
		{
			EncMapFree(sf->map); sf->map = NULL;
		}
		fv->b.map = EncMap1to1(sf->glyphcnt);
		if ( fv->b.nextsame==NULL )
		{
			sf->map = fv->b.map;
		}
		if ( sf->compacted )
		{
			fv->b.normal = fv->b.map;
			fv->b.map = CompactEncMap(EncMapCopy(fv->b.map),sf);
			if ( fv->b.nextsame==NULL )
			{
				sf->map = fv->b.map;
			}
		}
	}
	fv->b.selected = calloc(fv->b.map->enccount,sizeof(char));
	fv->user_requested_magnify = -1;
	fv->magnify = (ps<=9)? 3 : (ps<20) ? 2 : 1;
	fv->cbw = (ps*fv->magnify)+1;
	fv->cbh = (ps*fv->magnify)+1+fv->lab_height+1;
	fv->antialias = sf->display_antialias;
	fv->bbsized = sf->display_bbsized;
	fv->glyphlabel = default_fv_glyphlabel;

	fv->end_pos = -1;
#ifndef _NO_PYTHON
	PyFF_InitFontHook((FontViewBase *)fv);
#endif

	fv->pid_webfontserver = 0;
#ifdef CKS	// HYMODIFY :: 2016.02.05
	sf->registerfont = 0;
#endif

return( fv );
}

static int fontview_ready = false;

static void FontViewFinish() {
    if (!fontview_ready) return;
    mb2FreeGetText(mblist);
    mbFreeGetText(fvpopupmenu);
}

void FontViewFinishNonStatic() {
    FontViewFinish();
}

static void FontViewInit(void) {
    // static int done = false; // superseded by fontview_ready.

    if ( fontview_ready )
return;

    fontview_ready = true;

    mb2DoGetText(mblist);
    mbDoGetText(fvpopupmenu);
    atexit(&FontViewFinishNonStatic);
}

static struct resed fontview_re[] = {
    {N_("Glyph Info Color"), "GlyphInfoColor", rt_color, &fvglyphinfocol, N_("Color of the font used to display glyph information in the fontview"), NULL, { 0 }, 0, 0 },
    {N_("Empty Slot FG Color"), "EmptySlotFgColor", rt_color, &fvemtpyslotfgcol, N_("Color used to draw the foreground of empty slots"), NULL, { 0 }, 0, 0 },
    {N_("Selected BG Color"), "SelectedColor", rt_color, &fvselcol, N_("Color used to draw the background of selected glyphs"), NULL, { 0 }, 0, 0 },
    {N_("Selected FG Color"), "SelectedFgColor", rt_color, &fvselfgcol, N_("Color used to draw the foreground of selected glyphs"), NULL, { 0 }, 0, 0 },
    {N_("Changed Color"), "ChangedColor", rt_color, &fvchangedcol, N_("Color used to mark a changed glyph"), NULL, { 0 }, 0, 0 },
    {N_("Hinting Needed Color"), "HintingNeededColor", rt_color, &fvhintingneededcol, N_("Color used to mark glyphs that need hinting"), NULL, { 0 }, 0, 0 },
    {N_("Font Size"), "FontSize", rt_int, &fv_fontsize, N_("Size (in points) of the font used to display information and glyph labels in the fontview"), NULL, { 0 }, 0, 0 },
    {N_("Font Family"), "FontFamily", rt_stringlong, &fv_fontnames, N_("A comma separated list of font family names used to display small example images of glyphs over the user designed glyphs"), NULL, { 0 }, 0, 0 },
    RESED_EMPTY
};

static void FVCreateInnards(FontView *fv,GRect *pos) {
    GWindow gw = fv->gw;
    GWindowAttrs wattrs;
    GGadgetData gd;
    FontRequest rq;
    BDFFont *bdf;
    int as,ds,ld;
    extern int use_freetype_to_rasterize_fv;
    SplineFont *sf = fv->b.sf;

    fv->lab_height = FV_LAB_HEIGHT-13+GDrawPointsToPixels(NULL,fv_fontsize);

    memset(&gd,0,sizeof(gd));
    gd.pos.y = pos->y; gd.pos.height = pos->height;
    gd.pos.width = GDrawPointsToPixels(gw,_GScrollBar_Width);
    gd.pos.x = pos->width;
    gd.u.sbinit = NULL;
    gd.flags = gg_visible|gg_enabled|gg_pos_in_pixels|gg_sb_vert;
    gd.handle_controlevent = FVScroll;
    fv->vsb = GScrollBarCreate(gw,&gd,fv);


    memset(&wattrs,0,sizeof(wattrs));
    wattrs.mask = wam_events|wam_cursor|wam_backcol;
    wattrs.event_masks = ~(1<<et_charup);
    wattrs.cursor = ct_pointer;
    wattrs.background_color = view_bgcol;
    fv->v = GWidgetCreateSubWindow(gw,pos,v_e_h,fv,&wattrs);
    GDrawSetVisible(fv->v,true);
    GDrawSetWindowTypeName(fv->v, "FontView");

    fv->gic   = GDrawCreateInputContext(fv->v,gic_root|gic_orlesser);
    fv->gwgic = GDrawCreateInputContext(fv->gw,gic_root|gic_orlesser);
    GDrawSetGIC(fv->v,fv->gic,0,20);
    GDrawSetGIC(fv->gw,fv->gic,0,20);

    fv->fontset = calloc(_uni_fontmax,sizeof(GFont *));
    memset(&rq,0,sizeof(rq));
    rq.utf8_family_name = fv_fontnames;
    rq.point_size = fv_fontsize;
    rq.weight = 400;
    fv->fontset[0] = GDrawInstanciateFont(gw,&rq);
    GDrawSetFont(fv->v,fv->fontset[0]);
    GDrawWindowFontMetrics(fv->v,fv->fontset[0],&as,&ds,&ld);
    fv->lab_as = as;
    fv->showhmetrics = default_fv_showhmetrics;
    fv->showvmetrics = default_fv_showvmetrics && sf->hasvmetrics;
    bdf = SplineFontPieceMeal(fv->b.sf,fv->b.active_layer,sf->display_size<0?-sf->display_size:default_fv_font_size,72,
	    (fv->antialias?pf_antialias:0)|(fv->bbsized?pf_bbsized:0)|
		(use_freetype_to_rasterize_fv && !sf->strokedfont && !sf->multilayer?pf_ft_nohints:0),
	    NULL);
    fv->filled = bdf;
    if ( sf->display_size>0 ) {
	for ( bdf=sf->bitmaps; bdf!=NULL && bdf->pixelsize!=sf->display_size ;
		bdf=bdf->next );
	if ( bdf==NULL )
	    bdf = fv->filled;
    }
    if ( sf->onlybitmaps && bdf==fv->filled && sf->bitmaps!=NULL )
	bdf = sf->bitmaps;
    fv->cbw = -1;
    FVChangeDisplayFont(fv,bdf);
}

static FontView *FontView_Create( SplineFont *sf, int hide )
{
	FontView *fv = (FontView *) __FontViewCreate(sf);
	GRect pos;
	GWindow gw;
	GWindowAttrs wattrs;
	GGadgetData gd;
	GRect gsize;
	static GWindow icon = NULL;
	static int nexty=0;
	GRect size;

	FontViewInit();
	if ( icon==NULL )
	{
#ifdef LJS	//def HYMODIFY	// franklinj
		icon = GDrawSetWindowIcon(NULL);
#else
#ifdef BIGICONS
		icon = GDrawCreateBitmap(NULL,fontview_width,fontview_height,fontview_bits);
#else
		icon = GDrawCreateBitmap(NULL,fontview2_width,fontview2_height,fontview2_bits);
#endif
#endif
	}

	GDrawGetSize(GDrawGetRoot(NULL),&size);

	memset(&wattrs,0,sizeof(wattrs));
	wattrs.mask = wam_events|wam_cursor|wam_icon;
	wattrs.event_masks = ~(1<<et_charup);
	wattrs.cursor = ct_pointer;
	wattrs.icon = icon;
	pos.width = sf->desired_col_cnt*fv->cbw+1;
	pos.height = sf->desired_row_cnt*fv->cbh+1;
	pos.x = size.width-pos.width-30; pos.y = nexty;
	nexty += 2*fv->cbh+50;
	if ( nexty+pos.height > size.height )
	{
		nexty = 0;
	}
	fv->gw = gw = GDrawCreateTopWindow(NULL,&pos,fv_e_h,fv,&wattrs);
	FontViewSetTitle(fv);
	GDrawSetWindowTypeName(fv->gw, "FontView");

	if ( !fv_fs_init )
	{
		GResEditFind( fontview_re, "FontView.");
		view_bgcol = GResourceFindColor("View.Background",GDrawGetDefaultBackground(NULL));
		fv_fs_init = true;
	}

	memset(&gd,0,sizeof(gd));
	gd.flags = gg_visible | gg_enabled;
	helplist[0].invoke = FVMenuContextualHelp;
#ifndef _NO_PYTHON
	if ( fvpy_menu!=NULL )
	{
		mblist[3].ti.disabled = false;
	}
	mblist[3].sub = fvpy_menu;
#define CALLBACKS_INDEX 4 /* FIXME: There has to be a better way than this. */
#else
#define CALLBACKS_INDEX 3 /* FIXME: There has to be a better way than this. */
#endif		/* _NO_PYTHON */
#ifdef NATIVE_CALLBACKS
	if ( fv_menu!=NULL )
	{
		mblist[CALLBACKS_INDEX].ti.disabled = false;
	}
	mblist[CALLBACKS_INDEX].sub = fv_menu;
#endif      /* NATIVE_CALLBACKS */
	gd.u.menu2 = mblist;
	fv->mb = GMenu2BarCreate( gw, &gd, NULL);
	GGadgetGetSize(fv->mb,&gsize);
	fv->mbh = gsize.height;
	fv->infoh = 1+GDrawPointsToPixels(NULL,fv_fontsize);

	pos.x = 0; pos.y = fv->mbh+fv->infoh;
	FVCreateInnards(fv,&pos);

	if ( !hide )
	{
		GDrawSetVisible(gw,true);
		FontViewOpenKids(fv);
	}
	return( fv );
}

static FontView *FontView_Append(FontView *fv) {
    /* Normally fontviews get added to the fv list when their windows are */
    /*  created. but we don't create any windows here, so... */
    FontView *test;

    if ( fv_list==NULL ) fv_list = fv;
    else {
	for ( test = fv_list; test->b.next!=NULL; test=(FontView *) test->b.next );
	test->b.next = (FontViewBase *) fv;
    }
return( fv );
}

FontView *FontNew(void)
{
	return( FontView_Create(SplineFontNew(),false));
}

static void FontView_Free(FontView *fv)
{
	int i;
	FontView *prev;
	FontView *fvs;

	if ( fv->b.sf == NULL )	/* Happens when usurping a font to put it into an MM */
	{
		BDFFontFree(fv->filled);
	}
	else if ( fv->b.nextsame==NULL && fv->b.sf->fv==&fv->b )
	{
		EncMapFree(fv->b.map);
		if (fv->b.sf != NULL && fv->b.map == fv->b.sf->map)
		{
			fv->b.sf->map = NULL;
		}
		SplineFontFree(fv->b.cidmaster?fv->b.cidmaster:fv->b.sf);
		BDFFontFree(fv->filled);
	}
	else
	{
		EncMapFree(fv->b.map);
		if (fv->b.sf != NULL && fv->b.map == fv->b.sf->map)
		{
			fv->b.sf->map = NULL;
		}
		fv->b.map = NULL;
		for ( fvs=(FontView *) (fv->b.sf->fv), i=0 ; fvs!=NULL; fvs = (FontView *) (fvs->b.nextsame) )
		{
			if ( fvs->filled==fv->filled )
			{
				++i;
			}
		}
		if ( i==1 )
		{
			BDFFontFree(fv->filled);
		}
		if ( fv->b.sf->fv==&fv->b )
		{
			if ( fv->b.cidmaster==NULL )
			{
				fv->b.sf->fv = fv->b.nextsame;
			}
			else
			{
				fv->b.cidmaster->fv = fv->b.nextsame;
				for ( i=0; i<fv->b.cidmaster->subfontcnt; ++i )
				{
					fv->b.cidmaster->subfonts[i]->fv = fv->b.nextsame;
				}
			}
		}
		else
		{
			for ( prev = (FontView *) (fv->b.sf->fv); prev->b.nextsame!=&fv->b; prev=(FontView *) (prev->b.nextsame) );
			prev->b.nextsame = fv->b.nextsame;
		}
	}
#ifndef _NO_FFSCRIPT
	DictionaryFree(fv->b.fontvars);
	free(fv->b.fontvars);
#endif
	free(fv->b.selected);
	free(fv->fontset);
#ifndef _NO_PYTHON
	PyFF_FreeFV(&fv->b);
#endif
	free(fv);
}

static int FontViewWinInfo(FontView *fv, int *cc, int *rc) {
    if ( fv==NULL || fv->colcnt==0 || fv->rowcnt==0 ) {
	*cc = 16; *rc = 4;
return( -1 );
    }

    *cc = fv->colcnt;
    *rc = fv->rowcnt;

return( fv->rowoff*fv->colcnt );
}


static FontViewBase *FVAny(void) { return (FontViewBase *) fv_list; }

static int  FontIsActive(SplineFont *sf) {
    FontView *fv;

    for ( fv=fv_list; fv!=NULL; fv=(FontView *) (fv->b.next) )
	if ( fv->b.sf == sf )
return( true );

return( false );
}

static SplineFont *FontOfFilename(const char *filename)
{
	char buffer[1025];
	FontView *fv;

	hydebug( "[%s][%s] Do GFileGetAbsoluteName (filename: <%s>)\n", __FILE__, __FUNCTION__, filename );
	GFileGetAbsoluteName((char *) filename,buffer,sizeof(buffer));
	for ( fv=fv_list; fv!=NULL ; fv=(FontView *) (fv->b.next) )
	{
		if ( fv->b.sf->filename!=NULL && strcmp(fv->b.sf->filename,buffer)==0 )
		{
			return( fv->b.sf );
		}
		else if ( fv->b.sf->origname!=NULL && strcmp(fv->b.sf->origname,buffer)==0 )
		{
			return( fv->b.sf );
		}
	}
	return( NULL );
}

static void FVExtraEncSlots(FontView *fv, int encmax) {
    if ( fv->colcnt!=0 ) {		/* Ie. scripting vs. UI */
	fv->rowltot = (encmax+1+fv->colcnt-1)/fv->colcnt;
	GScrollBarSetBounds(fv->vsb,0,fv->rowltot,fv->rowcnt);
    }
}

static void FV_BiggerGlyphCache(FontView *fv, int gidcnt) {
    if ( fv->filled!=NULL )
	BDFOrigFixup(fv->filled,gidcnt,fv->b.sf);
}

static void FontView_Close(FontView *fv) {
    if ( fv->gw!=NULL )
	GDrawDestroyWindow(fv->gw);
    else
	FontViewRemove(fv);
}


struct fv_interface gdraw_fv_interface = {
    (FontViewBase *(*)(SplineFont *, int)) FontView_Create,
    (FontViewBase *(*)(SplineFont *)) __FontViewCreate,
    (void (*)(FontViewBase *)) FontView_Close,
    (void (*)(FontViewBase *)) FontView_Free,
    (void (*)(FontViewBase *)) FontViewSetTitle,
    FontViewSetTitles,
    FontViewRefreshAll,
    (void (*)(FontViewBase *)) FontView_ReformatOne,
    FontView_ReformatAll,
    (void (*)(FontViewBase *)) FV_LayerChanged,
    FV_ToggleCharChanged,
    (int  (*)(FontViewBase *, int *, int *)) FontViewWinInfo,
    FontIsActive,
    FVAny,
    (FontViewBase *(*)(FontViewBase *)) FontView_Append,
    FontOfFilename,
    (void (*)(FontViewBase *,int)) FVExtraEncSlots,
    (void (*)(FontViewBase *,int)) FV_BiggerGlyphCache,
    (void (*)(FontViewBase *,BDFFont *)) FV_ChangeDisplayBitmap,
    (void (*)(FontViewBase *)) FV_ShowFilled,
    FV_ReattachCVs,
    (void (*)(FontViewBase *)) FVDeselectAll,
    (void (*)(FontViewBase *,int )) FVScrollToGID,
    (void (*)(FontViewBase *,int )) FVScrollToChar,
    (void (*)(FontViewBase *,int )) FV_ChangeGID,
    SF_CloseAllInstrs
};

extern GResInfo charview_ri;
static struct resed view_re[] = {
    {N_("Color|Background"), "Background", rt_color, &view_bgcol, N_("Background color for the drawing area of all views"), NULL, { 0 }, 0, 0 },
    RESED_EMPTY
};
GResInfo view_ri = {
    NULL, NULL,NULL, NULL,
    NULL,
    NULL,
    NULL,
    view_re,
    N_("View"),
    N_("This is an abstract class which defines common features of the\nFontView, CharView, BitmapView and MetricsView"),
    "View",
    "fontforge",
    false,
    0,
    NULL,
    GBOX_EMPTY,
    NULL,
    NULL,
    NULL
};

GResInfo fontview_ri = {
    &charview_ri, NULL,NULL, NULL,
    NULL,
    NULL,
    NULL,
    fontview_re,
    N_("FontView"),
    N_("This is the main fontforge window displaying a font"),
    "FontView",
    "fontforge",
    false,
    0,
    NULL,
    GBOX_EMPTY,
    NULL,
    NULL,
    NULL
};

/* ************************************************************************** */
/* ***************************** Embedded FontViews ************************* */
/* ************************************************************************** */

static void FVCopyInnards(FontView *fv,GRect *pos,int infoh,
	FontView *fvorig,GWindow dw, int def_layer, struct fvcontainer *kf) {

    fv->notactive = true;
    fv->gw = dw;
    fv->infoh = infoh;
    fv->b.container = kf;
    fv->rowcnt = 4; fv->colcnt = 16;
    fv->b.active_layer = def_layer;
    FVCreateInnards(fv,pos);
    memcpy(fv->b.selected,fvorig->b.selected,fv->b.map->enccount);
    fv->rowoff = (fvorig->rowoff*fvorig->colcnt)/fv->colcnt;
}

void KFFontViewInits(struct kf_dlg *kf,GGadget *drawable) {
    GGadgetData gd;
    GRect pos, gsize, sbsize;
    GWindow dw = GDrawableGetWindow(drawable);
    int infoh;
    int ps;
    FontView *fvorig = (FontView *) kf->sf->fv;

    FontViewInit();

    kf->dw = dw;

    memset(&gd,0,sizeof(gd));
    gd.flags = gg_visible | gg_enabled;
    helplist[0].invoke = FVMenuContextualHelp;
    gd.u.menu2 = mblist;
    kf->mb = GMenu2BarCreate( dw, &gd, NULL);
    GGadgetGetSize(kf->mb,&gsize);
    kf->mbh = gsize.height;
    kf->guts = drawable;

    ps = kf->sf->display_size; kf->sf->display_size = -24;
    kf->first_fv = __FontViewCreate(kf->sf); kf->first_fv->b.container = (struct fvcontainer *) kf;
    kf->second_fv = __FontViewCreate(kf->sf); kf->second_fv->b.container = (struct fvcontainer *) kf;

    kf->infoh = infoh = 1+GDrawPointsToPixels(NULL,fv_fontsize);
    kf->first_fv->mbh = kf->mbh;
    pos.x = 0; pos.y = kf->mbh+infoh+kf->fh+4;
    pos.width = 16*kf->first_fv->cbw+1;
    pos.height = 4*kf->first_fv->cbh+1;

    GDrawSetUserData(dw,kf->first_fv);
    FVCopyInnards(kf->first_fv,&pos,infoh,fvorig,dw,kf->def_layer,(struct fvcontainer *) kf);
    pos.height = 4*kf->first_fv->cbh+1;		/* We don't know the real fv->cbh until after creating the innards. The size of the last window is probably wrong, we'll fix later */
    kf->second_fv->mbh = kf->mbh;
    kf->label2_y = pos.y + pos.height+2;
    pos.y = kf->label2_y + kf->fh + 2;
    GDrawSetUserData(dw,kf->second_fv);
    FVCopyInnards(kf->second_fv,&pos,infoh,fvorig,dw,kf->def_layer,(struct fvcontainer *) kf);

    kf->sf->display_size = ps;

    GGadgetGetSize(kf->second_fv->vsb,&sbsize);
    gsize.x = gsize.y = 0;
    gsize.width = pos.width + sbsize.width;
    gsize.height = pos.y+pos.height;
    GGadgetSetDesiredSize(drawable,NULL,&gsize);
}
/* ************************************************************************** */
/* ************************** Glyph Set from Selection ********************** */
/* ************************************************************************** */

struct gsd {
    struct fvcontainer base;
    FontView *fv;
    int done;
    int good;
    GWindow gw;
};

static void gs_activateMe(struct fvcontainer *UNUSED(fvc), FontViewBase *UNUSED(fvb)) {
    /*struct gsd *gs = (struct gsd *) fvc;*/
}

static void gs_charEvent(struct fvcontainer *fvc,void *event) {
    struct gsd *gs = (struct gsd *) fvc;
    FVChar(gs->fv,event);
}

static void gs_doClose(struct fvcontainer *fvc) {
    struct gsd *gs = (struct gsd *) fvc;
    gs->done = true;
}

#define CID_Guts	1000
#define CID_TopBox	1001

static void gs_doResize(struct fvcontainer *fvc, FontViewBase *UNUSED(fvb),
	int width, int height) {
    struct gsd *gs = (struct gsd *) fvc;
    /*FontView *fv = (FontView *) fvb;*/
    GRect size;

    memset(&size,0,sizeof(size));
    size.width = width; size.height = height;
    GGadgetSetDesiredSize(GWidgetGetControl(gs->gw,CID_Guts),
	    NULL,&size);
    GHVBoxFitWindow(GWidgetGetControl(gs->gw,CID_TopBox));
}

static struct fvcontainer_funcs glyphset_funcs = {
    fvc_glyphset,
    true,			/* Modal dialog. No charviews, etc. */
    gs_activateMe,
    gs_charEvent,
    gs_doClose,
    gs_doResize
};

static int GS_OK(GGadget *g, GEvent *e) {

    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	struct gsd *gs = GDrawGetUserData(GGadgetGetWindow(g));
	gs->done = true;
	gs->good = true;
    }
return( true );
}

static int GS_Cancel(GGadget *g, GEvent *e) {
    struct gsd *gs;

    if ( e->type==et_controlevent && e->u.control.subtype == et_buttonactivate ) {
	gs = GDrawGetUserData(GGadgetGetWindow(g));
	gs->done = true;
    }
return( true );
}

static void gs_sizeSet(struct gsd *gs,GWindow dw) {
    GRect size, gsize;
    int width, height, y;
    int cc, rc, topchar;
    GRect subsize;
    FontView *fv = gs->fv;

    if ( gs->fv->vsb==NULL )
return;

    GDrawGetSize(dw,&size);
    GGadgetGetSize(gs->fv->vsb,&gsize);
    width = size.width - gsize.width;
    height = size.height - gs->fv->mbh - gs->fv->infoh;

    y = gs->fv->mbh + gs->fv->infoh;

    topchar = fv->rowoff*fv->colcnt;
    cc = (width-1) / fv->cbw;
    if ( cc<1 ) cc=1;
    rc = (height-1)/ fv->cbh;
    if ( rc<1 ) rc = 1;
    subsize.x = 0; subsize.y = 0;
    subsize.width = cc*fv->cbw + 1;
    subsize.height = rc*fv->cbh + 1;
    GDrawResize(fv->v,subsize.width,subsize.height);
    GDrawMove(fv->v,0,y);
    GGadgetMove(fv->vsb,subsize.width,y);
    GGadgetResize(fv->vsb,gsize.width,subsize.height);

    fv->colcnt = cc; fv->rowcnt = rc;
    fv->width = subsize.width; fv->height = subsize.height;
    fv->rowltot = (fv->b.map->enccount+fv->colcnt-1)/fv->colcnt;
    GScrollBarSetBounds(fv->vsb,0,fv->rowltot,fv->rowcnt);
    fv->rowoff = topchar/fv->colcnt;
    if ( fv->rowoff>=fv->rowltot-fv->rowcnt )
        fv->rowoff = fv->rowltot-fv->rowcnt;
    if ( fv->rowoff<0 ) fv->rowoff =0;
    GScrollBarSetPos(fv->vsb,fv->rowoff);

    GDrawRequestExpose(fv->v,NULL,true);
}

static int gs_sub_e_h(GWindow pixmap, GEvent *event) {
    FontView *active_fv;
    struct gsd *gs;

    if ( event->type==et_destroy )
return( true );

    active_fv = (FontView *) GDrawGetUserData(pixmap);
    gs = (struct gsd *) (active_fv->b.container);

    if (( event->type==et_mouseup || event->type==et_mousedown ) &&
	    (event->u.mouse.button>=4 && event->u.mouse.button<=7) ) {
return( GGadgetDispatchEvent(active_fv->vsb,event));
    }


    switch ( event->type ) {
      case et_expose:
	FVDrawInfo(active_fv,pixmap,event);
      break;
      case et_char:
	gs_charEvent(&gs->base,event);
      break;
      case et_mousedown:
return(false);
      break;
      case et_mouseup: case et_mousemove:
return(false);
      case et_resize:
        gs_sizeSet(gs,pixmap);
      break;
    }
return( true );
}

static int gs_e_h(GWindow gw, GEvent *event) {
    struct gsd *gs = GDrawGetUserData(gw);

    switch ( event->type ) {
      case et_close:
	gs->done = true;
      break;
      case et_char:
	FVChar(gs->fv,event);
      break;
    }
return( true );
}

char *GlyphSetFromSelection(SplineFont *sf,int def_layer,char *current) {
    struct gsd gs;
    GRect pos;
    GWindowAttrs wattrs;
    GGadgetCreateData gcd[5], boxes[3];
    GGadgetCreateData *varray[21], *buttonarray[8];
    GTextInfo label[5];
    int i,j,k,guts_row,gid,enc,len;
    char *ret, *rpt;
    SplineChar *sc;
    GGadget *drawable;
    GWindow dw;
    GGadgetData gd;
    GRect gsize, sbsize;
    int infoh, mbh;
    int ps;
    FontView *fvorig = (FontView *) sf->fv;
    GGadget *mb;
    char *start, *pt; int ch;

    FontViewInit();

    memset(&wattrs,0,sizeof(wattrs));
    memset(&gcd,0,sizeof(gcd));
    memset(&boxes,0,sizeof(boxes));
    memset(&label,0,sizeof(label));
    memset(&gs,0,sizeof(gs));

    gs.base.funcs = &glyphset_funcs;

    wattrs.mask = wam_events|wam_cursor|wam_utf8_wtitle|wam_undercursor|wam_isdlg|wam_restrict;
    wattrs.event_masks = ~(1<<et_charup);
    wattrs.restrict_input_to_me = true;
    wattrs.undercursor = 1;
    wattrs.cursor = ct_pointer;
    wattrs.utf8_window_title = _("Glyph Set by Selection") ;
    wattrs.is_dlg = true;
    pos.x = pos.y = 0;
    pos.width = 100;
    pos.height = 100;
    gs.gw = GDrawCreateTopWindow(NULL,&pos,gs_e_h,&gs,&wattrs);

    i = j = 0;

    guts_row = j/2;
    gcd[i].gd.flags = gg_enabled|gg_visible;
    gcd[i].gd.cid = CID_Guts;
    gcd[i].gd.u.drawable_e_h = gs_sub_e_h;
    gcd[i].creator = GDrawableCreate;
    varray[j++] = &gcd[i++]; varray[j++] = NULL;

    label[i].text = (unichar_t *) _("Select glyphs in the font view above.\nThe selected glyphs become your glyph class.");
    label[i].text_is_1byte = true;
    gcd[i].gd.label = &label[i];
    gcd[i].gd.flags = gg_enabled|gg_visible;
    gcd[i].creator = GLabelCreate;
    varray[j++] = &gcd[i++]; varray[j++] = NULL;

    gcd[i].gd.flags = gg_visible | gg_enabled | gg_but_default;
    label[i].text = (unichar_t *) _("_OK");
    label[i].text_is_1byte = true;
    label[i].text_in_resource = true;
    gcd[i].gd.label = &label[i];
    gcd[i].gd.handle_controlevent = GS_OK;
    gcd[i++].creator = GButtonCreate;

    gcd[i].gd.flags = gg_visible | gg_enabled | gg_but_cancel;
    label[i].text = (unichar_t *) _("_Cancel");
    label[i].text_is_1byte = true;
    label[i].text_in_resource = true;
    gcd[i].gd.label = &label[i];
    gcd[i].gd.handle_controlevent = GS_Cancel;
    gcd[i++].creator = GButtonCreate;

    buttonarray[0] = GCD_Glue; buttonarray[1] = &gcd[i-2]; buttonarray[2] = GCD_Glue;
    buttonarray[3] = GCD_Glue; buttonarray[4] = &gcd[i-1]; buttonarray[5] = GCD_Glue;
    buttonarray[6] = NULL;
    boxes[2].gd.flags = gg_enabled|gg_visible;
    boxes[2].gd.u.boxelements = buttonarray;
    boxes[2].creator = GHBoxCreate;
    varray[j++] = &boxes[2]; varray[j++] = NULL; varray[j++] = NULL;

    boxes[0].gd.pos.x = boxes[0].gd.pos.y = 2;
    boxes[0].gd.flags = gg_enabled|gg_visible;
    boxes[0].gd.u.boxelements = varray;
    boxes[0].gd.cid = CID_TopBox;
    boxes[0].creator = GHVGroupCreate;

    GGadgetsCreate(gs.gw,boxes);

    GHVBoxSetExpandableRow(boxes[0].ret,guts_row);
    GHVBoxSetExpandableCol(boxes[2].ret,gb_expandgluesame);

    drawable = GWidgetGetControl(gs.gw,CID_Guts);
    dw = GDrawableGetWindow(drawable);

    memset(&gd,0,sizeof(gd));
    gd.flags = gg_visible | gg_enabled;
    helplist[0].invoke = FVMenuContextualHelp;
    gd.u.menu2 = mblist;
    mb = GMenu2BarCreate( dw, &gd, NULL);
    GGadgetGetSize(mb,&gsize);
    mbh = gsize.height;

    ps = sf->display_size; sf->display_size = -24;
    gs.fv = __FontViewCreate(sf);

    infoh = 1+GDrawPointsToPixels(NULL,fv_fontsize);
    gs.fv->mbh = mbh;
    pos.x = 0; pos.y = mbh+infoh;
    pos.width = 16*gs.fv->cbw+1;
    pos.height = 4*gs.fv->cbh+1;

    GDrawSetUserData(dw,gs.fv);
    FVCopyInnards(gs.fv,&pos,infoh,fvorig,dw,def_layer,(struct fvcontainer *) &gs);
    pos.height = 4*gs.fv->cbh+1;	/* We don't know the real fv->cbh until after creating the innards. The size of the last window is probably wrong, we'll fix later */
    memset(gs.fv->b.selected,0,gs.fv->b.map->enccount);
    if ( current!=NULL && strcmp(current,_("{Everything Else}"))!=0 ) {
	int first = true;
	for ( start = current; *start==' '; ++start );
	while ( *start ) {
	    for ( pt=start; *pt!='\0' && *pt!=' '; ++pt );
	    ch = *pt; *pt='\0';
	    sc = SFGetChar(sf,-1,start);
	    *pt = ch;
	    if ( sc!=NULL && (enc = gs.fv->b.map->backmap[sc->orig_pos])!=-1 ) {
		gs.fv->b.selected[enc] = true;
		if ( first ) {
		    first = false;
		    gs.fv->rowoff = enc/gs.fv->colcnt;
		}
	    }
	    start = pt;
	    while ( *start==' ' ) ++start;
	}
    }
    sf->display_size = ps;

    GGadgetGetSize(gs.fv->vsb,&sbsize);
    gsize.x = gsize.y = 0;
    gsize.width = pos.width + sbsize.width;
    gsize.height = pos.y+pos.height;
    GGadgetSetDesiredSize(drawable,NULL,&gsize);

    GHVBoxFitWindow(boxes[0].ret);
    GDrawSetVisible(gs.gw,true);
    while ( !gs.done )
	GDrawProcessOneEvent(NULL);

    ret = rpt = NULL;
    if ( gs.good ) {
	for ( k=0; k<2; ++k ) {
	    len = 0;
	    for ( enc=0; enc<gs.fv->b.map->enccount; ++enc ) {
		if ( gs.fv->b.selected[enc] &&
			(gid=gs.fv->b.map->map[enc])!=-1 &&
			(sc = sf->glyphs[gid])!=NULL ) {
		    char *repr = SCNameUniStr( sc );
		    if ( ret==NULL )
			len += strlen(repr)+2;
		    else {
			strcpy(rpt,repr);
			rpt += strlen( repr );
			free(repr);
			*rpt++ = ' ';
		    }
		}
	    }
	    if ( k==0 )
		ret = rpt = malloc(len+1);
	    else if ( rpt!=ret && rpt[-1]==' ' )
		rpt[-1]='\0';
	    else
		*rpt='\0';
	}
    }
    FontViewFree(&gs.fv->b);
    GDrawSetUserData(gs.gw,NULL);
    GDrawSetUserData(dw,NULL);
    GDrawDestroyWindow(gs.gw);
return( ret );
}


/****************************************/
/****************************************/
/****************************************/

int FontViewFind_byXUID( FontViewBase* fv, void* udata )
{
    if( !fv || !fv->sf )
	return 0;
    return !strcmp( fv->sf->xuid, (char*)udata );
}

int FontViewFind_byXUIDConnected( FontViewBase* fv, void* udata )
{
    if( !fv || !fv->sf )
	return 0;
    return ( fv->collabState == cs_server || fv->collabState == cs_client )
	&& fv->sf->xuid
	&& !strcmp( fv->sf->xuid, (char*)udata );
}

int FontViewFind_byCollabPtr( FontViewBase* fv, void* udata )
{
    if( !fv || !fv->sf )
	return 0;
    return fv->collabClient == udata;
}

int FontViewFind_byCollabBasePort( FontViewBase* fv, void* udata )
{
    if( !fv || !fv->sf || !fv->collabClient )
	return 0;
    int port = (int)(intptr_t)udata;
    return port == collabclient_getBasePort( fv->collabClient );
}

int FontViewFind_bySplineFont( FontViewBase* fv, void* udata )
{
    if( !fv || !fv->sf )
	return 0;
    return fv->sf == udata;
}

static int FontViewFind_ActiveWindow( FontViewBase* fvb, void* udata )
{
    FontView* fv = (FontView*)fvb;
    return( fv->gw == udata || fv->v == udata );
}

FontViewBase* FontViewFindActive()
{
    return (FontViewBase*) ActiveFontView;
    /* GWindow w = GWindowGetCurrentFocusTopWindow(); */
    /* FontViewBase* ret = FontViewFind( FontViewFind_ActiveWindow, w ); */
    /* return ret; */
}



FontViewBase* FontViewFind( int (*testFunc)( FontViewBase*, void* udata ), void* udata )
{
    FontViewBase *fv;
    printf("FontViewFind(top) fv_list:%p\n", fv_list );
    for ( fv = (FontViewBase*)fv_list; fv!=NULL; fv=fv->next )
    {
	if( testFunc( fv, udata ))
	    return fv;
    }
    return 0;
}

FontView* FontViewFindUI( int (*testFunc)( FontViewBase*, void* udata ), void* udata )
{
    return (FontView*)FontViewFind( testFunc, udata );
}



/****************************************/
/****************************************/
/****************************************/
/* local variables: */
/* tab-width: 8     */
/* end:             */
