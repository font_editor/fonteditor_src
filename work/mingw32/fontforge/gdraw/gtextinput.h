#define LIndex 0x1100
#define VIndex 0x1161
#define TIndex 0x11A7
#define VCount 21
#define TCount 28
#define SBase 0xAC00

#define Code(L, V, T) ( ( (L-LIndex)*VCount + (V-VIndex) )*TCount + (T-TIndex) + SBase )


enum STATE{
	ST_L,
	ST_LL,
	ST_LV,
	ST_LVV,
	ST_LVT,
	ST_LVTT,
	ST_COMP
};
typedef struct _tagLT
{
	char key;
	unsigned short L;
	unsigned short T;
	unsigned short O;
} LT;

typedef struct _tagTT
{
	char key[3];
	unsigned short T;
	unsigned short T1;
	unsigned short O;
} TT;

typedef struct _tagLL
{
	char key[3];
	unsigned short L;
	unsigned short O;
} LL;

typedef struct _tagVV
{
	char key[3];
	unsigned short V;
	unsigned short O;
} VV;

LT nLT[] = {
	{'r', 0x1100, 0x11A8, 0x3131},
	{'R', 0x1101, 0x11A9, 0x3132},
	{'s', 0x1102, 0x11AB, 0x3134},
	{'e', 0x1103, 0x11AE, 0x3137},
	{'f', 0x1105, 0x11AF, 0x3139},
	{'a', 0x1106, 0x11B7, 0x3141},
	{'q', 0x1107, 0x11B8, 0x3142},
	{'t', 0x1109, 0x11BA, 0x3145},
	{'T', 0x110A, 0x11BB, 0x3146},
	{'d', 0x110B, 0x11BC, 0x3147},
	{'w', 0x110C, 0x11BD, 0x3148},
	{'c', 0x110E, 0x11BE, 0x314A},
	{'z', 0x110F, 0x11BF, 0x314B},
	{'x', 0x1110, 0x11C0, 0x314C},
	{'v', 0x1111, 0x11C1, 0x314D},
	{'g', 0x1112, 0x11C2, 0x314E}
};

LL nLL[] = {
	{"rr", 0x1101, 0x3132},
	{"E", 0x1104, 0x3138},
	{"ee", 0x1104, 0x3138},
	{"Q", 0x1108, 0x3143},
	{"qq", 0x1108, 0x3143},
	{"tt", 0x110A, 0x3146},
	{"W", 0x110D, 0x3149},
	{"ww", 0x110D, 0x3149}
};

VV nVV[] = {
	{"k", 0x1161, 0x314F},
	{"o", 0x1162, 0x3150},
	{"i", 0x1163, 0x3151},
	{"O", 0x1164, 0x3152},
	{"j", 0x1165, 0x3153},
	{"p", 0x1166, 0x3154},
	{"u", 0x1167, 0x3155},
	{"P", 0x1168, 0x3156},
	{"h", 0x1169, 0x3157},
	{"hk", 0x116A, 0x3158},
	{"ho", 0x116B, 0x3159},
	{"hl", 0x116C, 0x315A},
	{"y", 0x116D, 0x315B},
	{"n", 0x116E, 0x315C},
	{"nj", 0x116F, 0x315D},
	{"np", 0x1170, 0x315E},
	{"nl", 0x1171, 0x315F},
	{"b", 0x1172, 0x3160},
	{"m", 0x1173, 0x3161},
	{"ml", 0x1174, 0x3162},
	{"l", 0x1175, 0x3163}
};

TT nTT[] = {
	{"rr", 0x11A9, 0x11AB, 0x3132},
	{"rt", 0x11AA, 0x11AB, 0x3133},
	{"sw", 0x11AC, 0x11AB, 0x3135},
	{"sg", 0x11AD, 0x11AB, 0x3136},
	{"fr", 0x11B0, 0x11AF, 0x313A},
	{"fa", 0x11B1, 0x11AF, 0x313B},
	{"fq", 0x11B2, 0x11AF, 0x313C},
	{"ft", 0x11B3, 0x11AF, 0x313D},
	{"fx", 0x11B4, 0x11AF, 0x313E},
	{"fv", 0x11B5, 0x11AF, 0x313F},
	{"fg", 0x11B6, 0x11AF, 0x3140},
	{"qt", 0x11B9, 0x11B8, 0x3144},
	{"tt", 0x11BB, 0x11BA, 0x3146}
};

static int nSizeLT = sizeof(nLT) / sizeof(LT);
static int nSizeLL = sizeof(nLL) / sizeof(LL);
static int nSizeVV = sizeof(nVV) / sizeof(VV);
static int nSizeTT = sizeof(nTT) / sizeof(TT);
static int nState;

enum TYPE{	OTHER=0, LETER,	VOWEL };

int getType(char ch)
{
	int i;
	char listL[] = { 'q', 'Q', 'w', 'W', 'e', 'E', 'r', 'R', 't', 'T',
		'a', 's', 'S', 'd', 'D', 'f', 'F', 'g', 'G',
		'z', 'x', 'c', 'C', 'v' };
	char listV[] = { 'y', 'u', 'i', 'o', 'p', 'O', 'P',
		'h', 'j', 'k', 'K', 'l',
		'b', 'n', 'm' };
	int nSizeL = sizeof(listL);
	int nSizeV = sizeof(listV);
	for (i = 0; i < nSizeL; i++)
	{
		if (listL[i] == ch) return LETER;
	}
	for (i = 0; i < nSizeV; i++)
	{
		if (listV[i] == ch) return VOWEL;
	}
	return OTHER;
}

struct StateCon{
	int nCount;
	char old[5];
	int nState;
	int nPrevState;
	unsigned short V;
	unsigned short L;
	unsigned short T;
	unsigned short completeStr;
	unsigned short compoudStr;
	unsigned short O;
} nStateCon;

int getLetter(char ch, unsigned short *l, unsigned short *o)
{
	int i;
	for (i = 0; i < nSizeLT; i++)
	{
		if (nLT[i].key == ch)
		{
			*l = nLT[i].L;
			*o = nLT[i].O;
			return 1;
		}
	}
	for (i = 0; i < nSizeLL; i++)
	{
		if (nLL[i].key[0] == ch)
		{
			*l = nLL[i].L;
			*o = nLL[i].O;
			return 1;
		}
	}

	return 0;
}

int getLetter2(char old, char ch, unsigned short *l, unsigned short *o)
{
	int i;
	for (i = 0; i < nSizeLL; i++)
	{
		if (nLL[i].key[0] == old && nLL[i].key[1] == ch)
		{
			*l = nLL[i].L;
			*o = nLL[i].O;
			return 1;
		}
	}
	return 0;
}

int getTail(char ch, unsigned short *t, unsigned short *o)
{
	int i;
	for (i = 0; i < nSizeLT; i++)
	{
		if (nLT[i].key == ch)
		{
			*t = nLT[i].T;
			*o = nLT[i].O;
			return 1;
		}
	}
	return 0;
}

int getTail2(char old, char ch, unsigned short *t, unsigned short *o)
{
	int i;
	for (i = 0; i < nSizeTT; i++)
	{
		if (nTT[i].key[0] == old && nTT[i].key[1] == ch)
		{
			*t = nTT[i].T;
			*o = nTT[i].O;
			return 1;
		}
	}
	return 0;
}

int getVowel(char ch, unsigned short *v, unsigned short *o)
{
	int i;
	for (i = 0; i < nSizeVV; i++)
	{
		if (nVV[i].key[0] == ch)
		{
			*v = nVV[i].V;
			*o = nVV[i].O;
			return 1;
		}
	}
	return 0;
}

int getVowel2(char old, char ch, unsigned short *v, unsigned short *o)
{
	int i;
	for (i = 0; i < nSizeVV; i++)
	{
		if (nVV[i].key[0] == old && nVV[i].key[1] == ch)
		{
			*v = nVV[i].V;
			*o = nVV[i].O;
			return 1;
		}
	}
	return 0;
}

void initState(){
	nStateCon.nCount = 0;
	nStateCon.nState = ST_COMP;
	nStateCon.V = VIndex;
	nStateCon.L = LIndex;
	nStateCon.T = TIndex;
	nStateCon.compoudStr = 0;
	nStateCon.nPrevState = 0;
	nStateCon.O = 0;
}

//////////////////////////////////////////////////////////////////////////
// success 0, skip
// fail   oldLetter, no skip
unsigned short makeST_L(char ch)
{
	unsigned short sRes, sOut;
	if (nStateCon.nState == ST_COMP){
		if (getLetter(ch, &sRes, &sOut))
		{
			nStateCon.nPrevState = nStateCon.nState;
			nStateCon.nCount = 0;
			nStateCon.old[nStateCon.nCount] = ch;
			nStateCon.nCount++;
			nStateCon.nState = ST_L;
			nStateCon.L = sRes;
			nStateCon.compoudStr =  nStateCon.O = sOut;
		}
		return 0;
	}
	else if (nStateCon.nState == ST_L)
	{
		char old = nStateCon.old[nStateCon.nCount - 1];
		if (getLetter2(old, ch, &sRes, &sOut))
		{

			nStateCon.nPrevState = nStateCon.nState;
			nStateCon.old[nStateCon.nCount] = ch;
			nStateCon.nCount++;
			nStateCon.nState = ST_L;
			nStateCon.L = sRes;
			nStateCon.compoudStr = nStateCon.O = sOut;
			
			return 0;
		}
		else
		{
			sOut = nStateCon.O;
			nStateCon.completeStr = nStateCon.compoudStr;
			initState();
			return sOut;
		}
	}
	else
	{
		sOut = nStateCon.O;
		initState();
		return sOut;
	}
}

//////////////////////////////////////////////////////////////////////////
// success 0, skip
// fail   oldLetter, no skip
unsigned short makeST_LV(char ch)
{
	unsigned short sRes, sOut;
	if (nStateCon.nState == ST_L){
		if (getVowel(ch, &sRes, &sOut))
		{
			nStateCon.nPrevState = nStateCon.nState;
			nStateCon.old[nStateCon.nCount] = ch;
			nStateCon.nCount++;
			nStateCon.nState = ST_LV;
			nStateCon.V = sRes;
			nStateCon.O = sOut;

			nStateCon.compoudStr = Code(nStateCon.L, nStateCon.V, nStateCon.T);
		}
		return 0;
	}
	else if (nStateCon.nState == ST_LV)
	{
		char old = nStateCon.old[nStateCon.nCount - 1];
		nStateCon.nPrevState = nStateCon.nState;
		if (getVowel2(old, ch, &sRes, &sOut))
		{
			nStateCon.old[nStateCon.nCount] = ch;
			nStateCon.nCount++;
			nStateCon.nState = ST_LV;
			nStateCon.V = sRes;
			nStateCon.O = sOut;
			nStateCon.compoudStr = Code(nStateCon.L, nStateCon.V, nStateCon.T);

			return 0;
		}
		else
		{
			sOut = Code( nStateCon.L, nStateCon.V, nStateCon.T);
			nStateCon.completeStr = sOut;
			initState();
			return sOut;
		}
	}
	else
	{
		sOut = Code(nStateCon.L, nStateCon.V, nStateCon.T);
		initState();
		return sOut;
	}
}

//////////////////////////////////////////////////////////////////////////
// success 0, skip
// fail   oldLetter, no skip
unsigned short makeST_LVT(char ch)
{
	unsigned short sRes, sOut;
	if (nStateCon.nState == ST_LV){
		if (getTail(ch, &sRes, &sOut))
		{
			nStateCon.nPrevState = nStateCon.nState;
			nStateCon.old[nStateCon.nCount] = ch;
			nStateCon.nCount++;
			nStateCon.nState = ST_LVT;
			nStateCon.T = sRes;
			nStateCon.O = sOut;
			nStateCon.compoudStr = Code(nStateCon.L, nStateCon.V, nStateCon.T);
		}
		else{
			sOut = Code(nStateCon.L, nStateCon.V, nStateCon.T);
			nStateCon.completeStr = sOut;
			initState();
			return sOut;
		}
		return 0;
	}
	else if (nStateCon.nState == ST_LVT)
	{
		char old = nStateCon.old[nStateCon.nCount - 1];
		if (getTail2(old, ch, &sRes, &sOut))
		{
			nStateCon.nPrevState = nStateCon.nState;
			nStateCon.old[nStateCon.nCount] = ch;
			nStateCon.nCount++;
			nStateCon.nState = ST_LVT;
			nStateCon.T = sRes;
			nStateCon.O = sOut;
			nStateCon.compoudStr = Code(nStateCon.L, nStateCon.V, nStateCon.T);
			return 0;
		}
		else
		{
			sOut = Code(nStateCon.L, nStateCon.V, nStateCon.T);
			nStateCon.completeStr = sOut;
			initState();
			return sOut;
		}
	}
	else
	{
		sOut = Code(nStateCon.L, nStateCon.V, nStateCon.T);
		initState();
		return sOut;
	}
}

int getState()
{
	return nStateCon.nState;
}

